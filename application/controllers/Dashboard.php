<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
    {
        parent::__construct(); 
        $this->load->library('form_validation');
        if ( !$this->session->userdata('isLogin'))
        { 
            redirect('access');
        }
    }

	public function index()
	{
        $level = $this->session->userdata('level');
		if($level == 'owner') 
        {
            // $this->load->model('m_manajemen');
            // $email = $this->session->userdata('email');
            // $detail_owner = $this->m_manajemen->getDetailOwner($email);
            // $id_owner = $detail_owner->id;
            // //$data[] = new array(); 

            // $query = $this->m_manajemen->getDataChart('id_owner');
            $this->load->model(array('m_manajemen', 'pembuatan_kapal', 'laporan_mingguan', 'workgroup_kapal', 'workgroup'));
            $email = $this->session->userdata('email');
            $detail_owner = $this->m_manajemen->getDetailOwner($email);
            $id_owner = $detail_owner->id;

            //$Data['list_kapal'] = $this->m_manajemen->getListKapal($id_owner);
            $Data['list_galangan'] = $this->m_manajemen->getListGalangan($id_owner);
            $this->pembuatan_kapal->join_galangan();
            $this->pembuatan_kapal->join_os();
            $this->pembuatan_kapal->order_by();
            $Data['rekap_kapal'] = $this->pembuatan_kapal->get(array('id_owner'=>$id_owner));
            $progres_kapal =array();
            foreach ($Data['rekap_kapal'] as $key => $rekap_kapal) {
                $progres_kapal[$rekap_kapal->id] = $this->m_manajemen->getDataChart($rekap_kapal->id);
            }
            $Data['progres_kapal'] = $progres_kapal;
            //var_dump($progres_kapal);exit();

            $today = date('d-F-Y');
            $tmp_periode = explode('-', $today);
            $periode = $tmp_periode[1].' '.$tmp_periode[2];
            $mid_end = ($tmp_periode[0]<=15) ? "0" : "1";
            
            
            //$progres_group = $this->laporan_mingguan->get(array('id_owner'=>$id_owner));
            // $progres_workgroup_kapal = array();
            // foreach ($progres_workgroup as $key => $progres_wg) {
            //     $progres_workgroup_kapal[$wg->id_kapal][$wg->id_workgroup]
            // }

            $this->workgroup_kapal->join_kapal();
            $this->workgroup_kapal->join_workgroup();
            $data_workgroup = $this->workgroup_kapal->get(array('id_owner'=>$id_owner));

            $rekap_progres = array();

            foreach ($data_workgroup as $wg) {
                $rekap_progres[$wg->id_kapal][$wg->id_workgroup] = 0;
                // foreach ($progres_group[$wg->id_workgroup_kapal] as $key => $grp_progres) {
                $this->laporan_mingguan->join_all();
                foreach ($this->laporan_mingguan->get(array('id_owner'=>$id_owner, 'id_workgroup_kapal'=>$wg->id, 'periode'=>$periode, 'mid_end'=>$mid_end)) as $key => $grp_progres) {
                    $rekap_progres[$wg->id_kapal][$wg->id_workgroup] = $rekap_progres[$wg->id_kapal][$wg->id_workgroup] + ($grp_progres->progres_group*$grp_progres->bobot_workgroup/100);
                }
            }
            $Data['workgroup'] = $this->workgroup->get();
            $Data['rekap_progres'] = $rekap_progres;

            $this->load->view('header');
            $this->load->view('sidebar-owner');
            $this->load->view('dashboard/home-owner', $Data);
            $this->load->view('footer');
            //redirect('ManajemenKapalOwner/');
        }
        else if($level == 'owner_surveyor')
        {
            redirect('ManajemenKapalOS/DaftarKapal');
            // $this->load->view('header');
            // $this->load->view('sidebar-os');
            // $this->load->view('ownerSurveyor/daftar-kapal');
            // $this->load->view('footer');
        }
        else if($level == 'project_manager')
        {
            $this->load->view('header');
            $this->load->view('sidebar-pm');
            $this->load->view('dashboard/home-pm');
            $this->load->view('footer');
        }
        else if($level == 'admin')
        {
            $this->load->view('header');
            $this->load->view('sidebar-admin');
            $this->load->view('dashboard/home-admin');
            $this->load->view('footer');
        }
	}
}



