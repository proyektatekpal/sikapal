<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
              <i class="fa fa-edit"></i> <strong>Detail Kapal</strong>
          </h1>
          <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <nav class="navbar navbar-default">
              <div class="container-fluid">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url()."manajemenKapalOwner/detailKapal/".$kapal->id?>"><strong><?php echo $kapal->nama_proyek;?></strong></a>
                  </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                      <!-- <li><a href="<?php echo base_url().'manajemenKapalOwner/Gambar/'.$kapal->id?>">Gambar</a></li> -->
                      <li><a href="<?php echo base_url().'manajemenKapalOwner/SCurve/'.$kapal->id?>">S-Curve</a></li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Laporan <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url().'manajemenKapalOwner/LaporanHarian/'.$kapal->id?>">Harian</a></li>
                            <li><a href="<?php echo base_url().'manajemenKapalOwner/LaporanMingguan/'.$kapal->id?>">2 Mingguan</a></li>
                          </ul>
                      </li>
                      <!-- <li><a href="#">Pesan</a></li> -->
                    </ul>
                  </div><!-- /.navbar-collapse -->

              </div><!-- /.container-fluid -->
          </nav>
                
          <div class="row">
            <form action="" method="post" role="form">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Laporan Mingguan</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                         <table id="data-table" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="col-md-1 text-center">No</th>
                                    <th class="text-center">Periode</th>
                                    <th class="text-center">Workgroup</th>
                                    <th class="text-center">Group</th>
                                    <th class="text-center">Progres (%)</th>
                                    <th class="no-sort class="text-center"" style="width: 60px;">Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 0;
                                foreach ($lap_mingguan as $lap) {
                                    $mid_end = ($lap->mid_end==0) ? "mid" : "end";
                                    $periode = $mid_end.' '.$lap->periode;
                                    echo '<tr>';
                                        echo '<td>' . ++$no . '</td>';
                                        echo '<td>' . $periode. '</td>';
                                        echo '<td>' . $lap->nama_pekerjaan .'</td>';
                                        echo '<td>' . $lap->group .'</td>';
                                        echo '<td>' . $lap->progres_group .'</td>';
                                        echo '<td>';
                                            echo '<a class="btn btn-primary text-center" href="' .base_url().'manajemenKapalOwner/DetailMingguan/'.$kapal->id.'/'. $lap->id. '">';
                                                echo '<i class="fa fa-file-o"></i>';
                                            echo '</a>';
                                        echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Periode</th>
                                    <th>Group</th>
                                    <th>Progres</th>
                                    <th class="no-sort" style="width: 60px;">Detail</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
              </div>
            </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<script type="text/javascript">
  $(function () {
        $('#data-table').dataTable();
  });
</script>