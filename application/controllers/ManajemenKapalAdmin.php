<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManajemenKapalAdmin extends CI_Controller {

	public function __construct()
    {
        parent::__construct(); 
        if ( !$this->session->userdata('isLogin') && $this->session->userdata('level') != 'owner')
        { 
            redirect('access');
        }
        $this->load->model('m_manajemenadmin');
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));
    }

	public function index()
	{  
	}

    public function AddWorkGroup()
    {
        $Data = $this->m_manajemenadmin->jumlahWorkgroup();
        $Data['cek_isi'] = 'kosong';

        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/tambahworkgroup', $Data);
        $this->load->view('footer');
    }

    public function SaveWorkGroup()
    {  
        $Query = $this->m_manajemenadmin->jumlahWorkgroup();
        $Data['cek_isi'] = 'kosong';
        $Data['eksekusi'] = 0;

        $this->form_validation->set_rules('workgroup', 'workgroup', 'required' );
        $this->form_validation->set_rules('bobot', 'bobot', 'required' );
        if ($this->form_validation->run() == FALSE)
        {      
        }
        else
        {
            if ($Query['bobot'] + $this->input->post("bobot") > 100 ) 
            {
               echo"<script language='javascript'> alert('Jumlah Bobot Melebihi 100');</script>";
            }

            else
            {
                $data = array(
               'bobot' => $this->input->post("bobot") ,
               'nama_pekerjaan' => $this->input->post("workgroup") 
                );

                $Data['cek_isi'] = 'isi';
                $Data['eksekusi'] = $this->m_manajemenadmin->addWorgroup($data);
            }
        }

        $Query = $this->m_manajemenadmin->jumlahWorkgroup();
        $Data['jumlahWorkgroup'] = $Query['jumlahWorkgroup'];
        $Data['bobot'] = $Query['bobot'];   

        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/tambahworkgroup', $Data); 
        $this->load->view('footer');     
    }

    public function ListWorkGroup()
    {
        $Data['list_workgroup'] = $this->m_manajemenadmin->getWorkgroup();

        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/daftar-workgroup', $Data);
        $this->load->view('footer');
    }

    public function DelWorkGroup($id)
    {
        $this->m_manajemenadmin->delWorkgroup($id);

        $this->ListWorkGroup();
    }

    public function UpdateWorkgroup($id)
    {
        $data = array(
                'id' => $id,
               'bobot' => $this->input->post("update_bobot") ,
               'nama_pekerjaan' => $this->input->post("update_workgroup") 
         );

        $this->m_manajemenadmin->updateworkgroup($data);

        $this->ListWorkGroup();
    }

    public function AddActivity()
    {
        $Data['list_workgroup'] = $this->m_manajemenadmin->getWorkgroup();

        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/tambahactivity', $Data);
        $this->load->view('footer');
    }


    public function SaveActivity($id_workgroup)
    {
        $jumlah_aktifitas = $this->input->post('jumlah_aktifitas');

        for ($i=0; $i <$jumlah_aktifitas ; $i++) { 
            $Data = array(
                'id_workgroup' => $id_workgroup,
                'aktifitas' => $this->input->post('aktifitas'.($i+1)),
                'bobot' => $this->input->post('bobot'.($i+1))
            );

        $this->m_manajemenadmin->insertAktifitas($Data);

        }

        $Data['list_workgroup'] = $this->m_manajemenadmin->getWorkgroup();

        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/tambahactivity', $Data);
        $this->load->view('footer');
    }

    public function getDataActivity()
    {
        $activity = $this->m_manajemenadmin->getActivity($this->input->post('id_workgroup'));

        $json = array();
        foreach ($activity as $activity) {
            $json[] = array('id'  => $activity->id,
                            'aktifitas' => $activity->aktifitas,
                            'bobot' => $activity->bobot
            );
        }
        echo json_encode($json);
    }


    public function ListActivity()
    {
        $Data['list_workgroup'] = $this->m_manajemenadmin->getWorkgroup();
        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/daftar-activity', $Data);
        $this->load->view('footer');
    }

    public function DelActivity($id)
    {
        $this->m_manajemenadmin->delActivity($id);

        $this->ListActivity();
    }

    public function UpdateActivity($id)
    {
        $data = array(
                'id' => $id,
               'bobot' => $this->input->post("update_bobot") ,
               'aktifitas' => $this->input->post("update_aktifitas") 
         );
        $this->m_manajemenadmin->updateActivity($data);
        $this->ListActivity();
    }

    public function AddOwner()
    {
        $Data['cek_isi'] = 'kosong';
        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/tambahowner', $Data);
        $this->load->view('footer');
    }

    public function SaveOwner()
    {  
        $Data['cek_isi'] = 'kosong';
        $Data['eksekusi'] = 0;

        //set preferences
        $config['upload_path'] = 'upload';
        $config['allowed_types'] = 'jpg|png|bmp|gif';

        //load upload class library
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('foto'))
        {
            // case - failure
            $upload_error = array('error' => $this->upload->display_errors());
            $this->load->view('admin/tambahowner', $upload_error);
        }
        else
        {
            // case - success
            $this->form_validation->set_rules('nama_perusahaan', 'nama_perusahaan', 'required' );
            $this->form_validation->set_rules('alamat_perusahaan', 'alamat_perusahaan', 'required' );
            $this->form_validation->set_rules('penanggung_jawab', 'penanggung jawab', 'required' );
            $this->form_validation->set_rules('telp', 'No telepon', 'required' );
            $this->form_validation->set_rules('email', 'email', 'required' );
            $this->form_validation->set_rules('password', 'password', 'required' );

            if ($this->form_validation->run() == FALSE)
            {      

            }
            else
            {
                $upload_data = $this->upload->data();
                
                $data = array(
                   'nama_perusahaan' => $this->input->post("nama_perusahaan") ,
                   'alamat_perusahaan' => $this->input->post("alamat_perusahaan"), 
                   'penanggung_jawab' => $this->input->post("penanggung_jawab"), 
                   'no_telp' => $this->input->post("telp"),
                   'email' => $this->input->post("email"),
                   'password' => $this->input->post("password"),
                   'link_foto' => $upload_data['file_name']
                );

                $Data['cek_isi'] = 'isi';
                $Data['eksekusi'] = $this->m_manajemenadmin->addOwner($data);
            }
        }

        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/tambahowner', $Data); 
        $this->load->view('footer');     
    }

    public function UpdateOwner($id)
    {
        $data = array(
                'id' => $id,
                'nama_perusahaan' => $this->input->post("nama_perusahaan") ,
                'alamat_perusahaan' => $this->input->post("alamat_perusahaan"), 
                'penanggung_jawab' => $this->input->post("penanggung_jawab"), 
                'no_telp' => $this->input->post("telp"),
                'email' => $this->input->post("email"),
                'password' => $this->input->post("password")
         );

        $this->m_manajemenadmin->updateOwner($data);

        $this->ListOwner();
    }

    public function ListOwner()
    {
        $Data['list_owner'] = $this->m_manajemenadmin->getOwner();

        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/daftar-owner', $Data);
        $this->load->view('footer');
    }

     public function DelOwner($id)
    {
        $this->m_manajemenadmin->delOwner($id);

        $this->ListOwner();
    }


    public function AddOS()
    {
        $Data['cek_isi'] = 'kosong';
        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/tambahOS', $Data);
        $this->load->view('footer');
    }

     public function SaveOS()
    {  
        $Data['cek_isi'] = 'kosong';
        $Data['eksekusi'] = 0;

        //set preferences
        $config['upload_path'] = 'upload';
        $config['allowed_types'] = 'jpg|png|bmp|gif';


        //load upload class library
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('foto'))
        {
            // case - failure
            $upload_error = array('error' => $this->upload->display_errors());
            $this->load->view('admin/tambahOS', $upload_error);
        }
        else
        {
            // case - success
            $this->form_validation->set_rules('nama', 'nama', 'required' );
            $this->form_validation->set_rules('alamat', 'nama', 'required' );
            $this->form_validation->set_rules('telp', 'No telepon', 'required' );
            $this->form_validation->set_rules('email', 'email', 'required' );
            $this->form_validation->set_rules('password', 'password', 'required' );

            if ($this->form_validation->run() == FALSE)
            {      
            }
            else
            {
                $upload_data = $this->upload->data();
                
                $data = array(
                   'nama_perusahaan' => $this->input->post("nama") ,
                   'alamat_perusahaan' => $this->input->post("alamat"), 
                   'no_telp' => $this->input->post("telp"), 
                   'email' => $this->input->post("email"),
                   'password' => $this->input->post("password"),
                   'link_foto' => $upload_data['file_name']
                );

                $Data['cek_isi'] = 'isi';
                $Data['eksekusi'] = $this->m_manajemenadmin->AddOS($data);
            }
        }

        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/tambahOS', $Data); 
        $this->load->view('footer');     
    }

    public function ListOS()
    {
        $Data['list_os'] = $this->m_manajemenadmin->getOS();

        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/daftar-OS', $Data);
        $this->load->view('footer');
    }

    public function UpdateOS($id)
    {
        $data = array(
                'id' => $id,
               'nama_perusahaan' => $this->input->post("nama") ,
               'alamat_perusahaan' => $this->input->post("alamat"),
               'no_telp' => $this->input->post("telp"),
               'email' => $this->input->post("email"),
               'password' => $this->input->post("password")
         );
        $this->m_manajemenadmin->updateOS($data);
        $this->ListOS();
    }

    public function DelOS($id)
    {
        $this->m_manajemenadmin->delOS($id);

        $this->ListOS();
    }

    public function AddGalangan()
    {
        $Data['cek_isi'] = 'kosong';
        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/tambahgalangan', $Data);
        $this->load->view('footer');
    }

    public function SaveGalangan()
    {  
        $Data['cek_isi'] = 'kosong';
        $Data['eksekusi'] = 0;

        $this->form_validation->set_rules('nama_perusahaan', 'nama perusahaan', 'required' );
        $this->form_validation->set_rules('alamat_perusahaan', 'alamat perusahaan', 'required' );
        $this->form_validation->set_rules('pimpinan_proyek', 'pimpinan proyek', 'required' );
        $this->form_validation->set_rules('telp', 'no telepon', 'required' );
        $this->form_validation->set_rules('email', 'email', 'required' );

        if ($this->form_validation->run() == FALSE)
        {      
        }
        else
        {
                $data = array(
                'nama_perusahaan' => $this->input->post("nama_perusahaan") ,
                'alamat_perusahaan' => $this->input->post("alamat_perusahaan"),
                'pimpinan_proyek' => $this->input->post("pimpinan_proyek"),
                'telp' => $this->input->post("telp"),
                'email' => $this->input->post("email"),
                );

                $Data['cek_isi'] = 'isi';
                $Data['eksekusi'] = $this->m_manajemenadmin->addGalangan($data);   
        }
   
        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/tambahgalangan', $Data); 
        $this->load->view('footer');     
    }

    public function ListGalangan()
    {
        $Data['list_galangan'] = $this->m_manajemenadmin->getGalangan();

        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/daftar-galangan', $Data);
        $this->load->view('footer');
    }

    public function UpdateGalangan($id)
    {
        $data = array(
                'id' => $id,
               'nama_perusahaan' => $this->input->post("nama_perusahaan") ,
               'alamat_perusahaan' => $this->input->post("alamat_perusahaan"),
               'pimpinan_proyek' => $this->input->post("pimpinan_proyek"),
               'telp' => $this->input->post("telp"),
               'email' => $this->input->post("email") 
         );

        $this->m_manajemenadmin->UpdateGalangan($data);

        $this->ListGalangan();
    }

    public function DelGalangan($id)
    {
        $this->m_manajemenadmin->delGalangan($id);

        $this->ListGalangan();
    }

    public function TambahKapal()
    {
        $Data['cek_isi'] = 'kosong';
        $Data['list_galangan'] = $this->m_manajemenadmin->getGalangan();
        $Data['list_os'] = $this->m_manajemenadmin->getOS();
        $Data['list_owner'] = $this->m_manajemenadmin->getOwner();

        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/tambah-kapal', $Data);
        $this->load->view('footer');
    }

    public function SimpanKapal()
    {
        $this->load->model(array('progres', 'baseline'));
        $data_kapal = array(
                'nama_proyek' => $this->input->post('nama_proyek'),
                'pemilik' => $this->input->post('pemilik'),
                'kontraktor' => $this->input->post('kontraktor'),
                'konsultan' => $this->input->post('konsultan'),
                'jenis_kapal' => $this->input->post('jenis_kapal'),
                'lpp' => $this->input->post('lpp'),
                'loa' => $this->input->post('loa'),
                'lebar' => $this->input->post('lebar'),
                'tinggi' => $this->input->post('tinggi'),
                'sarat_air' => $this->input->post('sarat_air'),
                'kecepatan' => $this->input->post('kecepatan'),
                'lpp' => $this->input->post('lpp'),
                'lama_pengerjaan' => $this->input->post('lama_pengerjaan'),
                'tanggal_mulai' => $this->input->post('tgl_mulai'),
                'id_galangan' => $this->input->post('id_galangan'),
                'id_os' => $this->input->post('id_os'),
                'id_owner' => $this->input->post('id_owner')
        );

        $Data['cek_isi'] = 'isi';
        $Data['eksekusi'] = $this->m_manajemenadmin->insertData('pembuatan_kapal', $data_kapal);

        $id_kapal = $this->m_manajemenadmin->get_id();
        $tmp_tgl=explode('/', $this->input->post('tgl_mulai'));
        $mulai_tgl = $tmp_tgl[0];
        $lama_pengerjaan = $this->input->post('lama_pengerjaan');

        $periode_mulai = "";
        $midend_mulai = "";
        if($mulai_tgl <= 15){
            $periode_mulai = $tmp_tgl[1].'-'.$tmp_tgl[2];
            $midend_mulai = 1;
            
        }
        else if($mulai_tgl > 15 && $tmp_tgl[1] < 12){
            $periode_mulai = ($tmp_tgl[1]+1).'-'.$tmp_tgl[2];
            $midend_mulai = 0;
        }
        else{
            $periode_mulai = '01-'.($tmp_tgl[2]+1);
            $midend_mulai = 0;
        }

        $init_periode =array();
        $init_periode[0]['periode'] = $periode_mulai;
        $init_periode[0]['mid_end'] = $midend_mulai;
        $init_periode[0]['progres'] = 0;
        
        for($i=1; $i<2*$lama_pengerjaan; $i++){
            $tmp_midend = 0;
            $tmp_bulan = explode('-', $init_periode[$i-1]['periode'])[0];
            $tmp_tahun = explode('-', $init_periode[$i-1]['periode'])[1];
            if($init_periode[$i-1]['mid_end'] ==0){
                $tmp_midend = 1;
            }
            else{
                if($tmp_bulan < 12){
                    $tmp_bulan++;
                }
                else{
                    $tmp_bulan = "01";
                    $tmp_tahun++;
                }
            }

            $init_periode[$i]['periode'] = $tmp_bulan.'-'.$tmp_tahun;
            $init_periode[$i]['mid_end'] = $tmp_midend;
            $init_periode[$i]['progres'] = 0;
        }

        foreach ($init_periode as $val) {
            $date = DateTime::createFromFormat('m-Y', $val['periode']);  
            $periode_lap = $date->format('F Y');
            $data_progres = array(
                    'id_kapal'=> $id_kapal->id,
                    'periode' => $periode_lap,
                    'mid_end' => $val['mid_end'],
                    'progres' => 0
                );
            $this->progres->insert($data_progres);

            $data_baseline = array(
                    'id_kapal'=> $id_kapal->id,
                    'periode' => $periode_lap,
                    'mid_end' => $val['mid_end'],
                    'target' => 0
                );
            $this->baseline->insert($data_baseline);
        }

        //initial to DB
        //$this->m_manajemenadmin->initBaseline($this->input->post('lama_pengerjaan'));
        //$this->m_manajemenadmin->initProgres($this->input->post('lama_pengerjaan'));
        $this->m_manajemenadmin->initWorkgroupkapal();

        $Data['list_galangan'] = $this->m_manajemenadmin->getGalangan();
        $Data['list_os'] = $this->m_manajemenadmin->getOS();
        $Data['list_owner'] = $this->m_manajemenadmin->getOwner();

        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/tambah-kapal', $Data);
        $this->load->view('footer');
    } 

    public function DaftarKapal()
    {
        $Data['daftar_proyek'] = $this->m_manajemenadmin->getProyek();

        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/daftar-kapal', $Data);
        $this->load->view('footer');
    }

    public function AddItem()
    {
        $Data['list_workgroup'] = $this->m_manajemenadmin->getWorkgroup();
        $Data['list_aktifitas'] = $this->m_manajemenadmin->getItem();

        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/tambahitem', $Data);
        $this->load->view('footer');
    }

    public function ListItem()
    {
        $Data['list_workgroup'] = $this->m_manajemenadmin->getWorkgroup();
        $Data['list_aktifitas'] = $this->m_manajemenadmin->getItem();

        $this->load->view('header');
        $this->load->view('sidebar-admin');
        $this->load->view('admin/daftar-item', $Data);
        $this->load->view('footer');
    }

    public function GetDataItem()
    {
        $activity = $this->m_manajemenadmin->getDataItem($this->input->post('id'));

        $json = array();
        foreach ($activity as $activity) {
            $json[] = array('id'  => $activity->id,
                            'item' => $activity->item,
                            // 'bobot' => $activity->bobot
            );
        }
        echo json_encode($json);
    }
    
    public function SaveItem($id)
    {
        $jumlah_item = $this->input->post('jlh_item');
        $item = '';

        for ($i=0; $i <$jumlah_item ; $i++) 
        {   
            $item .= $this->input->post('item'.($i+1)).'|0#';
            //$item = "memeriksa penandaan|0#memeriksa penyimpangan garis|0#memeriksa hasil penandaan|0";
        }

            $Data = array(
                'item' => $item
            );

            $this->m_manajemenadmin->insertItem($Data, $id);
    }

    public function DeleteItem()
    {
        // $Data['list_workgroup'] = $this->m_manajemenadmin->getWorkgroup();
        $this->m_manajemenadmin->deleteitem($this->input->post('id'));
    }   
}



