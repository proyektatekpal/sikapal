<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class Baseline extends CI_Model
{
	protected $table = "baseline";
    protected $all_field = "id,id_kapal,periode,mid_end,target";

    public function __construct()
    {
        parent::__construct();
    }
  
  
    public function get($where=NULL,$select=NULL){    //dalam array
        $query = $this->db->get_where($this->table, $where);
        if($select!=NULL){
            $this->db->select($select);
        }
        $this->db->select($this->all_field);
        if($where!=NULL){
            $this->db->where($where);
        }
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

    public function insert($data){  //dalam array
        return $this->db->insert($this->table, $data);
    }
    
    public function update($id_vertex, $data){
        $this->db->where('id', $id_vertex);
        return $this->db->update($this->table, $data);
    }

    public function update_baseline($where, $data){
        $this->db->where($where);
        return $this->db->update($this->table, $data);
    }

    public function count($where){
        $this->db->select('*');
        if($where!=NULL){
            $this->db->where($where);
        }
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    
}  

?>