<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Detail Kapal</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <form enctype="multipart/form-data" id="form1" action="<?php echo base_url()."ManajemenKapalOS/SaveInputLaporanHarian/".$id_kapal;?>" method="post" role="form">
        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <?php echo($menu); ?>
          <div class="row">
            
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Laporan Alat dan Tenaga Kerja</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-horizontal">
                        <div class="col-md-4">

                          <div class="form-group">
                            <label for="cuaca_pagi" class="col-sm-6 control-label">Tanggal Laporan:</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" name="tgl_laporan" readonly value="<?= date('Y-m-d'); ?> ">
                            </div> 
                          </div>
                         
                          <!-- <div class="form-group">
                            <label for="cuaca_pagi" class="col-sm-6 control-label">Keahlian:</label>
                            <div class="col-sm-6">
                              <input type="number" min="1" class="form-control input_number" placeholder="jumlah" id="jumlah_tenaga" name="jumlah_tenaga_kerja">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="cuaca_pagi" class="col-sm-6 control-label">Jenis Alat:</label>
                            <div class="col-sm-6">
                              <input type="number" min="1" class="form-control input_number" placeholder="jumlah" id="jumlah_alat" name="jumlah_alat">
                            </div>
                          </div -->
                          <!-- <a id="ok_button" style="margin-left:15px;" class="btn bg-navy pull-right">Ok</a> -->

                        </div>   

                        <div class="col-md-4 col-md-offset-4">
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-5 control-label">Workgroup:</label>
                            <div class="col-sm-7">
                              <select name="workgroup" id="workgroup" class="form-control selectpicker scrollable-menu " role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Workgroup</option>
                                   <?php 
                                      foreach ($list_workgroup as $data) {
                                          echo '<option value='.$data->id.'>'.$data->nama_pekerjaan.'</option>';
                                          }
                                  ?>
                              </select>
                            </div>
                          </div>
                          <div class="form-group pilih_group_hidden" hidden>
                            <label for="cuaca_siang" class="col-sm-5 control-label">Group:</label>
                            <div class="col-sm-7">
                              <select name="group" id="group" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                 
                                   
                              </select>
                            </div>
                          </div>
                          <div class="button_hidden" hidden>
                            <a id="ok_button2" style="margin-left:15px;" class="btn bg-navy pull-right"  hidden>Ok</a>
                          </div>
                        </div>
                      </div>
                </div><!-- /.box -->
              </div>
              <div class="col-md-4">
                <div class="box box-primary input_tenaga_hidden">
                    <div class="box-header with-border">
                      <h3 class="box-title">TENAGA KERJA</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <!-- <div class="box-body form-inline" id="input_tenaga_kerja"> -->
                       <table class="table table-bordered table-striped" id="tabelinput">
                          <thead>
                            <tr>
                              <th class="text-center">NO</th>
                              <th class="text-center">Keahlian</th>
                              <th class="text-center">jumlah</th>
                            </tr>
                          </thead>
                          <tbody id="data_tenaga_kerja">
                          <?php foreach ($keahlian as $key => $val) {
                          ?>
                            <tr>
                              <td class="col-md-1"><input type="text" class="form-control" value="<?php echo ($key+1)?>" readonly/></td>
                              <td class="col-md-3"><input type="text" class="form-control" value="<?php echo $val->keahlian?>" name="tenaga_kerja'+(i+1)+'" readonly/></td>
                              <td class="col-md-1"><input type="number" class="form-control input_number" name="jumlah_keahlian'+(i+1)+'"/></td>
                            <tr>
                          <?php
                          }?>
                            
                          </tbody>
                        </table>
                    </div>
                </div>
              

              <div class="col-md-4">
                <div class="box box-primary input_cuaca_hidden">
                    <div class="box-header with-border">
                      <h3 class="box-title">CUACA</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-horizontal">
                        <div class="form-group">
                            <label for="cuaca_pagi" class="col-sm-3 control-label">Pagi:</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="cuaca_pagi" placeholder="Pagi" name="cuaca_pagi">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-3 control-label">Siang:</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="cuaca_siang" placeholder="Siang" name="cuaca_siang">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cuaca_sore" class="col-sm-3 control-label">Sore:</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="cuaca_sore" placeholder="Sore" name="cuaca_sore">
                            </div>
                        </div>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
              </div>

               <div class="col-md-4">
                <div class="box box-primary input_alat_hidden">
                    <div class="box-header with-border">
                      <h3 class="box-title">ALAT YANG DIGUNAKAN</h3>
                    </div><!-- /.box-header -->
                       <table class="table table-bordered table-striped" id="tabelinput">
                          <thead>
                            <tr>
                              <th class="text-center">NO</th>
                              <th class="text-center">Nama Alat</th>
                              <th class="text-center">jumlah</th>
                            </tr>
                          </thead>
                          <tbody id="data_alat">
                          <?php foreach ($alat as $key => $val) {
                          ?>
                            <tr>
                              <td class="col-md-1"><input type="text" class="form-control" value="<?php echo ($key+1)?>" readonly/></td>
                              <td class="col-md-3"><input type="text" class="form-control" value="<?php echo $val->alat?>" name="nama_alat'+(i+1)+'" readonly/></td>
                              <td class="col-md-1"><input type="number" class="form-control input_number" name="total_alat'+(i+1)+'"/></td>
                            <tr>
                          <?php
                          }?>
                          </tbody>
                        </table>
                    </div>
                </div>
          </div><!-- /.row (main row) -->
          

          <div class="row workgroup_group">

              <!-- <div class="col-md-12"> -->
                <!-- <div class="box box-primary">
                    <div class="box-body with-border">
                      <table class="table table-bordered">
                        <tbody>
                          <tr>
                            <th>Catatan :</th>
                            <td><input name="catatan" type="text" class="form-control"/></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                </div> -->
                <!-- <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Pilih Workgroup dan Group</h3>
                    </div>                    
                    <div class="box-body form-horizontal">
                      <div class="col-md-4">
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-5 control-label">Workgroup:</label>
                            <div class="col-sm-7">
                              <select name="workgroup" id="workgroup" class="form-control selectpicker scrollable-menu " role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Workgroup</option>
                                   <?php 
                                      foreach ($list_workgroup as $data) {
                                          echo '<option value='.$data->id.'>'.$data->nama_pekerjaan.'</option>';
                                          }
                                  ?>
                              </select>
                            </div>
                          </div>
                          <div class="form-group pilih_group_hidden" hidden>
                            <label for="cuaca_siang" class="col-sm-5 control-label">Group:</label>
                            <div class="col-sm-7">
                              <select name="group" id="group" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                 
                                   
                              </select>
                            </div>
                          </div>
                          <div class="button_hidden" hidden>
                            <a id="ok_button2" style="margin-left:15px;" class="btn bg-navy pull-right"  hidden>Ok</a>
                          </div>
                      </div>
                    </div>
                </div> -->

                <div class="box-body form-horizontal">
                  <div class="box box-primary laporan_hidden" hidden>
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Laporan</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <div class="form-group" style="background-color:#eee; padding:10px;width:19%;float:right; margin-right:10px">
                            <div class="col-sm-12" id="div_group">
                              <strong>Keterangan status :</strong>
                            </div>
                            <div class="col-sm-11 col-sm-offset-1" id="div_group">
                              <strong>v = diterima </strong>
                            </div>
                            <div class="col-sm-11 col-sm-offset-1" id="div_group">
                              <strong>x = belum / ditolak </strong>
                            </div>
                        </div>
                        <table class="table table-bordered table-striped" id="tabelinput">
                          <thead>
                            <tr>
                              <th class="text-center">NO</th>
                              <th class="text-center">Proses</th>
                              <th class="text-center">Hasil/Rekomendasi</th>
                              <th class="text-center">Status</th>
                              <!-- <th class="text-center">Foto</th> -->
                            </tr>
                          </thead>
                          <tbody id="data_laporan">

                          </tbody>
                        </table>
                        <table class="table table-bordered">
                          <tbody>
                            <tr>
                              <th>Catatan :</th>
                              <td><input name="catatan" type="text" class="form-control"/></td>
                              </tr>
                            </tbody>
                        </table>       
                    </div><!-- /.box-body -->


                    <div class="box-footer">
                        <div class="pull-right">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                  </div><!-- /.box -->
                </div>
              <!-- </div> -->
            </form>
          </div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <script type="text/javascript">
	    $(document).ready(function()
      {

          $("#ok_button").click(function()
          {  
            //$("#data_activity").html('');
            // var jumlah_tenaga = $('#jumlah_tenaga').val();
            // var jumlah_alat = $('#jumlah_alat').val();

            // if(jumlah_tenaga =='' | jumlah_tenaga <= 0 | jumlah_alat =='' | jumlah_alat <= 0 )
            // {
            //       alert('Silahkan isi jumlah peekerjaan dengan benar');
            // }
            // else
            // {
            //   document.getElementById('data_tenaga_kerja').innerHTML = '';
            //   document.getElementById('data_alat').innerHTML = '';
                // for(var i=0; i<jumlah_tenaga; i++)
                // {
                //       var kolom = '';
                //       kolom += '<tr>';
                //       kolom += '<td class="col-md-1"><input type="text" class="form-control" value="'+(i+1)+'" readonly/></td>';
                //       kolom += '<td class="col-md-3"><input type="text" class="form-control" name="tenaga_kerja'+(i+1)+'"/></td>';
                //       kolom += '<td class="col-md-1"><input type="number" class="form-control input_number" name="jumlah_keahlian'+(i+1)+'"/></td>';
                //       kolom += '<tr>';
                //       $("#data_tenaga_kerja").append(kolom);
                // }

                // for(var i=0; i<jumlah_alat; i++)
                // {
                //       var kolom = '';
                //       kolom += '<tr>';
                //       kolom += '<td class="col-md-1"><input type="text" class="form-control" value="'+(i+1)+'" readonly/></td>';
                //       kolom += '<td class="col-md-3"><input type="text" class="form-control" name="nama_alat'+(i+1)+'"/></td>';
                //       kolom += '<td class="col-md-1"><input type="number" class="form-control input_number" name="total_alat'+(i+1)+'"/></td>';
                //       kolom += '<tr>';
                //       $("#data_alat").append(kolom);
                // }
                alert("tes");
                  $('.input_tenaga_hidden').show();
                  $('.input_cuaca_hidden').show();
                  $('.input_alat_hidden').show();
                  $('.workgroup_group').show();
           });

          $("#ok_button2").click(function()
          {
            document.getElementById('data_laporan').innerHTML = '';
            var e = document.getElementById("group");
            var id_group = e.options[e.selectedIndex].value;
            var id_workgroup = $('#workgroup').val();
            var num = 1;
            var kolom = '';

            //document.getElementById('data_laporan').innerHTML = '';
            $.ajax({
                        url: '<?php echo base_url()?>ManajemenKapalOS/getDataAktifitas/', 
                        dataType: 'json',
                        type: 'POST',
                        async: false,
                        data: {"id_group":id_group, "id_workgroup":id_workgroup},
                        success: function(data) 
                        {                      
                          $.each(data, function(i,item)
                          {
                            //alert(item.id);                            
                            if (item.id != 'empty') 
                            {
                              //mengolah data item
                              var item_split = item.item.split('#');
                              var count_item = item_split.length;

                              var data_item = item_split[0];
                              var data_item_split = data_item.split('|');
                              var nama_item = data_item_split[0];
                              var status_item = data_item_split[1];
                              // alert(status_item);

                              if(item.flag == 0)
                              {                            
                                //untuk baris pertama
                                kolom = '';
                                kolom += '<tr>';
                                kolom += '<input type="hidden" name="aktifitas.id'+(num)+'" value="'+item.aktifitas_id+'">';
                                kolom += '<td class="col-md-1"><input type="text" class="form-control" value="'+(num)+'" /></td>';
                                kolom += '<td class="col-md-4"><input type="text" value="'+ item.aktifitas+'" class="form-control" /></td>';
                                kolom += '<td class="col-md-4"><input type="text" value="'+nama_item+'" name="item'+(num)+''+0+'" class="form-control" /></td>';
                                kolom += '<td><select name="status'+(num)+''+ 0 +'" id="status'+(num)+''+(0)+'" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">';
                                kolom +=            '<option style="display: none;">Status</option>';
                                kolom +=            '<option value=0>x</option>';
                                kolom +=            '<option value=1>v</option>';
                                kolom += '</select></td>';
                                //kolom += '<td class="col-md-1"><input type="file" value="'+item.link_foto+'"name="foto'+(num)+'" id="foto"></td>';
                                kolom += '<tr>';

                                $("#data_laporan").append(kolom);
                                $("#status"+(num)+""+0).val(status_item);
                                // alert("#status"+(num)+"0");
                                // $("#status10").val(status_item);

                                for(var a=1; a<count_item-1; a++)
                                {
                                  var data_item = item_split[a];
                                  var data_item_split = data_item.split('|');
                                  var nama_item = data_item_split[0];
                                  var status_item = data_item_split[1];
                                  
                                  //untuk baris selanjutnya
                                    kolom = '';
                                    kolom += '<tr>';
                                    kolom += '<input type="hidden" name="aktifitas.id'+(num)+'" value="'+item.aktifitas_id+'">';
                                    kolom += '<td class="col-md-1"></td>';
                                    kolom += '<td class="col-md-4"></td>';
                                    kolom += '<td class="col-md-4"><input type="text" value="'+nama_item+'" name="item'+(num)+''+(a)+'" class="form-control" /></td>';
                                    kolom += '<td><select name="status'+(num)+''+(a)+'" id="status'+(num)+''+(a)+'" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3" >';
                                    kolom +=            '<option style="display: none;">Status</option>';
                                    kolom +=            '<option value=0>x</option>';
                                    kolom +=            '<option value=1>v</option>';
                                    kolom += '</select></td>';
                                    //kolom += '<td class="col-md-1"></td>';
                                    kolom += '<tr>';

                                    $("#data_laporan").append(kolom);
                                    $("#status"+(num)+""+a).val(status_item);
                                    // $("#status"+(num)+""+(a)).val(status_item);
                                }
                              }
                              else
                              {
                                if(status_item == 0)
                                {
                                  //untuk baris pertama
                                  kolom = '';
                                  kolom += '<tr>';
                                  kolom += '<input type="hidden" name="aktifitas.id'+(num)+'" value="'+item.aktifitas_id+'">';
                                  kolom += '<input type="hidden" name="hidden_gambar'+(num)+'" value="'+ item.link_foto+'" class="form-control" />';
                                  kolom += '<td class="col-md-1"><input type="text" class="form-control" value="'+(num)+'" /></td>';
                                  kolom += '<td class="col-md-4"><input type="text" value="'+ item.aktifitas+'" class="form-control" /></td>';
                                  kolom += '<td class="col-md-4"><input type="text" value="'+nama_item+'" name="item'+(num)+''+0+'" class="form-control" /></td>';
                                  kolom += '<td><select name="status'+(num)+''+ 0 +'" id="status'+(num)+''+(0)+'" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">';
                                  kolom +=            '<option style="display: none;">Status</option>';
                                  kolom +=            '<option value=0>x</option>';
                                  kolom +=            '<option value=1>v</option>';
                                  kolom += '</select></td>';
                                  //kolom += '<td class="col-md-1"><input type="file" value="'+item.link_foto+'"name="foto'+(num)+'" id="foto"></td>';
                                  kolom += '<tr>';

                                  $("#data_laporan").append(kolom);
                                  $("#status"+(num)+""+0).val(status_item);
                                  
                                  for(var a=1; a<count_item-1; a++)
                                  {
                                    var data_item = item_split[a];
                                    var data_item_split = data_item.split('|');
                                    var nama_item = data_item_split[0];
                                    var status_item = data_item_split[1];
                                    
                                    //untuk baris selanjutnya
                                      kolom = '';
                                      kolom += '<tr>';
                                      kolom += '<input type="hidden" name="aktifitas.id'+(num)+'" value="'+item.aktifitas_id+'">';
                                      kolom += '<td class="col-md-1"></td>';
                                      kolom += '<td class="col-md-4"></td>';
                                      kolom += '<td class="col-md-4"><input type="text" value="'+nama_item+'" name="item'+(num)+''+(a)+'" class="form-control" /></td>';
                                      kolom += '<td><select name="status'+(num)+''+(a)+'" id="status'+(num)+''+(a)+'" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3" >';
                                      kolom +=            '<option style="display: none;">Status</option>';
                                      kolom +=            '<option value=0>x</option>';
                                      kolom +=            '<option value=1>v</option>';
                                      kolom += '</select></td>';
                                      //kolom += '<td class="col-md-1"></td>';
                                      kolom += '<tr>';

                                      $("#data_laporan").append(kolom);
                                      $("#status"+(num)+""+a).val(status_item);
                                  }
                                }
                                else
                                {
                                  //untuk baris pertama
                                  kolom = '';
                                  kolom += '<tr>';
                                  kolom += '<input type="hidden" name="aktifitas.id'+(num)+'" value="'+item.aktifitas_id+'">';
                                  kolom += '<input type="hidden" name="hidden_gambar'+(num)+'" value="'+ item.link_foto+'" class="form-control" readonly/>';
                                  kolom += '<td class="col-md-1"><input type="text" class="form-control" value="'+(num)+'" readonly/></td>';
                                  kolom += '<td class="col-md-4"><input type="text" value="'+ item.aktifitas+'" class="form-control" readonly/></td>';
                                  kolom += '<td class="col-md-4"><input type="text" value="'+nama_item+'" name="item'+(num)+''+0+'" class="form-control" readonly/></td>';
                                  kolom += '<td><select name="status'+(num)+''+ 0 +'" id="status'+(num)+''+(0)+'" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3" readonly>';
                                  kolom +=            '<option style="display: none;">Status</option>';
                                  // kolom +=            '<option value=0>Ditolak</option>';
                                  kolom +=            '<option value=1>v</option>';
                                  kolom += '</select></td>';
                                  //kolom += '<td class="col-md-1"><input type="file" value="'+item.link_foto+'"name="foto'+(num)+'" id="foto"></td>';
                                  kolom += '<tr>';

                                  $("#data_laporan").append(kolom);
                                  $("#status"+(num)+""+0).val(status_item);
                                  
                                  for(var a=1; a<count_item-1; a++)
                                  {
                                    var data_item = item_split[a];
                                    var data_item_split = data_item.split('|');
                                    var nama_item = data_item_split[0];
                                    var status_item = data_item_split[1];
                                    
                                    //untuk baris selanjutnya
                                      kolom = '';
                                      kolom += '<tr>';
                                      kolom += '<input type="hidden" name="aktifitas.id'+(num)+'" value="'+item.aktifitas_id+'">';
                                      kolom += '<td class="col-md-1"></td>';
                                      kolom += '<td class="col-md-4"></td>';
                                      if(status_item == 0)
                                      {
                                        kolom += '<td class="col-md-4"><input type="text" value="'+nama_item+'" name="item'+(num)+''+(a)+'" class="form-control" /></td>';
                                        kolom += '<td><select name="status'+(num)+''+(a)+'" id="status'+(num)+''+(a)+'" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3" >';
                                        kolom +=            '<option style="display: none;">Status</option>';
                                        kolom +=            '<option value=0>x</option>';
                                        kolom +=            '<option value=1>v</option>';
                                        kolom += '</select></td>';
                                        //kolom += '<td class="col-md-1"></td>';
                                        kolom += '<tr>';
                                      }
                                      else
                                      {
                                        kolom += '<td class="col-md-4"><input type="text" value="'+nama_item+'" name="item'+(num)+''+(a)+'" class="form-control" readonly/></td>';
                                        kolom += '<td><select name="status'+(num)+''+(a)+'" id="status'+(num)+''+(a)+'" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3" readonly>';
                                        kolom +=            '<option style="display: none;">Status</option>';
                                        // kolom +=            '<option value=0>Ditolak</option>';
                                        kolom +=            '<option value=1>v</option>';
                                        kolom += '</select></td>';
                                        //kolom += '<td class="col-md-1"></td>';
                                        kolom += '<tr>';     
                                      }
                                     

                                      $("#data_laporan").append(kolom);
                                      $("#status"+(num)+""+a).val(status_item);
                                  }
                                }  
                              }
                              kolom = '';
                              kolom += '<input type="hidden" name="loop_item'+(num-1)+'" value="'+(count_item-1)+'">';
                              $("#data_laporan").append(kolom);
                                //$("#status"+(num)+"").val(item.status_pekerjaan);
                                //$('#status'+(num)+' :nth-child(4)').prop('selected', true);
                              num++;
                            } 
                            else {}
                          }); 
                        }
                  }); //end of ajax

            kolom =  '';
            kolom += '<input type="hidden" name="loop" value="'+(num-1)+'">';
            $("#data_laporan").append(kolom);
            $('.laporan_hidden').show();            
          });

          $('#workgroup').on('change', function() {
            document.getElementById('group').innerHTML = '';
            var id_workgroup = $('#workgroup').val();
            var kolom = '';
            kolom += '<option style="display: none;">Pilih Group</option>';
            //$("#group").append(kolom);
            //alert(id_workgroup);
            $.ajax({
                        url: '<?php echo base_url()?>ManajemenKapalOS/GetGroup/', 
                        dataType: 'json',
                        type: 'POST',
                        async: false,
                        data: {"id_workgroup_kapal":id_workgroup},
                        success: function(data) {
                          $.each(data, function(i,item){
                            //alert(item.id);                            
                            if (item.id != 'empty') {

                            kolom +=  '<option value="'+item.id+'">'+item.group+'</option>';
                            $("#group").append(kolom);
                            kolom='';                         
                            } 
                            else {}
                          });                
                        }
                      });
            $('.pilih_group_hidden').show();
            $('.button_hidden').show();                          
          });
	    });
</script>