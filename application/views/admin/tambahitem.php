<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>

<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Item</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>


        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
        <div class="row">
            <form enctype="multipart/form-data" id="form1" action="#" method="post" role="form">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Item</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-horizontal">
                      <div class="col-md-4">
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-5 control-label">Workgroup:</label>
                            <div class="col-sm-7">
                              <select name="workgroup" id="workgroup" class="form-control selectpicker scrollable-menu " role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Workgroup</option>
                                   <?php 
                                      foreach ($list_workgroup as $data) {
                                          echo '<option value='.$data->id_workgroup.'>'.$data->nama_pekerjaan.'</option>';
                                          }
                                  ?>
                              </select>
                            </div>
                          </div>
                          <div class="form-group pilih_group_hidden" hidden>
                            <label for="cuaca_siang" class="col-sm-5 control-label">Proses:</label>
                            <div class="col-sm-7">
                              <select name="proses" id="proses" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                 
                                   
                              </select>
                            </div>
                          </div>
                          <div class="form-group jumlah_item" hidden>
                            <label for="jumlahActivity" class="col-sm-5 control-label">Jumlah Item:</label>
                            <div class="col-sm-7">
                              <input type="number" min="1" class="form-control input_number" placeholder="jumlah item" id="jlh_item" name="jlh_item" value="">
                            </div>
                          </div>
                          <div class="button_hidden" hidden>
                            <a id="ok_button" style="margin-left:15px;" class="btn bg-navy pull-right"  hidden>Ok</a>
                          </div>
                      </div>
                    </div>
                </div><!-- /.box -->
              </div>


              <div class="col-md-6">
                <div class="box box-primary laporan_hidden" hidden>
                    <div class="box-header with-border">
                      <h3 class="box-title">Data Item</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <table class="table table-bordered table-striped" id="tabelinput">
                          <thead>
                            <tr>
                              <th class="text-center">NO</th>
                              <th class="text-center">Nama Item</th>
                            </tr>
                          </thead>
                          <tbody id="data_item">

                          </tbody>
                          
                        </table>
                        
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <div class="pull-right">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div><!-- /.box -->
              </div>
            </form>
        </div><!-- /.row (main row) -->


        </section><!-- right col -->
      </div><!-- /.content-wrapper -->

<script type="text/javascript">
	    $(document).ready(function()
	    {
        $('#workgroup').on('change', function() {
            document.getElementById('proses').innerHTML = '';
            var id_workgroup = $('#workgroup').val();
            var kolom = '';
            kolom += '<option style="display: none;">Pilih Proses</option>';

            $.ajax({
                        url: '<?php echo base_url()?>ManajemenKapalAdmin/getDataActivity/', 
                        dataType: 'json',
                        type: 'POST',
                        data: {"id_workgroup":id_workgroup},
                        success: function(data) 
                        {
                          $.each(data, function(i,item){
                            //alert(item.id);                            
                            if (item.id != 'empty') 
                            {
                              kolom +=  '<option value="'+item.id+'">'+item.aktifitas+'</option>';
                              $("#proses").append(kolom);
                              kolom='';                         
                            } 
                            else {}
                          });                
                        }
                      });
            $('.pilih_group_hidden').show();
            $('.jumlah_item').show();                          
            $('.button_hidden').show();                          
          });
          


          $("#ok_button").click(function()
          { 
            // alert("tst");
            var jlh_item = $('#jlh_item').val();

            var e = document.getElementById("proses");
            var strUser = e.options[e.selectedIndex].value;

            var baseurl = "<?php print base_url(); ?>";

            $('#form1').attr('action',  baseurl+'ManajemenKapalAdmin/SaveItem/' + strUser);

            if(jlh_item =='' | jlh_item <= 0)
            {
                  alert('Silahkan isi jumlah item dengan benar');
            }
            else
            {
              document.getElementById('data_item').innerHTML = '';
              for(var i=0; i<jlh_item; i++)
                {
                      var kolom = '';
                      kolom += '<tr>';
                      kolom += '<td class="col-md-2"><input type="text" class="form-control" value="'+(i+1)+'"/></td>';
                      kolom += '<td class="col-md-4"><input type="text" id="aktifitas" name="item'+(i+1)+'"  class="form-control"/></td>';
                      // kolom += '<td class="col-md-2"><input type="number" id="bobot" name="bobot'+(i+1)+'" class="form -control input_number"/></td>';
                      kolom += '<tr>';
                      $("#data_item").append(kolom);
                }
                $('.laporan_hidden').show();
            }            
           });
      }); 
</script>