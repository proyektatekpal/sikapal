<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class Aktifitas extends CI_Model
{
	protected $table = "aktifitas";
    protected $all_field = "group.id,group.id_workgroup_kapal,group,item, bobot_workgroup,kuantitas,satuan";

    public function __construct()
    {
        parent::__construct();
    }
  
  
    public function get($id_workgroup){    //dalam array    
        $res = $this->db->get_where($this->table, array('id_workgroup' => $id_workgroup));
        return $res->result();
    }

    
    
}  

?>