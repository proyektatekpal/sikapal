<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class Workgroup_kapal extends CI_Model
{
	protected $table = "workgroup_kapal";
    protected $all_field = "workgroup_kapal.id,id_kapal,workgroup_kapal.id_workgroup";

    public function __construct()
    {
        parent::__construct();
    }
  
  
    public function get($where=NULL,$select=NULL){    //dalam array
        //$query = $this->db->get_where($this->table, $where);
        if($select!=NULL){
            $this->db->select($select);
        }
        $this->db->select($this->all_field);
        if($where!=NULL){
            $this->db->where($where);
        }
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

    public function insert($data){  //dalam array
        return $this->db->insert($this->table, $data);
    }
    
    public function update($id_vertex, $data){
        $this->db->where('id', $id_vertex);
        return $this->db->update($this->table, $data);
    }

    public function count($where){
        $this->db->select('*');
        if($where!=NULL){
            $this->db->where($where);
        }
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function join($id_kapal){
        $this->db->select($this->all_field);
        $this->db->select("bobot,nama_pekerjaan");
        $this->db->from($this->table);
        $this->db->join("workgroup", "$this->table.id_workgroup = workgroup.id_workgroup", "left");
        $this->db->where(array('id_kapal' => $id_kapal));
        $query = $this->db->get();
        return $query->result();
    }

    public function join_workgroup(){
        $this->db->select('bobot,nama_pekerjaan');
        $this->db->join('workgroup', 'workgroup.id_workgroup = workgroup_kapal.id_workgroup', 'left');
    }

    public function join_kapal(){
        $this->db->select('nama_proyek,pemilik,status_pengerjaan,id_os,id_galangan,id_owner');
        $this->db->join('pembuatan_kapal','pembuatan_kapal.id = workgroup_kapal.id_kapal', 'left');
    }
    
    
}  

?>