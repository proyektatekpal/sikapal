<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Laporan 2 Minggu</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
           ?>
                
          <div class="row">
            <form action="" method="post" role="form">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Laporan 2 Minggu</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                          <table id="list-kapal" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>Periode Laporan</th>
                                <th>Tanggal Input</th>
                                <th>Project Aktual</th>
                                <th>Project Rencana</th>
                                <th>Status</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Januari Awal<a target="_blank" class="pull-right" href="<?php echo base_url()?>ManajemenKapalOwner/DetailMingguan/" title="Detail"><i class="fa fa-chevron-circle-right"></i></a></td>
                                <!-- <td>Belum Disetujui</td> -->
                              </tr>
                               <tr>
                                <td>Januari Akhir<a target="_blank" class="pull-right" href="<?php echo base_url()?>ManajemenKapalOS/DetailMingguan" title="Detail"><i class="fa fa-chevron-circle-right"></i></a></td>
                                <!-- <td>Belum Disetujui</td> -->
                              </tr>
                            </tbody>
                            <tfoot>
                              <tr>
                                <th>Periode Laporan</th>
                                <th>Tanggal Input</th>
                                <th>Project Aktual</th>
                                <th>Project Rencana</th>
                                <th>Status</th>
                              </tr>
                            </tfoot>
                          </table>
                      </div><!-- /.box-body -->

                </div><!-- /.box -->
              </div>
            </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

 