
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url()?>assets/theme/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>Alexander Pierce</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i><span>Profil</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>ProfilOwnerSurveyor/editprofil"><i class="fa fa-circle-o"></i>Edit Profil</a></li>
                <li><a href="<?php echo base_url()?>profilownersurveyor/ubahpassword"><i class="fa fa-circle-o"></i>Ubah Password</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url()?>ManajemenKapalOS/DaftarKapal"><i class="fa fa-share"></i> <span>Daftar Kapal</span>              
              </a>
            <!-- <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i><span>WorkGroup</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>ManajemenKapalOS/AddWorkGroup"><i class="fa fa-circle-o"></i>Tambah</a></li>
                <li><a href="<?php echo base_url()?>ManajemenKapalOS/LihatWorkGroup"><i class="fa fa-circle-o"></i>Daftar WorkGroup</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i><span>Sub WorkGroup</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>ManajemenKapalOS/AddSubWorkGroup"><i class="fa fa-circle-o"></i>Tambah</a></li>
                <li><a href="<?php echo base_url()?>ManajemenKapalOS/LihatSubWorkGroup"><i class="fa fa-circle-o"></i>Daftar Sub WorkGroup</a></li>
              </ul>
            </li> -->

              <!---
              <ul class="treeview-menu">
                <li>
                  <a href="#"><i class="fa fa-circle-o"></i> Kapal 1 <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="<?php echo base_url()?>OwnerSurveyor/gambar"><i class="fa fa-circle-o"></i> Gambar </a></li>
                    <li><a href="<?php echo base_url()?>OwnerSurveyor/s_curve"><i class="fa fa-circle-o"></i> S-Curve </a></li>
                    <li><a href="<?php echo base_url()?>OwnerSurveyor/laporan"><i class="fa fa-circle-o"></i> Laporan </a></li>
                  </ul>
                </li>
                <li>
                  <a href="#"><i class="fa fa-circle-o"></i> Kapal 2 <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Gambar </a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> S-Curve </a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Laporan </a></li>
                  </ul>
                  -->
                </li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>