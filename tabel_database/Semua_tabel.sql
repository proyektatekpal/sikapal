-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 17 Jan 2016 pada 11.32
-- Versi Server: 5.6.14
-- Versi PHP: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `sikapal`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` int(8) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `email`, `password`) VALUES
(00000001, 'administrator', 'admin@admin.com', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `aktifitas`
--

CREATE TABLE IF NOT EXISTS `aktifitas` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_workgroup` int(6) unsigned zerofill NOT NULL,
  `aktifitas` varchar(40) NOT NULL,
  `bobot` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

--
-- Dumping data untuk tabel `aktifitas`
--

INSERT INTO `aktifitas` (`id`, `id_workgroup`, `aktifitas`, `bobot`) VALUES
(000003, 000001, 'Design', '20'),
(000004, 000001, 'Owner Apporaval', '20'),
(000005, 000001, 'Class Apporaval', '30'),
(000006, 000001, 'Doc. Issued', '30'),
(000007, 000002, 'Purchase Order', '10'),
(000008, 000002, 'Arrival At yard', '10'),
(000009, 000002, 'Fab. Blasting & Shop Primer', '10'),
(000010, 000002, 'Fab. Marking', '10'),
(000011, 000002, 'Fab. Cutting', '10'),
(000012, 000002, 'Fab. Bending', '10'),
(000013, 000002, 'Ass. Fitt Up', '10'),
(000014, 000002, 'Ass. Welding', '10'),
(000015, 000002, 'Ass. Inspection', '5'),
(000016, 000002, 'Erec. Fitt Up', '5'),
(000017, 000002, 'Erec. Welding', '5'),
(000018, 000002, 'Erec. Inspection', '5'),
(000019, 000003, 'Purchase Order', '10'),
(000020, 000003, 'Arrival At yard', '10'),
(000021, 000003, 'Fab.Marking', '10'),
(000022, 000003, 'Fab. Cutting', '10'),
(000023, 000003, 'Fab. Fitt Up', '10'),
(000024, 000003, 'Fab. Welding', '10'),
(000025, 000003, 'Fab. Inspection', '10'),
(000026, 000003, 'On Board Fitt Up', '10'),
(000027, 000003, 'On Board Install', '10'),
(000028, 000003, 'On Board Inspection', '5'),
(000029, 000003, 'On Board Testing ', '5'),
(000030, 000004, 'Purchase Order', '10'),
(000031, 000004, 'Arrival At yard', '10'),
(000032, 000004, 'Fab. Fitt Up', '10'),
(000033, 000004, 'Fab. Welding ', '10'),
(000034, 000004, 'Fab. Inspection', '10'),
(000035, 000004, 'On Board Fitt Up', '10'),
(000036, 000004, 'On Board Install', '10'),
(000037, 000004, 'On Board Inspection', '15'),
(000038, 000004, 'On Board Testing', '15'),
(000039, 000005, 'Purchase Order', '10'),
(000040, 000005, 'Arrival At yard', '10'),
(000041, 000005, 'Fab. Marking', '10'),
(000042, 000005, 'Fab. Cutting', '10'),
(000043, 000005, 'Fab.  Fitt Up', '10'),
(000044, 000005, 'Fab.  Welding', '10'),
(000045, 000005, 'Fab. Inspection', '10'),
(000046, 000005, 'On Board Fitt Up', '10'),
(000047, 000005, 'On Board Install', '10'),
(000048, 000005, 'On Board Inspection', '5'),
(000049, 000005, 'On Board Testing', '5'),
(000050, 000006, 'Purchase Order', '10'),
(000051, 000006, 'Arrival At yard', '10'),
(000052, 000006, 'On Board Fitt Up', '10'),
(000053, 000006, 'On Board Install', '10'),
(000054, 000006, 'On Board Connect', '20'),
(000055, 000006, 'On Board Inspection', '20'),
(000056, 000006, 'On Board Testing', '20'),
(000057, 000007, 'Purchase Order', '20'),
(000058, 000007, 'Arrival At yard', '20'),
(000059, 000007, 'Surface Prep.', '20'),
(000060, 000007, 'Coating/Painting', '20'),
(000061, 000007, 'Inspection', '20'),
(000062, 000008, 'Purchase Order', '25'),
(000063, 000008, 'Arrival At yard', '25'),
(000064, 000008, 'Loading On Board', '25'),
(000065, 000008, 'Inspection', '25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `alat_kerja`
--

CREATE TABLE IF NOT EXISTS `alat_kerja` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `alat` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `alat_kerja`
--

INSERT INTO `alat_kerja` (`id`, `alat`) VALUES
(000001, 'Palu'),
(000002, 'Paku');

-- --------------------------------------------------------

--
-- Struktur dari tabel `baseline`
--

CREATE TABLE IF NOT EXISTS `baseline` (
  `id` int(8) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_kapal` int(6) unsigned zerofill NOT NULL,
  `periode` varchar(25) DEFAULT NULL,
  `mid_end` int(3) DEFAULT NULL,
  `target` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=159 ;

--
-- Dumping data untuk tabel `baseline`
--

INSERT INTO `baseline` (`id`, `id_kapal`, `periode`, `mid_end`, `target`) VALUES
(00000001, 000001, 'January 2015', 0, '0'),
(00000002, 000001, 'January 2015', 1, '0'),
(00000003, 000001, 'February 2015', 0, '0'),
(00000004, 000001, 'February 2015', 1, '0'),
(00000005, 000001, 'March 2015', 0, '0'),
(00000006, 000001, 'March 2015', 1, '0'),
(00000007, 000001, 'April 2015', 0, '0'),
(00000008, 000001, 'April 2015', 1, '0'),
(00000009, 000001, 'May 2015', 0, '0'),
(00000010, 000001, 'May 2015', 1, '0'),
(00000011, 000001, 'June 2015', 0, '0'),
(00000012, 000001, 'June 2015', 1, '100'),
(00000013, 000002, NULL, NULL, '0'),
(00000014, 000002, NULL, NULL, '0'),
(00000015, 000002, NULL, NULL, '0'),
(00000016, 000002, NULL, NULL, '0'),
(00000017, 000002, NULL, NULL, '0'),
(00000018, 000002, NULL, NULL, '0'),
(00000019, 000003, NULL, NULL, '0'),
(00000020, 000003, NULL, NULL, '0'),
(00000021, 000003, NULL, NULL, '0'),
(00000022, 000003, NULL, NULL, '0'),
(00000023, 000004, NULL, NULL, '0'),
(00000024, 000004, NULL, NULL, '0'),
(00000025, 000004, NULL, NULL, '0'),
(00000026, 000004, NULL, NULL, '0'),
(00000027, 000004, NULL, NULL, '0'),
(00000028, 000004, NULL, NULL, '0'),
(00000029, 000004, NULL, NULL, '0'),
(00000030, 000004, NULL, NULL, '0'),
(00000031, 000004, NULL, NULL, '0'),
(00000032, 000004, NULL, NULL, '0'),
(00000033, 000004, NULL, NULL, '0'),
(00000034, 000004, NULL, NULL, '0'),
(00000035, 000005, NULL, NULL, '0'),
(00000036, 000005, NULL, NULL, '0'),
(00000037, 000005, NULL, NULL, '0'),
(00000038, 000005, NULL, NULL, '0'),
(00000039, 000005, NULL, NULL, '0'),
(00000040, 000005, NULL, NULL, '0'),
(00000041, 000005, NULL, NULL, '0'),
(00000042, 000005, NULL, NULL, '0'),
(00000043, 000005, NULL, NULL, '0'),
(00000044, 000005, NULL, NULL, '0'),
(00000045, 000005, NULL, NULL, '0'),
(00000046, 000005, NULL, NULL, '0'),
(00000047, 000006, 'January 2015', NULL, '2'),
(00000048, 000006, 'January 2015', NULL, '3'),
(00000049, 000006, 'February 2015', NULL, '10'),
(00000050, 000006, 'February 2015', NULL, '17'),
(00000051, 000006, 'Maret 2015', NULL, '28'),
(00000052, 000006, 'Maret 2015', NULL, '40'),
(00000053, 000006, 'April 2015', NULL, '60'),
(00000054, 000006, 'April 2015', NULL, '80'),
(00000055, 000006, 'Mei 2015', NULL, '94'),
(00000056, 000006, 'Mei 2015', NULL, '96'),
(00000057, 000006, 'Juni 2015', NULL, '98'),
(00000058, 000006, 'Juni 2015', NULL, '100'),
(00000059, 000007, NULL, NULL, '0'),
(00000060, 000007, NULL, NULL, '0'),
(00000061, 000007, NULL, NULL, '0'),
(00000062, 000007, NULL, NULL, '0'),
(00000063, 000007, NULL, NULL, '0'),
(00000064, 000007, NULL, NULL, '0'),
(00000065, 000007, NULL, NULL, '0'),
(00000066, 000007, NULL, NULL, '0'),
(00000067, 000007, NULL, NULL, '0'),
(00000068, 000007, NULL, NULL, '0'),
(00000069, 000007, NULL, NULL, '0'),
(00000070, 000007, NULL, NULL, '0'),
(00000071, 000008, NULL, NULL, '0'),
(00000072, 000008, NULL, NULL, '0'),
(00000073, 000008, NULL, NULL, '0'),
(00000074, 000008, NULL, NULL, '0'),
(00000075, 000008, NULL, NULL, '0'),
(00000076, 000008, NULL, NULL, '0'),
(00000077, 000008, NULL, NULL, '0'),
(00000078, 000008, NULL, NULL, '0'),
(00000079, 000008, NULL, NULL, '0'),
(00000080, 000008, NULL, NULL, '0'),
(00000081, 000008, NULL, NULL, '0'),
(00000082, 000008, NULL, NULL, '0'),
(00000083, 000009, NULL, NULL, '0'),
(00000084, 000009, NULL, NULL, '0'),
(00000085, 000009, NULL, NULL, '0'),
(00000086, 000009, NULL, NULL, '0'),
(00000087, 000009, NULL, NULL, '0'),
(00000088, 000009, NULL, NULL, '0'),
(00000089, 000009, NULL, NULL, '0'),
(00000090, 000009, NULL, NULL, '0'),
(00000091, 000009, NULL, NULL, '0'),
(00000092, 000009, NULL, NULL, '0'),
(00000093, 000009, NULL, NULL, '0'),
(00000094, 000009, NULL, NULL, '0'),
(00000095, 000010, NULL, NULL, '0'),
(00000096, 000010, NULL, NULL, '0'),
(00000097, 000010, NULL, NULL, '0'),
(00000098, 000010, NULL, NULL, '0'),
(00000099, 000010, NULL, NULL, '0'),
(00000100, 000010, NULL, NULL, '0'),
(00000101, 000010, NULL, NULL, '0'),
(00000102, 000010, NULL, NULL, '0'),
(00000103, 000010, NULL, NULL, '0'),
(00000104, 000010, NULL, NULL, '0'),
(00000105, 000010, NULL, NULL, '0'),
(00000106, 000010, NULL, NULL, '0'),
(00000107, 000011, NULL, NULL, '0'),
(00000108, 000011, NULL, NULL, '0'),
(00000109, 000011, NULL, NULL, '0'),
(00000110, 000011, NULL, NULL, '0'),
(00000111, 000011, NULL, NULL, '0'),
(00000112, 000011, NULL, NULL, '0'),
(00000113, 000011, NULL, NULL, '0'),
(00000114, 000011, NULL, NULL, '0'),
(00000115, 000011, NULL, NULL, '0'),
(00000116, 000011, NULL, NULL, '0'),
(00000117, 000011, NULL, NULL, '0'),
(00000118, 000011, NULL, NULL, '0'),
(00000119, 000012, NULL, NULL, '0'),
(00000120, 000012, NULL, NULL, '0'),
(00000121, 000012, NULL, NULL, '0'),
(00000122, 000012, NULL, NULL, '0'),
(00000123, 000012, NULL, NULL, '0'),
(00000124, 000012, NULL, NULL, '0'),
(00000125, 000012, NULL, NULL, '0'),
(00000126, 000012, NULL, NULL, '0'),
(00000127, 000012, NULL, NULL, '0'),
(00000128, 000012, NULL, NULL, '0'),
(00000129, 000013, NULL, NULL, '0'),
(00000130, 000013, NULL, NULL, '0'),
(00000131, 000013, NULL, NULL, '0'),
(00000132, 000013, NULL, NULL, '0'),
(00000133, 000013, NULL, NULL, '0'),
(00000134, 000013, NULL, NULL, '0'),
(00000135, 000013, NULL, NULL, '0'),
(00000136, 000013, NULL, NULL, '0'),
(00000137, 000013, NULL, NULL, '0'),
(00000138, 000013, NULL, NULL, '0'),
(00000139, 000014, NULL, NULL, '0'),
(00000140, 000014, NULL, NULL, '0'),
(00000141, 000014, NULL, NULL, '0'),
(00000142, 000014, NULL, NULL, '0'),
(00000143, 000014, NULL, NULL, '0'),
(00000144, 000014, NULL, NULL, '0'),
(00000145, 000014, NULL, NULL, '0'),
(00000146, 000014, NULL, NULL, '0'),
(00000147, 000014, NULL, NULL, '0'),
(00000148, 000014, NULL, NULL, '0'),
(00000149, 000015, NULL, NULL, '0'),
(00000150, 000015, NULL, NULL, '0'),
(00000151, 000015, NULL, NULL, '0'),
(00000152, 000015, NULL, NULL, '0'),
(00000153, 000015, NULL, NULL, '0'),
(00000154, 000015, NULL, NULL, '0'),
(00000155, 000015, NULL, NULL, '0'),
(00000156, 000015, NULL, NULL, '0'),
(00000157, 000015, NULL, NULL, '0'),
(00000158, 000015, NULL, NULL, '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_laporan_harian`
--

CREATE TABLE IF NOT EXISTS `data_laporan_harian` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `progres_harian` varchar(15) NOT NULL,
  `id_aktifitas` int(6) unsigned zerofill NOT NULL,
  `id_laporan_harian` int(6) unsigned zerofill NOT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `link_foto` varchar(100) DEFAULT NULL,
  `status_pekerjaan` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=790 ;

--
-- Dumping data untuk tabel `data_laporan_harian`
--

INSERT INTO `data_laporan_harian` (`id`, `progres_harian`, `id_aktifitas`, `id_laporan_harian`, `deskripsi`, `link_foto`, `status_pekerjaan`) VALUES
(000283, '', 000007, 000053, 'Memeriksa jumlah dan tanggal penerima', NULL, 2),
(000284, '', 000008, 000053, 'Memeriksa sertifikat dan kondisi material', NULL, 1),
(000285, '', 000009, 000053, '', NULL, 0),
(000286, '', 000010, 000053, '', NULL, 0),
(000287, '', 000011, 000053, '', NULL, 0),
(000288, '', 000012, 000053, '', NULL, 0),
(000289, '', 000013, 000053, '', NULL, 0),
(000290, '', 000014, 000053, '', NULL, 0),
(000291, '', 000015, 000053, '', NULL, 0),
(000292, '', 000016, 000053, '', NULL, 0),
(000293, '', 000017, 000053, '', NULL, 0),
(000294, '', 000018, 000053, '', NULL, 0),
(000295, '', 000007, 000054, 'Memeriksa jumlah dan tanggal penerima', NULL, 2),
(000296, '', 000008, 000054, 'Memeriksa sertifikat dan kondisi material', NULL, 1),
(000297, '', 000009, 000054, '', NULL, 0),
(000298, '', 000010, 000054, '', NULL, 0),
(000299, '', 000011, 000054, '', NULL, 0),
(000300, '', 000012, 000054, '', NULL, 0),
(000301, '', 000013, 000054, '', NULL, 0),
(000302, '', 000014, 000054, '', NULL, 0),
(000303, '', 000015, 000054, '', NULL, 0),
(000304, '', 000016, 000054, '', NULL, 0),
(000305, '', 000017, 000054, '', NULL, 0),
(000306, '', 000018, 000054, '', NULL, 0),
(000307, '', 000007, 000055, 'Memeriksa jumlah dan tanggal penerima', NULL, 2),
(000308, '', 000008, 000055, 'Memeriksa sertifikat dan kondisi material', NULL, 1),
(000309, '', 000009, 000055, '', NULL, 0),
(000310, '', 000010, 000055, '', NULL, 0),
(000311, '', 000011, 000055, '', NULL, 0),
(000312, '', 000012, 000055, '', NULL, 0),
(000313, '', 000013, 000055, '', NULL, 0),
(000314, '', 000014, 000055, '', NULL, 0),
(000315, '', 000015, 000055, '', NULL, 0),
(000316, '', 000016, 000055, '', NULL, 0),
(000317, '', 000017, 000055, '', NULL, 0),
(000318, '', 000018, 000055, '', NULL, 0),
(000319, '', 000007, 000056, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000320, '', 000008, 000056, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000321, '', 000009, 000056, 'Memeriksa kekerasan permukaan', 'dummy.png', 1),
(000322, '', 000010, 000056, '', 'dummy.png', 0),
(000323, '', 000011, 000056, '', 'dummy.png', 0),
(000324, '', 000012, 000056, '', 'dummy.png', 0),
(000325, '', 000013, 000056, '', 'dummy.png', 0),
(000326, '', 000014, 000056, '', 'dummy.png', 0),
(000327, '', 000015, 000056, '', 'dummy.png', 0),
(000328, '', 000016, 000056, '', 'dummy.png', 0),
(000329, '', 000017, 000056, '', 'dummy.png', 0),
(000330, '', 000018, 000056, '', 'dummy.png', 0),
(000331, '', 000007, 000057, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000332, '', 000008, 000057, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000333, '', 000009, 000057, 'Memeriksa kekerasan permukaan', 'dummy.png', 1),
(000334, '', 000010, 000057, '', 'dummy.png', 0),
(000335, '', 000011, 000057, '', 'dummy.png', 0),
(000336, '', 000012, 000057, '', 'dummy.png', 0),
(000337, '', 000013, 000057, '', 'dummy.png', 0),
(000338, '', 000014, 000057, '', 'dummy.png', 0),
(000339, '', 000015, 000057, '', 'dummy.png', 0),
(000340, '', 000016, 000057, '', 'dummy.png', 0),
(000341, '', 000017, 000057, '', 'dummy.png', 0),
(000342, '', 000018, 000057, '', 'dummy.png', 0),
(000343, '', 000007, 000058, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000344, '', 000008, 000058, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000345, '', 000009, 000058, 'Memeriksa kekerasan permukaan', 'dummy.png', 1),
(000346, '', 000010, 000058, '', 'dummy.png', 0),
(000347, '', 000011, 000058, '', 'dummy.png', 0),
(000348, '', 000012, 000058, '', 'dummy.png', 0),
(000349, '', 000013, 000058, '', 'dummy.png', 0),
(000350, '', 000014, 000058, '', 'dummy.png', 0),
(000351, '', 000015, 000058, '', 'dummy.png', 0),
(000352, '', 000016, 000058, '', 'dummy.png', 0),
(000353, '', 000017, 000058, '', 'dummy.png', 0),
(000354, '', 000018, 000058, '', 'dummy.png', 0),
(000355, '', 000007, 000059, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000356, '', 000008, 000059, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000357, '', 000009, 000059, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000358, '', 000010, 000059, 'Memerika Posisi konstruksi', 'dummy.png', 1),
(000359, '', 000011, 000059, '', 'dummy.png', 0),
(000360, '', 000012, 000059, '', 'dummy.png', 0),
(000361, '', 000013, 000059, '', 'dummy.png', 0),
(000362, '', 000014, 000059, '', 'dummy.png', 0),
(000363, '', 000015, 000059, '', 'dummy.png', 0),
(000364, '', 000016, 000059, '', 'dummy.png', 0),
(000365, '', 000017, 000059, '', 'dummy.png', 0),
(000366, '', 000018, 000059, '', 'dummy.png', 0),
(000367, '', 000007, 000060, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000368, '', 000008, 000060, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000369, '', 000009, 000060, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000370, '', 000010, 000060, 'Memerika Posisi konstruksi', 'dummy.png', 1),
(000371, '', 000011, 000060, '', 'dummy.png', 0),
(000372, '', 000012, 000060, '', 'dummy.png', 0),
(000373, '', 000013, 000060, '', 'dummy.png', 0),
(000374, '', 000014, 000060, '', 'dummy.png', 0),
(000375, '', 000015, 000060, '', 'dummy.png', 0),
(000376, '', 000016, 000060, '', 'dummy.png', 0),
(000377, '', 000017, 000060, '', 'dummy.png', 0),
(000378, '', 000018, 000060, '', 'dummy.png', 0),
(000379, '', 000007, 000061, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000380, '', 000008, 000061, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000381, '', 000009, 000061, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000382, '', 000010, 000061, 'Memerika Posisi konstruksi', 'dummy.png', 1),
(000383, '', 000011, 000061, '', 'dummy.png', 0),
(000384, '', 000012, 000061, '', 'dummy.png', 0),
(000385, '', 000013, 000061, '', 'dummy.png', 0),
(000386, '', 000014, 000061, '', 'dummy.png', 0),
(000387, '', 000015, 000061, '', 'dummy.png', 0),
(000388, '', 000016, 000061, '', 'dummy.png', 0),
(000389, '', 000017, 000061, '', 'dummy.png', 0),
(000390, '', 000018, 000061, '', 'dummy.png', 0),
(000391, '', 000007, 000062, 'Memeriksa jumlah dan tanggal penerima', NULL, 2),
(000392, '', 000008, 000062, 'Memeriksa sertifikat dan kondisi material', NULL, 2),
(000393, '', 000009, 000062, 'Memeriksa kekerasan permukaan', NULL, 1),
(000394, '', 000010, 000062, '', NULL, 0),
(000395, '', 000011, 000062, '', NULL, 0),
(000396, '', 000012, 000062, '', NULL, 0),
(000397, '', 000013, 000062, '', NULL, 0),
(000398, '', 000014, 000062, '', NULL, 0),
(000399, '', 000015, 000062, '', NULL, 0),
(000400, '', 000016, 000062, '', NULL, 0),
(000401, '', 000017, 000062, '', NULL, 0),
(000402, '', 000018, 000062, '', NULL, 0),
(000403, '', 000007, 000063, 'Memeriksa jumlah dan tanggal penerima', NULL, 2),
(000404, '', 000008, 000063, 'Memeriksa sertifikat dan kondisi material', NULL, 2),
(000405, '', 000009, 000063, 'Memeriksa kekerasan permukaan', NULL, 1),
(000406, '', 000010, 000063, '', NULL, 0),
(000407, '', 000011, 000063, '', NULL, 0),
(000408, '', 000012, 000063, '', NULL, 0),
(000409, '', 000013, 000063, '', NULL, 0),
(000410, '', 000014, 000063, '', NULL, 0),
(000411, '', 000015, 000063, '', NULL, 0),
(000412, '', 000016, 000063, '', NULL, 0),
(000413, '', 000017, 000063, '', NULL, 0),
(000414, '', 000018, 000063, '', NULL, 0),
(000415, '', 000007, 000064, 'Memeriksa jumlah dan tanggal penerima', NULL, 2),
(000416, '', 000008, 000064, 'Memeriksa sertifikat dan kondisi material', NULL, 2),
(000417, '', 000009, 000064, 'Memeriksa kekerasan permukaan', NULL, 1),
(000418, '', 000010, 000064, '', NULL, 0),
(000419, '', 000011, 000064, '', NULL, 0),
(000420, '', 000012, 000064, '', NULL, 0),
(000421, '', 000013, 000064, '', NULL, 0),
(000422, '', 000014, 000064, '', NULL, 0),
(000423, '', 000015, 000064, '', NULL, 0),
(000424, '', 000016, 000064, '', NULL, 0),
(000425, '', 000017, 000064, '', NULL, 0),
(000426, '', 000018, 000064, '', NULL, 0),
(000427, '', 000007, 000065, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000428, '', 000008, 000065, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000429, '', 000009, 000065, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000430, '', 000010, 000065, 'Memerika Posisi konstruksi', 'dummy.png', 2),
(000431, '', 000011, 000065, 'Memeriksa dimensi dan cacat pemotongan', 'dummy.png', 1),
(000432, '', 000012, 000065, '', 'dummy.png', 0),
(000433, '', 000013, 000065, '', 'dummy.png', 0),
(000434, '', 000014, 000065, '', 'dummy.png', 0),
(000435, '', 000015, 000065, '', 'dummy.png', 0),
(000436, '', 000016, 000065, '', 'dummy.png', 0),
(000437, '', 000017, 000065, '', 'dummy.png', 0),
(000438, '', 000018, 000065, '', 'dummy.png', 0),
(000439, '', 000007, 000066, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000440, '', 000008, 000066, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000441, '', 000009, 000066, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000442, '', 000010, 000066, 'Memerika Posisi konstruksi', 'dummy.png', 2),
(000443, '', 000011, 000066, 'Memeriksa dimensi dan cacat pemotongan', 'dummy.png', 1),
(000444, '', 000012, 000066, '', 'dummy.png', 0),
(000445, '', 000013, 000066, '', 'dummy.png', 0),
(000446, '', 000014, 000066, '', 'dummy.png', 0),
(000447, '', 000015, 000066, '', 'dummy.png', 0),
(000448, '', 000016, 000066, '', 'dummy.png', 0),
(000449, '', 000017, 000066, '', 'dummy.png', 0),
(000450, '', 000018, 000066, '', 'dummy.png', 0),
(000451, '', 000007, 000067, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000452, '', 000008, 000067, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000453, '', 000009, 000067, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000454, '', 000010, 000067, 'Memerika Posisi konstruksi', 'dummy.png', 2),
(000455, '', 000011, 000067, 'Memeriksa dimensi dan cacat pemotongan', 'dummy.png', 1),
(000456, '', 000012, 000067, '', 'dummy.png', 0),
(000457, '', 000013, 000067, '', 'dummy.png', 0),
(000458, '', 000014, 000067, '', 'dummy.png', 0),
(000459, '', 000015, 000067, '', 'dummy.png', 0),
(000460, '', 000016, 000067, '', 'dummy.png', 0),
(000461, '', 000017, 000067, '', 'dummy.png', 0),
(000462, '', 000018, 000067, '', 'dummy.png', 0),
(000463, '', 000007, 000068, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000464, '', 000008, 000068, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000465, '', 000009, 000068, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000466, '', 000010, 000068, 'Memerika Posisi konstruksi', 'dummy.png', 1),
(000467, '', 000011, 000068, '', 'dummy.png', 0),
(000468, '', 000012, 000068, '', 'dummy.png', 0),
(000469, '', 000013, 000068, '', 'dummy.png', 0),
(000470, '', 000014, 000068, '', 'dummy.png', 0),
(000471, '', 000015, 000068, '', 'dummy.png', 0),
(000472, '', 000016, 000068, '', 'dummy.png', 0),
(000473, '', 000017, 000068, '', 'dummy.png', 0),
(000474, '', 000018, 000068, '', 'dummy.png', 0),
(000475, '', 000007, 000069, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000476, '', 000008, 000069, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000477, '', 000009, 000069, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000478, '', 000010, 000069, 'Memerika Posisi konstruksi', 'dummy.png', 1),
(000479, '', 000011, 000069, '', 'dummy.png', 0),
(000480, '', 000012, 000069, '', 'dummy.png', 0),
(000481, '', 000013, 000069, '', 'dummy.png', 0),
(000482, '', 000014, 000069, '', 'dummy.png', 0),
(000483, '', 000015, 000069, '', 'dummy.png', 0),
(000484, '', 000016, 000069, '', 'dummy.png', 0),
(000485, '', 000017, 000069, '', 'dummy.png', 0),
(000486, '', 000018, 000069, '', 'dummy.png', 0),
(000487, '', 000007, 000070, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000488, '', 000008, 000070, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000489, '', 000009, 000070, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000490, '', 000010, 000070, 'Memerika Posisi konstruksi', 'dummy.png', 1),
(000491, '', 000011, 000070, '', 'dummy.png', 0),
(000492, '', 000012, 000070, '', 'dummy.png', 0),
(000493, '', 000013, 000070, '', 'dummy.png', 0),
(000494, '', 000014, 000070, '', 'dummy.png', 0),
(000495, '', 000015, 000070, '', 'dummy.png', 0),
(000496, '', 000016, 000070, '', 'dummy.png', 0),
(000497, '', 000017, 000070, '', 'dummy.png', 0),
(000498, '', 000018, 000070, '', 'dummy.png', 0),
(000499, '', 000007, 000071, 'Memeriksa jumlah dan tanggal penerima', NULL, 2),
(000500, '', 000008, 000071, 'Memeriksa sertifikat dan kondisi material', NULL, 2),
(000501, '', 000009, 000071, 'Memeriksa kekerasan permukaan', NULL, 1),
(000502, '', 000010, 000071, '', NULL, 0),
(000503, '', 000011, 000071, '', NULL, 0),
(000504, '', 000012, 000071, '', NULL, 0),
(000505, '', 000013, 000071, '', NULL, 0),
(000506, '', 000014, 000071, '', NULL, 0),
(000507, '', 000015, 000071, '', NULL, 0),
(000508, '', 000016, 000071, '', NULL, 0),
(000509, '', 000017, 000071, '', NULL, 0),
(000510, '', 000018, 000071, '', NULL, 0),
(000511, '', 000019, 000072, 'Memeriksa jumlah dan tanggal penerima', NULL, 2),
(000512, '', 000020, 000072, 'Memeriksa sertifikat dan kondisi material', NULL, 2),
(000513, '', 000021, 000072, '', NULL, 0),
(000514, '', 000022, 000072, '', NULL, 0),
(000515, '', 000023, 000072, '', NULL, 0),
(000516, '', 000024, 000072, '', NULL, 0),
(000517, '', 000025, 000072, '', NULL, 0),
(000518, '', 000026, 000072, '', NULL, 0),
(000519, '', 000027, 000072, '', NULL, 0),
(000520, '', 000028, 000072, '', NULL, 0),
(000521, '', 000029, 000072, '', NULL, 0),
(000522, '', 000019, 000073, 'Memeriksa jumlah dan tanggal penerima', NULL, 2),
(000523, '', 000020, 000073, 'Memeriksa sertifikat dan kondisi material', NULL, 2),
(000524, '', 000021, 000073, '', NULL, 0),
(000525, '', 000022, 000073, '', NULL, 0),
(000526, '', 000023, 000073, '', NULL, 0),
(000527, '', 000024, 000073, '', NULL, 0),
(000528, '', 000025, 000073, '', NULL, 0),
(000529, '', 000026, 000073, '', NULL, 0),
(000530, '', 000027, 000073, '', NULL, 0),
(000531, '', 000028, 000073, '', NULL, 0),
(000532, '', 000029, 000073, '', NULL, 0),
(000533, '', 000019, 000074, 'Memeriksa jumlah dan tanggal penerima', NULL, 2),
(000534, '', 000020, 000074, 'Memeriksa sertifikat dan kondisi material', NULL, 2),
(000535, '', 000021, 000074, '', NULL, 0),
(000536, '', 000022, 000074, '', NULL, 0),
(000537, '', 000023, 000074, '', NULL, 0),
(000538, '', 000024, 000074, '', NULL, 0),
(000539, '', 000025, 000074, '', NULL, 0),
(000540, '', 000026, 000074, '', NULL, 0),
(000541, '', 000027, 000074, '', NULL, 0),
(000542, '', 000028, 000074, '', NULL, 0),
(000543, '', 000029, 000074, '', NULL, 0),
(000544, '', 000019, 000075, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000545, '', 000020, 000075, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000546, '', 000021, 000075, 'Memeriksa posisi konstruksi', 'dummy.png', 2),
(000547, '', 000022, 000075, '', 'dummy.png', 0),
(000548, '', 000023, 000075, '', 'dummy.png', 0),
(000549, '', 000024, 000075, '', 'dummy.png', 0),
(000550, '', 000025, 000075, '', 'dummy.png', 0),
(000551, '', 000026, 000075, '', 'dummy.png', 0),
(000552, '', 000027, 000075, '', 'dummy.png', 0),
(000553, '', 000028, 000075, '', 'dummy.png', 0),
(000554, '', 000029, 000075, '', 'dummy.png', 0),
(000555, '', 000019, 000076, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000556, '', 000020, 000076, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000557, '', 000021, 000076, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000558, '', 000022, 000076, '', 'dummy.png', 0),
(000559, '', 000023, 000076, '', 'dummy.png', 0),
(000560, '', 000024, 000076, '', 'dummy.png', 0),
(000561, '', 000025, 000076, '', 'dummy.png', 0),
(000562, '', 000026, 000076, '', 'dummy.png', 0),
(000563, '', 000027, 000076, '', 'dummy.png', 0),
(000564, '', 000028, 000076, '', 'dummy.png', 0),
(000565, '', 000029, 000076, '', 'dummy.png', 0),
(000566, '', 000019, 000077, 'Memeriksa jumlah dan tanggal penerima', NULL, 2),
(000567, '', 000020, 000077, 'Memeriksa sertifikat dan kondisi material', NULL, 2),
(000568, '', 000021, 000077, '', NULL, 0),
(000569, '', 000022, 000077, '', NULL, 0),
(000570, '', 000023, 000077, '', NULL, 0),
(000571, '', 000024, 000077, '', NULL, 0),
(000572, '', 000025, 000077, '', NULL, 0),
(000573, '', 000026, 000077, '', NULL, 0),
(000574, '', 000027, 000077, '', NULL, 0),
(000575, '', 000028, 000077, '', NULL, 0),
(000576, '', 000029, 000077, '', NULL, 0),
(000577, '', 000019, 000078, 'Memeriksa jumlah dan tanggal penerima', NULL, 2),
(000578, '', 000020, 000078, 'Memeriksa sertifikat dan kondisi material', NULL, 2),
(000579, '', 000021, 000078, '', NULL, 0),
(000580, '', 000022, 000078, '', NULL, 0),
(000581, '', 000023, 000078, '', NULL, 0),
(000582, '', 000024, 000078, '', NULL, 0),
(000583, '', 000025, 000078, '', NULL, 0),
(000584, '', 000026, 000078, '', NULL, 0),
(000585, '', 000027, 000078, '', NULL, 0),
(000586, '', 000028, 000078, '', NULL, 0),
(000587, '', 000029, 000078, '', NULL, 0),
(000588, '', 000007, 000079, 'Memeriksa jumlah dan tanggal penerima', NULL, 2),
(000589, '', 000008, 000079, 'Memeriksa sertifikat dan kondisi material', NULL, 2),
(000590, '', 000009, 000079, '', NULL, 0),
(000591, '', 000010, 000079, '', NULL, 0),
(000592, '', 000011, 000079, '', NULL, 0),
(000593, '', 000012, 000079, '', NULL, 0),
(000594, '', 000013, 000079, '', NULL, 0),
(000595, '', 000014, 000079, '', NULL, 0),
(000596, '', 000015, 000079, '', NULL, 0),
(000597, '', 000016, 000079, '', NULL, 0),
(000598, '', 000017, 000079, '', NULL, 0),
(000599, '', 000018, 000079, '', NULL, 0),
(000600, '', 000007, 000080, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000601, '', 000008, 000080, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000602, '', 000009, 000080, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000603, '', 000010, 000080, '', 'dummy.png', 0),
(000604, '', 000011, 000080, '', 'dummy.png', 0),
(000605, '', 000012, 000080, '', 'dummy.png', 0),
(000606, '', 000013, 000080, '', 'dummy.png', 0),
(000607, '', 000014, 000080, '', 'dummy.png', 0),
(000608, '', 000015, 000080, '', 'dummy.png', 0),
(000609, '', 000016, 000080, '', 'dummy.png', 0),
(000610, '', 000017, 000080, '', 'dummy.png', 0),
(000611, '', 000018, 000080, '', 'dummy.png', 0),
(000612, '', 000007, 000081, 'Memeriksa jumlah dan tanggal penerima', NULL, 2),
(000613, '', 000008, 000081, 'Memeriksa sertifikat dan kondisi material', NULL, 2),
(000614, '', 000009, 000081, '', NULL, 0),
(000615, '', 000010, 000081, '', NULL, 0),
(000616, '', 000011, 000081, '', NULL, 0),
(000617, '', 000012, 000081, '', NULL, 0),
(000618, '', 000013, 000081, '', NULL, 0),
(000619, '', 000014, 000081, '', NULL, 0),
(000620, '', 000015, 000081, '', NULL, 0),
(000621, '', 000016, 000081, '', NULL, 0),
(000622, '', 000017, 000081, '', NULL, 0),
(000623, '', 000018, 000081, '', NULL, 0),
(000624, '', 000007, 000082, 'Memeriksa jumlah dan tanggal penerima', NULL, 2),
(000625, '', 000008, 000082, 'Memeriksa sertifikat dan kondisi material', NULL, 2),
(000626, '', 000009, 000082, '', NULL, 0),
(000627, '', 000010, 000082, '', NULL, 0),
(000628, '', 000011, 000082, '', NULL, 0),
(000629, '', 000012, 000082, '', NULL, 0),
(000630, '', 000013, 000082, '', NULL, 0),
(000631, '', 000014, 000082, '', NULL, 0),
(000632, '', 000015, 000082, '', NULL, 0),
(000633, '', 000016, 000082, '', NULL, 0),
(000634, '', 000017, 000082, '', NULL, 0),
(000635, '', 000018, 000082, '', NULL, 0),
(000636, '', 000019, 000083, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000637, '', 000020, 000083, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000638, '', 000021, 000083, 'Memeriksa posisi konstruksi', 'dummy.png', 2),
(000639, '', 000022, 000083, 'Memerika dimensi dan cacat pemotongan', 'dummy.png', 2),
(000640, '', 000023, 000083, '', 'dummy.png', 0),
(000641, '', 000024, 000083, '', 'dummy.png', 0),
(000642, '', 000025, 000083, '', 'dummy.png', 0),
(000643, '', 000026, 000083, '', 'dummy.png', 0),
(000644, '', 000027, 000083, '', 'dummy.png', 0),
(000645, '', 000028, 000083, '', 'dummy.png', 0),
(000646, '', 000029, 000083, '', 'dummy.png', 0),
(000647, '', 000019, 000084, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000648, '', 000020, 000084, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000649, '', 000021, 000084, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000650, '', 000022, 000084, 'Memerika dimensi dan cacat pemotongan', 'dummy.png', 2),
(000651, '', 000023, 000084, '', 'dummy.png', 0),
(000652, '', 000024, 000084, '', 'dummy.png', 0),
(000653, '', 000025, 000084, '', 'dummy.png', 0),
(000654, '', 000026, 000084, '', 'dummy.png', 0),
(000655, '', 000027, 000084, '', 'dummy.png', 0),
(000656, '', 000028, 000084, '', 'dummy.png', 0),
(000657, '', 000029, 000084, '', 'dummy.png', 0),
(000658, '', 000007, 000085, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000659, '', 000008, 000085, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000660, '', 000009, 000085, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000661, '', 000010, 000085, '', 'dummy.png', 0),
(000662, '', 000011, 000085, '', 'dummy.png', 0),
(000663, '', 000012, 000085, '', 'dummy.png', 0),
(000664, '', 000013, 000085, '', 'dummy.png', 0),
(000665, '', 000014, 000085, '', 'dummy.png', 0),
(000666, '', 000015, 000085, '', 'dummy.png', 0),
(000667, '', 000016, 000085, '', 'dummy.png', 0),
(000668, '', 000017, 000085, '', 'dummy.png', 0),
(000669, '', 000018, 000085, '', 'dummy.png', 0),
(000670, '', 000007, 000086, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000671, '', 000008, 000086, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000672, '', 000009, 000086, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000673, '', 000010, 000086, '', 'dummy.png', 0),
(000674, '', 000011, 000086, '', 'dummy.png', 0),
(000675, '', 000012, 000086, '', 'dummy.png', 0),
(000676, '', 000013, 000086, '', 'dummy.png', 0),
(000677, '', 000014, 000086, '', 'dummy.png', 0),
(000678, '', 000015, 000086, '', 'dummy.png', 0),
(000679, '', 000016, 000086, '', 'dummy.png', 0),
(000680, '', 000017, 000086, '', 'dummy.png', 0),
(000681, '', 000018, 000086, '', 'dummy.png', 0),
(000682, '', 000007, 000087, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000683, '', 000008, 000087, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000684, '', 000009, 000087, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000685, '', 000010, 000087, 'Memerika Posisi konstruksi', 'dummy.png', 2),
(000686, '', 000011, 000087, 'Memeriksa dimensi dan cacat pemotongan', 'dummy.png', 2),
(000687, '', 000012, 000087, 'Memeriksa kesesuaian bentuk dengan desain gambar', 'dummy.png', 2),
(000688, '', 000013, 000087, '', 'dummy.png', 0),
(000689, '', 000014, 000087, '', 'dummy.png', 0),
(000690, '', 000015, 000087, '', 'dummy.png', 0),
(000691, '', 000016, 000087, '', 'dummy.png', 0),
(000692, '', 000017, 000087, '', 'dummy.png', 0),
(000693, '', 000018, 000087, '', 'dummy.png', 0),
(000694, '', 000007, 000088, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000695, '', 000008, 000088, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000696, '', 000009, 000088, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000697, '', 000010, 000088, 'Memerika Posisi konstruksi', 'dummy.png', 2),
(000698, '', 000011, 000088, 'Memeriksa dimensi dan cacat pemotongan', 'dummy.png', 2),
(000699, '', 000012, 000088, 'Memeriksa kesesuaian bentuk dengan desain gambar', 'dummy.png', 2),
(000700, '', 000013, 000088, '', 'dummy.png', 0),
(000701, '', 000014, 000088, '', 'dummy.png', 0),
(000702, '', 000015, 000088, '', 'dummy.png', 0),
(000703, '', 000016, 000088, '', 'dummy.png', 0),
(000704, '', 000017, 000088, '', 'dummy.png', 0),
(000705, '', 000018, 000088, '', 'dummy.png', 0),
(000706, '', 000007, 000089, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000707, '', 000008, 000089, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000708, '', 000009, 000089, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000709, '', 000010, 000089, 'Memerika Posisi konstruksi', 'dummy.png', 2),
(000710, '', 000011, 000089, 'Memeriksa dimensi dan cacat pemotongan', 'dummy.png', 2),
(000711, '', 000012, 000089, 'Memeriksa kesesuaian bentuk dengan desain gambar', 'dummy.png', 2),
(000712, '', 000013, 000089, '', 'dummy.png', 0),
(000713, '', 000014, 000089, '', 'dummy.png', 0),
(000714, '', 000015, 000089, '', 'dummy.png', 0),
(000715, '', 000016, 000089, '', 'dummy.png', 0),
(000716, '', 000017, 000089, '', 'dummy.png', 0),
(000717, '', 000018, 000089, '', 'dummy.png', 0),
(000718, '', 000007, 000090, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000719, '', 000008, 000090, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000720, '', 000009, 000090, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000721, '', 000010, 000090, 'Memerika Posisi konstruksi', 'dummy.png', 2),
(000722, '', 000011, 000090, 'Memeriksa dimensi dan cacat pemotongan', 'dummy.png', 2),
(000723, '', 000012, 000090, '', 'dummy.png', 0),
(000724, '', 000013, 000090, '', 'dummy.png', 0),
(000725, '', 000014, 000090, '', 'dummy.png', 0),
(000726, '', 000015, 000090, '', 'dummy.png', 0),
(000727, '', 000016, 000090, '', 'dummy.png', 0),
(000728, '', 000017, 000090, '', 'dummy.png', 0),
(000729, '', 000018, 000090, '', 'dummy.png', 0),
(000730, '', 000007, 000091, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000731, '', 000008, 000091, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000732, '', 000009, 000091, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000733, '', 000010, 000091, 'Memerika Posisi konstruksi', 'dummy.png', 2),
(000734, '', 000011, 000091, 'Memeriksa dimensi dan cacat pemotongan', 'dummy.png', 2),
(000735, '', 000012, 000091, '', 'dummy.png', 0),
(000736, '', 000013, 000091, '', 'dummy.png', 0),
(000737, '', 000014, 000091, '', 'dummy.png', 0),
(000738, '', 000015, 000091, '', 'dummy.png', 0),
(000739, '', 000016, 000091, '', 'dummy.png', 0),
(000740, '', 000017, 000091, '', 'dummy.png', 0),
(000741, '', 000018, 000091, '', 'dummy.png', 0),
(000742, '', 000007, 000092, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000743, '', 000008, 000092, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000744, '', 000009, 000092, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000745, '', 000010, 000092, 'Memerika Posisi konstruksi', 'dummy.png', 2),
(000746, '', 000011, 000092, '', 'dummy.png', 0),
(000747, '', 000012, 000092, '', 'dummy.png', 0),
(000748, '', 000013, 000092, '', 'dummy.png', 0),
(000749, '', 000014, 000092, '', 'dummy.png', 0),
(000750, '', 000015, 000092, '', 'dummy.png', 0),
(000751, '', 000016, 000092, '', 'dummy.png', 0),
(000752, '', 000017, 000092, '', 'dummy.png', 0),
(000753, '', 000018, 000092, '', 'dummy.png', 0),
(000754, '', 000007, 000093, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000755, '', 000008, 000093, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000756, '', 000009, 000093, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000757, '', 000010, 000093, 'Memerika Posisi konstruksi', 'dummy.png', 2),
(000758, '', 000011, 000093, '', 'dummy.png', 0),
(000759, '', 000012, 000093, '', 'dummy.png', 0),
(000760, '', 000013, 000093, '', 'dummy.png', 0),
(000761, '', 000014, 000093, '', 'dummy.png', 0),
(000762, '', 000015, 000093, '', 'dummy.png', 0),
(000763, '', 000016, 000093, '', 'dummy.png', 0),
(000764, '', 000017, 000093, '', 'dummy.png', 0),
(000765, '', 000018, 000093, '', 'dummy.png', 0),
(000766, '', 000007, 000094, 'Memeriksa jumlah dan tanggal penerima', 'dummy.png', 2),
(000767, '', 000008, 000094, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000768, '', 000009, 000094, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000769, '', 000010, 000094, 'Memerika Posisi konstruksi', 'dummy.png', 2),
(000770, '', 000011, 000094, '', 'dummy.png', 0),
(000771, '', 000012, 000094, '', 'dummy.png', 0),
(000772, '', 000013, 000094, '', 'dummy.png', 0),
(000773, '', 000014, 000094, '', 'dummy.png', 0),
(000774, '', 000015, 000094, '', 'dummy.png', 0),
(000775, '', 000016, 000094, '', 'dummy.png', 0),
(000776, '', 000017, 000094, '', 'dummy.png', 0),
(000777, '', 000018, 000094, '', 'dummy.png', 0),
(000778, '', 000007, 000095, 'Memeriksa jumlah dan tanggal penerima', 'windows12.png', 2),
(000779, '', 000008, 000095, 'Memeriksa sertifikat dan kondisi material', 'dummy.png', 2),
(000780, '', 000009, 000095, 'Memeriksa kekerasan permukaan', 'dummy.png', 2),
(000781, '', 000010, 000095, 'Memerika Posisi konstruksi', 'dummy.png', 2),
(000782, '', 000011, 000095, 'Memeriksa dimensi dan cacat pemotongan', 'dummy.png', 2),
(000783, '', 000012, 000095, 'Memeriksa kesesuaian bentuk dengan desain gambar', 'dummy.png', 2),
(000784, '', 000013, 000095, '', 'dummy.png', 0),
(000785, '', 000014, 000095, '', 'dummy.png', 0),
(000786, '', 000015, 000095, '', 'dummy.png', 0),
(000787, '', 000016, 000095, '', 'dummy.png', 0),
(000788, '', 000017, 000095, '', 'dummy.png', 0),
(000789, '', 000018, 000095, '', 'dummy.png', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_laporan_mingguan`
--

CREATE TABLE IF NOT EXISTS `data_laporan_mingguan` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_laporan_mingguan` int(6) unsigned zerofill NOT NULL,
  `id_aktivitas` int(6) unsigned zerofill NOT NULL,
  `status` int(3) NOT NULL,
  `progres_aktual` varchar(10) NOT NULL,
  `deskripsi` varchar(50) NOT NULL,
  `link_foto` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `galangan`
--

CREATE TABLE IF NOT EXISTS `galangan` (
  `id_galangan` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` varchar(50) NOT NULL,
  `alamat_perusahaan` varchar(50) NOT NULL,
  `pimpinan_proyek` varchar(50) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id_galangan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `galangan`
--

INSERT INTO `galangan` (`id_galangan`, `nama_perusahaan`, `alamat_perusahaan`, `pimpinan_proyek`, `telp`, `email`) VALUES
(000001, 'PT. DUMAS TANJUNG PERAK SHIPYARD', 'SURABAYA', 'Wimpy', '09909090', 'Galangan@galangan.com'),
(000002, 'PT. Adiluhung Saranasegara Indonesia', 'JL. Raya Ujung Piring, Kec. Bangkalan, 69118', 'Sugiyarto', '(031) 3097039', 'adiluhung@gmail.com'),
(000003, 'PT. Lamongan Marine Industry (LMI)', 'Lamongan', 'Fikri', '09909090', 'lmi@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gambar_rancangan`
--

CREATE TABLE IF NOT EXISTS `gambar_rancangan` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_kapal` int(6) unsigned zerofill NOT NULL,
  `link` varchar(70) NOT NULL,
  `judul` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `gambar_rancangan`
--

INSERT INTO `gambar_rancangan` (`id`, `id_kapal`, `link`, `judul`) VALUES
(000001, 000001, 'kapal1.jpg', 'kapal1.jpg'),
(000002, 000001, 'kapal1.jpg', 'kapal1.jpg'),
(000004, 000006, 'kapal2.jpg', 'kapal2.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `group`
--

CREATE TABLE IF NOT EXISTS `group` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_workgroup_kapal` int(6) unsigned zerofill NOT NULL,
  `group` varchar(40) NOT NULL,
  `bobot_workgroup` varchar(15) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

--
-- Dumping data untuk tabel `group`
--

INSERT INTO `group` (`id`, `id_workgroup_kapal`, `group`, `bobot_workgroup`, `kuantitas`, `satuan`) VALUES
(000007, 000002, 'BLOK 100', '5', 20, 'TON'),
(000008, 000002, 'BLOK 101', '5', 20, 'TON'),
(000009, 000002, 'BLOK 102', '5', 20, 'TON'),
(000010, 000002, 'BLOK 103', '5', 20, 'TON'),
(000011, 000002, 'BLOK 104', '5', 20, 'TON'),
(000012, 000002, 'BLOK 105', '5', 20, 'TON'),
(000013, 000002, 'BLOK 106', '5', 20, 'TON'),
(000014, 000002, 'BLOK 107', '5', 20, 'TON'),
(000015, 000002, 'BLOK 108', '5', 20, 'TON'),
(000016, 000002, 'BLOK 109', '5', 20, 'TON'),
(000017, 000002, 'BLOK 110', '5', 20, 'TON'),
(000018, 000002, 'BLOK 111', '5', 20, 'TON'),
(000019, 000002, 'BLOK 112', '5', 20, 'TON'),
(000020, 000002, 'BLOK 113', '5', 20, 'TON'),
(000021, 000002, 'BLOK 114', '5', 20, 'TON'),
(000022, 000002, 'BLOK 115', '5', 20, 'TON'),
(000023, 000002, 'BLOK 116', '5', 20, 'TON'),
(000024, 000002, 'BLOK 117', '5', 20, 'TON'),
(000025, 000002, 'BLOK 118', '5', 20, 'TON'),
(000026, 000002, 'BLOK 119', '5', 20, 'TON'),
(000027, 000003, 'Pintu', '4', 1, 'SHIPSHET'),
(000028, 000003, 'Seal Trunk', '4', 1, 'SHIPSHET'),
(000029, 000003, 'Jendela Aluminium Persegi', '4', 1, 'SHIPSHET'),
(000030, 000003, 'Jendela Aluminium Bulat', '4', 1, 'SHIPSHET'),
(000031, 000003, 'Exhaust Fan 10" WC/KM', '4', 1, 'SHIPSHET'),
(000032, 000003, 'Double Bollard', '4', 12, 'BH'),
(000033, 000003, 'Lantai Deck ABK/PMP Dilapisi Deck  Kompo', '4', 1, 'SHIPSHET'),
(000034, 000003, 'Dinding Isolasi  ( Klass B-15 uncomb )', '4', 1, 'SHIPSHET'),
(000035, 000003, 'Langit-langit ', '4', 1, 'SHIPSHET'),
(000036, 000003, 'House Pipe & Belimouth', '4', 1, 'SHIPSHET'),
(000037, 000003, 'WC/KM dengan Pelengkap', '4', 1, 'SHIPSHET'),
(000038, 000003, 'Cerobong Asap', '4', 1, 'SHIPSHET'),
(000039, 000003, 'Life Boat ', '4', 2, 'SHIPSHET'),
(000040, 000003, 'Alat Lift / Release untuk Life Boat ( de', '4', 2, 'SHIPSHET'),
(000041, 000003, 'Perlengkapan Tidur Penumpang Ekonomi', '4', 1, 'SHIPSHET'),
(000042, 000003, 'Tangga Samping', '4', 1, 'SHIPSHET'),
(000043, 000003, 'Perlengkapan Tidur Kamar ABK', '4', 1, 'SHIPSHET'),
(000044, 000003, 'Tangga dengan Railing', '4', 1, 'SHIPSHET'),
(000045, 000003, 'Plat Nama dan Tanda-tanda', '4', 1, 'SHIPSHET'),
(000046, 000003, 'Mast Tiang Compass', '4', 1, 'SHIPSHET'),
(000047, 000003, 'Natural Ventilasi Ruang PNP', '4', 1, 'SET'),
(000048, 000003, 'Natural Ventilasi Ruang Cargo', '4', 1, 'SET'),
(000049, 000003, 'Mekanik Ventilasi Ruang PNP', '4', 1, 'SET'),
(000050, 000003, 'AC Split 1 PK', '4', 1, 'SET'),
(000051, 000003, 'Televisi uk. 29"', '', 1, 'SET'),
(000052, 000004, 'MAIN ENGINE', '10', 2, 'UNIT'),
(000053, 000004, 'Poros, Propeller & St. Lube', '10', 2, 'UNIT'),
(000054, 000004, 'Kemudi & Perlengkapannya', '10', 2, 'UNIT'),
(000055, 000004, 'Ventilator Alam Kamar Mesin', '10', 2, 'UNIT'),
(000056, 000004, 'Ventilator Mekanis Kamar Mesin', '10', 2, 'UNIT'),
(000057, 000004, 'Steering Gear dengan Perlengkapannya', '10', 2, 'UNIT'),
(000058, 000004, 'Engine telegraph', '10', 2, 'UNIT'),
(000059, 000004, 'Windlass', '10', 2, 'UNIT'),
(000060, 000004, 'Unit Peralatan Hydraulic', '10', 2, 'UNIT'),
(000061, 000004, 'Cargo Crane', '10', 2, 'UNIT'),
(000062, 000006, 'Kabel Fitting', '10', 1, 'UNIT'),
(000063, 000006, 'Panel Penerangan', '10', 1, 'UNIT'),
(000064, 000006, 'MSB', '10', 1, 'UNIT'),
(000065, 000006, 'ESB', '10', 1, 'UNIT'),
(000066, 000006, 'Panel Navigasi & Komunikasi', '10', 1, 'UNIT'),
(000067, 000006, 'Panel Stater', '10', 1, 'UNIT'),
(000068, 000006, 'Panel Darurat', '10', 1, 'UNIT'),
(000069, 000006, 'Flood Light Haluan', '10', 1, 'UNIT'),
(000070, 000006, 'Lampu Navigasi', '10', 1, 'UNIT'),
(000071, 000006, 'Genset Pelabuhan', '10', 1, 'UNIT');

-- --------------------------------------------------------

--
-- Struktur dari tabel `laporan_alat`
--

CREATE TABLE IF NOT EXISTS `laporan_alat` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama_alat` varchar(6) NOT NULL,
  `id_laporan_harian` int(6) unsigned zerofill NOT NULL,
  `jumlah_alat` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=105 ;

--
-- Dumping data untuk tabel `laporan_alat`
--

INSERT INTO `laporan_alat` (`id`, `nama_alat`, `id_laporan_harian`, `jumlah_alat`) VALUES
(000053, 'Forkli', 000053, 1),
(000054, '', 000054, 0),
(000055, '', 000055, 0),
(000056, '', 000056, 0),
(000057, '', 000057, 0),
(000058, '', 000058, 0),
(000059, '', 000059, 0),
(000060, '', 000060, 0),
(000061, '', 000061, 0),
(000062, '', 000062, 0),
(000063, '', 000063, 0),
(000064, '', 000064, 0),
(000065, '', 000065, 0),
(000066, '', 000066, 0),
(000067, '', 000067, 0),
(000068, '', 000068, 0),
(000069, '', 000069, 0),
(000070, '', 000070, 0),
(000071, '', 000071, 0),
(000072, '', 000072, 0),
(000073, '', 000073, 0),
(000074, '', 000074, 0),
(000075, '', 000075, 0),
(000076, '', 000076, 0),
(000077, '', 000077, 0),
(000078, '', 000078, 0),
(000079, '', 000079, 0),
(000080, '', 000080, 0),
(000081, '', 000081, 0),
(000082, '', 000082, 0),
(000083, '', 000083, 0),
(000084, '', 000084, 0),
(000085, '', 000085, 0),
(000086, '', 000086, 0),
(000087, '', 000087, 0),
(000088, '', 000088, 0),
(000089, '', 000089, 0),
(000090, '', 000090, 0),
(000091, '', 000091, 0),
(000092, '', 000092, 0),
(000093, '', 000093, 0),
(000094, '', 000094, 0),
(000095, '', 000095, 0),
(000096, '', 000095, 0),
(000097, '', 000095, 0),
(000098, '', 000095, 0),
(000099, '', 000095, 0),
(000100, '', 000095, 0),
(000101, '', 000095, 0),
(000102, '', 000095, 0),
(000103, '', 000095, 0),
(000104, '', 000095, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `laporan_harian`
--

CREATE TABLE IF NOT EXISTS `laporan_harian` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_group` int(6) unsigned zerofill NOT NULL,
  `tgl_laporan` date NOT NULL,
  `cuaca_pagi` varchar(30) NOT NULL,
  `cuaca_siang` varchar(30) NOT NULL,
  `cuaca_sore` varchar(30) NOT NULL,
  `catatan_galangan` varchar(100) NOT NULL,
  `catatan_pm` varchar(100) NOT NULL,
  `tanggapan_galangan` varchar(100) NOT NULL,
  `tanggapan_pm` varchar(100) NOT NULL,
  `verifikasi_owner` int(3) NOT NULL,
  `verifikasi_pm` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=96 ;

--
-- Dumping data untuk tabel `laporan_harian`
--

INSERT INTO `laporan_harian` (`id`, `id_group`, `tgl_laporan`, `cuaca_pagi`, `cuaca_siang`, `cuaca_sore`, `catatan_galangan`, `catatan_pm`, `tanggapan_galangan`, `tanggapan_pm`, `verifikasi_owner`, `verifikasi_pm`) VALUES
(000053, 000007, '2015-12-28', 'Cerah', 'Cerah', 'Cerah', '', '', '', '', 0, 0),
(000054, 000008, '2015-12-28', 'Cerah', 'Cerah', 'Cerah', '', '', '', '', 0, 0),
(000055, 000009, '2015-12-28', '', '', '', '', '', '', '', 0, 0),
(000056, 000007, '2015-12-29', '', '', '', '', '', '', '', 0, 0),
(000057, 000008, '2015-12-29', '', '', '', '', '', '', '', 0, 0),
(000058, 000009, '2015-12-29', '', '', '', '', '', '', '', 0, 0),
(000059, 000007, '2015-12-30', 'Cerah', 'Cerah', 'Cerah', '', '', '', '', 0, 0),
(000060, 000008, '2015-12-30', '', '', '', '', '', '', '', 0, 0),
(000061, 000009, '2015-12-30', '', '', '', '', '', '', '', 0, 0),
(000062, 000010, '2015-12-30', '', '', '', '', '', '', '', 0, 0),
(000063, 000011, '2015-12-30', '', '', '', '', '', '', '', 0, 0),
(000064, 000012, '2015-12-30', '', '', '', '', '', '', '', 0, 0),
(000065, 000007, '2015-12-31', '', '', '', '', '', '', '', 0, 0),
(000066, 000008, '2015-12-31', '', '', '', '', '', '', '', 0, 0),
(000067, 000009, '2015-12-31', '', '', '', '', '', '', '', 0, 0),
(000068, 000010, '2015-12-31', '', '', '', '', '', '', '', 0, 0),
(000069, 000011, '2015-12-31', '', '', '', '', '', '', '', 0, 0),
(000070, 000012, '2015-12-31', '', '', '', '', '', '', '', 0, 0),
(000071, 000013, '2015-12-31', '', '', '', '', '', '', '', 0, 0),
(000072, 000027, '2016-01-01', '', '', '', '', '', '', '', 0, 0),
(000073, 000028, '2016-01-01', '', '', '', '', '', '', '', 0, 0),
(000074, 000029, '2016-01-01', '', '', '', '', '', '', '', 0, 0),
(000075, 000027, '2016-01-01', '', '', '', '', '', '', '', 0, 0),
(000076, 000028, '2016-01-01', '', '', '', '', '', '', '', 0, 0),
(000077, 000030, '2016-01-02', '', '', '', '', '', '', '', 0, 0),
(000078, 000031, '2016-01-02', '', '', '', '', '', '', '', 0, 0),
(000079, 000014, '2016-01-03', '', '', '', '', '', '', '', 0, 0),
(000080, 000014, '2016-01-03', '', '', '', '', '', '', '', 0, 0),
(000081, 000016, '2016-01-04', '', '', '', '', '', '', '', 0, 0),
(000082, 000015, '2016-01-04', '', '', '', '', '', '', '', 0, 0),
(000083, 000027, '2016-01-05', '', '', '', '', '', '', '', 0, 0),
(000084, 000028, '2016-01-05', '', '', '', '', '', '', '', 0, 0),
(000085, 000016, '2016-01-06', '', '', '', '', '', '', '', 0, 0),
(000086, 000015, '2016-01-06', '', '', '', '', '', '', '', 0, 0),
(000087, 000007, '2016-01-07', '', '', '', '', '', '', '', 0, 0),
(000088, 000008, '2016-01-07', '', '', '', '', '', '', '', 0, 0),
(000089, 000009, '2016-01-08', '', '', '', '', '', '', '', 0, 0),
(000090, 000010, '2016-01-08', '', '', '', '', '', '', '', 0, 0),
(000091, 000011, '2016-01-09', '', '', '', '', '', '', '', 0, 0),
(000092, 000013, '2016-01-09', '', '', '', '', '', '', '', 0, 0),
(000093, 000014, '2016-01-10', '', '', '', '', '', '', '', 0, 0),
(000094, 000015, '2016-01-10', '', '', '', '', '', '', '', 0, 0),
(000095, 000007, '2016-01-11', '', '', '', '', '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `laporan_mingguan`
--

CREATE TABLE IF NOT EXISTS `laporan_mingguan` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_laporan_mingguan` int(6) unsigned zerofill NOT NULL,
  `tgl_input` date NOT NULL,
  `periode` varchar(40) NOT NULL,
  `mid_end` int(3) NOT NULL COMMENT '0=mid, 1=end',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `laporan_tenaga_kerja`
--

CREATE TABLE IF NOT EXISTS `laporan_tenaga_kerja` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_laporan_harian` int(6) unsigned zerofill NOT NULL,
  `tenaga_kerja` varchar(50) NOT NULL,
  `jumlah` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=155 ;

--
-- Dumping data untuk tabel `laporan_tenaga_kerja`
--

INSERT INTO `laporan_tenaga_kerja` (`id`, `id_laporan_harian`, `tenaga_kerja`, `jumlah`) VALUES
(000057, 000053, 'Project Manager', 1),
(000058, 000053, 'Site Produksi', 1),
(000059, 000053, 'labor', 20),
(000060, 000054, '', 0),
(000061, 000054, '', 0),
(000062, 000054, '', 0),
(000063, 000055, '', 0),
(000064, 000056, '', 0),
(000065, 000057, '', 0),
(000066, 000058, '', 0),
(000067, 000059, '', 0),
(000068, 000059, '', 0),
(000069, 000059, '', 0),
(000070, 000060, '', 0),
(000071, 000061, '', 0),
(000072, 000062, '', 0),
(000073, 000063, '', 0),
(000074, 000064, '', 0),
(000075, 000065, '', 0),
(000076, 000066, '', 0),
(000077, 000067, '', 0),
(000078, 000068, '', 0),
(000079, 000069, '', 0),
(000080, 000070, '', 0),
(000081, 000071, '', 0),
(000082, 000072, '', 0),
(000083, 000073, '', 0),
(000084, 000074, '', 0),
(000085, 000075, '', 0),
(000086, 000076, '', 0),
(000087, 000077, '', 0),
(000088, 000078, '', 0),
(000089, 000079, '', 0),
(000090, 000080, '', 0),
(000091, 000081, '', 0),
(000092, 000082, '', 0),
(000093, 000083, '', 0),
(000094, 000084, '', 0),
(000095, 000085, '', 0),
(000096, 000086, '', 0),
(000097, 000087, '', 0),
(000098, 000088, '', 0),
(000099, 000089, '', 0),
(000100, 000090, '', 0),
(000101, 000091, '', 0),
(000102, 000092, '', 0),
(000103, 000093, '', 0),
(000104, 000094, '', 0),
(000105, 000095, '', 0),
(000106, 000095, '', 0),
(000107, 000095, '', 0),
(000108, 000095, '', 0),
(000109, 000095, '', 0),
(000110, 000095, '', 0),
(000111, 000095, '', 0),
(000112, 000095, '', 0),
(000113, 000095, '', 0),
(000114, 000095, '', 0),
(000115, 000095, '', 0),
(000116, 000095, '', 0),
(000117, 000095, '', 0),
(000118, 000095, '', 0),
(000119, 000095, '', 0),
(000120, 000095, '', 0),
(000121, 000095, '', 0),
(000122, 000095, '', 0),
(000123, 000095, '', 0),
(000124, 000095, '', 0),
(000125, 000095, '', 0),
(000126, 000095, '', 0),
(000127, 000095, '', 0),
(000128, 000095, '', 0),
(000129, 000095, '', 0),
(000130, 000095, '', 0),
(000131, 000095, '', 0),
(000132, 000095, '', 0),
(000133, 000095, '', 0),
(000134, 000095, '', 0),
(000135, 000095, '', 0),
(000136, 000095, '', 0),
(000137, 000095, '', 0),
(000138, 000095, '', 0),
(000139, 000095, '', 0),
(000140, 000095, '', 0),
(000141, 000095, '', 0),
(000142, 000095, '', 0),
(000143, 000095, '', 0),
(000144, 000095, '', 0),
(000145, 000095, '', 0),
(000146, 000095, '', 0),
(000147, 000095, '', 0),
(000148, 000095, '', 0),
(000149, 000095, '', 0),
(000150, 000095, '', 0),
(000151, 000095, '', 0),
(000152, 000095, '', 0),
(000153, 000095, '', 0),
(000154, 000095, '', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `owner`
--

CREATE TABLE IF NOT EXISTS `owner` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `alamat_perusahaan` varchar(50) DEFAULT NULL,
  `penanggung_jawab` varchar(50) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `link_foto` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `owner`
--

INSERT INTO `owner` (`id`, `nama_perusahaan`, `alamat_perusahaan`, `penanggung_jawab`, `no_telp`, `link_foto`, `email`, `password`) VALUES
(000001, 'DIRJEN PERHUBUNGAN LAUT', 'JAKARTA', 'Sugiarto', '97987987979', NULL, 'owner@owner.com', 'owner');

-- --------------------------------------------------------

--
-- Struktur dari tabel `owner_surveyor`
--

CREATE TABLE IF NOT EXISTS `owner_surveyor` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `alamat_perusahaan` varchar(50) DEFAULT NULL,
  `penanggung_jawab` varchar(50) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `link_foto` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `owner_surveyor`
--

INSERT INTO `owner_surveyor` (`id`, `nama_perusahaan`, `alamat_perusahaan`, `penanggung_jawab`, `no_telp`, `link_foto`, `email`, `password`) VALUES
(000001, 'PT OS Berkarya', 'Surabaya', NULL, '085728640629', NULL, 'os@os.com', 'os');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembuatan_kapal`
--

CREATE TABLE IF NOT EXISTS `pembuatan_kapal` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama_proyek` varchar(50) NOT NULL,
  `pemilik` varchar(50) NOT NULL,
  `kontraktor` varchar(50) NOT NULL,
  `konsultan` varchar(50) NOT NULL,
  `jenis_kapal` varchar(50) NOT NULL,
  `lpp` int(11) NOT NULL,
  `loa` int(11) NOT NULL,
  `lebar` int(11) NOT NULL,
  `tinggi` int(11) NOT NULL,
  `sarat_air` int(11) NOT NULL,
  `kecepatan` int(11) NOT NULL,
  `lama_pengerjaan` int(11) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `status_pengerjaan` int(11) NOT NULL,
  `id_os` int(6) unsigned zerofill NOT NULL,
  `id_galangan` int(6) unsigned zerofill NOT NULL,
  `id_owner` int(6) unsigned zerofill NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data untuk tabel `pembuatan_kapal`
--

INSERT INTO `pembuatan_kapal` (`id`, `nama_proyek`, `pemilik`, `kontraktor`, `konsultan`, `jenis_kapal`, `lpp`, `loa`, `lebar`, `tinggi`, `sarat_air`, `kecepatan`, `lama_pengerjaan`, `tanggal_mulai`, `status_pengerjaan`, `id_os`, `id_galangan`, `id_owner`) VALUES
(000006, 'Kapal Perintis 750GT paket A', 'Direktorat Jendral Perhubungan laut', '', '', 'Penumpang', 53, 55, 12, 5, 3, 2, 6, '0000-00-00', 0, 000001, 000001, 000001),
(000007, 'Kapal Perintis 750GT paket B', 'Direktorat Jendral Perhubungan laut', '', '', 'Penumpang', 53, 55, 12, 5, 3, 2, 6, '0000-00-00', 0, 000001, 000002, 000001),
(000008, 'Kapal Perintis 750GT paket C', 'Direktorat Jendral Perhubungan laut', '', '', 'Penumpang', 53, 55, 12, 5, 3, 2, 6, '0000-00-00', 0, 000001, 000003, 000001),
(000009, 'Kapal Perintis 750GT paket D', 'Direktorat Jendral Perhubungan laut', '', '', 'Penumpang', 53, 55, 12, 5, 3, 2, 6, '0000-00-00', 0, 000001, 000001, 000001),
(000012, 'Kapal Perintis 1200GT paket A', 'Direktorat Jendral Perhubungan laut', '', '', 'Penumpang', 57, 63, 12, 4, 3, 12, 5, '0000-00-00', 0, 000001, 000002, 000001),
(000013, 'Kapal Perintis 1200GT paket B', 'Direktorat Jendral Perhubungan laut', '', '', 'Penumpang', 57, 63, 12, 4, 3, 12, 5, '0000-00-00', 0, 000001, 000003, 000001),
(000014, 'Kapal Perintis 1200GT paket C', 'Direktorat Jendral Perhubungan laut', '', '', 'Penumpang', 57, 63, 12, 4, 3, 12, 5, '0000-00-00', 0, 000001, 000001, 000001),
(000015, 'Kapal Perintis 1200GT paket D', 'Direktorat Jendral Perhubungan laut', '', '', 'Penumpang', 57, 63, 12, 4, 3, 12, 5, '0000-00-00', 0, 000001, 000002, 000001);

-- --------------------------------------------------------

--
-- Struktur dari tabel `progres`
--

CREATE TABLE IF NOT EXISTS `progres` (
  `id` int(8) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_kapal` int(6) unsigned zerofill NOT NULL,
  `periode` varchar(25) DEFAULT NULL,
  `mid_end` int(3) DEFAULT NULL,
  `progres` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=159 ;

--
-- Dumping data untuk tabel `progres`
--

INSERT INTO `progres` (`id`, `id_kapal`, `periode`, `mid_end`, `progres`) VALUES
(00000001, 000001, 'January 2015', 0, '0'),
(00000002, 000001, 'January 2015', 1, '0'),
(00000003, 000001, 'February 2015', 0, '0'),
(00000004, 000001, 'February 2015', 1, '0'),
(00000005, 000001, 'March 2015', 0, '0'),
(00000006, 000001, 'March 2015', 1, '0'),
(00000007, 000001, 'April 2015', 0, '0'),
(00000008, 000001, 'April 2015', 1, '0'),
(00000009, 000001, 'May 2015', 0, '0'),
(00000010, 000001, 'May 2015', 1, '80'),
(00000011, 000001, 'June 2015', 0, '90'),
(00000012, 000001, 'June 2015', 1, '100'),
(00000013, 000002, NULL, NULL, '0'),
(00000014, 000002, NULL, NULL, '0'),
(00000015, 000002, NULL, NULL, '0'),
(00000016, 000002, NULL, NULL, '0'),
(00000017, 000002, NULL, NULL, '0'),
(00000018, 000002, NULL, NULL, '0'),
(00000019, 000003, NULL, NULL, '0'),
(00000020, 000003, NULL, NULL, '0'),
(00000021, 000003, NULL, NULL, '0'),
(00000022, 000003, NULL, NULL, '0'),
(00000023, 000004, NULL, NULL, '0'),
(00000024, 000004, NULL, NULL, '0'),
(00000025, 000004, NULL, NULL, '0'),
(00000026, 000004, NULL, NULL, '0'),
(00000027, 000004, NULL, NULL, '0'),
(00000028, 000004, NULL, NULL, '0'),
(00000029, 000004, NULL, NULL, '0'),
(00000030, 000004, NULL, NULL, '0'),
(00000031, 000004, NULL, NULL, '0'),
(00000032, 000004, NULL, NULL, '0'),
(00000033, 000004, NULL, NULL, '0'),
(00000034, 000004, NULL, NULL, '0'),
(00000035, 000005, NULL, NULL, '0'),
(00000036, 000005, NULL, NULL, '0'),
(00000037, 000005, NULL, NULL, '0'),
(00000038, 000005, NULL, NULL, '0'),
(00000039, 000005, NULL, NULL, '0'),
(00000040, 000005, NULL, NULL, '0'),
(00000041, 000005, NULL, NULL, '0'),
(00000042, 000005, NULL, NULL, '0'),
(00000043, 000005, NULL, NULL, '0'),
(00000044, 000005, NULL, NULL, '0'),
(00000045, 000005, NULL, NULL, '0'),
(00000046, 000005, NULL, NULL, '0'),
(00000047, 000006, 'January 2015', 0, '3'),
(00000048, 000006, 'January 2015', 1, '0'),
(00000049, 000006, NULL, NULL, '0'),
(00000050, 000006, NULL, NULL, '0'),
(00000051, 000006, NULL, NULL, '0'),
(00000052, 000006, NULL, NULL, '0'),
(00000053, 000006, NULL, NULL, '0'),
(00000054, 000006, NULL, NULL, '0'),
(00000055, 000006, NULL, NULL, '0'),
(00000056, 000006, NULL, NULL, '0'),
(00000057, 000006, NULL, NULL, '0'),
(00000058, 000006, NULL, NULL, '0'),
(00000059, 000007, NULL, NULL, '0'),
(00000060, 000007, NULL, NULL, '0'),
(00000061, 000007, NULL, NULL, '0'),
(00000062, 000007, NULL, NULL, '0'),
(00000063, 000007, NULL, NULL, '0'),
(00000064, 000007, NULL, NULL, '0'),
(00000065, 000007, NULL, NULL, '0'),
(00000066, 000007, NULL, NULL, '0'),
(00000067, 000007, NULL, NULL, '0'),
(00000068, 000007, NULL, NULL, '0'),
(00000069, 000007, NULL, NULL, '0'),
(00000070, 000007, NULL, NULL, '0'),
(00000071, 000008, NULL, NULL, '0'),
(00000072, 000008, NULL, NULL, '0'),
(00000073, 000008, NULL, NULL, '0'),
(00000074, 000008, NULL, NULL, '0'),
(00000075, 000008, NULL, NULL, '0'),
(00000076, 000008, NULL, NULL, '0'),
(00000077, 000008, NULL, NULL, '0'),
(00000078, 000008, NULL, NULL, '0'),
(00000079, 000008, NULL, NULL, '0'),
(00000080, 000008, NULL, NULL, '0'),
(00000081, 000008, NULL, NULL, '0'),
(00000082, 000008, NULL, NULL, '0'),
(00000083, 000009, NULL, NULL, '0'),
(00000084, 000009, NULL, NULL, '0'),
(00000085, 000009, NULL, NULL, '0'),
(00000086, 000009, NULL, NULL, '0'),
(00000087, 000009, NULL, NULL, '0'),
(00000088, 000009, NULL, NULL, '0'),
(00000089, 000009, NULL, NULL, '0'),
(00000090, 000009, NULL, NULL, '0'),
(00000091, 000009, NULL, NULL, '0'),
(00000092, 000009, NULL, NULL, '0'),
(00000093, 000009, NULL, NULL, '0'),
(00000094, 000009, NULL, NULL, '0'),
(00000095, 000010, NULL, NULL, '0'),
(00000096, 000010, NULL, NULL, '0'),
(00000097, 000010, NULL, NULL, '0'),
(00000098, 000010, NULL, NULL, '0'),
(00000099, 000010, NULL, NULL, '0'),
(00000100, 000010, NULL, NULL, '0'),
(00000101, 000010, NULL, NULL, '0'),
(00000102, 000010, NULL, NULL, '0'),
(00000103, 000010, NULL, NULL, '0'),
(00000104, 000010, NULL, NULL, '0'),
(00000105, 000010, NULL, NULL, '0'),
(00000106, 000010, NULL, NULL, '0'),
(00000107, 000011, NULL, NULL, '0'),
(00000108, 000011, NULL, NULL, '0'),
(00000109, 000011, NULL, NULL, '0'),
(00000110, 000011, NULL, NULL, '0'),
(00000111, 000011, NULL, NULL, '0'),
(00000112, 000011, NULL, NULL, '0'),
(00000113, 000011, NULL, NULL, '0'),
(00000114, 000011, NULL, NULL, '0'),
(00000115, 000011, NULL, NULL, '0'),
(00000116, 000011, NULL, NULL, '0'),
(00000117, 000011, NULL, NULL, '0'),
(00000118, 000011, NULL, NULL, '0'),
(00000119, 000012, NULL, NULL, '0'),
(00000120, 000012, NULL, NULL, '0'),
(00000121, 000012, NULL, NULL, '0'),
(00000122, 000012, NULL, NULL, '0'),
(00000123, 000012, NULL, NULL, '0'),
(00000124, 000012, NULL, NULL, '0'),
(00000125, 000012, NULL, NULL, '0'),
(00000126, 000012, NULL, NULL, '0'),
(00000127, 000012, NULL, NULL, '0'),
(00000128, 000012, NULL, NULL, '0'),
(00000129, 000013, NULL, NULL, '0'),
(00000130, 000013, NULL, NULL, '0'),
(00000131, 000013, NULL, NULL, '0'),
(00000132, 000013, NULL, NULL, '0'),
(00000133, 000013, NULL, NULL, '0'),
(00000134, 000013, NULL, NULL, '0'),
(00000135, 000013, NULL, NULL, '0'),
(00000136, 000013, NULL, NULL, '0'),
(00000137, 000013, NULL, NULL, '0'),
(00000138, 000013, NULL, NULL, '0'),
(00000139, 000014, NULL, NULL, '0'),
(00000140, 000014, NULL, NULL, '0'),
(00000141, 000014, NULL, NULL, '0'),
(00000142, 000014, NULL, NULL, '0'),
(00000143, 000014, NULL, NULL, '0'),
(00000144, 000014, NULL, NULL, '0'),
(00000145, 000014, NULL, NULL, '0'),
(00000146, 000014, NULL, NULL, '0'),
(00000147, 000014, NULL, NULL, '0'),
(00000148, 000014, NULL, NULL, '0'),
(00000149, 000015, NULL, NULL, '0'),
(00000150, 000015, NULL, NULL, '0'),
(00000151, 000015, NULL, NULL, '0'),
(00000152, 000015, NULL, NULL, '0'),
(00000153, 000015, NULL, NULL, '0'),
(00000154, 000015, NULL, NULL, '0'),
(00000155, 000015, NULL, NULL, '0'),
(00000156, 000015, NULL, NULL, '0'),
(00000157, 000015, NULL, NULL, '0'),
(00000158, 000015, NULL, NULL, '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `project_manager`
--

CREATE TABLE IF NOT EXISTS `project_manager` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` varchar(50) NOT NULL,
  `alamat_perusahaan` varchar(50) NOT NULL,
  `penanggung_jawab` varchar(50) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `link_foto` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tenaga_kerja`
--

CREATE TABLE IF NOT EXISTS `tenaga_kerja` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `keahlian` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `tenaga_kerja`
--

INSERT INTO `tenaga_kerja` (`id`, `keahlian`) VALUES
(000001, 'Tenaga 1'),
(000002, 'Tenaga 2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `workgroup`
--

CREATE TABLE IF NOT EXISTS `workgroup` (
  `id_workgroup` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `bobot` int(11) NOT NULL,
  `nama_pekerjaan` varchar(40) NOT NULL,
  PRIMARY KEY (`id_workgroup`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `workgroup`
--

INSERT INTO `workgroup` (`id_workgroup`, `bobot`, `nama_pekerjaan`) VALUES
(000002, 20, 'Hull Construction'),
(000003, 15, 'Hull Outfitting'),
(000004, 15, 'Machinery Outfitting'),
(000005, 20, 'Pipping System'),
(000006, 10, 'Electrical Outfitting'),
(000007, 10, 'Painting & Coating'),
(000008, 10, 'Inventory');

-- --------------------------------------------------------

--
-- Struktur dari tabel `workgroup_kapal`
--

CREATE TABLE IF NOT EXISTS `workgroup_kapal` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_kapal` int(6) unsigned zerofill NOT NULL,
  `id_workgroup` int(6) unsigned zerofill NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=121 ;

--
-- Dumping data untuk tabel `workgroup_kapal`
--

INSERT INTO `workgroup_kapal` (`id`, `id_kapal`, `id_workgroup`) VALUES
(000002, 000001, 000002),
(000003, 000001, 000003),
(000004, 000001, 000004),
(000005, 000001, 000005),
(000006, 000001, 000006),
(000007, 000001, 000007),
(000008, 000001, 000008),
(000010, 000002, 000002),
(000011, 000002, 000003),
(000012, 000002, 000004),
(000013, 000002, 000005),
(000014, 000002, 000006),
(000015, 000002, 000007),
(000016, 000002, 000008),
(000018, 000003, 000002),
(000019, 000003, 000003),
(000020, 000003, 000004),
(000021, 000003, 000005),
(000022, 000003, 000006),
(000023, 000003, 000007),
(000024, 000003, 000008),
(000026, 000004, 000002),
(000027, 000004, 000003),
(000028, 000004, 000004),
(000029, 000004, 000005),
(000030, 000004, 000006),
(000031, 000004, 000007),
(000032, 000004, 000008),
(000034, 000005, 000002),
(000035, 000005, 000003),
(000036, 000005, 000004),
(000037, 000005, 000005),
(000038, 000005, 000006),
(000039, 000005, 000007),
(000040, 000005, 000008),
(000042, 000006, 000002),
(000043, 000006, 000003),
(000044, 000006, 000004),
(000045, 000006, 000005),
(000046, 000006, 000006),
(000047, 000006, 000007),
(000048, 000006, 000008),
(000050, 000007, 000002),
(000051, 000007, 000003),
(000052, 000007, 000004),
(000053, 000007, 000005),
(000054, 000007, 000006),
(000055, 000007, 000007),
(000056, 000007, 000008),
(000058, 000008, 000002),
(000059, 000008, 000003),
(000060, 000008, 000004),
(000061, 000008, 000005),
(000062, 000008, 000006),
(000063, 000008, 000007),
(000064, 000008, 000008),
(000066, 000009, 000002),
(000067, 000009, 000003),
(000068, 000009, 000004),
(000069, 000009, 000005),
(000070, 000009, 000006),
(000071, 000009, 000007),
(000072, 000009, 000008),
(000074, 000010, 000002),
(000075, 000010, 000003),
(000076, 000010, 000004),
(000077, 000010, 000005),
(000078, 000010, 000006),
(000079, 000010, 000007),
(000080, 000010, 000008),
(000082, 000011, 000002),
(000083, 000011, 000003),
(000084, 000011, 000004),
(000085, 000011, 000005),
(000086, 000011, 000006),
(000087, 000011, 000007),
(000088, 000011, 000008),
(000090, 000012, 000002),
(000091, 000012, 000003),
(000092, 000012, 000004),
(000093, 000012, 000005),
(000094, 000012, 000006),
(000095, 000012, 000007),
(000096, 000012, 000008),
(000098, 000013, 000002),
(000099, 000013, 000003),
(000100, 000013, 000004),
(000101, 000013, 000005),
(000102, 000013, 000006),
(000103, 000013, 000007),
(000104, 000013, 000008),
(000106, 000014, 000002),
(000107, 000014, 000003),
(000108, 000014, 000004),
(000109, 000014, 000005),
(000110, 000014, 000006),
(000111, 000014, 000007),
(000112, 000014, 000008),
(000114, 000015, 000002),
(000115, 000015, 000003),
(000116, 000015, 000004),
(000117, 000015, 000005),
(000118, 000015, 000006),
(000119, 000015, 000007),
(000120, 000015, 000008);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
