<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class M_manajemen extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
  
  
    public function getPekerja()
    {
        $query = $this->db->get('jenis_pekerja');
        return $query->result();
    }

    public function getAlat()
    {
        $query = $this->db->get('jenis_alat');
        return $query->result();
    }

    public function getPM()
    {
        $query = $this->db->get('project_manager');
        return $query->result();
    }

    public function getOS()
    {
        $query = $this->db->get('owner_surveyor');
        return $query->result();
    }

    public function getDetailPM($id_pm)
    {
        $query = $this->db->query("SELECT p.id_pm, p.id_galangan, g.nama_galangan, p.nama, p.email, p.telepon
                                    FROM project_manager p, galangan g
                                    WHERE p.id_galangan = g.id_galangan");
        return $query->row();
    }

    public function getDetailOwner($email)
    {
        $query = $this->db->query("SELECT *
                                    FROM owner
                                    WHERE email = '$email'");
        return $query->row();
    }

    public function insertData($tabel, $data)
    {
        $this->db->insert($tabel, $data);
        return $this->db->affected_rows();
    }

    public function getDetailKapal($id_pembuatan)
    {
        $query = $this->db->get_where('pembuatan_kapal', array('id' => $id_pembuatan));
        return $query->row();
    }

    public function getKapalOwner($id)
    {
        $query = $this->db->query("SELECT k.id, k.nama_proyek, k.id_galangan, g.nama_perusahaan, k.id_os, os.nama_perusahaan as nama_os, k.status_pengerjaan
                                    FROM pembuatan_kapal k, galangan g, owner_surveyor os
                                    WHERE k.id_owner = '$id' AND k.id_galangan=g.id_galangan AND k.id_os = os.id");
         //print_r($query->result());
        return $query->result();
    }

    public function getKapalPM($id)
    {
        $query = $this->db->query("SELECT k.id_pembuatan, k.nama_kapal, k.id_galangan, g.nama_galangan, k.id_pm, p.nama as nama_pm, k.id_os, os.nama as nama_os, k.status_pengerjaan
                                    FROM pembuatan_kapal k, galangan g, owner_surveyor os
                                    WHERE k.id_pm= '$id' AND k.id_galangan=g.id_galangan AND k.id_pm = p.id_pm AND k.id_os = os.id_os");

        return $query->result();
    }

    public function getDetailBaseline($id_pembuatan)
    {
        $query = $this->db->get_where('baseline', array('id_kapal' => $id_pembuatan));
        return $query->result();
    }

    public function saveTanggapan($id_laporan, $tanggapan)
    {
        $data = array(
               'tanggapan_pm' => $tanggapan,
            );
        $this->db->update('laporan_harian', $data, array('id' => $id_laporan));
    }
    public function validasiLaporan($tgl_laporan)
    {
        $data = array(
               'verifikasi_owner' => 1,
            );
        $this->db->update('laporan_harian', $data, array('tgl_laporan' => $tgl_laporan));
    }

    public function getprojekAktual($id_kapal, $id_owner)
    {
        //getID laporan dulu -___-
        $this->db->select('progres.periode, progres.mid_end, progres.progres');
        $this->db->from('progres');
        $this->db->join('pembuatan_kapal', 'pembuatan_kapal.id = progres.id_kapal', 'left');
        $this->db->where(array('pembuatan_kapal.id_owner' => $id_owner, 'progres.id_kapal' => $id_kapal)); 
        $query = $this->db->get();
       
        return $query->result();
    }

    public function getprojekRencana($id_kapal, $id_owner)
    {
        //getID laporan dulu -___-
        $this->db->select('b.periode, b.mid_end, b.target');
        $this->db->from('baseline b ');
        $this->db->join('pembuatan_kapal', 'pembuatan_kapal.id = b.id_kapal', 'left');
        $this->db->where(array('pembuatan_kapal.id_owner' => $id_owner, 'b.id_kapal' => $id_kapal)); 
        $query = $this->db->get();
       
        return $query->result();
    }


    public function getidKapal($id_owner)
    {
        //getID laporan dulu -___-
        $this->db->select('id, nama_proyek');
        $this->db->from('pembuatan_kapal');
        $this->db->where(array('pembuatan_kapal.id_owner' => $id_owner)); 
        $query = $this->db->get(); 
       
        return $query->result();
    }

    public function getDataChart($id)
    {
        $this->db->select_max('progres.progres');
        $this->db->from('progres');
        // $this->db->join('pembuatan_kapal', 'pembuatan_kapal.id = progres.id_kapal', 'right');
        $this->db->where(array('progres.id_kapal' => $id)); 
        //$this->db->group_by('progres.id_kapal');

        $query = $this->db->get();
        return $query->row();
    }

    public function getStatusLaporan($id_kapal, $tgl_laporan)
    {
        //getID laporan dulu -___-
        $this->db->select_max('lp.id');
        $this->db->from('laporan_harian as lp');
        $this->db->join('group', 'lp.id_group = group.id', 'left');
        $this->db->join('workgroup_kapal', 'workgroup_kapal.id = group.id_workgroup_kapal', 'left');
        $this->db->where(array('workgroup_kapal.id_kapal' => $id_kapal, 'lp.tgl_laporan' => $tgl_laporan)); 
        $id_laporan = $this->db->get()->row()->id;

        $this->db->select('lp.verifikasi_owner');
        $this->db->from('laporan_harian lp ');
        $this->db->where(array('id' => $id_laporan)); 
        $query = $this->db->get();
        return $query->row()->verifikasi_owner;
    }

    public function getListKapal($id_owner)
    {
        //getID laporan dulu -___-
        $this->db->select('nama_proyek');
        $this->db->from('pembuatan_kapal');
        $this->db->where(array('pembuatan_kapal.id_owner' => $id_owner)); 
        $query = $this->db->get(); 
       
        return $query->result();
    }

    public function getListGalangan($id_owner)
    {
        $this->db->select('g.id_galangan, g.nama_perusahaan');
        $this->db->from('galangan g');
        $this->db->join('pembuatan_kapal', 'g.id_galangan = pembuatan_kapal.id_galangan', 'left');
        $this->db->where(array('pembuatan_kapal.id_owner' => $id_owner)); 

        $query = $this->db->get();
        return $query->result();
    }

    public function getidKapalbyIdGalangan($id_galangan)
    {
        //getID laporan dulu -___-
        $this->db->select('id, nama_proyek');
        $this->db->from('pembuatan_kapal');
        $this->db->where(array('pembuatan_kapal.id_galangan' => $id_galangan)); 
        $query = $this->db->get(); 
       
        return $query->result();
    }
          
}  

?>