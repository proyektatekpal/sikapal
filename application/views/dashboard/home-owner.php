      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Selamat Datang
            <small>Owner</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
      <section class="content">
          <div class="modal fade bs-example-modal-lg" id="Rangkuman" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Rangkuman Projek Kapal</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="box box-primary">
                      <div class="box-header with-border">
                        <i class="fa fa-bar-chart-o"></i>
                        <h3 class="box-title">Progres kapal %</h3>                        
                      </div>

                      <div class="box-body">
                        <div id="bar-chart" style=" width:800px; height: 300px;"></div>
                      </div>
                      
                    </div>
                  </div>
                </div>
                
              </div>
              <!-- <div class="col-xs-12"> -->
                  <div class="box box-primary box-solid">
                      <div class="box-header">
                          <h3 class="box-title">Keterangan kapal</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                          <table id="list-kapal" class="table table-bordered table-striped data-table">
                            <thead>
                              <tr>
                                <th>No. </th>
                                <th>Nama Kapal</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td class="col-md-1">1</td>
                                <td>Kapal Perintis 750GT paket A</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">2</td>
                                <td>Kapal Perintis 750GT paket B</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">3</td>
                                <td>Kapal Perintis 750GT paket C</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">4</td>
                                <td>Kapal Perintis 750GT paket D</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">5</td>
                                <td>Kapal Perintis 1200GT paket A</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">6</td>
                                <td>Kapal Perintis 1200GT paket B</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">7</td>
                                <td>Kapal Perintis 1200GT paket C</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">8</td>
                                <td>Kapal Perintis 1200GT paket D</td>
                              </tr>
                            </tbody>
                            
                          </table>
                      </div><!-- /.box-body -->
                  <!-- </div> -->
                </div>
            </div>
          </div>

          <div class="modal fade bs-example-modal-lg" id="Rangkuman2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Rangkuman Projek Kapal</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="box box-primary">
                      <div class="box-header with-border">
                        <i class="fa fa-bar-chart-o"></i>
                        <h3 class="box-title">Progres kapal %</h3>                        
                      </div>

                      <div class="box-body">
                        <div id="bar-chart2" style=" width:800px; height: 300px;"></div>
                      </div>
                      
                    </div>
                  </div>
                </div>
                
              </div>
              <!-- <div class="col-xs-12"> -->
                  <div class="box box-primary box-solid">
                      <div class="box-header">
                          <h3 class="box-title">Keterangan kapal</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                          <table id="list-kapal" class="table table-bordered table-striped data-table">
                            <thead>
                              <tr>
                                <th>No. </th>
                                <th>Nama Kapal</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td class="col-md-1">1</td>
                                <td>Kapal Perintis 750GT paket A</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">2</td>
                                <td>Kapal Perintis 750GT paket B</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">3</td>
                                <td>Kapal Perintis 750GT paket C</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">4</td>
                                <td>Kapal Perintis 750GT paket D</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">5</td>
                                <td>Kapal Perintis 1200GT paket A</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">6</td>
                                <td>Kapal Perintis 1200GT paket B</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">7</td>
                                <td>Kapal Perintis 1200GT paket C</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">8</td>
                                <td>Kapal Perintis 1200GT paket D</td>
                              </tr>
                            </tbody>
                            
                          </table>
                      </div><!-- /.box-body -->
                  <!-- </div> -->
                </div>
            </div>
          </div>

          <div class="modal fade bs-example-modal-lg" id="Rangkuman0" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Rangkuman Projek Kapal</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="box box-primary">
                      <div class="box-header with-border">
                        <i class="fa fa-bar-chart-o"></i>
                        <h3 class="box-title">Progres kapal %</h3>                        
                      </div>

                      <div class="box-body">
                        <div id="bar-chart0" style=" width:800px; height: 300px;"></div>
                      </div>
                      
                    </div>
                  </div>
                </div>
                
              </div>
              <!-- <div class="col-xs-12"> -->
                  <div class="box box-primary box-solid">
                      <div class="box-header">
                          <h3 class="box-title">Keterangan kapal</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                          <table id="list-kapal" class="table table-bordered table-striped data-table">
                            <thead>
                              <tr>
                                <th>No. </th>
                                <th>Nama Kapal</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td class="col-md-1">1</td>
                                <td>Kapal Perintis 750GT paket A</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">2</td>
                                <td>Kapal Perintis 750GT paket B</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">3</td>
                                <td>Kapal Perintis 750GT paket C</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">4</td>
                                <td>Kapal Perintis 750GT paket D</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">5</td>
                                <td>Kapal Perintis 1200GT paket A</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">6</td>
                                <td>Kapal Perintis 1200GT paket B</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">7</td>
                                <td>Kapal Perintis 1200GT paket C</td>
                              </tr>
                              <tr>
                                <td class="col-md-1">8</td>
                                <td>Kapal Perintis 1200GT paket D</td>
                              </tr>
                            </tbody>
                            
                          </table>
                      </div><!-- /.box-body -->
                  <!-- </div> -->
                </div>
            </div>
          </div>

          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-xs-12">

              <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa fa-bar-chart-o"></i>
                  <h3 class="box-title">Progres kapal Total %</h3>                        
                </div>
                <div class="box-body">
                  <div id="bar-total" style=" height: 300px;"></div>
                </div>
              </div>

              <div class="box box-primary box-solid">
                <div class="box-header">
                  <h3 class="box-title">Keterangan kapal</h3>
                </div>
                <div class="box-body">
                  <div class="col-xs-12">
                    <table id="list-kapal" class="table table-bordered table-striped data-table">
                      <thead>
                        <tr>
                          <th>No. </th>
                          <th>Nama Galangan</th>
                          <th>Nama Kapal</th>
                          <th>Nama Owner Surveyor</th>
                          <th>Progres (%)</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                        foreach ($rekap_kapal as $num => $rekap) {
                            echo '<tr>';
                            echo '<td class="col-md-1">'.($num+1).'</td>';
                            echo '<td class="col-md-3">'.$rekap->perusahaan_galangan.'</td>';
                            echo '<td class="col-md-3">'.$rekap->nama_proyek.'</td>';
                            echo '<td class="col-md-3">'.$rekap->penanggung_jawab.'</td>';
                            echo '<td class="col-md-2">'.$progres_kapal[$rekap->id]->progres.'</td>';
                            echo '</tr>';
                              }
                        ?>
                      </tbody>
                
                    </table>
                  </div>        
                </div>
              </div>

              <div class="box box-primary box-solid">
                <div class="box-header">
                  <h3 class="box-title">Progres Workgroup (%)</h3>
                </div>
                <div class="box-body">
                  <div class="col-xs-12">
                    <table id="list-kapal" class="table table-bordered table-striped data-table">
                      <thead>
                        <tr>
                          <th class="text-center" style="width:4%">No. </th>
                          <th class="text-center" style="width:20%">Workgroup</th>
                          <?php 
                          foreach ($rekap_kapal as $key => $kpl) {
                              echo '<th class="text-center">'.($key+1).'</th>';
                          }
                          ?>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                        foreach ($workgroup as $no => $wg) {
                            echo '<tr>';
                            echo '<td class="text-center">'.($no+1).'</td>';
                            echo '<td>'.$wg->nama_pekerjaan.'</td>';
                            foreach ($rekap_progres as $num => $prgs) {
                                echo '<td class="text-center">'.$rekap_progres[$num][$wg->id_workgroup].'</td>';
                            }
                            echo '</tr>';
                        }
                        ?>
                      </tbody>
                
                    </table>
                  </div>        
                </div>
              </div>
                      
            </div>

          </div>
            
      </section><!-- right col -->
      </div><!-- /.row (main row) -->
<!-- 
<script src="../../plugins/flot/jquery.flot.min.js"></script>
<script src="../../plugins/flot/jquery.flot.resize.min.js"></script>
<script src="../../plugins/flot/jquery.flot.pie.min.js"></script>
<script src="../../plugins/flot/jquery.flot.categories.min.js"></script> -->

<script src="<?php echo base_url()?>assets/theme/plugins/flot/jquery.flot.min.js"></script>
<script src="<?php echo base_url()?>assets/theme/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url()?>assets/theme/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="<?php echo base_url()?>assets/theme/plugins/flot/jquery.flot.categories.min.js"></script>


<script type="text/javascript">
 $(document).ready(function(){
    /*
     * BAR CHART
     * ---------
     */
     $.ajax({
                        url: '<?php echo base_url()?>ManajemenKapalOwner/GetDataChart/', 
                        dataType: 'json',
                        type: 'POST',
                        //async: false,
                        success: function(data) 
                        { 
                          var adata = [];
                          var num = 0;
                          $.each(data, function(i,item)
                          {
                            // var x = 0;
                            //var y = item. progres;
                            var temp = [];
                            temp.push(num+1, item.progres);
                            adata.push(temp);
                            num++; 
                          });

                          // var bar_data = bar_data.push(data);

                          //alert(data.join(' '));

                          // $mergedData = array();
                          // $mergedData[] =  array('data' => $data, 'color' => '#6bcadb');

                          //alert("l");
                          // var bar_data = {
                          //     data :  [["1", 3], ["2", 0], ["3", 0], ["4", 0], ["5", 0], ["6", 0], ["7", 0], ["8", 0]],
                          //     color: "#3c8dbc"
                          //   };
                            $.plot("#bar-total", [adata], {
                              grid: {
                                borderWidth: 1,
                                borderColor: "#f3f3f3",
                                tickColor: "#f3f3f3"
                              },
                              series: {
                                bars: {
                                  show: true,
                                  barWidth: 0.5,
                                  align: "center"
                                }
                              },
                              xaxis: {
                                mode: "categories",
                                tickLength: 0
                              }
                            });
                        }
    });

    // function onOutboundReceived(series) {
    //     // var bar_data = 
    //     // {
    //     //   data: data_kapal,
    //     //   color: "#3c8dbc"
    //     // };
    //     alert("series");
    //     $.plot("#bar-chart", [series], {
    //       grid: {
    //         borderWidth: 1,
    //         borderColor: "#f3f3f3",
    //         tickColor: "#f3f3f3"
    //       },
    //       series: {
    //         bars: {
    //           show: true,
    //           barWidth: 0.5,
    //           align: "center"
    //         }
    //       },
    //       xaxis: {
    //         mode: "categories",
    //         tickLength: 0
    //       }
    //     });
        
    // }

// var bar_data = {
//           data: [["1", 3], ["2", 0], ["3", 0], ["4", 0], ["5", 0], ["6", 0], ["7", 0], ["8", 0]],
//           color: "#3c8dbc"
//         };
//         $.plot("#bar-total", [bar_data], {
//           grid: {
//             borderWidth: 1,
//             borderColor: "#f3f3f3",
//             tickColor: "#f3f3f3"
//           },
//           series: {
//             bars: {
//               show: true,
//               barWidth: 0.5,
//               align: "center"
//             }
//           },
//           xaxis: {
//             mode: "categories",
//             tickLength: 0
//           }
//         });
      
// var bar_data = {
//           data: [["1", 2.2], ["2", 0], ["3", 0], ["4", 0], ["5", 0], ["6", 0], ["7", 0], ["8", 0]],
//           color: "#3c8dbc"
//         };
//         $.plot("#bar-chart", [bar_data], {
//           grid: {
//             borderWidth: 1,
//             borderColor: "#f3f3f3",
//             tickColor: "#f3f3f3"
//           },
//           series: {
//             bars: {
//               show: true,
//               barWidth: 0.5,
//               align: "center"
//             }
//           },
//           xaxis: {
//             mode: "categories",
//             tickLength: 0
//           }
//         });

// var bar_data2 = {
//           data: [["1", 0.8], ["2", 0], ["3", 0], ["4", 0], ["5", 0], ["6", 0], ["7", 0], ["8", 0]],
//           color: "#3c8dbc"
//         };
//         $.plot("#bar-chart2", [bar_data2], {
//           grid: {
//             borderWidth: 1,
//             borderColor: "#f3f3f3",
//             tickColor: "#f3f3f3"
//           },
//           series: {
//             bars: {
//               show: true,
//               barWidth: 0.5,
//               align: "center"
//             }
//           },
//           xaxis: {
//             mode: "categories",
//             tickLength: 0
//           }
//         });

// var bar_data0 = {
//           data: [["1", 0], ["2", 0], ["3", 0], ["4", 0], ["5", 0], ["6", 0], ["7", 0], ["8", 0]],
//           color: "#3c8dbc"
//         };
//         $.plot("#bar-chart0", [bar_data0], {
//           grid: {
//             borderWidth: 1,
//             borderColor: "#f3f3f3",
//             tickColor: "#f3f3f3"
//           },
//           series: {
//             bars: {
//               show: true,
//               barWidth: 0.5,
//               align: "center"
//             }
//           },
//           xaxis: {
//             mode: "categories",
//             tickLength: 0
//           }
//         });

      $("#ok_button").click(function()
      {  
          var e = document.getElementById("galangan_1");
          var galangan_1_selected = e.options[e.selectedIndex].value;

          var e = document.getElementById("galangan_2");
          var galangan_2_selected = e.options[e.selectedIndex].value;
          
          $.ajax({
              url: '<?php echo base_url()?>ManajemenKapalOwner/GetDataProgres/', 
              dataType: 'json',
              type: 'POST',
              data: {"id_galangan":galangan_1_selected},

              success: function(data) {
                var num = 1;
                $.each(data, function(i,item)
                {
                  if (item.id != 'empty') 
                  {
                    var kolom='';
                    kolom += '<tr>';
                    kolom += '<td>' + num +'</td>';
                    kolom += '<td>' + item.nama_proyek + '</td>';
                    kolom += '<td>' + item.progres +'% </td>';              
                    // kolom += '<td><a  class="btn bg-navy update" data-url="<?= base_url()?>" data-bobot="'+ item.bobot +'" data-nama_pekerjaan="'+ item.aktifitas +'" data-id="'+item.id+'" data-toggle="modal" data-target="#UpdateData">Edit</a><a style="margin-left:10px;" class="btn bg-navy hapus" data-url="<?= base_url()?>" data-id="'+item.id+'" data-toggle="modal" data-target="#DelConf">Hapus</a></td>';
                    kolom += '</tr>';
                    num = num + 1;
                    $("#data_activity1").append(kolom);
                  } 

                    else {
                    }
                });                
              }
          });

          $.ajax({
              url: '<?php echo base_url()?>ManajemenKapalOwner/GetDataProgres/', 
              dataType: 'json',
              type: 'POST',
              data: {"id_galangan":galangan_2_selected},

              success: function(data) {
                var num = 1;
                $.each(data, function(i,item)
                {
                  if (item.id != 'empty') 
                  {
                    var kolom='';
                    kolom += '<tr>';
                    kolom += '<td>' + num +'</td>';
                    kolom += '<td>' + item.nama_proyek + '</td>';
                    kolom += '<td>' + item.progres +'% </td>';              
                    // kolom += '<td><a  class="btn bg-navy update" data-url="<?= base_url()?>" data-bobot="'+ item.bobot +'" data-nama_pekerjaan="'+ item.aktifitas +'" data-id="'+item.id+'" data-toggle="modal" data-target="#UpdateData">Edit</a><a style="margin-left:10px;" class="btn bg-navy hapus" data-url="<?= base_url()?>" data-id="'+item.id+'" data-toggle="modal" data-target="#DelConf">Hapus</a></td>';
                    kolom += '</tr>';
                    num = num + 1;
                    $("#data_activity2").append(kolom);
                  } 

                    else {
                    }
                });                
              }
          });
          
          $('#list-kapal1').show();
          $('#list-kapal2').show();      

       
      })

  }); //end of script    
</script>