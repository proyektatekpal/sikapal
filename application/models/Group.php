<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class Group extends CI_Model
{
	protected $table = "group";
    protected $all_field = "group.id,group.id_workgroup_kapal,group,bobot_workgroup,kuantitas,satuan";

    public function __construct()
    {
        parent::__construct();
    }
  
  
    public function get($where=NULL,$select=NULL){    //dalam array
        //$query = $this->db->get_where($this->table, $where);
        if($select!=NULL){
            $this->db->select($select);
        }
        $this->db->select($this->all_field);
        if($where!=NULL){
            $this->db->where($where);
        }
        $this->db->from($this->table);
        $query = $this->db->get();
        if($query->num_rows() == 1){
            return $query->row();
        }
        else{
            return $query->result();
        }
        
    }

    public function join_workgroup(){
        $this->db->select('id_kapal, workgroup_kapal.id_workgroup');
        $this->db->select('bobot,nama_pekerjaan');
        $this->db->join('workgroup_kapal', 'workgroup_kapal.id = group.id_workgroup_kapal');
        $this->db->join('workgroup', 'workgroup.id_workgroup = workgroup_kapal.id_workgroup', 'left');
    }

    public function insert($data){  //dalam array
        return $this->db->insert($this->table, $data);
    }
    
    public function update($id_vertex, $data){
        $this->db->where('id', $id_vertex);
        return $this->db->update($this->table, $data);
    }

    public function count($where){
        $this->db->select('*');
        if($where!=NULL){
            $this->db->where($where);
        }
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function join_detail($where){
        $this->db->select($this->all_field);
        $this->db->select("id_kapal, workgroup_kapal.id_workgroup");
        $this->db->select("bobot,nama_pekerjaan");
        $this->db->from($this->table);
        $this->db->join("workgroup_kapal", "$this->table.id_workgroup_kapal = workgroup_kapal.id", "left");
        $this->db->join("workgroup", "workgroup_kapal.id_workgroup = workgroup.id_workgroup", "left");
        //$this->db->where("id_kapal =".$id_kapal);
        if($where != NULL){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->result();
    }
    
    
}  

?>