<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Detail Kapal</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          
          <?php echo($menu); ?>
          <div class="row">

                <div class="col-md-12">
                <div class="box box-primary box-solid">

                    <div class="box-header with-border">
                      <h3 class="box-title">Gambar Rancangan</h3>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                        
                        <?php foreach($photo->result() as $row):?>
                        <div class="col-md-4">
                        <?php     
                          
                              $image = array(
                                'src' => 'gambar_rancangan/'.($row->link),
                                'alt' => ($row->judul),
                                'class' => 'photo',
                                'width' => '240',
                                'height' => '180',
                                'title' => ($row->judul),
                                'rel' => 'lightbox',
                              );
                              echo img($image); ?>
                          <br/>
                          <?php echo($row->judul);?>
                          <br/>
                          </div>
                          <?php endforeach; ?>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
              </div>

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <script type="text/javascript">
	    $(document).ready(function(){
	      	$('#list-kapal').DataTable();
	    }); 
	</script>