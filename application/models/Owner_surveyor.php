<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class Owner_surveyor extends CI_Model
{
	protected $table = "owner_surveyor";
    protected $all_field = "id,nama_perusahaan,alamat_perusahaan,penanggung_jawab,no_telp,link_foto,email,password";



    public function __construct()
    {
        parent::__construct();
    }
  
  
    public function get($where){    //dalam array
        $query = $this->db->get_where($this->table, $where);
        return $query->result();
    }

    public function insert($data){  //dalam array
        return $this->db->insert($this->table, $data);
    }
    
    
}  

?>