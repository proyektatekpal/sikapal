
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 23, 2016 at 09:27 AM
-- Server version: 10.0.20-MariaDB
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u339919467_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `aktifitas`
--

CREATE TABLE IF NOT EXISTS `aktifitas` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_workgroup` int(6) unsigned zerofill NOT NULL,
  `aktifitas` varchar(40) NOT NULL,
  `item` text,
  `bobot` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `aktifitas`
--

INSERT INTO `aktifitas` (`id`, `id_workgroup`, `aktifitas`, `item`, `bobot`) VALUES
(000003, 000001, 'Design', NULL, '20'),
(000004, 000001, 'Owner Apporaval', NULL, '20'),
(000005, 000001, 'Class Apporaval', NULL, '30'),
(000006, 000001, 'Doc. Issued', NULL, '30'),
(000007, 000002, 'Purchase Order', 'Bukti Pembelian Barang|0#', '10'),
(000008, 000002, 'Arrival At yard', 'Grade/Type|0#Nomor Sertifikat|0#Quantity|0#Ukuran Material|0#Lubang|0#Deformasi|0#Pengelupasan permukaan|0#', '10'),
(000009, 000002, 'Fab. Blasting & Shop Primer', 'Paint maker|0#Colour|0#Product name|0#Aplication Methode|0#Cleaning Standard|0#Blasting/Roughness|0#Relative Humidity (RH)|0#Steel Temperature|0#Grade|0#Wet Temp|0#Dry Temp|0#', '10'),
(000010, 000002, 'Fab. Marking', 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', '10'),
(000011, 000002, 'Fab. Cutting', 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', '10'),
(000012, 000002, 'Fab. Bending', 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', '10'),
(000013, 000002, 'Ass. Fitt Up', 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '10'),
(000014, 000002, 'Ass. Welding', 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '10'),
(000015, 000002, 'Ass. Inspection', 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', '5'),
(000016, 000002, 'Erec. Fitt Up', 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '5'),
(000017, 000002, 'Erec. Welding', 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '5'),
(000018, 000002, 'Erec. Inspection', 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', '5'),
(000019, 000003, 'Purchase Order', 'Bukti Pembelian Barang|0#', '10'),
(000020, 000003, 'Arrival At yard', 'Grade/Type|0#Nomor Sertifikat|0#Quantity|0#Ukuran Material|0#Visual Check|0#', '10'),
(000021, 000003, 'Fab.Marking', 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', '10'),
(000022, 000003, 'Fab. Cutting', 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', '10'),
(000023, 000003, 'Fab. Fitt Up', 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '10'),
(000024, 000003, 'Fab. Welding', NULL, '10'),
(000025, 000003, 'Fab. Inspection', NULL, '10'),
(000026, 000003, 'On Board Fitt Up', NULL, '10'),
(000027, 000003, 'On Board Install', NULL, '10'),
(000029, 000003, 'On Board Testing ', NULL, '10'),
(000030, 000004, 'Purchase Order', NULL, '10'),
(000031, 000004, 'Arrival At yard', NULL, '10'),
(000032, 000004, 'Fab. Fitt Up', NULL, '10'),
(000033, 000004, 'Fab. Welding ', NULL, '10'),
(000034, 000004, 'Fab. Inspection', NULL, '10'),
(000035, 000004, 'On Board Fitt Up', NULL, '10'),
(000036, 000004, 'On Board Install', NULL, '10'),
(000037, 000004, 'On Board Inspection', NULL, '15'),
(000038, 000004, 'On Board Testing', NULL, '15'),
(000039, 000005, 'Purchase Order', NULL, '10'),
(000040, 000005, 'Arrival At yard', NULL, '10'),
(000041, 000005, 'Fab. Marking', NULL, '10'),
(000042, 000005, 'Fab. Cutting', NULL, '10'),
(000043, 000005, 'Fab.  Fitt Up', NULL, '10'),
(000044, 000005, 'Fab.  Welding', NULL, '10'),
(000045, 000005, 'Fab. Inspection', NULL, '10'),
(000046, 000005, 'On Board Fitt Up', NULL, '10'),
(000047, 000005, 'On Board Install', NULL, '10'),
(000048, 000005, 'On Board Inspection', NULL, '5'),
(000049, 000005, 'On Board Testing', NULL, '5'),
(000050, 000006, 'Purchase Order', NULL, '10'),
(000051, 000006, 'Arrival At yard', NULL, '10'),
(000052, 000006, 'On Board Fitt Up', NULL, '10'),
(000053, 000006, 'On Board Install', NULL, '10'),
(000054, 000006, 'On Board Connect', NULL, '20'),
(000055, 000006, 'On Board Inspection', NULL, '20'),
(000056, 000006, 'On Board Testing', NULL, '20'),
(000057, 000007, 'Purchase Order', NULL, '20'),
(000058, 000007, 'Arrival At yard', NULL, '20'),
(000059, 000007, 'Surface Prep.', NULL, '20'),
(000060, 000007, 'Coating/Painting', NULL, '20'),
(000061, 000007, 'Inspection', NULL, '20'),
(000062, 000008, 'Purchase Order', NULL, '25'),
(000063, 000008, 'Arrival At yard', NULL, '25'),
(000064, 000008, 'Loading On Board', NULL, '25'),
(000065, 000008, 'Inspection', NULL, '25');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
