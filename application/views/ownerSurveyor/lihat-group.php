<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Detail Kapal</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          
          <?php echo($menu); ?>
          <div class="row">

            <div class="col-md-12">
              <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title">List Group</h3>
                    </div>

                    <div class="box-body">
                        <table id="list-group" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="col-md-1">NO</th>
                                    <th>Group</th>
                                    <th>Workgroup</th>
                                    <th>Bobot</th>
                                    <th class="no-sort" style="width: 60px;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($group as $group) {
                                    
                                    echo '<tr>';
                                        echo '<td>' . $no . '</td>';
                                        echo '<td>' . $group->group . '</td>';
                                        echo '<td>' . $group->nama_pekerjaan . '</td>';
                                        echo '<td>' . $group->bobot_workgroup . '</td>';
                                        echo '<td>';
                                            echo '<a class="btn btn-warning" href="' .base_url().'ManajemenKapalOS/editGroup/'. $kapal->id.'/'.$group->id. '">';
                                                echo '<i class="fa fa-edit"></i>';
                                            echo '</a>';
                                            echo '<a class="delete-driver btn btn-danger pull-right" href="javascript:void(0);" data-id="' .base_url().'ManajemenKapalOS/deleteGroup/'. $group->id .'">';
                                                echo '<i class="fa fa-times"></i>';
                                            echo '</a>';
                                        echo '</td>';
                                    echo '</tr>';
                                    $no++;
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Group</th>
                                    <th>Workgroup</th>
                                    <th>Bobot</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
              </div><!-- /.box -->
            </div>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <script type="text/javascript">
	    $(document).ready(function(){
            $('#list-group').DataTable();
	    }); 
	</script>