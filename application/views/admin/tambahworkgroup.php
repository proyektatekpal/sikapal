<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>WorkGroup</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
          <?php echo validation_errors(); ?>
          <form enctype="multipart/form-data" id="form1" action="<?php echo base_url()."ManajemenKapalAdmin/SaveWorkGroup/";?>" method="post" role="form">
            <!-- Left col -->

            <div class="col-md-12">
                <?php
                  if ($cek_isi=='isi') {
                      if ($eksekusi) { ?>
                          <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <h4><i class="icon fa fa-check"></i> Sukses!</h4>
                              Data Berhasil Dimasukkan
                          </div>
                      <?php } else { ?>
                          <div class="alert alert-warning alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <h4><i class="icon fa fa-warning"></i> Gagal!</h4>
                              Data Gagal Dimasukkan
                          </div>
                      <?php }
                  }
                  ?>
              </div>
            <section class="col-lg-7 connectedSortable">
            	<div class="box box-primary box-solid">
	                <div class="box-header with-border">
	                  	<h3 class="box-title">Tambah WorkGroup</h3>
	                  	<div class="box-tools pull-right">
	                    	<button class="btn btn-box-tool" data-widget="remove"></button>
	                  	</div><!-- /.box-tools -->
	                </div><!-- /.box-header -->
	                <div class="box-body">
			            <div class="form-group">
			                <input type="text" class="form-control" id="workgroup" name="workgroup" placeholder="Masukkan Nama WorkGroup" >
			            </div>
			            <div class="form-group">
			                <input type="number" class="form-control number" id="bobot" name="bobot" placeholder="Masukkan Bobot WorkGroup (%)" >
			            </div>
			            <div class="box-footer">
	                	<button type="submit" class="btn btn-primary pull-right">Simpan</button>
	                	<!-- <a style="margin-left:15px;margin-right:15px;" class="btn bg-navy pull-right" href="<?php echo base_url()?>dashboard" title="Kembali">Kembali</a> -->
                        <button style="margin-right:15px;" type="reset" class="btn btn-warning pull-right">Reset</button>
			        	</div>
			            
			            <br>
	                </div><!-- /.box-body -->
              	</div><!-- /.box -->
            </section><!-- right col -->
          </form>

          <section class="col-lg-5 connectedSortable">
            	<div class="box box-primary box-solid">
	                <div class="box-header with-border">
	                  	<h3 class="box-title">Data Workgroup</h3>
	                  	<div class="box-tools pull-right">
	                    	<button class="btn btn-box-tool" data-widget="remove"></button>
	                  	</div><!-- /.box-tools -->
	                </div><!-- /.box-header -->
	                <div class="box-body">
			            <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Jumlah Workgroup</strong></span>
                            <input type="text" class="form-control" placeholder="" value="<?= $jumlahWorkgroup ?>" readonly/>
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Jumlah Bobot</strong></span>
                            <input type="text" class="form-control" placeholder="" value="<?= $bobot ?>" readonly/>
                        </div>
			            <br>
	                </div><!-- /.box-body -->
              	</div><!-- /.box -->
            </section><!-- right col -->


          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

    <script type="text/javascript">
	    
	</script>