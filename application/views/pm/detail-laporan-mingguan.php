<link rel="stylesheet" href="<?php echo base_url()?>assets/theme/plugins/lightbox2-master/css/lightbox.css">
<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>

<script src="<?php echo base_url()?>assets/theme/plugins/lightbox2-master/js/lightbox.js"></script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Detail Laporan 2 Minggu</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          

          <div class="row">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Laporan 2 Minggu</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                </div><!-- /.box -->
              </div>

              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <table class="table table-bordered">
                        <tbody>
                          <tr>
                            <th>Periode</th>
                            <td>Januari Awal</td>
                          </tr>
                         
                          <tr>
                            <th>Status</th>
                            <td><p style="color:red"><strong>Belum disetujui</strong></p></td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
 
             
            <form action="" method="post" role="form">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Pilih Workgroup dan Group</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-horizontal">
                      <div class="col-md-4">
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-5 control-label">Workgroup:</label>
                            <div class="col-sm-7">
                              <select name="workgroup" id="workgroup" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Workgroup</option>
                                  <option>Design & Approval Drawing</option>
                                  <option>Work Preparation & General</option>
                                  <option>Hull construction</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-5 control-label">Group:</label>
                            <div class="col-sm-7">
                              <select name="workgroup" id="workgroup" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Group</option>
                                  <option>110</option>
                                  <option>111</option>
                                  <option>112</option>
                              </select>
                            </div>
                          </div>
                          <a id="ok_button2" style="margin-left:15px;" class="btn bg-navy pull-right"  title="ok">Ok</a>
                        </div>
                    </div>

                    <div class="box-body form-horizontal">
                      <div class="box box-primary laporan_hidden" >
                        <div class="box-header with-border">
                            <h3 class="box-title">Data Laporan</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <table class="table table-bordered table-striped" id="tabelinput">
                              <thead>
                                <tr>
                                  <th class="text-center">NO</th>
                                  <th class="text-center">Aktivitas</th>
                                  <th class="text-center">Status</th>
                                  <th class="text-center">Projek Aktual %</th>
                                  <th class="text-center">Deskripsi</th>
                                  <th class="text-center">Foto</th>
                                </tr>
                              </thead>
                              <tbody id="data_laporan">

                              </tbody>
                            </table>       
                        </div><!-- /.box-body -->
                        </div>
                      </div><!-- /.box -->

                      

                    </div>
                    
                    
                </div><!-- /.box -->
              </div>
            </form>
          
          <div class="col-md-12">
                <div class="box box-primary box-solid">

                    <div class="box-header with-border">
                      <h3 class="box-title">Catatan Owner Surveyor</h3>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                         <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Catatan</strong></span>
                            <input type="Email" class="form-control" placeholder="" value="" readonly="" />
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Kirim Tanggapan</strong></span>
                            <input type="Email" class="form-control" placeholder="" value=""/>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                         
                        </div>       
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
 </div><!-- /.content-wrapper -->

