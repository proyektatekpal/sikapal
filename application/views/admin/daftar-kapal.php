<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Daftar Kapal</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Modal -->
          <div class="modal fade" id="DelConf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus Data</h4>
                </div>
                <div class="modal-body">
                  Data akan dihapus, Anda yakin?
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                  <a class="btn bg-navy" id="modalDelete" href="#">Ya</a>
                  <!-- <button type="button" class="btn btn-primary" href="<?php echo base_url()?>ManajemenKapalAdmin/DelWorkGroup/">Hapus</button> -->
                </div>
              </div>
            </div>
          </div>

          <div class="modal fade" id="UpdateData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Update Data</h4>
                </div>
                <div class="modal-body">
                <form enctype="multipart/form-data" id="form1" action="#" method="post" role="form">
                  <div class="box-body">
                    <div class="form-group">
                      <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" placeholder="Masukkan Nama WorkGroup" value="" >
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control number" id="alamat_perusahaan" name="alamat_perusahaan" placeholder="Masukkan Bobot WorkGroup" value="" >
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control number" id="penanggung_jawab" name="penanggung_jawab" placeholder="Masukkan Bobot WorkGroup" value="" >
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control number" id="telp" name="telp" placeholder="Masukkan Bobot WorkGroup" value="" >
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control number" id="email" name="email" placeholder="Masukkan Bobot WorkGroup" value="" >
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control number" id="password" name="password" placeholder="Masukkan Bobot WorkGroup" value="" >
                    </div>
                  </div><!-- /.box-body -->
                
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>

                  <!-- <a class="btn bg-navy" id="modalUpdate" type="submit" >Simpan</a> -->
                  <!-- <button type="button" class="btn btn-primary" href="<?php echo base_url()?>ManajemenKapalAdmin/DelWorkGroup/">Hapus</button> -->
                </div>
                </form>
              </div>
            </div>
          </div>


          
          <!-- Main row -->
          <div class="row">
            	<div class="col-xs-12">
                	<div class="box box-primary box-solid">
                      <div class="box-header">
                          <h3 class="box-title">Daftar Owner</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                          <table id="list-kapal" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th class="col-xs-1">No. </th>
                                <th>Nama Proyek</th>
                                <th>Galangan</th>
                                <th>Owner</th>
                                <th>Owner Surveyor</th>
                                <th>Status Pengerjaan</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                              $num = 1;
                              foreach ($daftar_proyek as $kapal) {
                                  echo '<tr>';
                                  echo '<td>'.$num.'</td>';
                                  echo '<td>'.$kapal->nama_proyek.'</td>';
                                  echo '<td>'.$kapal->nama_galangan.'</td>';
                                  echo '<td>'.$kapal->nama_owner.'</td>';
                                  echo '<td>'.$kapal->nama_os.'</td>';
                                  if($kapal->status_pengerjaan == 0 )
                                  {
                                      echo '<td>Belum selesai</td>';
                                  }
                                  else
                                  {
                                    echo '<td>Selesai</td>';
                                  }
                                  
                                  $num++;
                              }
                              ?>

                            </tbody>
                            <tfoot>
                              <tr>
                                <th class="col-xs-1">No. </th>
                                <th>Nama Proyek</th>
                                <th>Galangan</th>
                                <th>Owner</th>
                                <th>Owner Surveyor</th>
                                <th>Status Pengerjaan</th>
                              </tr>
                              </tr>
                            </tfoot>
                          </table>
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>
            

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <script type="text/javascript">
	    $('.hapus').click(function(){
          var id = $(this).data('id');
          var base_url = $(this).data('url');

          $('#modalDelete').attr('href',base_url +'ManajemenKapalAdmin/DelOwner/'+id);
      });


       $('.update').click(function(){
          var id = $(this).data('id');
          var nama_perusahaan = $(this).data('nama_perusahaan');
          var alamat_perusahaan = $(this).data('alamat_perusahaan');
          var penanggung_jawab = $(this).data('penanggung_jawab');
          var telp = $(this).data('telp');
          var email = $(this).data('email');
          var password = $(this).data('password');
          var base_url = $(this).data('url');
          
          $('#nama_perusahaan').attr('value',nama_perusahaan);
          $('#alamat_perusahaan').attr('value',alamat_perusahaan);
          $('#penanggung_jawab').attr('value',penanggung_jawab);
          $('#telp').attr('value',telp);
          $('#email').attr('value',email);
          $('#password').attr('value',password);
          $('#form1').attr('action', base_url + 'ManajemenKapalAdmin/UpdateOwner/'+id);
      });

	</script>