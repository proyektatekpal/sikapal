<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManajemenKapalOS extends CI_Controller {

	public function __construct()
    {
        parent::__construct(); 
        if ( !$this->session->userdata('isLogin') && $this->session->userdata('level') != 'ownersurveyor')
        { 
            redirect('access');
        }
        // $this->load->library('form_validation');
        $this->load->model('M_profil');
        $this->load->model('M_manajemenOS');
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));

    }

	public function index()
	{
        $this->load->model(array('pembuatan_kapal'));
        $data['kapal'] = $this->pembuatan_kapal->get();
	}

    public function DaftarKapal()
    {
        $this->load->model('pembuatan_kapal');
        $email = $this->session->userdata('email');
        $detail_os = $this->M_manajemenOS->getDetailOS($email);
        $id_os = $detail_os->id;
        //$Data['list_kapal'] = $this->M_manajemenOS->getKapalOS($id_os);
        $Data['list_kapal'] = $this->pembuatan_kapal->get(array('id_os' => $id_os), NULL);
        // print_r($Data['list_kapal']);

        $this->load->view('header');
        $this->load->view('sidebar-os');
        $this->load->view('ownerSurveyor/daftar-kapal', $Data);
        $this->load->view('footer');
    }

    public function DetailKapal($id_kapal)
    {
        $this->load->model(array('pembuatan_kapal'));
        $this->pembuatan_kapal->join_galangan();
        $Data['kapal'] = $this->pembuatan_kapal->get(array('id' => $id_kapal))[0];
        //var_dump($Data['kapal']);exit();
        $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true);   

        $this->load->view('header');
        $this->load->view('sidebar-os');
        $this->load->view('ownerSurveyor/detail-kapal', $Data);
        $this->load->view('footer');
    }

    public function LihatSCurve($id_kapal)
    {
        $this->load->model(array('baseline', 'pembuatan_kapal', 'progres'));
        $baseline = $this->baseline->get(array('id_kapal'=>$id_kapal));
        $progres = $this->progres->get(array('id_kapal'=>$id_kapal));
        $label = array();
        $bobot_baseline = array();
        $bobot_progres = array();
        foreach ($baseline as $b) {
            if($b->mid_end == 0){
                $label[] = 'mid '.$b->periode;
            }
            else{
                $label[] = 'end '.$b->periode;
            }
            $bobot_baseline[] = $b->target;
        }

        foreach ($progres as $p) {
            $bobot_progres[] = $p->progres;
        }


        $detail_kapal = $this->pembuatan_kapal->get(array('id' => $id_kapal));
        $Data['kapal'] = $detail_kapal[0];
        $Data['label'] = json_encode($label);
        $Data['bobot_baseline'] = json_encode($bobot_baseline);
        $Data['bobot_progres'] = json_encode($bobot_progres);
        $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true);

        $this->load->view('header');
        $this->load->view('sidebar-os');
        $this->load->view('ownerSurveyor/lihat-scurve', $Data);
        $this->load->view('footer');
    }
    
    public function LihatGambar($id_kapal)
    {
        $this->load->library('pagination');
        $this->load->model('gambar_rancangan');
        //------------------------------------------------------------------------------------
        $getData = $this->db->get('gambar_rancangan');
        $a = $getData->num_rows();
        $config['base_url'] = site_url().'/ManajemenKapalOS'; //set the base url for pagination
        $config['total_rows'] = $a; //total rows
        $config['per_page'] = '6'; //the number of per page for pagination
        $config['uri_segment'] = 3; //see from base_url. 3 for this case
        $config['full_tag_open'] = '<p class=pagination>';
        $config['full_tag_close'] = '</p>';
        $this->pagination->initialize($config); //initialize pagination
        //------------------------------------------------------------------------------------  
        
        $Data['photo'] = $this->gambar_rancangan->get_photo($config['per_page'],$this->uri->segment(3));

        //$this->load->view('view_photo', $data);

        $Data['kapal'] = $this->M_manajemenOS->getDetailKapal($id_kapal);
        $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 

        //var_dump($Data['output']);
        $this->load->view('header');
        $this->load->view('sidebar-os');
        $this->load->view('ownerSurveyor/gambar', $Data);
        $this->load->view('footer');
    }

    public function InputLaporanHarian($id_kapal)
    {
        $this->load->model(array('tenaga_kerja', 'alat_kerja'));
        //Cek udah pernah input atau belum (dalam satu hari)
        $status = $this->M_manajemenOS->cekDataLaporanHarian($id_kapal);
        //print_r($status);
        if ($status == null) {
            $Data['status']  = "tidak";
        }
        else $Data['status']  = "ada";

        $this->load->model('Workgroup_kapal');
        $this->load->model('Group');

        //menu navbar
        $Data['kapal'] = $this->M_manajemenOS->getDetailKapal($id_kapal);
        $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true);

        //get data workgroup 
        $Data['list_workgroup'] = $this->Workgroup_kapal->join($id_kapal);
        //get data group
        $Data['list_group'] = $this->Group->join_detail('id_kapal ='.$id_kapal);
        $Data['id_kapal'] = $id_kapal;
        $Data['keahlian'] = $this->tenaga_kerja->get();
        $Data['alat'] = $this->alat_kerja->get();
        // var_dump($Data['keahlian']);exit();
        if($Data['status'] == "tidak")
        {
            $this->load->view('header');
            $this->load->view('sidebar-os');
            $this->load->view('ownerSurveyor/input-laporan-harian', $Data);
            $this->load->view('footer');    
        }
        else
        {
            $Data['data_tenaga_kerja'] = $this->M_manajemenOS->getLaporanTenagaKerja($id_kapal);
            $Data['data_cuaca'] = $this->M_manajemenOS->getlaporanCuaca($id_kapal);
            $Data['data_alat'] = $this->M_manajemenOS->getlaporanAlat($id_kapal);
            $this->load->view('header');
            $this->load->view('sidebar-os');
            $this->load->view('ownerSurveyor/input-laporan-harian', $Data);
            $this->load->view('footer');   
        }
    }

    public function GetHistoryLaporan()
    {
        //load data laporan sebelumnya
        $Data['history_laporan'] = $this->M_manajemenOS->historyLaporan();    
    }

    public function SaveInputLaporanHarian($id_kapal)
    {
        //insert to tabel laporan_harian
        $Data = array(
                'id_group' => $this->input->post('group'),
                'tgl_laporan' => date('Y-m-d'),
                'cuaca_pagi' => $this->input->post('cuaca_pagi'),
                'cuaca_siang' => $this->input->post('cuaca_siang'),
                'cuaca_sore' => $this->input->post('cuaca_sore'),
                'catatan_pm' => $this->input->post('catatan')
            );
        $this->M_manajemenOS->insertLaporanHarian($Data);

        //Cari id laporan_harian paling besar perhari
        $id_laporan_harian_Max = $this->M_manajemenOS->getMaxIdLapHarian();

        //buat upload gambar
        //set preferences
        $config['upload_path'] = 'upload';
        $config['allowed_types'] = 'jpg|png|bmp|gif';


        //load upload class library
        $this->load->library('upload', $config);

        for ($i=0; $i<$this->input->post('loop'); $i++) 
        {
            //mengolah data item
            $item = '';
            for($a=0; $a<$this->input->post('loop_item'.($i)); $a++)
            {
                $nama_item = $this->input->post('item'.($i+1).($a));
                $status_item = $this->input->post('status'.($i+1).($a));
                $item .= $nama_item.'|'.$status_item.'#';
            }

            if (!$this->upload->do_upload('foto'.($i+1)))
            {
                // // case - failure
                $Data = array(
                    'id_aktifitas' => $this->input->post('aktifitas_id'.($i+1)),
                    'id_laporan_harian' => $id_laporan_harian_Max,
                    'item' => $item,
                    // 'deskripsi' => $this->input->post('deskripsi'.($i+1)),
                    'link_foto' => $this->input->post('hidden_gambar'.($i+1)),
                    // 'status_pekerjaan' => $this->input->post('status'.($i+1))
                );
                $this->M_manajemenOS->insertDataLaporanHarian($Data);
            }
            else
            {
                $upload_data = $this->upload->data();

                $Data = array(
                    'id_aktifitas' => $this->input->post('aktifitas_id'.($i+1)),
                    'id_laporan_harian' => $id_laporan_harian_Max,
                    'item' => $item,
                    // 'deskripsi' => $this->input->post('deskripsi'.($i+1)),
                    'link_foto' => $upload_data['file_name'],
                    // 'status_pekerjaan' => $this->input->post('status'.($i+1))
                );
                $this->M_manajemenOS->insertDataLaporanHarian($Data);
            }
        }

        //save tenaga kerja
        $jumlah_tenaga = $this->input->post('jumlah_tenaga_kerja');
        for ($i=0; $i <$jumlah_tenaga ; $i++) { 
            $Data = array(
                'id_laporan_harian' => $id_laporan_harian_Max,
                'tenaga_kerja' => $this->input->post('tenaga_kerja'.($i+1)),
                'jumlah' => $this->input->post('jumlah_keahlian'.($i+1))
            );
            $this->M_manajemenOS->insertLaporantenagakerja($Data);
        }

        //save jumlah alat
        $jumlah_alat = $this->input->post('jumlah_alat');
        for ($i=0; $i <$jumlah_alat ; $i++) { 
            $Data = array(
                'id_laporan_harian' => $id_laporan_harian_Max,
                'nama_alat' => $this->input->post('nama_alat'.($i+1)),
                'jumlah_alat' => $this->input->post('total_alat'.($i+1))
            );
            $this->M_manajemenOS->insertLaporanalat($Data);
        }

        $this->InputLaporanHarian($id_kapal);
    }

    public function getDataAktifitas()
    {
        //get id laporan terbaru
        $id_laporan_max = $this->M_manajemenOS->getIdMax($this->input->post('id_group'));

        //cek apa laporan untuk sebuah group sudah ada
        if($id_laporan_max == NULL)
        {
            $id_workgroup_kapal = $this->input->post('id_workgroup');
            $id_workgroup = $this->M_manajemenOS->getIdWorkgroup($id_workgroup_kapal);
            $data_aktifitas = $this->M_manajemenOS->getAktifitasDefault($id_workgroup);

            $json = array();
            foreach ($data_aktifitas as $activity) {
                $json[] = array('aktifitas_id'  => $activity->id,
                                'aktifitas' => $activity->aktifitas,
                                'bobot' => $activity->bobot,
                                'item' => $activity->item,
                                'link_foto' => 'dummy.png',
                                'id_workgroup'=> $activity->id_workgroup,
                                'flag' => 0            
                );
            }
            echo json_encode($json);
        }
        else
        {
            $data_aktifitas = $this->M_manajemenOS->getAktifitas($this->input->post('id_group'), $id_laporan_max);

            $json = array();
            foreach ($data_aktifitas as $activity) {
                if($activity->link_foto === NULL)
                {
                   $json[] = array('id_data'  => $activity->id_data,
                                 //'id_group' => $activity->id_group,
                                // 'tgl_laporan' => $activity->tgl_laporan,
                                'aktifitas_id' => $activity->aktifitas_id,
                                'aktifitas' => $activity->aktifitas,
                                'deskripsi' => $activity->deskripsi,
                                'link_foto' => 'dummy.png',
                                'item' => $activity->item,
                                'status_pekerjaan' => $activity->status_pekerjaan,
                                'catatan_pm'=> $activity->catatan_pm,
                                'tanggapan_pm'=> $activity->tanggapan_pm,
                                'flag' => 1
                    ); 
                }
                else
                {
                    $json[] = array('id_data'  => $activity->id_data,
                                 //'id_group' => $activity->id_group,
                                // 'tgl_laporan' => $activity->tgl_laporan,
                                'aktifitas_id' => $activity->aktifitas_id,
                                'aktifitas' => $activity->aktifitas,
                                'deskripsi' => $activity->deskripsi,
                                'link_foto' => $activity->link_foto,
                                'item' => $activity->item,
                                'status_pekerjaan' => $activity->status_pekerjaan,
                                'catatan_pm'=> $activity->catatan_pm,
                                'tanggapan_pm'=> $activity->tanggapan_pm,
                                'flag' => 1
                    );
                }
                
            }
            echo json_encode($json);
        }
        
    }    

    public function getDataTenagaKerja()
    {
        $tenagaKerja = $this->M_manajemenOS->getTenagakerja();

        $json = array();
        foreach ($tenagaKerja as $activity) {
            $json[] = array('id'  => $activity->id,
                            'keahlian' => $activity->keahlian
            );
        }
        echo json_encode($json);
    }


    public function getDataAlat()
    {
        $tenagaKerja = $this->M_manajemenOS->getAlat();

        $json = array();
        foreach ($tenagaKerja as $activity) {
            $json[] = array('id'  => $activity->id,
                            'alat' => $activity->alat
            );
        }
        echo json_encode($json);
    }

    public function getGroup()
    {
        $this->load->model('Group');
        $listgroup = $this->Group->get('id_workgroup_kapal='.$this->input->post('id_workgroup_kapal'), NULL);

        $json = array();
        foreach ($listgroup as $activity) {
            $json[] = array('id'  => $activity->id,
                            'group' => $activity->group,
                            'bobot' => $activity->bobot_workgroup
            );
        }
        echo json_encode($json);
    }

    public function LaporanHarian($id_kapal)
    {
        $Data['kapal'] = $this->M_manajemenOS->getDetailKapal($id_kapal);
        $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 
        $this->load->model('laporan_harian');
        $Data['tanggal_laporan'] = $this->laporan_harian->getjoin_group($id_kapal);
       // print_r($Data['tanggal_laporan']);

        $this->load->view('header');
        $this->load->view('sidebar-os');
        $this->load->view('ownerSurveyor/laporan-harian', $Data);
        $this->load->view('footer');
    }

    public function DetailHarian()
    {
        //ambil data dari URL
        $id_kapal =  $this->uri->segment(3);
        $tgl_laporan =  $this->uri->segment(4);
        $status = $this->uri->segment(5);

        $Data['tgl_laporan'] = $tgl_laporan;
        $Data['status'] = $status;

        //get data tenaga kerja
        $Data['tenaga_kerja'] = $this->M_manajemenOS->getDetailLapTenagaKerja($id_kapal, $tgl_laporan);
        //get data cuaca
        $Data['cuaca'] = $this->M_manajemenOS->getDetailLapCuaca($id_kapal, $tgl_laporan);
        //get data laporan_alat
        $Data['alat'] = $this->M_manajemenOS->getDetailLapAlat($id_kapal, $tgl_laporan);

        //get Data workgroup
        $this->load->model('workgroup_kapal');
        $Data['list_workgroup'] = $this->workgroup_kapal->join($id_kapal);

        // menu navigasi tab
        $Data['kapal'] = $this->M_manajemenOS->getDetailKapal($id_kapal);
        $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 

        $this->load->view('header');
        $this->load->view('sidebar-os');
        $this->load->view('ownerSurveyor/detail-laporan-harian', $Data);
        $this->load->view('footer');
    }

    public function InputLaporanMingguan($id_kapal)
    {
        $this->load->model(array('workgroup_kapal', 'baseline'));
        $Data['kapal'] = $this->M_manajemenOS->getDetailKapal($id_kapal);
        $Data['workgroup'] = $this->workgroup_kapal->join($id_kapal);
        $Data['periode'] = $this->baseline->get(array('id_kapal' => $id_kapal));
        $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 
        $Data['today'] = date('d-m-Y');

        $this->load->view('header');
        $this->load->view('sidebar-os');
        $this->load->view('ownerSurveyor/laporan-mingguan', $Data);
        $this->load->view('footer');
    }

    public function LaporanMingguan($id_kapal)
    {
        $this->load->model(array('laporan_mingguan','m_manajemenOS'));
        $this->laporan_mingguan->join_workgroup();
        
        $Data['lap_mingguan'] = $this->laporan_mingguan->get(array('id_kapal'=>$id_kapal));
        //var_dump($Data['lap_mingguan']);exit();
        $Data['kapal'] = $this->m_manajemenOS->getDetailKapal($id_kapal);
        $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 

        $this->load->view('header');
        $this->load->view('sidebar-owner');
        $this->load->view('ownerSurveyor/daftar-laporan-mingguan', $Data);
        $this->load->view('footer');
    }

    public function DetailMingguan($id_kapal, $id_laporan_mingguan)
    {
        $this->load->model(array('data_laporan_mingguan', 'm_manajemenOS', 'laporan_mingguan'));

        $this->data_laporan_mingguan->join_mingguan();
        $this->data_laporan_mingguan->join_aktifitas();
        $Data['detail_mingguan'] = $this->data_laporan_mingguan->get(array('id_laporan_mingguan' => $id_laporan_mingguan));
        $this->laporan_mingguan->join_workgroup();
        $Data['data_mingguan'] = $this->laporan_mingguan->get(array('laporan_mingguan.id'=>$id_laporan_mingguan))[0];
        //var_dump($Data['data_mingguan']);exit();
        $Data['kapal'] = $this->m_manajemenOS->getDetailKapal($id_kapal);
        $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 

        $this->load->view('header');
        $this->load->view('sidebar-owner');
        $this->load->view('ownerSurveyor/detail-laporan-mingguan', $Data);
        $this->load->view('footer');
    }

    public function simpanLaporanMingguan()
    {
        $this->load->model(array('progres', 'workgroup_kapal', 'baseline', 'laporan_mingguan', 'data_laporan_mingguan'));
        $id_kapal = $this->input->post('id_kapal');
        $id_workgroup_kapal = $this->input->post('pilih_workgroup');
        $id_group = $this->input->post('pilih_group');
        $tmp_periode = explode('-', $this->input->post('periode'));
        $mid_end = $tmp_periode[0];
        $periode = $tmp_periode[1];
        $tgl_lap = date('Y-m-d');

        $tmp_bobot_workgroup = $this->input->post('bobot_workgroup');
        $tmp_bobot_group = $this->input->post('bobot_group');
        $bobot_workgroup = ($tmp_bobot_workgroup[$id_workgroup_kapal]/100);
        $bobot_group = ($tmp_bobot_group[$id_group]/100);

        $id_aktifitas = $this->input->post('id_aktifitas');
        $persentase_aktifitas = $this->input->post('project_actual');
        $bobot_aktifitas = $this->input->post('bobot_aktifitas');
        $persentase_group = 0;
        foreach ($this->input->post('id_aktifitas') as $aktifitas) {
            $persentase_group = $persentase_group + (($persentase_aktifitas[$aktifitas]*$bobot_aktifitas[$aktifitas])/100);   //masih blm dalam persen
        }
        $progres = $persentase_group * $bobot_group * $bobot_workgroup;
        $progres_before = $this->progres->get(array('id_kapal'=>$id_kapal, 'periode'=>$periode, 'mid_end'=>$mid_end));
        // var_dump($id_kapal);
        // var_dump($progres_before[0]);exit();
        //var_dump($progres);
        $data = array(
                'progres' => ($progres + $progres_before[0]->progres),
                'tgl_input' => $tgl_lap
            );
       // var_dump($data);exit();
        $this->progres->update_data($data, array('periode' => $periode, 'id_kapal' => $id_kapal, 'mid_end' => $mid_end));

        $laporan_mingguan = array(
                'id_group' => $id_group,
                'tgl_input' => $tgl_lap,
                'periode' => $periode,
                'mid_end' => $mid_end,
                'progres_group' => $persentase_group,
                'verif_owner' => 0,
                'os_verifikator' => $this->input->post('os_verifikator'),
                'class_verifikator' => $this->input->post('class_verifikator'),
                'qa_verifikator' => $this->input->post('qa_verifikator'),
                'catatan_os' => $this->input->post('catatan_os')
            );
        $this->laporan_mingguan->insert($laporan_mingguan);

        $id_laporan_mingguan = $this->laporan_mingguan->getMaxID();

        $id_aktifitas = $this->input->post('id_aktifitas');
        $item_pengawasan = $this->input->post('item_pengawasan');
        $progres_aktual = $this->input->post('project_actual');
        $deskripsi = $this->input->post('deskripsi');
        foreach ($id_aktifitas as $key => $act) {
            $data_laporan_harian = array(
                'id_laporan_mingguan' => $id_laporan_mingguan,
                'id_aktivitas' => $key,
                'item_pengawasan' => $item_pengawasan[$key],
                'progres_aktual' => $progres_aktual[$key],
                'deskripsi' => $deskripsi[$key]
            );
            $this->data_laporan_mingguan->insert($data_laporan_harian);
        }
        
        redirect('ManajemenKapalOS/InputLaporanMingguan/'.$id_kapal);
        // $Data['kapal'] = $this->M_manajemenOS->getDetailKapal($id_kapal);
        // $Data['workgroup'] = $this->workgroup_kapal->join($id_kapal);
        // $Data['periode'] = $this->baseline->get(array('id_kapal' => $id_kapal));
        // $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 
        // $Data['today'] = date('d-m-Y');

        // $this->load->view('header');
        // $this->load->view('sidebar-os');
        // $this->load->view('ownerSurveyor/laporan-mingguan', $Data);
        // $this->load->view('footer');
    }

    public function getGroupByWorkgroup($id_workgroup_kapal){
        $this->load->model(array('group'));
        if ($id_workgroup_kapal != NULL) {
            $group_kapal = $this->group->join_detail(array('id_workgroup_kapal' => $id_workgroup_kapal));
            $json = array();
            foreach ($group_kapal as $group) {
                $json[] = array(
                        'id_group'  => $group->id,
                        'name' => $group->group,
                        'bobot' => $group->bobot_workgroup
                );
            }

            echo json_encode($json);
                  
        } else {

            $json[] = array('id' => 'empty');
            echo json_encode($json);
        }
    }

    public function getLastLaporanHarian()
    {
        $this->load->model(array('laporan_harian', 'data_laporan_harian', 'workgroup_kapal', 'aktifitas'));

        $id_group = $this->input->post('id_group');
        $id_workgroup_kapal = $this->input->post('id_workgroup');
        if(!empty($id_group))
        {
            $last_lap = $this->laporan_harian->getLaporanTerakhir($id_group);
            if(!empty($last_lap)){
                $data_laporan = $this->data_laporan_harian->getDataLaporanTerakhir($last_lap->id);
                //var_dump($data_laporan);
                foreach ($data_laporan as $lap_mingguan) {
                    $json[] = array(
                            'id'  => $lap_mingguan->id_aktifitas,
                            'aktifitas' => $lap_mingguan->aktifitas,
                            'item_pengawasan' => $lap_mingguan->item,
                            'bobot_aktifitas' => $lap_mingguan->bobot,
                            'status' => $lap_mingguan->status_pekerjaan,
                            'deskripsi' => $lap_mingguan->deskripsi,
                            'foto' => $lap_mingguan->link_foto
                    );
                }
                echo json_encode($json);
            }
            else{
                $id_workgroup = $this->workgroup_kapal->get(array('id' => $id_workgroup_kapal))[0]->id_workgroup;
                $list_aktifitas = $this->aktifitas->get($id_workgroup);
                //var_dump($list_aktifitas);
                $json = array();
                foreach ($list_aktifitas as $list) {
                    $json[] = array(
                        'id' => $list->id,
                        'aktifitas' => $list->aktifitas,
                        'item_pengawasan' => $list->item,
                        'bobot_aktifitas' => $list->bobot,
                        'status' => "none",
                        'deskripsi' => "-",
                        'foto' => "-"
                    );
                }
                echo json_encode($json);
            }
            
        }

    }

    // public function LaporanMingguan($id_kapal)
    // {
    //     $Data['kapal'] = $this->M_manajemenOS->getDetailKapal($id_kapal);
    //     $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 

    //     $this->load->view('header');
    //     $this->load->view('sidebar-os');
    //     $this->load->view('ownerSurveyor/daftar-laporan-mingguan', $Data);
    //     $this->load->view('footer');
    // }



    // public function DetailMingguan($id_kapal)
    // {
    //     $Data['kapal'] = $this->M_manajemenOS->getDetailKapal($id_kapal);
    //     $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 

    //     $this->load->view('header');
    //     $this->load->view('sidebar-os');
    //     $this->load->view('ownerSurveyor/detail-laporan-mingguan', $Data);
    //     $this->load->view('footer');
    // }


    public function AddGroup($id_kapal)
    {
        $this->load->model('workgroup_kapal');
        $Data['cek_isi'] = 'kosong';
        $Data['kapal'] = $this->M_manajemenOS->getDetailKapal($id_kapal);
        $Data['workgroup'] = $this->workgroup_kapal->join($id_kapal);
        $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 

        $this->load->view('header');
        $this->load->view('sidebar-os');
        $this->load->view('ownerSurveyor/tambah-group', $Data);
        $this->load->view('footer');
    }

    public function SimpanGroup(){
        $this->load->model(array('group', 'workgroup_kapal'));
        $jml_group = $this->input->post('jumlah_group');
        $id_kapal = $this->input->post('id_kapal');
        $id_workgroup = $this->input->post('workgroup');
        for($i=1; $i<=$jml_group; $i++){
            $Data = array(
                'id_workgroup_kapal' => $id_workgroup,
                'group' => $this->input->post('group'.$i),
                'bobot_workgroup' => $this->input->post('bobot'.$i),
                'kuantitas' => $this->input->post('kuantitas'.$i),
                'satuan' => $this->input->post('satuan'.$i)
            );
            $this->group->insert($Data);
        }

        $Data['cek_isi'] = 'isi';
        $Data['eksekusi'] = TRUE;
        $Data['kapal'] = $this->M_manajemenOS->getDetailKapal($id_kapal);
        $Data['workgroup'] = $this->workgroup_kapal->join($id_kapal);
        $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true);

        $this->load->view('header');
        $this->load->view('sidebar-os');
        $this->load->view('ownerSurveyor/tambah-group', $Data);
        $this->load->view('footer'); 
    }

    public function LihatGroup($id_kapal){
        $this->load->model(array('pembuatan_kapal', 'group'));
        $detail_kapal = $this->pembuatan_kapal->get(array('id' => $id_kapal)); //array('id_kapal' => $id_kapal)
        //$detail_group = $this->group->join_detail(array('id_kapal' => $id_kapal));
        $Data['kapal'] = $detail_kapal[0];
        $Data['group'] = $this->group->join_detail(array('id_kapal' => $id_kapal));
        $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 
        //print_r($detail_group);
        $this->load->view('header');
        $this->load->view('sidebar-os');
        $this->load->view('ownerSurveyor/lihat-group', $Data);
        $this->load->view('footer');

    }

    public function EditGroup($id_kapal, $id_group){
        $this->load->model(array('pembuatan_kapal', 'group'));
        $detail_kapal = $this->pembuatan_kapal->get(array('id' => $id_kapal));
        $Data['cek_isi'] = "kosong";
        $this->group->join_workgroup();
        $Data['group'] = $this->group->get(array('group.id'=>$id_group));
        $Data['kapal'] = $detail_kapal[0];
        $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 
        $this->load->view('header');
        $this->load->view('sidebar-os');
        $this->load->view('ownerSurveyor/edit-group', $Data);
        $this->load->view('footer'); 
    }

    public function simpanEditGroup(){
        $this->load->model(array('pembuatan_kapal', 'group'));
        $id_kapal = $this->input->post('id_kapal');
        $id_group = $this->input->post('id_group');
        $data_group = array(
                'group' => $this->input->post('group'),
                'bobot_workgroup' => $this->input->post('bobot'),
                'kuantitas' =>  $this->input->post('kuantitas'),
                'satuan' => $this->input->post('satuan')
            );
        $Data['cek_isi'] = "isi";
        $Data['eksekusi'] = $this->group->update($id_group, $data_group);
        $this->group->join_workgroup();
        $Data['group'] = $this->group->get(array('group.id'=>$id_group));
        $detail_kapal = $this->pembuatan_kapal->get(array('id' => $id_kapal));
        $Data['kapal'] = $detail_kapal[0];
        $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true);
        $this->load->view('header');
        $this->load->view('sidebar-os');
        $this->load->view('ownerSurveyor/edit-group', $Data);
        $this->load->view('footer'); 
    }

    public function InputBaseline($id_kapal)
    {
        $this->load->model(array('baseline', 'pembuatan_kapal'));
        $Data['id_kapal'] = $id_kapal;
        $detail_kapal = $this->pembuatan_kapal->get(array('id' => $id_kapal));
        $Data['kapal'] = $detail_kapal[0];
        $Data['vertex'] = $this->baseline->get(array('id_kapal' => $id_kapal));
        $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 

        $this->load->view('header');
        $this->load->view('sidebar-os');
        $this->load->view('ownerSurveyor/input-baseline', $Data);
        $this->load->view('footer');
    }

    public function SaveBaseline(){
        $this->load->model('baseline');
        $id_kapal = $this->input->post('id_kapal');
        $loop = $this->baseline->count(array('id_kapal' => $id_kapal));
        
        for($i=1; $i<=$loop; $i++){
            $vertex = array(
                'target' => $this->input->post('progres'.$i)
            );

            $mid_end = ($this->input->post('mid_end'.$i)=='mid') ? "0" : "1";
            $where = array(
                'id' => $this->input->post('id_vertex'.$i),
                'periode' => $this->input->post('periode'.$i),
                'mid_end' => $mid_end
            );
            
            $this->baseline->update_baseline($where, $vertex);
        }   
    }

    public function SCurve($id_kapal)
    {
        $baseline = $this->m_manajemen->getDetailBaseline($id_kapal);
        $label = array();
        $bobot = array();
        foreach ($baseline as $b) {
            if($b->mid_end == 0){
                $label[] = 'mid '.$b->periode;
            }
            else{
                $label[] = 'end '.$b->periode;
            }
            $bobot[] = $b->bobot;
        }

        $Data['kapal'] = $this->m_manajemen->getDetailKapal($id_kapal);
        $Data['label'] = json_encode($label);
        $Data['bobot_baseline'] = json_encode($bobot);

        $this->load->view('header');
        $this->load->view('sidebar-os');
        $this->load->view('os/lihat-scurve', $Data);
        $this->load->view('footer');
    }

}



