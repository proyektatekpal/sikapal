<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Detail Kapal</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <nav class="navbar navbar-default">
              <div class="container-fluid">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url()."manajemenKapalPM/DetailKapal/".$kapal->id_pembuatan;?>"><strong><?php echo $kapal->nama_kapal;?></strong></a>
                  </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                      <li><a href="<?php echo base_url()."manajemenKapalPM/gambar/".$kapal->id_pembuatan;?>">Gambar</a></li>
                      <li><a href="<?php echo base_url()."manajemenKapalPM/SCurve/".$kapal->id_pembuatan;?>">S-Curve</a></li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Laporan <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url()."manajemenKapalPM/lapHarian/".$kapal->id_pembuatan;?>">Harian</a></li>
                            <li><a href="<?php echo base_url()."manajemenKapalPM/lapBulanan/".$kapal->id_pembuatan;?>">2 Mingguan</a></li>
                          </ul>
                      </li>
                      <li><a href="#">Pesan</a></li>
                    </ul>
                  </div><!-- /.navbar-collapse -->

              </div><!-- /.container-fluid -->
          </nav>

          <div class="row">
            <form action="" method="post" role="form">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Laporan Harian</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-horizontal">
                        <div class="col-md-4">

                          <div class="form-group">
                            <label for="cuaca_pagi" class="col-sm-5 control-label">Tanggal Laporan:</label>
                            <div class="col-sm-7">
                              <input type="date" class="form-control" placeholder="Pagi">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-5 control-label">Workgroup:</label>
                            <div class="col-sm-7">
                              <select name="workgroup" id="workgroup" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Kluster</option>
                                  <option>Design & Approval Drawing</option>
                                  <option>Work Preparation & General</option>
                                  <option>Hull construction</option>
                              </select>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="cuaca_pagi" class="col-sm-5 control-label">Jumlah pekerjaan:</label>
                            <div class="col-sm-7">
                              <input type="number" min="1" class="form-control input_number" placeholder="jumlah pekerjaan" id="jumlah_pekerjaan">
                            </div>
                          </div>
                        </div>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
              </div>
              <div class="col-md-4">
                <div class="box box-primary laporan_hidden" hidden>
                    <div class="box-header with-border">
                      <h3 class="box-title">TENAGA KERJA</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-horizontal">
                        <div class="form-group">
                            <label for="jumlah_pm" class="col-sm-5 control-label">Project Manager:</label>
                            <div class="col-sm-7">
                              <input type="number" class="form-control" id="jumlah_pm" placeholder="Project Manager">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="jumlah_de" class="col-sm-5 control-label">Design Enginer:</label>
                            <div class="col-sm-7">
                              <input type="number" class="form-control" id="jumlah_de" placeholder="Design Enginer">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="jumlah_ppc" class="col-sm-5 control-label">PP Control:</label>
                            <div class="col-sm-7">
                              <input type="number" class="form-control" id="jumlah_ppc" placeholder="PP Control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="jumlah_spro" class="col-sm-5 control-label">Site Produksi:</label>
                            <div class="col-sm-7">
                              <input type="number" class="form-control" id="jumlah_spro" placeholder="Site Produksi">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="jumlah_labour" class="col-sm-5 control-label">Labour:</label>
                            <div class="col-sm-7">
                              <input type="number" class="form-control" id="jumlah_labour" placeholder="Labour">
                            </div>
                        </div>
                    </div>
                      <!-- <div class="box-footer">
                          <button type="submit" class="btn btn-default">Cancel</button>
                          <button type="submit" class="btn btn-info pull-right">Sign in</button>
                      </div> --><!-- /.box-footer -->
                </div>
              </div>

              <div class="col-md-4">
                <div class="box box-primary laporan_hidden" hidden>
                    <div class="box-header with-border">
                      <h3 class="box-title">CUACA</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-horizontal">
                        <div class="form-group">
                            <label for="cuaca_pagi" class="col-sm-3 control-label">Pagi:</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="cuaca_pagi" placeholder="Pagi">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-3 control-label">Siang:</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="cuaca_siang" placeholder="Siang">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cuaca_sore" class="col-sm-3 control-label">Sore:</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="cuaca_sore" placeholder="Sore">
                            </div>
                        </div>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
              </div>

              <div class="col-md-4">
                <div class="box box-primary laporan_hidden" hidden>
                    <div class="box-header with-border">
                      <h3 class="box-title">ALAT YANG DIGUNAKAN</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-horizontal">
                        <div class="form-group">
                            <label for="cuaca_pagi" class="col-sm-5 control-label">Mesin Sandblast:</label>
                            <div class="col-sm-7">
                              <input type="number" class="form-control" id="cuaca_pagi" placeholder="Mesin Sandblast">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-5 control-label">Mesin Las:</label>
                            <div class="col-sm-7">
                              <input type="number" class="form-control" id="cuaca_siang" placeholder="Mesin Las">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cuaca_sore" class="col-sm-5 control-label">Crane:</label>
                            <div class="col-sm-7">
                              <input type="number" class="form-control" id="cuaca_sore" placeholder="Crane">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cuaca_sore" class="col-sm-5 control-label">Mesin CNC:</label>
                            <div class="col-sm-7">
                              <input type="number" class="form-control" id="cuaca_sore" placeholder="Mesin CNC">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cuaca_sore" class="col-sm-5 control-label">Mesin Potong:</label>
                            <div class="col-sm-7">
                              <input type="number" class="form-control" id="cuaca_sore" placeholder="Mesin Potong">
                            </div>
                        </div>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
              </div>

              <div class="col-md-12">
                <div class="box box-primary laporan_hidden" hidden>
                    <div class="box-header with-border">
                      <h3 class="box-title">DATA PEKERJAAN</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <table class="table table-bordered table-striped" id="tabelinput">
                          <thead>
                            <tr>
                              <th class="text-center">NO</th>
                              <th class="text-center">JENIS PEKERJAAN<span style="color:red;"><b>*</b></span></th>
                              <th class="text-center">URAIAN PEKERJAAN</th>
                              <th class="text-center">JAM KERJA</th>
                              <th class="text-center">TENAGA KERJA<span style="color:red;"><b>*</b></span></th>
                            </tr>
                          </thead>
                          <tbody id="detail_pekerjaan">

                          </tbody>
                          <tfoot>
                            <tr>
                              <th class="text-center">NO</th>
                              <th class="text-center">JENIS PEKERJAAN<span style="color:red;"><b>*</b></span></th>
                              <th class="text-center">URAIAN PEKERJAAN</th>
                              <th class="text-center">JAM KERJA</th>
                              <th class="text-center">TENAGA KERJA<span style="color:red;"><b>*</b></span></th>
                            </tr>
                          </tfoot>
                        </table>
                        
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <div class="pull-right">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div><!-- /.box -->
              </div>
            </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <script type="text/javascript">
	    $(document).ready(function(){
	      	$('#list-kapal').DataTable();

          $("#workgroup").change(function()
          {  
              $("#detail_pekerjaan").html('');
              var row = $('#jumlah_pekerjaan').val();
              if(row =='' | row <= 0)
              {
                  alert('Silahkan isi jumlah peekerjaan dengan benar');
              }
              else
              {
                  for(var i=0; i<row; i++)
                  {
                      var kolom = '';
                      kolom += '<tr>';
                      kolom += '<td class="col-md-1"><input type="text" class="form-control"/></td>';
                      kolom += '<td class="col-md-2"><input type="text" class="form-control"/></td>';
                      kolom += '<td class="col-md-5"><input type="text" class="form-control"/></td>';
                      kolom += '<td class="col-md-2"><input type="text" class="form-control"/></td>';
                      kolom += '<td class="col-md-2"><input type="number" class="form-control"/></td>';
                      kolom += '<tr>';
                      $("#detail_pekerjaan").append(kolom);
                  }
                  $('.laporan_hidden').show();
              }
              
          });

          $(".input_number").keydown(function(event) {
              if ( event.keyCode == 46 || event.keyCode == 8 ) {

              }
              else {
                if (event.keyCode < 48 || event.keyCode > 57 ) {
                  event.preventDefault(); 
                } 
              }
          });
	    }); 

        
	</script>