<style type="text/css">
  .ui-datepicker-calendar {
        display: none;
        }
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Data Baseline</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <?php echo($menu); ?>

          <div class="row">
            <form action="<?php echo base_url()?>ManajemenKapalOS/saveBaseline" method="post">
            	<div class="col-xs-10">
                	<div class="box box-primary box-solid">
                      <div class="box-header">
                          <h3 class="box-title">Input Data Baseline</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                          <input type="hidden" name="id_kapal" value="<?php echo $id_kapal;?>">
                          <table class="table table-bordered table-striped" id="tabelinput">
                          <thead>
                            <tr>
                              <th class="text-center col-sm-1">NO</th>
                              <th class="text-center col-sm-5">PERIODE</th>
                              <th class="text-center col-sm-3">MID/END</th>
                              <th class="text-center col-sm-3">TARGET BOBOT</th>
                            </tr>
                          </thead>
                          <tbody id="data-baseline">
                          <?php 
                            $no = 0;
                            foreach ($vertex as $vertex) {
                                ++$no;
                          ?>
                               
                                <tr>
                                <input type="hidden" name="id_vertex<?=$no?>" value="<?php echo $vertex->id?>">
                                <td><input type="text" class="form-control" value="<?php echo $no?>"></td>
                                <td><input type="text" name="periode<?=$no?>" class="form-control" placeholder="Periode" value="<?php echo $vertex->periode?>" readonly></td>
                                <td><input type="text" name="mid_end<?=$no?>" class="form-control text-center" placeholder="mid / end" value="<?php echo ($vertex->mid_end == 0) ? 'mid' : 'end'; ?>" readonly></td>
                                <td><input type="number" min="0" name="progres<?=$no?>" class="form-control input_number text-center" placeholder="Bobot target" value="<?php echo $vertex->target?>"></td>
                                </tr>
                          <?php
                            }
                          ?>
                          </tbody>
                          <tfoot>
                            <tr>
                              <th class="text-center">NO</th>
                              <th class="text-center">MID/END</th>
                              <th class="text-center">PERIODE</th>
                              <th class="text-center">TARGET BOBOT</th>
                            </tr>
                          </tfoot>
                        </table>
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->

                  <div class="box box-primary">
                      <div class="box-footer">
                          <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                          <a style="margin-left:15px;margin-right:15px;" class="btn bg-navy pull-right" href="<?php echo base_url()?>dashboard" title="Kembali">Kembali</a>
                          <button style="margin-left:15px;" type="reset" class="btn btn-warning pull-right">Reset</button>
                      </div>
                  </div>
              </div>
            </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <script type="text/javascript">
	    // $(document).ready(function(){
     //      //tampilkan();
	    //   	$('.date-picker').datepicker( {
     //        changeMonth: true,
     //        changeYear: true,
     //        showButtonPanel: true,
     //        dateFormat: 'MM yy',
     //        onClose: function(dateText, inst) { 
     //            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
     //            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
     //            $(this).datepicker('setDate', new Date(year, month, 1));
     //        }
     //      });

          
          
	    // }); 

      // function tampilkan()
      // {
      //     var jum_bln = 12;
      //     var row = jum_bln*2;
      //     for(var i=0; i<row; i++)
      //     {
      //         var kolom = '';
      //         kolom += '<tr>';
      //         kolom += '<td><input type="text" class="form-control" value="'+(i+1)+'"></td>';
      //         kolom += '<td><input type="text" name="periode'+i+'" class="form-control date-picker" placeholder="Periode"></td>';
      //         kolom += '<td>';
      //         kolom += '<select class="form-control" name="mid_end'+i+'">';
      //         kolom += '<option style="display: none;">-- Mid / End --</option>';
      //         kolom += '<option value="0">Mid</option>';
      //         kolom += '<option value="1">End</option>';
      //         kolom += '</select>';
      //         kolom += '</td>';
      //         kolom += '<td><input type="number" name="progres'+i+'" class="form-control input_number" placeholder="Bobot target"></td>';
      //         kolom += '</tr>';
      //         $("#data-baseline").append(kolom);
      //     }
      // }
	</script>