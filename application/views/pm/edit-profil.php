<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Edit Profil</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">

          <form enctype="multipart/form-data" id="form1" action="<?php echo base_url()."profilowner/simpanedit/";?>" method="post" role="form">
            <!-- Left col -->
            <section class="col-lg-7 connectedSortable">
            	<div class="box box-primary box-solid">
	                <div class="box-header with-border">
	                  	<h3 class="box-title">Data Diri</h3>
	                  	<div class="box-tools pull-right">
	                    	<button class="btn btn-box-tool" data-widget="remove"></button>
	                  	</div><!-- /.box-tools -->
	                </div><!-- /.box-header -->
	                <div class="box-body">
			            <div class="form-group">
			                <label for="nama_owner">Nama</label>
			                <input type="text" class="form-control" id="nama_owner" name="nama_owner" placeholder="Nama" value="<?php echo $detailPM->nama?>">
			            </div>
			            <!-- <div class="form-group">
			                <label for="alamat_rumah">Alamat Rumah</label>
			                <input type="text" class="form-control" id="alamat_rumah" name="alamat_rumah" placeholder="Alamat Rumah" value="<?php //echo $detailPM->ALAMATDOKTER?>">
			            </div> -->
<!-- 			            <div class="form-group">
			                <label for="tgl_lahir">Tanggal Lahir</label>
			                <input type="text" class="form-control" id="tgl_lahir" name="tgl_lahir" placeholder="Tanggal Lahir" value="<?php //echo date('d-m-Y', strtotime($detailPM->TLDOKTER))?>">
			            </div> -->
			            <div class="form-group">
			                <label for="email_owner">Email</label>
			                <input type="email" class="form-control" id="email_owner" name="email_owner" placeholder="Email" value="<?php echo $detailPM->email?>">
			            </div>
			            <div class="form-group">
			                <label for="no_hp">No. HP</label>
			                <input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="No. HP" value="<?php echo $detailPM->telepon?>">
			            </div>
	                </div><!-- /.box-body -->
              	</div><!-- /.box -->
            </section><!-- right col -->

            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <section class="col-lg-7">
            	<div class="box box-primary">
	                <!-- <div class="box-header with-border">
	                
	                </div> --><!-- /.box-header -->
	                <div class="box-footer">
	                	<button type="submit" class="btn btn-primary pull-right">Simpan</button>
	                	<a style="margin-left:15px;margin-right:15px;" class="btn bg-navy pull-right" href="<?php echo base_url()?>dashboard" title="Kembali">Kembali</a>
                        <button style="margin-left:15px;" type="reset" class="btn btn-warning pull-right">Reset</button>
			        </div>
              	</div><!-- /.box -->
            </section>

          </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

    <script type="text/javascript">
	    $(document).ready(function(){
	      	$(".tgl").datepicker({
	            dateFormat:'dd-mm-yy',
	            onSelect: function(date) {
	                // $("#tgl_pesanan").val(date);
	                // $("#tgl_pesan").text(date);
	            },
	            
	        });
	    }); 
	</script>