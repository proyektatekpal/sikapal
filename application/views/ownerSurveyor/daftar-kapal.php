<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Daftar Kapal</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
              <div class="col-xs-12">
                  <div class="box box-primary box-solid">
                      <div class="box-header">
                          <h3 class="box-title">Daftar Proyek Kapal</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                          <table id="list-kapal" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>No. </th>
                                <th>Nama Proyek</th>
                                <th>Status Pengerjaan</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                              $num = 1;
                              foreach ($list_kapal as $kapal) {
                                  echo '<tr>';
                                  echo '<td>'.$num.'</td>';
                                  echo '<td>'.$kapal->nama_proyek.' <a target="_blank" class="pull-right" href="'.base_url().'ManajemenKapalOS/DetailKapal/'.$kapal->id.'" title="Detail"><i class="fa fa-chevron-circle-right"></i></a></td>';
                                  if($kapal->status_pengerjaan == 0)
                                    echo '<td>Sedang Dikerjakan</td>';
                                  else
                                    echo '<td>Sudah Selesai</td>';
                                  echo '</tr>';
                                  $num++;
                              }
                              ?>
                            </tbody>
                            <tfoot>
                              <tr>
                                <th>No. </th>
                                <th>Nama Proyek</th>
                                <th>Status Pengerjaan</th>
                              </tr>
                            </tfoot>
                          </table>
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>
            

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <script type="text/javascript">
	    $(document).ready(function(){
	      	$('#list-kapal').DataTable();
	    }); 
	</script>