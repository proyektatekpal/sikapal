<link rel="stylesheet" href="<?php echo base_url()?>assets/theme/plugins/lightbox2-master/css/lightbox.css">
<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>

<script src="<?php echo base_url()?>assets/theme/plugins/lightbox2-master/js/lightbox.js"></script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Detail Kapal</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <?php echo($menu); ?>

          <div class="row">
            <form action="" method="post" role="form">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Laporan Harian</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                </div><!-- /.box -->
              </div>

              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <table class="table table-bordered">
                        <tbody>
                          <tr>
                            <th>Tanggal</th>
                            <td><?= $tgl_laporan ?></td>
                          </tr>
                          <tr>
                            <th>Status</th>
                            <?php
                                    
                                          //echo '<tr>';
                                          //echo '<td>'.$data->tgl_laporan.'<a target="_blank" class="pull-right" href="'.base_url().'ManajemenKapalOS/DetailHarian/'.$kapal->id.'/'.$data->tgl_laporan.'/'.$data->verifikasi_owner.'" title="Detail"><i class="fa fa-chevron-circle-right"></i></a></td>';
                                          if($status == 0)
                                          {
                                            echo '<td><p style="color:red"><strong>Belum disetujui</strong></p></td>';
                                          }
                                          else
                                          {
                                            echo '<td><p><strong>Telah disetujui</strong></p></td>';
                                          }
                                          //echo '<tr>';
                                      
                                  ?>
                          </tr>
                           


                      </tbody>
                    </table>
                  </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Tenaga Kerja</h3>
                    </div>
                    <div class="box-body with-border">
                      <table class="table table-bordered">
                        <tbody>
                          <?php
                              foreach ($tenaga_kerja as $data) 
                              {
                                  echo '<tr>';
                                    echo '<th>'.$data->tenaga_kerja.'</th>';
                                    echo '<td>'.$data->jumlah.' orang</td>';
                                  echo '<tr>';
                              }   
                            ?>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Cuaca</h3>
                    </div>
                    <div class="box-body with-border">
                      <table class="table table-bordered">
                        <tbody>
                          <?php
                            foreach ($cuaca as $data) 
                              {
                                  echo '<tr>';
                                    echo '<th>Pagi</th>';
                                    echo '<td>'.$data->cuaca_pagi.'</td>';
                                  echo '<tr>'; 
                                  echo '<tr>';
                                    echo '<th>Pagi</th>';
                                    echo '<td>'.$data->cuaca_siang.'</td>';
                                  echo '<tr>';
                                  echo '<tr>';
                                    echo '<th>Pagi</th>';
                                    echo '<td>'.$data->cuaca_sore.'</td>';
                                  echo '<tr>'; 
                                }
                            ?>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                     <h3 class="box-title">Alat yg Digunakan</h3>
                    </div>
                    <div class="box-body with-border">
                      <table class="table table-bordered">
                        <tbody>
                          <?php
                            foreach ($alat as $data) 
                              {
                                  echo '<tr>';
                                    echo '<td>'.$data->nama_alat.'</td>';
                                    echo '<td>'.$data->jumlah_alat.'</td>';
                                  echo '<tr>'; 

                                  
                                }
                            ?>                          
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>



            <!-- <div class="col-md-12">
                <div class="box box-primary">          
                    <div class="box-body with-border">
                      <table class="table table-bordered">
                      <thead>
                              <tr>
                                <th>No. </th>
                                <th>Jenis Pekerjaan</th>
                                <th>Uraian Pekerjaan</th>
                                <th>Gambar</th>
                                <th>Tenaga Kerja</th>
                                
                              </tr>
                      </thead>
                      <tbody>
                           <tr>
                            <td>1</td>
                            <td>Produksi Block 116-01 </td>
                            <td><i>Marking dan cutting pelat konstruksi (Pemotongan dengan menggunakan mesin CNC dan manual). Pemasangan penegar pelat Double bottom</i></td>  
                            <td>
                              <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" data-lightbox="example-set" data-title="Contoh Gambar"><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt="" height="70px" width="70px" /></a>
                            </td>                 
                            <td>7 Orang</td>
                            
                          </tr>
                          <tr>
                            <td>2</td>
                            <td>Produksi Block 117-01</td>
                            <td><i>Jig fitting pelat main tank top (double bottom). Sandblasting dan Primer painting pelat</i></td>
                            <td>
                              <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" data-lightbox="example-set" data-title="Contoh Gambar"><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt="" height="70px" width="70px" /></a>
                            </td>
                            <td>5 Orang</td>
                          </tr>
                         <tr>
                            <td>3</td>
                            <td>Produksi Block 115-01</td>
                            <td><i>Marking dan cutting pelat konstruksi (Pemotongan dengan menggunakan mesin CNC dan manual). Pemasangan penegar pelat Double bottom</i></td>
                            <td>
                              <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" data-lightbox="example-set" data-title="Contoh Gambar"><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt="" height="70px" width="70px" /></a>
                            </td>
                            <td>5 Orang</td>
                          </tr>
                          <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><strong>20 Orang</strong></td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div> -->

            
        

            </form>
          </div><!-- /.row (main row) -->

          <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Pilih Workgroup dan Group</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-horizontal">
                      <div class="col-md-4">
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-5 control-label">Workgroup:</label>
                            <div class="col-sm-7">
                              <select name="workgroup" id="workgroup" class="form-control selectpicker scrollable-menu " role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Workgroup</option>
                                   <?php 
                                      foreach ($list_workgroup as $data) {
                                          echo '<option value='.$data->id.'>'.$data->nama_pekerjaan.'</option>';
                                          }
                                  ?>
                              </select>
                            </div>
                          </div>
                          <div class="form-group pilih_group_hidden" hidden>
                            <label for="cuaca_siang" class="col-sm-5 control-label">Group:</label>
                            <div class="col-sm-7">
                              <select name="group" id="group" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                 
                                   
                              </select>
                            </div>
                          </div>
                          <div class="button_hidden" hidden>
                            <a id="ok_button2" style="margin-left:15px;" class="btn bg-navy pull-right"  hidden>Ok</a>
                          </div>
                        </div>
                    </div>

                    <div class="box-body form-horizontal">
                      <div class="box box-primary laporan_hidden" hidden>
                        <div class="box-header with-border">
                            <h3 class="box-title">Data Laporan</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group" style="background-color:#eee; padding:10px;width:19%;float:right">
                                <div class="col-sm-12" id="div_group">
                                  <strong>Keterangan status :</strong>
                                </div>
                                <div class="col-sm-11 col-sm-offset-1" id="div_group">
                                  <strong>v = diterima </strong>
                                </div>
                                <div class="col-sm-11 col-sm-offset-1" id="div_group">
                                  <strong>x = belum / ditolak </strong>
                                </div>
                            </div>
                            <table class="table table-bordered table-striped" id="tabelinput">
                              <thead>
                                <tr>
                                  <th class="text-center">NO</th>
                                  <th class="text-center">Proses</th>
                                  <th class="text-center">Hasil/Rekomendasi</th>
                                  <th class="text-center">Status</th>
                                  <!-- <th class="text-center">Foto</th> -->
                                </tr>
                              </thead>
                              <tbody id="data_laporan">

                              </tbody>
                            </table>
                            <table class="table table-bordered">
                              <tbody>
                                <tr>
                                  <th>Catatan :</th>
                                  <td><input name="catatan" type="text" class="form-control" value="" id="catatan" readonly/></td>
                                 </tr>
                                </tbody>
                            </table>       
                        </div><!-- /.box-body -->


                        <!-- <div class="box-footer">
                            <div class="pull-right">
                              <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div> -->
                      </div><!-- /.box -->
                    </div>
                    
                </div><!-- /.box -->

        </section><!-- /.content -->
 </div><!-- /.content-wrapper -->

<script type="text/javascript">
	    $(document).ready(function()
	    {	
	    		 $('#workgroup').on('change', function() {
            document.getElementById('group').innerHTML = '';
            var id_workgroup = $('#workgroup').val();
            var kolom = '';
            kolom += '<option style="display: none;">Pilih Group</option>';
            //$("#group").append(kolom);
            //alert(id_workgroup);
            $.ajax({
                        url: '<?php echo base_url()?>ManajemenKapalOS/GetGroup/', 
                        dataType: 'json',
                        type: 'POST',
                        async: false,
                        data: {"id_workgroup_kapal":id_workgroup},
                        success: function(data) {
                          $.each(data, function(i,item){
                            //alert(item.id);                            
                            if (item.id != 'empty') {

                            kolom +=  '<option value="'+item.id+'">'+item.group+'</option>';
                            $("#group").append(kolom);
                            kolom='';                         
                            } 
                            else {}
                          });                
                        }
                      });
            $('.pilih_group_hidden').show();
            $('.button_hidden').show();                          
          });

					$("#ok_button2").click(function()
          {
            document.getElementById('data_laporan').innerHTML = '';
            var e = document.getElementById("group");
            var id_group = e.options[e.selectedIndex].value;
            var id_workgroup = $('#workgroup').val();
            var num = 1;
            var kolom = '';
            var baseurl = "<?php print base_url(); ?>";

            //document.getElementById('data_laporan').innerHTML = '';
            $.ajax({
                        url: '<?php echo base_url()?>ManajemenKapalOS/getDataAktifitas/', 
                        dataType: 'json',
                        type: 'POST',
                        async: false,
                        data: {"id_group":id_group, "id_workgroup":id_workgroup},
                        success: function(data) 
                        {                      
                          $.each(data, function(i,item)
                          {
                            //alert(item.id);                            
                            if (item.id != 'empty') 
                            {
                              //mengolah data item
                              var item_split = item.item.split('#');
                              var count_item = item_split.length;

                              var data_item = item_split[0];
                              var data_item_split = data_item.split('|');
                              var nama_item = data_item_split[0];
                              var status_item = data_item_split[1];
                              // alert(status_item);

                              if(item.flag == 0)
                              {                            
                                //untuk baris pertama
                                kolom = '';
                                kolom += '<tr>';
                                kolom += '<input type="hidden" name="aktifitas.id'+(num)+'" value="'+item.aktifitas_id+'">';
                                kolom += '<td class="col-md-1"><input type="text" class="form-control" value="'+(num)+'" /></td>';
                                kolom += '<td class="col-md-2"><input type="text" value="'+ item.aktifitas+'" class="form-control" /></td>';
                                kolom += '<td class="col-md-4"><input type="text" value="'+nama_item+'" name="item'+(num)+''+0+'" class="form-control" /></td>';
                                kolom += '<td><select name="status'+(num)+''+ 0 +'" id="status'+(num)+''+(0)+'" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">';
                                kolom +=            '<option style="display: none;">Status</option>';
                                kolom +=            '<option value=0>x</option>';
                                kolom +=            '<option value=1>v</option>';
                                kolom += '</select></td>';
                                //kolom += '<td class="col-md-2"><center><a class="example-image-link center-block" href="'+baseurl +'upload/' +item.link_foto+'" data-lightbox="example-set" data-title="'+item.link_foto+'"><img class="example-image" src="'+baseurl +'upload/' +item.link_foto+'" alt="" height="70px" width="85px" /></center></td>';
                                
                                // kolom += '<td class="col-md-1"><input type="file" value="'+item.link_foto+'"name="foto'+(num)+'" id="foto"></td>';
                                kolom += '<tr>';

                                $("#data_laporan").append(kolom);
                                $("#status"+(num)+""+0).val(status_item);
                                // alert("#status"+(num)+"0");
                                // $("#status10").val(status_item);

                                for(var a=1; a<count_item-1; a++)
                                {
                                  var data_item = item_split[a];
                                  var data_item_split = data_item.split('|');
                                  var nama_item = data_item_split[0];
                                  var status_item = data_item_split[1];
                                  
                                  //untuk baris selanjutnya
                                    kolom = '';
                                    kolom += '<tr>';
                                    kolom += '<input type="hidden" name="aktifitas.id'+(num)+'" value="'+item.aktifitas_id+'">';
                                    kolom += '<td class="col-md-1"></td>';
                                    kolom += '<td class="col-md-2"></td>';
                                    kolom += '<td class="col-md-4"><input type="text" value="'+nama_item+'" name="item'+(num)+''+(a)+'" class="form-control" /></td>';
                                    kolom += '<td><select name="status'+(num)+''+(a)+'" id="status'+(num)+''+(a)+'" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3" >';
                                    kolom +=            '<option style="display: none;">Status</option>';
                                    kolom +=            '<option value=0>x</option>';
                                    kolom +=            '<option value=1>v</option>';
                                    kolom += '</select></td>';
                                    //kolom += '<td class="col-md-1"></td>';
                                    // kolom += '<td class="col-md-2"><center><a class="example-image-link center-block" href="'+baseurl +'upload/' +item.link_foto+'" data-lightbox="example-set" data-title="'+item.link_foto+'"><img class="example-image" src="'+baseurl +'upload/' +item.link_foto+'" alt="" height="70px" width="85px" /></center></td>';
                                    kolom += '<tr>';

                                    $("#data_laporan").append(kolom);
                                    $("#status"+(num)+""+a).val(status_item);
                                    // $("#status"+(num)+""+(a)).val(status_item);
                                }
                              }
                              else
                              {
                                if(status_item == 0)
                                {
                                  //untuk baris pertama
                                  kolom = '';
                                  kolom += '<tr>';
                                  kolom += '<input type="hidden" name="aktifitas.id'+(num)+'" value="'+item.aktifitas_id+'">';
                                  kolom += '<input type="hidden" name="hidden_gambar'+(num)+'" value="'+ item.link_foto+'" class="form-control" />';
                                  kolom += '<td class="col-md-1"><input type="text" class="form-control" value="'+(num)+'" /></td>';
                                  kolom += '<td class="col-md-2"><input type="text" value="'+ item.aktifitas+'" class="form-control" /></td>';
                                  kolom += '<td class="col-md-4"><input type="text" value="'+nama_item+'" name="item'+(num)+''+0+'" class="form-control" /></td>';
                                  kolom += '<td><select name="status'+(num)+''+ 0 +'" id="status'+(num)+''+(0)+'" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">';
                                  kolom +=            '<option style="display: none;">Status</option>';
                                  kolom +=            '<option value=0>x</option>';
                                  kolom +=            '<option value=1>v</option>';
                                  kolom += '</select></td>';
                                  //kolom += '<td class="col-md-2"><center><a class="example-image-link center-block" href="'+baseurl +'upload/' +item.link_foto+'" data-lightbox="example-set" data-title="'+item.link_foto+'"><img class="example-image" src="'+baseurl +'upload/' +item.link_foto+'" alt="" height="70px" width="85px" /></center></td>';
                                  // kolom += '<td class="col-md-1"><input type="file" value="'+item.link_foto+'"name="foto'+(num)+'" id="foto"></td>';
                                  kolom += '<tr>';

                                  $("#data_laporan").append(kolom);
                                  $("#status"+(num)+""+0).val(status_item);
                                  
                                  for(var a=1; a<count_item-1; a++)
                                  {
                                    var data_item = item_split[a];
                                    var data_item_split = data_item.split('|');
                                    var nama_item = data_item_split[0];
                                    var status_item = data_item_split[1];
                                    
                                    //untuk baris selanjutnya
                                      kolom = '';
                                      kolom += '<tr>';
                                      kolom += '<input type="hidden" name="aktifitas.id'+(num)+'" value="'+item.aktifitas_id+'">';
                                      kolom += '<td class="col-md-1"></td>';
                                      kolom += '<td class="col-md-2"></td>';
                                      kolom += '<td class="col-md-4"><input type="text" value="'+nama_item+'" name="item'+(num)+''+(a)+'" class="form-control" /></td>';
                                      kolom += '<td><select name="status'+(num)+''+(a)+'" id="status'+(num)+''+(a)+'" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3" >';
                                      kolom +=            '<option style="display: none;">Status</option>';
                                      kolom +=            '<option value=0>x</option>';
                                      kolom +=            '<option value=1>v</option>';
                                      kolom += '</select></td>';
                                      // kolom += '<td class="col-md-2"><center><a class="example-image-link center-block" href="'+baseurl +'upload/' +item.link_foto+'" data-lightbox="example-set" data-title="'+item.link_foto+'"><img class="example-image" src="'+baseurl +'upload/' +item.link_foto+'" alt="" height="70px" width="85px" /></center></td>';
                                      //kolom += '<td class="col-md-1"></td>';
                                      kolom += '<tr>';

                                      $("#data_laporan").append(kolom);
                                      $("#status"+(num)+""+a).val(status_item);
                                  }
                                }
                                else
                                {
                                  //untuk baris pertama
                                  kolom = '';
                                  kolom += '<tr>';
                                  kolom += '<input type="hidden" name="aktifitas.id'+(num)+'" value="'+item.aktifitas_id+'">';
                                  kolom += '<input type="hidden" name="hidden_gambar'+(num)+'" value="'+ item.link_foto+'" class="form-control" readonly/>';
                                  kolom += '<td class="col-md-1"><input type="text" class="form-control" value="'+(num)+'" readonly/></td>';
                                  kolom += '<td class="col-md-2"><input type="text" value="'+ item.aktifitas+'" class="form-control" readonly/></td>';
                                  kolom += '<td class="col-md-4"><input type="text" value="'+nama_item+'" name="item'+(num)+''+0+'" class="form-control" readonly/></td>';
                                  kolom += '<td><select name="status'+(num)+''+ 0 +'" id="status'+(num)+''+(0)+'" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3" readonly>';
                                  kolom +=            '<option style="display: none;">Status</option>';
                                  // kolom +=            '<option value=0>Ditolak</option>';
                                  kolom +=            '<option value=1>v</option>';
                                  kolom += '</select></td>';
                                  //kolom += '<td class="col-md-2"><center><a class="example-image-link center-block" href="'+baseurl +'upload/' +item.link_foto+'" data-lightbox="example-set" data-title="'+item.link_foto+'"><img class="example-image" src="'+baseurl +'upload/' +item.link_foto+'" alt="" height="70px" width="85px" /></center></td>';
                                  // kolom += '<td class="col-md-1"><input type="file" value="'+item.link_foto+'"name="foto'+(num)+'" id="foto"></td>';
                                  kolom += '<tr>';

                                  $("#data_laporan").append(kolom);
                                  $("#status"+(num)+""+0).val(status_item);
                                  
                                  for(var a=1; a<count_item-1; a++)
                                  {
                                    var data_item = item_split[a];
                                    var data_item_split = data_item.split('|');
                                    var nama_item = data_item_split[0];
                                    var status_item = data_item_split[1];
                                    
                                    //untuk baris selanjutnya
                                      kolom = '';
                                      kolom += '<tr>';
                                      kolom += '<input type="hidden" name="aktifitas.id'+(num)+'" value="'+item.aktifitas_id+'">';
                                      kolom += '<td class="col-md-1"></td>';
                                      kolom += '<td class="col-md-2"></td>';
                                      if(status_item == 0)
                                      {
                                        kolom += '<td class="col-md-4"><input type="text" value="'+nama_item+'" name="item'+(num)+''+(a)+'" class="form-control" /></td>';
                                        kolom += '<td><select name="status'+(num)+''+(a)+'" id="status'+(num)+''+(a)+'" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3" >';
                                        kolom +=            '<option style="display: none;">Status</option>';
                                        kolom +=            '<option value=0>x</option>';
                                        kolom +=            '<option value=1>v</option>';
                                        kolom += '</select></td>';
                                        // kolom += '<td class="col-md-2"><center><a class="example-image-link center-block" href="'+baseurl +'upload/' +item.link_foto+'" data-lightbox="example-set" data-title="'+item.link_foto+'"><img class="example-image" src="'+baseurl +'upload/' +item.link_foto+'" alt="" height="70px" width="85px" /></center></td>';
                                        //kolom += '<td class="col-md-1"></td>';
                                        kolom += '<tr>';
                                      }
                                      else
                                      {
                                        kolom += '<td class="col-md-4"><input type="text" value="'+nama_item+'" name="item'+(num)+''+(a)+'" class="form-control" readonly/></td>';
                                        kolom += '<td><select name="status'+(num)+''+(a)+'" id="status'+(num)+''+(a)+'" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3" readonly>';
                                        kolom +=            '<option style="display: none;">Status</option>';
                                        // kolom +=            '<option value=0>Ditolak</option>';
                                        kolom +=            '<option value=1>v</option>';
                                        kolom += '</select></td>';
                                        // kolom += '<td class="col-md-2"><center><a class="example-image-link center-block" href="'+baseurl +'upload/' +item.link_foto+'" data-lightbox="example-set" data-title="'+item.link_foto+'"><img class="example-image" src="'+baseurl +'upload/' +item.link_foto+'" alt="" height="70px" width="85px" /></center></td>';
                                        //kolom += '<td class="col-md-1"></td>';
                                        kolom += '<tr>';     
                                      }

                                      $("#data_laporan").append(kolom);
                                      $("#status"+(num)+""+a).val(status_item);
                                  }
                                }  
                              }
                              kolom = '';
                              kolom += '<input type="hidden" name="loop_item'+(num-1)+'" value="'+(count_item-1)+'">';
                              $("#data_laporan").append(kolom);
                              $('#catatan').attr('value',item.catatan_pm);
                              $('#tanggapan').attr('value',item.tanggapan_pm);
                                //$("#status"+(num)+"").val(item.status_pekerjaan);
                                //$('#status'+(num)+' :nth-child(4)').prop('selected', true);
                              num++;
                            } 
                            else {}
                          }); 
                        }
                  }); //end of ajax

            kolom =  '';
            kolom += '<input type="hidden" name="loop" value="'+(num-1)+'">';
            $("#data_laporan").append(kolom);
            $('.laporan_hidden').show();

            // //var id_group = $('#group').val();
            // var e = document.getElementById("group");
            // var id_group = e.options[e.selectedIndex].value;
            // var id_workgroup = $('#workgroup').val();
            // var num = 1;
            // var baseurl = "<?php print base_url(); ?>";

            // document.getElementById('data_laporan').innerHTML = '';
            // $('#catatan').attr('value','');
            // $('#tanggapan').attr('value','');
            // $.ajax({
            //             url: '<?php echo base_url()?>ManajemenKapalOS/getDataAktifitas/', 
            //             dataType: 'json',
            //             type: 'POST',
            //             async: false,
            //             data: {"id_group":id_group, "id_workgroup":id_workgroup},
            //             success: function(data) {                      
            //               $.each(data, function(i,item)
            //               {
            //                 //alert(item.id);                            
            //                 if (item.id != 'empty') {
            //                   if(item.flag == 0)
            //                   {
            //                     //alert("df");
            //                     var kolom = '';
            //                     kolom += '<tr>';
            //                     kolom += '<input type="hidden" name="aktifitas.id'+(num)+'" value="'+item.aktifitas_id+'" readonly>';
            //                     kolom += '<td class="col-md-1"><input type="text" class="form-control" value="'+(num)+'" readonly/></td>';
            //                     kolom += '<td class="col-md-2"><input type="text" value="'+ item.aktifitas+'" class="form-control" readonly/></td>';
                               
            //                     kolom += '<td class="col-md-4"><input type="text" value="" name="deskripsi'+(num)+'" class="form-control" readonly/></td>';
            //                     kolom += '<td class="col-md-2"><center><a class="example-image-link center-block" href="'+baseurl +'upload/' +item.link_foto+'" data-lightbox="example-set" data-title="'+item.link_foto+'"><img class="example-image" src="'+baseurl +'upload/' +item.link_foto+'" alt="" height="70px" width="85px" /></center></td>';
            //                     kolom += '<td><select name="status'+(num)+'" id="status" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3" readonly>';
            //                     // kolom +=            '<option style="display: none;">Status</option>';
            //                     kolom +=            '<option value=0>Belum dikerjakan</option>';
            //                     // kolom +=            '<option value=1>Sedang dikerjakan</option>';
            //                     // kolom +=            '<option value=2>Selesai</option>';
            //                     kolom += '</select></td>';
            //                     kolom += '<tr>';
            //                   }
            //                   else
            //                   {
            //                     var kolom = '';
            //                     kolom += '<tr>';
            //                     kolom += '<input type="hidden" name="aktifitas.id'+(num)+'" value="'+item.aktifitas_id+'">';
            //                     kolom += '<td class="col-md-1"><input type="text" class="form-control" value="'+(num)+'" readonly/></td>';
            //                     kolom += '<td class="col-md-2"><input type="text" value="'+ item.aktifitas+'" class="form-control" readonly/></td>';
            //                     kolom += '<input type="hidden" name="hidden_gambar'+(num)+'" value="'+ item.link_foto+'" class="form-control" readonly/>';
            //                     if(item.status_pekerjaan != 2)
            //                     {
            //                       kolom += '<td class="col-md-4"><input type="text" value="'+item.deskripsi+'" name="deskripsi'+(num)+'" class="form-control " readonly/></td>';
            //                       kolom += '<td class="col-md-2"><center><a class="example-image-link center-block" href="'+baseurl +'upload/' +item.link_foto+'" data-lightbox="example-set" data-title="'+item.link_foto+'"><img class="example-image" src="'+baseurl +'upload/' +item.link_foto+'" alt="" height="70px" width="85px" /></center></td>';
            //                       kolom += '<td><select name="status'+(num)+'" id="status'+(num)+'" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3" readonly>';
            //                       kolom +=            '<option style="display: none;">Status</option>';
            //                       kolom +=            '<option value=0>Belum dikerjakan</option>';
            //                       kolom +=            '<option value=1>Sedang dikerjakan</option>';
            //                       kolom +=            '<option value=2>Selesai</option>';
            //                       kolom += '</select></td>';
            //                       kolom += '<tr>';
            //                     }
            //                     else
            //                     {
            //                       kolom += '<td class="col-md-4"><input type="text" value="'+item.deskripsi+'" name="deskripsi'+(num)+'" class="form-control" readonly/></td>';
            //                       kolom += '<td class="col-md-2"><center><a class="example-image-link center-block" href="'+baseurl +'upload/' +item.link_foto+'" data-lightbox="example-set" data-title="'+item.link_foto+'"><img class="example-image" src="'+baseurl +'upload/' +item.link_foto+'" alt="" height="70px" width="85px" /></a></center></td>';
            //                       kolom += '<td><select name="status'+(num)+'" id="status'+(num)+'" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3" readonly>';
            //                       kolom +=            '<option style="display: none;">Status</option>';
            //                       kolom +=            '<option value=2>Selesai</option>';
            //                       kolom += '</select></td>';
            //                       kolom += '<tr>';
            //                     }
            //                   }
                                                            
            //                   $("#data_laporan").append(kolom);
            //                   $("#status"+(num)+"").val(item.status_pekerjaan);
            //                   $('#catatan').attr('value',item.catatan_pm);
            //                   $('#tanggapan').attr('value',item.tanggapan_pm);
            //                   //$('#status'+(num)+' :nth-child(4)').prop('selected', true);
            //                   num++;
            //                 } 
            //                 else {}
            //               }); 
            //             }
            //           });

            // var kolom =  '';
            // kolom += '<input type="hidden" name="loop" value="'+(num-1)+'">';
            // $("#data_laporan").append(kolom);
            // $('.laporan_hidden').show();            
          });

	    		// var jlh_workgroup = $('#jlh_workgroup').val();
	    		
	    		// for(a=0; a<jlh_workgroup; a++)
	    		// {
	    		// 	var id = $("body").find('#id_workgroup' + a);
	    		// 	var id_workgroup = $(id).val();
	    		// 	var kolom= ''	;

	    		// 	$.ajax({
				   //             url: '<?php echo base_url()?>ManajemenKapalOS/GetWorkgroup/', 
				   //                      dataType: 'json',
				   //                      type: 'POST',
				   //                      async: false,
				   //                      // data: {"id_workgroup_kapal":id_workgroup},
				   //                      success: function(data) {
				   //                        $.each(data, function(i,item){   
					  //                         kolom += '<div class="col-md-12">';                        
					  //                           kolom += '<div class="box box-success collapsed-box">';
							// 					             		kolom += '<div class="box-header with-border">';
							// 					             			kolom += '<h3 class="box-title">'+item.nama_pekerjaan+'</h3>';
							// 					              		kolom += '<div class="box-tools pull-right">';
							// 					                		kolom += '<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>';
							// 					              		kolom += '</div>';
							// 					            		kolom += '</div>';

							// 						            	kolom += '<div class="box-body form-horizontal" id="aktivitas" >';
							// 						            	kolom += '</div>';
							// 						            kolom += '</div>';
							// 						           kolom += '</div>';

							// 					            $('#render').html('');
							// 					            $('#render').append(kolom);
							// 					            //kolom = '';
				   //                        });                
				   //                      }
				   //                    });

	    		// }
		

				}); 
</script>