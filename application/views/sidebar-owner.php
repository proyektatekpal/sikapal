      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url()?>assets/theme/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>Alexander Pierce</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Profil</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>profilOwner/editprofil"><i class="fa fa-circle-o"></i>Edit Profil</a></li>
                <li><a href="<?php echo base_url()?>profilOwner/ubahpassword"><i class="fa fa-circle-o"></i>Ubah Password</a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Kapal</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <!-- <li><a href="<?php echo base_url()?>ManajemenKapalOwner/tambahkapal"><i class="fa fa-circle-o"></i>Tambah kapal</a></li> -->
                <li><a href="<?php echo base_url()?>Dashboard"><i class="fa fa-circle-o"></i>Dashboard</a></li>
                <li><a href="<?php echo base_url()?>ManajemenKapalOwner/daftarkapal"><i class="fa fa-circle-o"></i>Daftar kapal</a></li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>