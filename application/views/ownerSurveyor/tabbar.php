 <nav class="navbar navbar-default">
              <div class="container-fluid">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button> -->
                    <a class="navbar-brand" href="<?php echo base_url()."ManajemenKapalOS/DetailKapal/".$kapal->id;?>"><strong>Detail</strong></a>
                  </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                      <!-- <li><a href="<?php echo base_url()."ManajemenKapalOS/LihatGambar/".$kapal->id;?>">Gambar</a></li> -->
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">S-Curve<b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url()."ManajemenKapalOS/InputBaseline/".$kapal->id;?>">Input Baseline</a></li>
                            <li><a href="<?php echo base_url()."ManajemenKapalOS/LihatSCurve/".$kapal->id;?>">lihat S-Curve</a></li>
                          </ul>
                      </li>
                       <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Laporan <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url()."ManajemenKapalOS/InputLaporanHarian/".$kapal->id;?>">Input Laporan Harian</a></li>
                            <li><a href="<?php echo base_url()."ManajemenKapalOS/LaporanHarian/".$kapal->id;?>">Lihat Laporan Harian</a></li>
                            <li><a href="<?php echo base_url()."ManajemenKapalOS/InputLaporanMingguan/".$kapal->id;?>">Input Laporan 2 Minggu</a></li>
                            <li><a href="<?php echo base_url()."ManajemenKapalOS/LaporanMingguan/".$kapal->id;?>">Lihat Laporan 2 Minggu</a></li>
                          </ul>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Group<b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url()."ManajemenKapalOS/AddGroup/".$kapal->id;?>">Tambah</a></li>
                            <li><a href="<?php echo base_url()."ManajemenKapalOS/LihatGroup/".$kapal->id;?>">Daftar Group</a></li>
                          </ul>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Sub Workgroup<b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="<?php// echo base_url()."ManajemenKapalOS/AddSubWorkGroup/".$kapal->id;?>">Tambah</a></li>
                            <li><a href="<?php// echo base_url()."ManajemenKapalOS/LihatSubWorkGroup/".$kapal->id;?>">Daftar Sub Workgroup</a></li>
                          </ul>
                      </li> -->
                      
                      
                    </ul>
                  </div><!-- /.navbar-collapse -->

              </div><!-- /.container-fluid -->
          </nav>