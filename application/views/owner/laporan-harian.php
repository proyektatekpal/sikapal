<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
              <i class="fa fa-edit"></i> <strong>Detail Kapal</strong>
          </h1>
          <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <nav class="navbar navbar-default">
              <div class="container-fluid">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url()."manajemenKapalOwner/detailKapal/".$kapal->id?>"><strong><?php echo $kapal->nama_proyek;?></strong></a>
                  </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                      <!-- <li><a href="<?php echo base_url().'manajemenKapalOwner/Gambar/'.$kapal->id?>">Gambar</a></li> -->
                      <li><a href="<?php echo base_url().'manajemenKapalOwner/SCurve/'.$kapal->id?>">S-Curve</a></li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Laporan <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url().'manajemenKapalOwner/LaporanHarian/'.$kapal->id?>">Harian</a></li>
                            <li><a href="<?php echo base_url().'manajemenKapalOwner/LaporanMingguan/'.$kapal->id?>">2 Mingguan</a></li>
                          </ul>
                      </li>
                      <!-- <li><a href="#">Pesan</a></li> -->
                    </ul>
                  </div><!-- /.navbar-collapse -->

              </div><!-- /.container-fluid -->
          </nav>
                
          <div class="row">
            <form action="" method="post" role="form">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Laporan Harian</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                          <table id="list-kapal" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>Tanggal Laporan</th>
                                <th>Status</th>
                              </tr>
                            </thead>
                            <tbody>
                                  <?php
                                      foreach ($tanggal_laporan as $data) 
                                      {
                                          echo '<tr>';
                                          echo '<td>'.$data->tgl_laporan.'<a target="_blank" class="pull-right" href="'.base_url().'ManajemenKapalOwner/DetailHarian/'.$kapal->id.'/'.$data->tgl_laporan.'/'.$data->verifikasi_owner.'" title="Detail"><i class="fa fa-chevron-circle-right"></i></a></td>';
                                          if($data->verifikasi_owner==0)
                                          {
                                            echo '<td>Belum Terverikasi</td>';
                                          }
                                          else
                                          {
                                            echo '<td>Sudah Terverikasi</td>';
                                          }
                                          echo '<tr>';
                                      }
                                  ?>

                              <!-- <tr>
                                <td>Selasa, 17 September 2015<a target="_blank" class="pull-right" href="<?php echo base_url().'ManajemenKapalOS/DetailHarian/'.$kapal->id?>" title="Detail"><i class="fa fa-chevron-circle-right"></i></a></td>
                                <td>Belum Disetujui</td>
                              </tr>
                              <tr>            
                              <td>Rabu, 18 September 2015 <a target="_blank" class="pull-right" href="<?php echo base_url()?>ManajemenKapalOS/DetailHarian" title="Detail"><i class="fa fa-chevron-circle-right"></i></a></td>
                                <td>Belum Disetujui</td>
                              </tr> -->
                            </tbody>
                            <tfoot>
                              <tr>
                                 <th>Tanggal Laporan</th>
                                 <th>Status</th>
                              </tr>
                            </tfoot>
                          </table>
                      </div><!-- /.box-body -->

                </div><!-- /.box -->
              </div>
            </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

 