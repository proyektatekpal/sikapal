<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class Laporan_mingguan extends CI_Model
{

	protected $table = "laporan_mingguan";
    protected $all_field = 'laporan_mingguan.id,id_group,tgl_input,periode,mid_end,progres_group,verif_owner,os_verifikator,class_verifikator,qa_verifikator,catatan_os,catatan_owner';

    public function __construct()
    {
        parent::__construct();
    }
  
  
    public function get($where=NULL)
    {
        
        $this->db->select($this->all_field);
        if(!empty($where)){
            $this->db->where($where);
        }
        $this->db->from($this->table);
        $this->db->order_by("str_to_date(periode,'%M') ASC, mid_end ASC");
        $res = $this->db->get();
        // if($res->num_rows()==1){
        //     return $res->row();
        // }
        // else{
            
        // }
        return $res->result();
    }

    public function join_workgroup()
    {
        $this->db->select('id_workgroup_kapal,group,bobot_workgroup,kuantitas,satuan');
        $this->db->select('id_kapal,workgroup_kapal.id_workgroup');
        $this->db->select('bobot, nama_pekerjaan');
        $this->db->join('group', 'group.id = laporan_mingguan.id_group', 'left');
        $this->db->join('workgroup_kapal', 'workgroup_kapal.id = group.id_workgroup_kapal', 'left');
        $this->db->join('workgroup', 'workgroup.id_workgroup = workgroup_kapal.id_workgroup', 'left');
    }

    public function join_all()
    {
        $this->db->select('id_workgroup_kapal,group,bobot_workgroup,kuantitas,satuan');
        $this->db->select('id_kapal,workgroup_kapal.id_workgroup');
        $this->db->select('bobot, nama_pekerjaan');
        $this->db->select('nama_proyek,pemilik,jenis_kapal,lama_pengerjaan,tanggal_mulai,status_pengerjaan,id_os,id_galangan,id_owner');
        $this->db->join('group', 'group.id = laporan_mingguan.id_group', 'left');
        $this->db->join('workgroup_kapal', 'workgroup_kapal.id = group.id_workgroup_kapal', 'left');
        $this->db->join('workgroup', 'workgroup.id_workgroup = workgroup_kapal.id_workgroup', 'left');
        $this->db->join('pembuatan_kapal', 'pembuatan_kapal.id = workgroup_kapal.id_kapal', 'left');
    }

    public function insert($data)
    {
    	return $this->db->insert($this->table, $data);
    }

    public function update($data, $where)
    {
        $this->db->where($where);
        return $this->db->update($this->table, $data);
    }

    public function getMaxID()
    {
        $this->db->select_max('id');
        $query = $this->db->get($this->table);
        return $query->row()->id;
    }
}  

?>