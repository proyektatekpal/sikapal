<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManajemenKapalPM extends CI_Controller {

	public function __construct()
    {
        parent::__construct(); 
        if ( !$this->session->userdata('isLogin') && $this->session->userdata('level') != 'owner')
        { 
            redirect('access');
        }
        // $this->load->library('form_validation');
        $this->load->model('m_profil');
        $this->load->model('m_manajemen');
    }

	public function index()
	{        
	}

    public function TambahKapal()
    {
        $this->load->view('header');
        $this->load->view('sidebar-pm');
        $this->load->view('pm/tambah-kapal');
        $this->load->view('footer');
    }

    public function DaftarKapal()
    {
        $email = $this->session->userdata('email');
        $detail_pm = $this->m_manajemen->getDetailPM($email);
        $id_pm = $detail_pm->id_pm;
        $Data['list_kapal'] = $this->m_manajemen->getKapalPM($id_pm);

        $this->load->view('header');
        $this->load->view('sidebar-pm');
        $this->load->view('pm/daftar-kapal', $Data);
        $this->load->view('footer');
    }

    public function DetailKapal($id_pembuatan)
    {
        $Data['kapal'] = $this->m_manajemen->getDetailKapal($id_pembuatan);
        $this->load->view('header');
        $this->load->view('sidebar-pm');
        $this->load->view('pm/detail-kapal', $Data);
        $this->load->view('footer');
    }

    public function Gambar($id_pembuatan)
    {
        $Data['kapal'] = $this->m_manajemen->getDetailKapal($id_pembuatan);

        //print_r($Data['kapal'] );
        $this->load->view('header');
        $this->load->view('sidebar-pm');
        $this->load->view('pm/gambar', $Data);
        $this->load->view('footer');
    }

    public function SCurve($id_pembuatan)
    {
        $baseline = $this->m_manajemen->getDetailBaseline($id_pembuatan);
        $label = array();
        $bobot = array();
        foreach ($baseline as $b) {
            if($b->mid_end == 0){
                $label[] = 'mid '.$b->periode;
            }
            else{
                $label[] = 'end '.$b->periode;
            }
            $bobot[] = $b->bobot;
        }

        $Data['kapal'] = $this->m_manajemen->getDetailKapal($id_pembuatan);
        $Data['label'] = json_encode($label);
        $Data['bobot_baseline'] = json_encode($bobot);

        $this->load->view('header');
        $this->load->view('sidebar-pm');
        $this->load->view('pm/s-curve', $Data);
        $this->load->view('footer');
    }

    // public function LapHarian($id_pembuatan)
    // {
    //     $DataLaporan['kapal'] = $this->m_manajemen->getDetailKapal($id_pembuatan);
    //     $DataLaporan['jenis_pekerja'] = $this->m_manajemen->getPekerja();
    //     $DataLaporan['jenis_alat'] = $this->m_manajemen->getAlat();
    //     $this->load->view('header');
    //     $this->load->view('sidebar-pm');
    //     $this->load->view('pm/laporan-harian', $DataLaporan);
    //     $this->load->view('footer');
    // }

    // public function LapBulanan($id_pembuatan)
    // {
    //     $this->load->view('header');
    //     $this->load->view('sidebar-pm');
    //     $this->load->view('pm/laporan-bulanan');
    //     $this->load->view('footer');
    // }

    public function getDataPekerja()
    {
        $pekerja = $this->m_manajemen->getPekerja();
        $json = array();
        foreach ($pekerja as $pekerja) {
            $json[] = array('id'  => $pekerja->id_pekerja,
                            'nama' => $pekerja->jenis_pekerja
            );
        }

        echo json_encode($json);
    }

    public function getDataAlat()
    {
        $alat = $this->m_manajemen->getAlat();
        $json = array();
        foreach ($alat as $alat) {
            $json[] = array('id'  => $alat->id_alat,
                            'nama' => $alat->jenis_alat
            );
        }

        echo json_encode($json);
    }


    public function LaporanHarian($id_pembuatan)
    {
        // $Data['kapal'] = $this->m_manajemenOS->getDetailKapal($id_pembuatan);
        // $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 

        $this->load->view('header');
        $this->load->view('sidebar-pm');
        $this->load->view('pm/laporan-harian');
        $this->load->view('footer');
    }

    public function DetailHarian($id_pembuatan)
    {
        // $Data['kapal'] = $this->m_manajemenOS->getDetailKapal($id_pembuatan);
        // $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 

        $this->load->view('header');
        $this->load->view('sidebar-pm');
        $this->load->view('pm/detail-laporan-harian');
        $this->load->view('footer');
    }

    public function LaporanMingguan()
    {
        // $Data['kapal'] = $this->m_manajemenOS->getDetailKapal($id_pembuatan);
        // $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 

        $this->load->view('header');
        $this->load->view('sidebar-pm');
        $this->load->view('pm/daftar-laporan-mingguan');
        $this->load->view('footer');
    }

    public function DetailMingguan()
    {
        // $Data['kapal'] = $this->m_manajemenOS->getDetailKapal($id_pembuatan);
        // $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 

        $this->load->view('header');
        $this->load->view('sidebar-pm');
        $this->load->view('pm/detail-laporan-mingguan');
        $this->load->view('footer');
    }
    
}



