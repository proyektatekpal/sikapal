<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Daftar Workgroup</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Modal -->
          <div class="modal fade" id="DelConf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus Data</h4>
                </div>
                <div class="modal-body">
                  Data akan dihapus, Anda yakin?
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                  <a class="btn bg-navy" id="modalDelete" href="#">Ya</a>
                  <!-- <button type="button" class="btn btn-primary" href="<?php echo base_url()?>ManajemenKapalAdmin/DelWorkGroup/">Hapus</button> -->
                </div>
              </div>
            </div>
          </div>

          <div class="modal fade" id="UpdateData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Update Data</h4>
                </div>
                <div class="modal-body">
                <form enctype="multipart/form-data" id="form1" action="#" method="post" role="form">
                  <div class="box-body">
                    <div class="form-group">
                      <input type="text" class="form-control" id="update_workgroup" name="update_workgroup" placeholder="Masukkan Nama WorkGroup" value="" >
                    </div>
                    <div class="form-group">
                      <input type="number" class="form-control number" id="update_bobot" name="update_bobot" placeholder="Masukkan Bobot WorkGroup" value="" >
                    </div>
                  </div><!-- /.box-body -->
                
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>

                  <!-- <a class="btn bg-navy" id="modalUpdate" type="submit" >Simpan</a> -->
                  <!-- <button type="button" class="btn btn-primary" href="<?php echo base_url()?>ManajemenKapalAdmin/DelWorkGroup/">Hapus</button> -->
                </div>
                </form>
              </div>
            </div>
          </div>


          
          <!-- Main row -->
          <div class="row">
            	<div class="col-xs-12">
                	<div class="box box-primary box-solid">
                      <div class="box-header">
                          <h3 class="box-title">Daftar Workgroup</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                          <table id="list-kapal" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th class="col-xs-1">No. </th>
                                <th>Nama Workgroup</th>
                                <th>Bobot</th>
                                <th>Tindakan</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                              $num = 1;
                              foreach ($list_workgroup as $kapal) {
                                  echo '<tr>';
                                  echo '<td>'.$num.'</td>';
                                  echo '<td>'.$kapal->nama_pekerjaan.'</td>';
                                  echo '<td>'.$kapal->bobot.'</td>';
                                  // echo '<td><a style="margin-left:15px;margin-right:15px;" class="btn bg-navy pull-left" href="'.base_url().'ManajemenKapalOS/EditWorkgroup/'.$kapal->id_workgroup.'">Edit</a>
                                  // <a  class="btn bg-navy pull-left" href="'.base_url().'ManajemenKapalOS/DelWorkgroup/'.$kapal->id_workgroup.'">Hapus</a></td>';
                                  // echo '</tr>';
                                  
                                  echo '<td><a  class="btn bg-navy update" data-url="'.base_url().'" data-bobot="'.$kapal->bobot.'" data-nama_pekerjaan="'.$kapal->nama_pekerjaan.'" data-id="'.$kapal->id_workgroup.'" data-toggle="modal" data-target="#UpdateData">Edit</a>
                                  <a  class="btn bg-navy hapus" data-url="'.base_url().'" data-id="'.$kapal->id_workgroup.'" data-toggle="modal" data-target="#DelConf">Hapus</a></td>';
                                  echo '</tr>';
                                  $num++;
                              }
                              ?>

                              <!-- <tr>
                                <td>1</td>
                                <td>Contoh 1</td>
                                <td>
                                  <button type="submit" class="btn btn-primary pull-left">Edit</button>
                                  <button style="margin-left:15px;" type="reset" class="btn btn-warning pull-left">Delete</button>
                                </td>  
                              </tr>
                              <tr>
                                <td>2</td>
                                <td>Contoh 2</td>
                                <td>
                                  <button type="submit" class="btn btn-primary pull-left">Edit</button>
                                  <button style="margin-left:15px;" type="reset" class="btn btn-warning pull-left">Delete</button>
                                </td> 
                              </tr> -->
                            </tbody>
                            <tfoot>
                              <tr>
                                <th>No. </th>
                                <th>Nama Workgroup</th>
                                <th>Bobot</th>
                                <th>Tindakan</th>
                              </tr>
                            </tfoot>
                          </table>
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>
            

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <script type="text/javascript">
	    $('.hapus').click(function(){
          var id = $(this).data('id');
          var base_url = $(this).data('url');

          $('#modalDelete').attr('href',base_url +'ManajemenKapalAdmin/DelWorkgroup/'+id);
      });


       $('.update').click(function(){
          var id = $(this).data('id');
          var nama_pekerjaan = $(this).data('nama_pekerjaan');
          var bobot = $(this).data('bobot');
          var base_url = $(this).data('url');
          
          $('#update_workgroup').attr('value',nama_pekerjaan);
          $('#update_bobot').attr('value',bobot);
          $('#form1').attr('action', base_url + 'ManajemenKapalAdmin/UpdateWorkgroup/'+id);
      });

	</script>