<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Daftar Proses</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Modal -->
          <div class="modal fade" id="DelConf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus Data</h4>
                </div>
                <div class="modal-body">
                  Data akan dihapus, Anda yakin?
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                  <a class="btn bg-navy" id="modalDelete" href="#">Ya</a>
                  <!-- <button type="button" class="btn btn-primary" href="<?php echo base_url()?>ManajemenKapalAdmin/DelWorkGroup/">Hapus</button> -->
                </div>
              </div>
            </div>
          </div>

          <div class="modal fade" id="UpdateData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Update Data</h4>
                </div>
                <div class="modal-body">
                <form enctype="multipart/form-data" id="form1" action="#" method="post" role="form">
                  <div class="box-body">
                    <div class="form-group">
                      <input type="text" class="form-control" id="update_aktifitas" name="update_aktifitas" placeholder="Masukkan Nama Aktifitas" value="" >
                    </div>
                    <div class="form-group">
                      <input type="number" class="form-control number" id="update_bobot" name="update_bobot" placeholder="Masukkan Bobot Aktifitas" value="" >
                    </div>
                  </div><!-- /.box-body -->
                
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>

                  <!-- <a class="btn bg-navy" id="modalUpdate" type="submit" >Simpan</a> -->
                  <!-- <button type="button" class="btn btn-primary" href="<?php echo base_url()?>ManajemenKapalAdmin/DelWorkGroup/">Hapus</button> -->
                </div>
                </form>
              </div>
            </div>
          </div>


          
          <!-- Main row -->
          <div class="row">
              <div class="col-md-12 ">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Pilih Workgroup</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-horizontal">
                        <div class="col-md-4 ">
                          <div class="form-group">
                            <label for="workgroup" class="col-sm-5 control-label pull-left">Workgroup:</label>
                            <div class="col-sm-7 ">
                              <select name="workgroup" id="workgroup" class="form-control selectpicker scrollable-menu " role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Workgroup</option>
                                   <?php 
                                      foreach ($list_workgroup as $data) {
                                          echo '<option value='.$data->id_workgroup.'>'.$data->nama_pekerjaan.'</option>';
                                          }
                                  ?>
                              </select>
                            </div>
                          </div>
                        </div>

                       <a id="ok_button" data-url="<?= base_url(); ?>"  style="margin-left:15px;margin-right:15px;" class="btn bg-navy pull-left"  title="ok">Ok</a>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
              </div>

            	<div class="col-xs-12 daftar_aktifitas" hidden >
                	<div class="box box-primary box-solid">
                      <div class="box-header">
                          <h3 class="box-title">Daftar Proses</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                          <table id="list-kapal" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th class="col-xs-1">No. </th>
                                <th>Nama Proses</th>
                                <th>Bobot</th>
                                <th>Tindakan</th>
                              </tr>
                            </thead>
                            <tbody id="data_activity">
                             

                            </tbody>
                            <tfoot>
                              <tr>
                                <th>No. </th>
                                <th>Nama Proses</th>
                                <th>Nama Workgroup</th>
                                <th>Tindakan</th>
                              </tr>
                            </tfoot>
                          </table>
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>
            

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<script type="text/javascript">
  $(document).ready(function()
      {

      $("#update").click(function(){
          
          
      });


      $("#ok_button").click(function()
      {  
          $("#data_activity").html('');

          var e = document.getElementById("workgroup");
          var strUser = e.options[e.selectedIndex].value;
          //alert(strUser);

          $.ajax({
              url: '<?php echo base_url()?>ManajemenKapalAdmin/getDataActivity/', 
              dataType: 'json',
              type: 'POST',
              data: {"id_workgroup":strUser},

              success: function(data) {
                var num = 1;
                $.each(data, function(i,item){
                  //console.log(item.id);
                  
                  if (item.id != 'empty') {

                    var kolom='';
                    kolom += '<tr>';
                    kolom += '<td>' + num +'</td>';
                    kolom += '<td>' + item.aktifitas + '</td>';
                    kolom += '<td>' + item.bobot +'</td>';              
                    kolom += '<td><a  class="btn bg-navy update" data-url="<?= base_url()?>" data-bobot="'+ item.bobot +'" data-nama_pekerjaan="'+ item.aktifitas +'" data-id="'+item.id+'" data-toggle="modal" data-target="#UpdateData">Edit</a><a style="margin-left:10px;" class="btn bg-navy hapus" data-url="<?= base_url()?>" data-id="'+item.id+'" data-toggle="modal" data-target="#DelConf">Hapus</a></td>';
                    kolom += '</tr>';
                    num = num + 1;
                    $("#data_activity").append(kolom);
                  } 

                    else {
                    }
                });                
              }
          });
          
          $('.daftar_aktifitas').show();      

       
      });

	    $(document).on("click", "[class^='btn bg-navy hapus']", function() {
          var id = $(this).data('id');
          var base_url = $(this).data('url');

          $('#modalDelete').attr('href',base_url +'ManajemenKapalAdmin/DelActivity/'+ id);          
      });

      $(document).on("click", "[class^='btn bg-navy update']", function() {
          var id = $(this).data('id');
          var nama_pekerjaan = $(this).data('nama_pekerjaan');
          var bobot = $(this).data('bobot');
          var base_url = $(this).data('url');
          
          $('#update_aktifitas').attr('value',nama_pekerjaan);
          $('#update_bobot').attr('value',bobot);
          $('#form1').attr('action', base_url + 'ManajemenKapalAdmin/UpdateActivity/'+id);
      });

});

</script>