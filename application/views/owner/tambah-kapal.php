<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Tambah Kapal</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="<?php echo base_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
              <li>Kapal</li>
	            <li class="active">Tambah Kapal</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
            <form action="<?php echo base_url()?>ManajemenKapalOwner/SimpanKapal" method="post" role="form">
              <div class="col-md-12">
                <?php
                  if ($cek_isi=='isi') {
                      if ($eksekusi) { ?>
                          <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <h4><i class="icon fa fa-check"></i> Sukses!</h4>
                              Data Berhasil Dimasukkan
                          </div>
                      <?php } else { ?>
                          <div class="alert alert-warning alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <h4><i class="icon fa fa-warning"></i> Gagal!</h4>
                              Data Gagal Dimasukkan
                          </div>
                      <?php }
                  }
                  ?>
              </div>
            
            	<div class="col-md-6">
                	<div class="box box-primary box-solid">
                      <div class="box-header">
                          <h3 class="box-title">Data Ukuran Kapal</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                          <div class="form-group">
                              <!-- <label for="nama_kapal">Nama</label> -->
                              <input type="text" class="form-control" id="nama_kapal" name="nama_kapal" placeholder="Nama Kapal">
                          </div>
                          <div class="form-group">
                              <!-- <label for="pjg_kapal">Panjang Kapal</label> -->
                              <input type="text" class="form-control" id="pjg_antara" name="pjg_antara" placeholder="Panjang Keseluruhan">
                          </div>
                          <div class="form-group">
                              <!-- <label for="pjg_kapal">Panjang Kapal</label> -->
                              <input type="text" class="form-control" id="pjg_kapal" name="pjg_kapal" placeholder="Panjang Antara Garis Tegak">
                          </div>
                          <div class="form-group">
                              <!-- <label for="lbr_kapal">Lebar Kapal</label> -->
                              <input type="text" class="form-control" id="lbr_kapal" name="lbr_kapal" placeholder="Lebar Kapal">
                          </div>
                          <div class="form-group">
                              <!-- <label for="tinggi_kapal">Tinggi Kapal</label> -->
                              <input type="text" class="form-control" id="tinggi_kapal" name="tinggi_kapal" placeholder="Tinggi Kapal">
                          </div>
                          <div class="form-group">
                              <!-- <label for="kecepatan">Kecepatan Kapal</label> -->
                              <input type="text" class="form-control" id="sarat_air" name="sarat_air" placeholder="Sarat Air">
                          </div>
                          <div class="form-group">
                              <!-- <label for="daya">Daya Mesin</label> -->
                              <input type="text" class="form-control" id="jarak_jelajah" name="jarak_jelajah" placeholder="Jarak Jelajah">
                          </div>
                          <div class="form-group">
                              <!-- <label for="daya">Daya Mesin</label> -->
                              <input type="text" class="form-control" id="tenaga_penggerak" name="tenaga_penggerak" placeholder="Tenaga Penggerak Utama">
                          </div>
                          <div class="form-group">
                              <!-- <label for="daya">Daya Mesin</label> -->
                              <input type="text" class="form-control" id="tenaga_bantu" name="tenaga_bantu" placeholder="Tenaga Motor Bantu">
                          </div>
                          <div class="form-group">
                              <!-- <label for="daya">Daya Mesin</label> -->
                              <input type="text" class="form-control" id="kls_klasifikasi" name="kls_klasifikasi" placeholder="Kelas Kalisifikasi">
                          </div>
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>

              <div class="col-md-6">
                  <div class="box box-primary box-solid">
                      <div class="box-header">
                          <h3 class="box-title">Data Kapasitas Kapal</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                          <div class="form-group">
                              <!-- <label for="nama_kapal">Nama</label> -->
                              <input type="text" class="form-control" id="jml_abk" name="jml_abk" placeholder="ABK">
                          </div>
                          <div class="form-group">
                              <!-- <label for="pjg_kapal">Panjang Kapal</label> -->
                              <input type="text" class="form-control" id="jml_penumpang" name="jml_penumpang" placeholder="Penumpang">
                          </div>
                          <div class="form-group">
                              <!-- <label for="lbr_kapal">Lebar Kapal</label> -->
                              <input type="text" class="form-control" id="tangki_bbm" name="tangki_bbm" placeholder="Tangki Bahan Bakar">
                          </div>
                          <div class="form-group">
                              <!-- <label for="tinggi_kapal">Tinggi Kapal</label> -->
                              <input type="text" class="form-control" id="tangki_ballast" name="tangki_ballast" placeholder="Tangki Ballast">
                          </div>
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>

              <div class="col-md-6">
                  <div class="box box-primary box-solid">
                      <div class="box-header">
                          <h3 class="box-title">Tenaga Kerja</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                          <div class="form-group">
                              <!-- <label for="nama_kapal">Nama</label> -->
                              <select class="form-control" name="id_pm">
                                  <option style="display:none;">-- Pilih Project Manager --</option>
                                  <?php 
                                  foreach ($list_pm as $pm) {
                                      echo '<option value="'.$pm->id_pm.'">'.$pm->nama.'</option>';
                                  }
                                  ?>
                              </select>
                          </div>
                          <div class="form-group" >
                              <!-- <label for="pjg_kapal">Panjang Kapal</label> -->
                              <select class="form-control" name="id_os">
                                  <option style="display:none;">-- Pilih Surveyor --</option>
                                  <?php 
                                  foreach ($list_os as $os) {
                                      echo '<option value="'.$os->id_os.'">'.$os->nama.'</option>';
                                  }
                                  ?>
                              </select>
                          </div>
                          
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>

              <div class="col-md-6">
                  <div class="box box-primary box-solid">
                      <div class="box-header">
                          <h3 class="box-title">Waktu Pengerjaan</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                          <div class="form-group">
                              <input type="number" class="form-control input_number" id="lama_pengerjaan" name="lama_pengerjaan" placeholder="Lama Pengerjaan (bulan)">
                          </div>
                          <div class="form-group">
                              <input type="text" class="form-control date-picker" id="tgl_mulai" name="tgl_mulai" placeholder="Tanggal Mulai">
                          </div>
                          
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>

              <div class="col-md-12">
                  <div class="box box-primary">
                      <div class="box-footer">
                          <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                          <a style="margin-left:15px;margin-right:15px;" class="btn bg-navy pull-right" href="<?php echo base_url()?>dashboard" title="Kembali">Kembali</a>
                          <button style="margin-left:15px;" type="reset" class="btn btn-warning pull-right">Reset</button>
                      </div>
                  </div>
              </div>
            
            </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <!-- <script type="text/javascript">
	    // $(document).ready(function(){
     //      $('.date-picker').datepicker( {
     //        changeMonth: true,
     //        changeYear: true,
     //        dateFormat: 'dd/mm/yy'
     //      });
	    // }); 
	</script> -->