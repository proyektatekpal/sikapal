<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfilOwner extends CI_Controller {

	public function __construct()
    {
        parent::__construct(); 
        if ( !$this->session->userdata('isLogin') && $this->session->userdata('level') != 'owner')
        { 
            redirect('access');
        }
        // $this->load->library('form_validation');
        $this->load->model('m_profil');
    }

	public function index()
	{
        
	}

    public function editProfil()
    {
        $email = $this->session->userdata('email');
        $DataOwner['detailowner'] = $this->m_profil->getOwnerProfil($email);
        $this->load->view('header');
        $this->load->view('sidebar-owner');
        $this->load->view('owner/edit-profil', $DataOwner);
        $this->load->view('footer');
    }

    public function simpanEdit()
    {
        
    }

    public function ubahPassword()
    {
        $this->load->view('header');
        $this->load->view('sidebar-owner');
        $this->load->view('owner/ubah-password');
        $this->load->view('footer');
    }

    public function simpanPassword()
    {
        
    }
}



