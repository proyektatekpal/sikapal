<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Laporan</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
            	<div class="box box-primary box-solid">
	                <div class="box-header with-border">
	                  	<h3 class="box-title">Buat laporan</h3>
	                  	<div class="box-tools pull-left">
	                    	<button class="btn btn-box-tool" data-widget="remove"></button>
	                  	</div><!-- /.box-tools -->
	                </div><!-- /.box-header -->
	                <div class="box-body">
			            <div class="box">
                <div class="box-header">
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                  <form>
                    <textarea class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </form>
                </div>
              </div>
	                </div><!-- /.box-body -->
              	</div><!-- /.box -->
            </section><!-- right col -->

           
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <section class="col-lg-12">
            	<div class="box box-primary">
	                <!-- <div class="box-header with-border">
	                
	                </div> --><!-- /.box-header -->
	                <div class="box-footer">
	                	<button type="submit" class="btn btn-primary pull-right">Simpan</button>
	                	<a style="margin-left:15px;margin-right:15px;" class="btn bg-navy pull-right" href="<?php echo base_url()?>dashboard" title="Kembali">Kembali</a>
                        <!-- <button style="margin-left:15px;" type="reset" class="btn btn-warning pull-right">Reset</button> -->
			        </div>
              	</div><!-- /.box -->
            </section>

          </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

