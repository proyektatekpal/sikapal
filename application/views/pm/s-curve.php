<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }

  ul {
    margin:       0px!important;
    padding:      0px!important;
    list-style-type:  none;
  }

  label {
    font-weight:    normal!important;
    margin:       10px 10px;
  }

  #demoLegend ul > li {
    margin:       0px 0px 20px 0px;
  }

  #demoLegend ul > li > span {
    padding:      5px!important;
    margin-right:     10px;
  }
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Detail Kapal</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <nav class="navbar navbar-default">
              <div class="container-fluid">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url()."manajemenKapalPM/DetailKapal/".$kapal->id_pembuatan;?>"><strong><?php echo $kapal->nama_kapal;?></strong></a>
                  </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                      <li><a href="<?php echo base_url()."manajemenKapalPM/gambar/".$kapal->id_pembuatan;?>">Gambar</a></li>
                      <li class="active"><a href="<?php echo base_url()."manajemenKapalPM/SCurve/".$kapal->id_pembuatan;?>">S-Curve</a></li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Laporan <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url()."manajemenKapalPM/lapHarian/".$kapal->id_pembuatan;?>">Harian</a></li>
                            <li><a href="<?php echo base_url()."manajemenKapalPM/lapBulanan/".$kapal->id_pembuatan;?>">2 Mingguan</a></li>
                          </ul>
                      </li>
                      <li><a href="#">Pesan</a></li>
                    </ul>
                  </div><!-- /.navbar-collapse -->

              </div><!-- /.container-fluid -->
          </nav>

          <div class="row">
              <div class="col-md-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title">S-Curve</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-sm-11">
                          <canvas id="demoChart" width="950" height="550"> </canvas>
                        </div>

                        <div class="col-sm-1">
                          <div id="demoLegend"> </div>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <script type="text/javascript">
	    $(document).ready(function(){
	      	$('#list-kapal').DataTable();

          var data = {
              labels: <?php echo $label; ?>,
              datasets: [
                  {
                      label: "Baseline",
                      fillColor:  "rgba(220,220,220,0.2)",
                      strokeColor: "rgba(220,220,220,1)",
                      pointColor:  "rgba(220,220,220,1)",
                      pointStrokeColor: "#FFF",
                      pointHighlightFill: "#FFF",
                      pointHighlightStroke: "rgba(220,220,220,1)",
                      data: <?php echo $bobot_baseline; ?>
                  },
                  {
                      label: "Progress",
                      fillColor:  "rgba(151,187,205,0.2)",
                      strokeColor: "rgba(151,187,205,1)",
                      pointColor:  "rgba(151,187,205,1)",
                      pointStrokeColor: "#FFF",
                      pointHighlightFill: "#FFF",
                      pointHighlightStroke: "rgba(151,187,205,1)",
                      data: <?php echo $bobot_baseline; ?>
                  }
              ]
          };

          var ctx = document.getElementById("demoChart").getContext("2d");
          var chart = new Chart(ctx).Line(data);
          document.getElementById("demoLegend").innerHTML = chart.generateLegend();
	    }); 
      
	</script>