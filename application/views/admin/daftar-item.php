<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>

<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Daftar Item</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>


        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
        <div class="row">
            <form enctype="multipart/form-data" id="form1" action="#" method="post" role="form">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Pilih Workgroup dan Proses</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-horizontal">
                      <div class="col-md-4">
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-5 control-label">Workgroup:</label>
                            <div class="col-sm-7">
                              <select name="workgroup" id="workgroup" class="form-control selectpicker scrollable-menu " role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Workgroup</option>
                                   <?php 
                                      foreach ($list_workgroup as $data) {
                                          echo '<option value='.$data->id_workgroup.'>'.$data->nama_pekerjaan.'</option>';
                                          }
                                  ?>
                              </select>
                            </div>
                          </div>
                          <div class="form-group pilih_group_hidden" hidden>
                            <label for="cuaca_siang" class="col-sm-5 control-label">Proses:</label>
                            <div class="col-sm-7">
                              <select name="proses" id="proses" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                 
                                   
                              </select>
                            </div>
                          </div>
                          <!-- <div class="form-group jumlah_item" hidden>
                            <label for="jumlahActivity" class="col-sm-5 control-label">Jumlah Item:</label>
                            <div class="col-sm-7">
                              <input type="number" min="1" class="form-control input_number" placeholder="jumlah item" id="jlh_item" name="jlh_item" value="">
                            </div>
                          </div> -->
                          <div class="button_hidden" hidden>
                            <a id="ok_button" style="margin-left:15px;" class="btn bg-navy pull-right"  hidden>Ok</a>
                          </div>
                        </div>
                    </div>
                </div><!-- /.box -->
              </div>

              <div class="col-xs-12 daftar_aktifitas" hidden >
                  <div class="box box-primary box-solid">
                      <div class="box-header">
                          <h3 class="box-title">Daftar item</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                          <table id="list-kapal" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th class="col-xs-1">No. </th>
                                <th>Nama item</th>
                                <!-- <th>Tindakan</th> -->
                                <!-- <th>Bobot</th> -->
                                <!-- <th>Tindakan</th> -->
                              </tr>
                            </thead>
                            <tbody id="data_activity">
                             

                            </tbody>
                            <tfoot>
                              <tr>
                                <th>No. </th>
                                <th>Nama item</th>
                                <!-- <th>Tindakan</th> -->
                               <!--  <th>Nama Workgroup</th>
                                <th>Tindakan</th> -->
                              </tr>
                            </tfoot>
                          </table>
                      </div><!-- /.box-body -->
                      <div class="box-footer">
                        <div class="pull-right">
                          <button id="btn_delItem" class="btn btn-primary">Hapus Item</button>
                      </div>
                    </div>
                  </div><!-- /.box -->
                    
                </div>
              </div>

            </form>
          </div><!-- /.row (main row) -->


        </section><!-- right col -->

            <!-- right col (We are only adding the ID to make the widgets sortable)-->
          </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<script type="text/javascript">
	    $(document).ready(function()
	    {
        $('#workgroup').on('change', function() {
            document.getElementById('proses').innerHTML = '';
            var id_workgroup = $('#workgroup').val();
            var kolom = '';
            kolom += '<option style="display: none;">Pilih Proses</option>';

            $.ajax({
                        url: '<?php echo base_url()?>ManajemenKapalAdmin/getDataActivity/', 
                        dataType: 'json',
                        type: 'POST',
                        data: {"id_workgroup":id_workgroup},
                        success: function(data) 
                        {
                          $.each(data, function(i,item){
                            //alert(item.id);                            
                            if (item.id != 'empty') 
                            {
                              kolom +=  '<option value="'+item.id+'">'+item.aktifitas+'</option>';
                              $("#proses").append(kolom);
                              kolom='';                         
                            } 
                            else {}
                          });                
                        }
                      });
            $('.pilih_group_hidden').show();
            $('.jumlah_item').show();                          
            $('.button_hidden').show();                          
          });
          


          $("#ok_button").click(function()
          {
            $("#data_activity").html('');

            var e = document.getElementById("proses");
            var strUser = e.options[e.selectedIndex].value;

            $.ajax({
                url: '<?php echo base_url()?>ManajemenKapalAdmin/GetDataItem/', 
                dataType: 'json',
                type: 'POST',
                data: {"id":strUser},

                success: function(data) {
                  var num = 1;
                  $.each(data, function(i,item)
                  {
                    var item_split = item.item.split('#');
                    var count_item = item_split.length;

                    for(var a=0; a<count_item-1; a++)
                    {
                      var data_item = item_split[a];
                      var data_item_split = data_item.split('|');
                      var nama_item = data_item_split[0];

                      var kolom='';
                      kolom += '<tr>';
                      kolom += '<td>' + (a+1) +'</td>';
                      kolom += '<td>' + nama_item + '</td>';
                      // kolom += '<td>' + item.bobot +'</td>';              
                      // kolom += '<td><a  class="btn bg-navy update" data-url="<?= base_url()?>"  data-item="'+ nama_item +'" data-id="'+item.id+'" data-toggle="modal" data-target="#UpdateData">Edit</a><a style="margin-left:10px;" class="btn bg-navy hapus" data-url="<?= base_url()?>" data-id="'+item.id+'" data-toggle="modal" data-target="#DelConf">Hapus</a></td>';
                      kolom += '</tr>';
                      // num = num + 1;
                      $("#data_activity").append(kolom);
                    }
                  });                
                }
            });
            
            $('.daftar_aktifitas').show();
          });


          $(document).on("click", "[class^='btn bg-navy hapus']", function() {
              var id = $(this).data('id');
              var base_url = $(this).data('url');

              $('#modalDelete').attr('href',base_url +'ManajemenKapalAdmin/DelActivity/'+ id);          
          });

           $(document).on("click", "[class^='btn bg-navy update']", function() {
              var id = $(this).data('id');
              var nama_pekerjaan = $(this).data('nama_pekerjaan');
              // var bobot = $(this).data('bobot');
              var base_url = $(this).data('url');
              
              $('#update_aktifitas').attr('value',nama_pekerjaan);
              $('#update_bobot').attr('value',bobot);
              $('#form1').attr('action', base_url + 'ManajemenKapalAdmin/UpdateActivity/'+id);
          });

          $("#btn_delItem").click(function()
          {
            var e = document.getElementById("proses");
            var strUser = e.options[e.selectedIndex].value;

            $.ajax({
                        url: '<?php echo base_url()?>ManajemenKapalAdmin/DeleteItem/', 
                        dataType: 'json',
                        type: 'POST',
                        async: false,
                        data: {"id": strUser}
            });
            location.reload();
          });

      }); //end of script
</script>