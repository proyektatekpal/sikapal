<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Tambah Sub Workgroup</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">

         <?php echo($menu); ?>
          <!-- Main row -->

          <div class="row">
            <form action="" method="post" role="form">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Pilih WorkGroup</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-horizontal">
                        <div class="col-md-4">

                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-5 control-label">Workgroup:</label>
                            <div class="col-sm-7">
                              <select name="workgroup" id="workgroup" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Workgroup</option>
                                  <option>Design & Approval Drawing</option>
                                  <option>Work Preparation & General</option>
                                  <option>Hull construction</option>
                              </select>
                            </div>
                          </div>
                        <div class="form-group">
                           <input type="text" class="form-control" id="nama_owner" name="old_pass" placeholder="Masukkan Nama Sub WorkGroup" >
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div>

              <table class="table table-bordered table-striped" id="tabelinput">
                <div id="level_2">
                
                </div>
              </table>

            </form>
          </div><!-- /.row (main row) -->

          <div class="box-footer">
            <div class="pull-right">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->