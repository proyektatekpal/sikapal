<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Detail Kapal</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <nav class="navbar navbar-default">
              <div class="container-fluid">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url()."manajemenKapalPM/DetailKapal/".$kapal->id_pembuatan;?>"><strong><?php echo $kapal->nama_kapal;?></strong></a>
                  </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                      <li><a href="<?php echo base_url()."manajemenKapalPM/gambar/".$kapal->id_pembuatan;?>">Gambar</a></li>
                      <li><a href="<?php echo base_url()."manajemenKapalPM/SCurve/".$kapal->id_pembuatan;?>">S-Curve</a></li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Laporan <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url()."manajemenKapalPM/lapHarian/".$kapal->id_pembuatan;?>">Harian</a></li>
                            <li><a href="<?php echo base_url()."manajemenKapalPM/lapBulanan/".$kapal->id_pembuatan;?>">2 Mingguan</a></li>
                          </ul>
                      </li> -->
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Laporan <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url().'manajemenKapalOwner/LaporanHarian/'.$kapal->id_pembuatan?>">Harian</a></li>
                            <li><a href="<?php echo base_url().'manajemenKapalOwner/LaporanMingguan/'.$kapal->id_pembuatan?>">2 Mingguan</a></li>
                          </ul>
                      </li>

                      <li><a href="#">Pesan</a></li>
                    </ul>
                  </div><!-- /.navbar-collapse -->

              </div><!-- /.container-fluid -->
          </nav>

          <div class="row">

              <div class="col-md-6">
                <div class="box box-primary box-solid">

                    <div class="box-header with-border">
                      <h3 class="box-title">Ukuran Utama</h3>
                    </div>

                    <div class="box-body">
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Panjang Keseluruhan</strong></span>
                            <input type="text" class="form-control" placeholder="" value="" readonly/>
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Panjang Antara Garis Tegak</strong></span>
                            <input type="text" class="form-control" placeholder="" value="" readonly/>
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Lebar</strong></span>
                            <input type="text" class="form-control" placeholder="" value="" readonly/>
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Tinggi</strong></span>
                            <input type="text" class="form-control" placeholder="" value="" readonly/>
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Sarat air</strong></span>
                            <input type="text" class="form-control" placeholder="" value="" readonly/>
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Jarak Jelajah</strong></span>
                            <input type="text" class="form-control" placeholder="" value="" readonly/>
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Tenaga penggerak utama</strong></span>
                            <input type="text" class="form-control" placeholder="" value="" readonly/>
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Tenaga Motor Bantu</strong></span>
                            <input type="text" class="form-control" placeholder="" value="" readonly/>
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Kelas klasifikasi</strong></span>
                            <input type="text" class="form-control" placeholder="" value="" readonly/>
                        </div>            
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
              </div>

              <div class="col-md-6">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title">Kapasitas</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>ABK</strong></span>
                            <input type="text" class="form-control" placeholder="" value="" readonly/>
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Penumpang</strong></span>
                            <input type="text" class="form-control" placeholder="" value="" readonly/>
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Tangki bahan bakar</strong></span>
                            <input type="text" class="form-control" placeholder="" value="" readonly/>
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Tangki Ballast</strong></span>
                            <input type="text" class="form-control" placeholder="" value="" readonly/>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div>

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <script type="text/javascript">
	    $(document).ready(function(){
	      	$('#list-kapal').DataTable();
	    }); 
	</script>