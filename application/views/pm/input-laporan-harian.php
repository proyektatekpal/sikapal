<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Detail Kapal</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <nav class="navbar navbar-default">
              <div class="container-fluid">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url()."manajemenKapalPM/DetailKapal/".$kapal->id_pembuatan;?>"><strong><?php echo $kapal->nama_kapal;?></strong></a>
                  </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                      <li><a href="<?php echo base_url()."manajemenKapalPM/gambar/".$kapal->id_pembuatan;?>">Gambar</a></li>
                      <li><a href="<?php echo base_url()."manajemenKapalPM/SCurve/".$kapal->id_pembuatan;?>">S-Curve</a></li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Laporan <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url()."manajemenKapalPM/lapHarian/".$kapal->id_pembuatan;?>">Harian</a></li>
                            <li><a href="<?php echo base_url()."manajemenKapalPM/lapBulanan/".$kapal->id_pembuatan;?>">2 Mingguan</a></li>
                          </ul>
                      </li>
                      <li><a href="#">Pesan</a></li>
                    </ul>
                  </div><!-- /.navbar-collapse -->

              </div><!-- /.container-fluid -->
          </nav>

          <div class="row">
            <form action="" method="post" role="form">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Laporan Harian</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-horizontal">
                        <div class="col-md-4">

                          <div class="form-group">
                            <label for="cuaca_pagi" class="col-sm-5 control-label">Tanggal Laporan:</label>
                            <div class="col-sm-7">
                              <input type="text" class="form-control date-picker" placeholder="dd/mm/yyyy">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-5 control-label">Workgroup:</label>
                            <div class="col-sm-7">
                              <select name="workgroup" id="workgroup" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Kluster</option>
                                  <option>Design & Approval Drawing</option>
                                  <option>Work Preparation & General</option>
                                  <option>Hull construction</option>
                              </select>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="cuaca_pagi" class="col-sm-5 control-label">Jumlah pekerjaan:</label>
                            <div class="col-sm-7">
                              <input type="number" min="1" class="form-control input_number" placeholder="jumlah pekerjaan" id="jumlah_pekerjaan">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="cuaca_pagi" class="col-sm-5 control-label">Jenis tenaga kerja:</label>
                            <div class="col-sm-7">
                              <input type="number" min="1" class="form-control input_number" placeholder="jenis tenaga kerja" id="jenis_tenaga">
                            </div>
                          </div>
                        </div>

                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="cuaca_pagi" class="col-sm-5 control-label">Jenis alat:</label>
                            <div class="col-sm-7">
                              <input type="number" min="1" class="form-control input_number" placeholder="Jenis alat" id="jenis_alat">
                            </div>
                          </div>
                        </div>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
              </div>
              <div class="col-md-4">
                <div class="box box-primary laporan_hidden" hidden>
                    <div class="box-header with-border">
                      <h3 class="box-title">TENAGA KERJA</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-inline" id="list_pekerja">
                        
                    </div>
                </div>
              </div>

              <div class="col-md-4">
                <div class="box box-primary laporan_hidden" hidden>
                    <div class="box-header with-border">
                      <h3 class="box-title">CUACA</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-horizontal">
                        <div class="form-group">
                            <label for="cuaca_pagi" class="col-sm-3 control-label">Pagi:</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="cuaca_pagi" placeholder="Pagi">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-3 control-label">Siang:</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="cuaca_siang" placeholder="Siang">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cuaca_sore" class="col-sm-3 control-label">Sore:</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="cuaca_sore" placeholder="Sore">
                            </div>
                        </div>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
              </div>

              <div class="col-md-4">
                <div class="box box-primary laporan_hidden" hidden>
                    <div class="box-header with-border">
                      <h3 class="box-title">ALAT YANG DIGUNAKAN</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-inline" id="list_alat">
                        
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
              </div>

              <div class="col-md-12">
                <div class="box box-primary laporan_hidden" hidden>
                    <div class="box-header with-border">
                      <h3 class="box-title">DATA PEKERJAAN</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <table class="table table-bordered table-striped" id="tabelinput">
                          <thead>
                            <tr>
                              <th class="text-center">NO</th>
                              <th class="text-center">JENIS PEKERJAAN<span style="color:red;"><b>*</b></span></th>
                              <th class="text-center">URAIAN PEKERJAAN</th>
                              <th class="text-center">TENAGA KERJA<span style="color:red;"><b>*</b></span></th>
                              <th class="text-center">UPLOAD GAMBAR</th>
                            </tr>
                          </thead>
                          <tbody id="detail_pekerjaan">

                          </tbody>
                          <!-- <tfoot>
                            <tr>
                              <th class="text-center">NO</th>
                              <th class="text-center">JENIS PEKERJAAN<span style="color:red;"><b>*</b></span></th>
                              <th class="text-center">URAIAN PEKERJAAN</th>
                              <th class="text-center">TENAGA KERJA<span style="color:red;"><b>*</b></span></th>
                              <th class="text-center">UPLOAD GAMBAR</th>
                            </tr>
                          </tfoot> -->
                        </table>
                        
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <div class="pull-right">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div><!-- /.box -->
              </div>
            </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <script type="text/javascript">
	    $(document).ready(function(){
	      	$('#list-kapal').DataTable();

          $("#workgroup").change(function()
          {  
              $("#detail_pekerjaan").html('');
              var row = $('#jumlah_pekerjaan').val();
              var jns_alat = $('#jenis_alat').val();
              var jns_tenaga = $('#jenis_tenaga').val();
              if(row=='' | row<= 0 | jns_alat=='' | jns_alat<=0 | jns_tenaga=='' | jns_tenaga<=0)
              {
                  alert('Silahkan isi jumlah peekerjaan dengan benar');
              }
              else
              {
                  for(var i=0; i<row; i++)
                  {
                      var kolom = '';
                      kolom += '<tr>';
                      kolom += '<td class="col-md-1"><input type="text" class="form-control" value="'+(i+1)+'"/></td>';
                      kolom += '<td class="col-md-3"><input type="text" class="form-control"/></td>';
                      kolom += '<td class="col-md-5"><input type="text" class="form-control"/></td>';
                      kolom += '<td class="col-md-2"><input type="number" class="form-control input_number"/></td>';
                      kolom += '<td class="col-md-1"><input type="file" id="foto"></td>';
                      kolom += '<tr>';
                      $("#detail_pekerjaan").append(kolom);
                  }

                  for(var j=0; j<jns_alat; j++)
                  {
                       $.ajax({
                          url: '<?php echo base_url()?>manajemenKapalPM/getDataAlat', 
                          dataType: 'json',
                          type: 'POST',
                          success: function(data) {
                              // console.log(data);
                              var kolom = '';
                              kolom += '<div class="input-group">';
                              kolom += '<div class="input-group-btn">';
                              kolom += '<select type="button" class="btn btn-default dropdown-toggle">';
                              $.each(data, function(i,item){
                                  console.log(item.id);
                                  if (item.id != 'empty' ) {
                                      kolom += '<option>'+item.nama+'</option>';
                                  } else {
                                      
                                  }
                              });

                              kolom += '</select>';
                              kolom += '</div>';
                              kolom += '<input class="form-control" type="number" value="" placeholder="Jumlah" name="">';
                              kolom += '</div>';
                              $("#list_alat").append(kolom);
                          }
                      }); 
                  }

                  for(var k=0; k<jns_tenaga; k++)
                  {
                      $.ajax({
                          url: '<?php echo base_url()?>manajemenKapalPM/getDataPekerja', 
                          dataType: 'json',
                          type: 'POST',
                          success: function(data) {
                              // console.log(data);
                              var kolom = '';
                              kolom += '<div class="input-group">';
                              kolom += '<div class="input-group-btn">';
                              kolom += '<select type="button" class="btn btn-default dropdown-toggle">';
                              $.each(data, function(i,item){
                                  console.log(item.id);
                                  if (item.id != 'empty' ) {
                                      kolom += '<option>'+item.nama+'</option>';
                                  } else {
                                      
                                  }
                              });

                              kolom += '</select>';
                              kolom += '</div>';
                              kolom += '<input class="form-control" type="number" value="" placeholder="Jumlah" name="">';
                              kolom += '</div>';
                              $("#list_pekerja").append(kolom);
                          }
                      });
                      
                  } 

                  $('.laporan_hidden').show();
              }

          });

          // $(".input_number").keydown(function(event) {
          //     if ( event.keyCode == 46 || event.keyCode == 8 ) {

          //     }
          //     else {
          //       if (event.keyCode < 48 || event.keyCode > 57 ) {
          //         event.preventDefault(); 
          //       } 
          //     }
          // });
	    }); 

        
	</script>