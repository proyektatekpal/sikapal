-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 27, 2016 at 06:00 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sikapal`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(8) UNSIGNED ZEROFILL NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `email`, `password`) VALUES
(00000001, 'administrator', 'admin@admin.com', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `aktifitas`
--

CREATE TABLE `aktifitas` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_workgroup` int(6) UNSIGNED ZEROFILL NOT NULL,
  `aktifitas` varchar(40) NOT NULL,
  `item` text,
  `bobot` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aktifitas`
--

INSERT INTO `aktifitas` (`id`, `id_workgroup`, `aktifitas`, `item`, `bobot`) VALUES
(000003, 000001, 'Design', NULL, '20'),
(000004, 000001, 'Owner Apporaval', NULL, '20'),
(000005, 000001, 'Class Apporaval', NULL, '30'),
(000006, 000001, 'Doc. Issued', NULL, '30'),
(000007, 000002, 'Purchase Order', 'Bukti Pembelian Barang|0#', '10'),
(000008, 000002, 'Arrival At yard', 'Grade/Type|0#Nomor Sertifikat|0#Quantity|0#Ukuran Material|0#Lubang|0#Deformasi|0#Pengelupasan permukaan|0#', '10'),
(000009, 000002, 'Fab. Blasting & Shop Primer', 'Paint maker|0#Colour|0#Product name|0#Aplication Methode|0#Cleaning Standard|0#Blasting/Roughness|0#Relative Humidity (RH)|0#Steel Temperature|0#Grade|0#Wet Temp|0#Dry Temp|0#', '10'),
(000010, 000002, 'Fab. Marking', 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', '10'),
(000011, 000002, 'Fab. Cutting', 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', '10'),
(000012, 000002, 'Fab. Bending', 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', '10'),
(000013, 000002, 'Ass. Fitt Up', 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '10'),
(000014, 000002, 'Ass. Welding', 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '10'),
(000015, 000002, 'Ass. Inspection', 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', '5'),
(000016, 000002, 'Erec. Fitt Up', 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '5'),
(000017, 000002, 'Erec. Welding', 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '5'),
(000018, 000002, 'Erec. Inspection', 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', '5'),
(000019, 000003, 'Purchase Order', 'Bukti Pembelian Barang|0#', '20'),
(000020, 000003, 'Arrival At yard', 'Grade/Type|0#Nomor Sertifikat|0#Quantity|0#Ukuran Material|0#Visual Check|0#', '20'),
(000021, 000003, 'Fab.Marking', 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', '10'),
(000022, 000003, 'Fab. Cutting', 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', '10'),
(000023, 000003, 'Fab. Fitt Up', 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '10'),
(000024, 000003, 'Fab. Welding', 'Crack|0#Pemakaian elektrode|0#Panjang kaki las|0#Bentuk Hasil Las|0#Cacat Bekas Stopper|0#Spatter|0#Porosity|0#', '10'),
(000026, 000003, 'On Board', 'Alignment Check|0#Install Ceck|0#Function Test|0#', '20'),
(000030, 000004, 'Purchase Order', NULL, '10'),
(000031, 000004, 'Arrival At yard', NULL, '10'),
(000032, 000004, 'Fab. Fitt Up', NULL, '10'),
(000033, 000004, 'Fab. Welding ', NULL, '10'),
(000034, 000004, 'Fab. Inspection', NULL, '10'),
(000035, 000004, 'On Board Fitt Up', NULL, '10'),
(000036, 000004, 'On Board Install', NULL, '10'),
(000037, 000004, 'On Board Inspection', NULL, '15'),
(000038, 000004, 'On Board Testing', NULL, '15'),
(000039, 000005, 'Purchase Order', NULL, '10'),
(000040, 000005, 'Arrival At yard', NULL, '10'),
(000041, 000005, 'Fab. Marking', NULL, '10'),
(000042, 000005, 'Fab. Cutting', NULL, '10'),
(000043, 000005, 'Fab.  Fitt Up', NULL, '10'),
(000044, 000005, 'Fab.  Welding', NULL, '10'),
(000045, 000005, 'Fab. Inspection', NULL, '10'),
(000046, 000005, 'On Board Fitt Up', NULL, '10'),
(000047, 000005, 'On Board Install', NULL, '10'),
(000048, 000005, 'On Board Inspection', NULL, '5'),
(000049, 000005, 'On Board Testing', NULL, '5'),
(000050, 000006, 'Purchase Order', NULL, '10'),
(000051, 000006, 'Arrival At yard', NULL, '10'),
(000052, 000006, 'On Board Fitt Up', NULL, '10'),
(000053, 000006, 'On Board Install', NULL, '10'),
(000054, 000006, 'On Board Connect', NULL, '20'),
(000055, 000006, 'On Board Inspection', NULL, '20'),
(000056, 000006, 'On Board Testing', NULL, '20'),
(000057, 000007, 'Purchase Order', NULL, '20'),
(000058, 000007, 'Arrival At yard', NULL, '20'),
(000059, 000007, 'Surface Prep.', NULL, '20'),
(000060, 000007, 'Coating/Painting', NULL, '20'),
(000061, 000007, 'Inspection', NULL, '20'),
(000062, 000008, 'Purchase Order', NULL, '25'),
(000063, 000008, 'Arrival At yard', NULL, '25'),
(000064, 000008, 'Loading On Board', NULL, '25'),
(000065, 000008, 'Inspection', NULL, '25');

-- --------------------------------------------------------

--
-- Table structure for table `alat_kerja`
--

CREATE TABLE `alat_kerja` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `alat` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alat_kerja`
--

INSERT INTO `alat_kerja` (`id`, `alat`) VALUES
(000003, 'alat1'),
(000004, 'alat2'),
(000005, 'alat3');

-- --------------------------------------------------------

--
-- Table structure for table `baseline`
--

CREATE TABLE `baseline` (
  `id` int(8) UNSIGNED ZEROFILL NOT NULL,
  `id_kapal` int(6) UNSIGNED ZEROFILL NOT NULL,
  `periode` varchar(25) DEFAULT NULL,
  `mid_end` int(3) DEFAULT NULL,
  `target` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `baseline`
--

INSERT INTO `baseline` (`id`, `id_kapal`, `periode`, `mid_end`, `target`) VALUES
(00000596, 000032, 'February 2017', 1, '0'),
(00000595, 000032, 'February 2017', 0, '0'),
(00000594, 000032, 'January 2017', 1, '0'),
(00000593, 000032, 'January 2017', 0, '0'),
(00000592, 000032, 'December 2016', 1, '0'),
(00000591, 000032, 'December 2016', 0, '0'),
(00000590, 000032, 'November 2016', 1, '0'),
(00000589, 000032, 'November 2016', 0, '0'),
(00000588, 000032, 'October 2016', 1, '0'),
(00000587, 000032, 'October 2016', 0, '0'),
(00000586, 000032, 'September 2016', 1, '0'),
(00000585, 000032, 'September 2016', 0, '0'),
(00000584, 000032, 'August 2016', 1, '0'),
(00000583, 000032, 'August 2016', 0, '0'),
(00000582, 000032, 'July 2016', 1, '0'),
(00000581, 000032, 'July 2016', 0, '0'),
(00000580, 000032, 'June 2016', 1, '0'),
(00000579, 000032, 'June 2016', 0, '0'),
(00000578, 000032, 'May 2016', 1, '0'),
(00000577, 000032, 'May 2016', 0, '0'),
(00000576, 000032, 'April 2016', 1, '0'),
(00000575, 000032, 'April 2016', 0, '0'),
(00000574, 000032, 'March 2016', 1, '0'),
(00000573, 000032, 'March 2016', 0, '0'),
(00000572, 000032, 'February 2016', 1, '0'),
(00000571, 000032, 'February 2016', 0, '0'),
(00000570, 000032, 'January 2016', 1, '0'),
(00000569, 000032, 'January 2016', 0, '0'),
(00000568, 000031, 'February 2017', 1, '0'),
(00000567, 000031, 'February 2017', 0, '0'),
(00000566, 000031, 'January 2017', 1, '0'),
(00000565, 000031, 'January 2017', 0, '0'),
(00000564, 000031, 'December 2016', 1, '0'),
(00000563, 000031, 'December 2016', 0, '0'),
(00000562, 000031, 'November 2016', 1, '0'),
(00000561, 000031, 'November 2016', 0, '0'),
(00000560, 000031, 'October 2016', 1, '0'),
(00000559, 000031, 'October 2016', 0, '0'),
(00000558, 000031, 'September 2016', 1, '0'),
(00000557, 000031, 'September 2016', 0, '0'),
(00000556, 000031, 'August 2016', 1, '0'),
(00000555, 000031, 'August 2016', 0, '0'),
(00000554, 000031, 'July 2016', 1, '0'),
(00000553, 000031, 'July 2016', 0, '0'),
(00000552, 000031, 'June 2016', 1, '0'),
(00000551, 000031, 'June 2016', 0, '0'),
(00000550, 000031, 'May 2016', 1, '0'),
(00000549, 000031, 'May 2016', 0, '0'),
(00000548, 000031, 'April 2016', 1, '0'),
(00000547, 000031, 'April 2016', 0, '0'),
(00000546, 000031, 'March 2016', 1, '0'),
(00000545, 000031, 'March 2016', 0, '0'),
(00000544, 000031, 'February 2016', 1, '0'),
(00000543, 000031, 'February 2016', 0, '0'),
(00000542, 000031, 'January 2016', 1, '0'),
(00000541, 000031, 'January 2016', 0, '0'),
(00000540, 000030, 'February 2017', 0, '1'),
(00000539, 000030, 'January 2017', 1, '2'),
(00000538, 000030, 'January 2017', 0, '3'),
(00000537, 000030, 'December 2016', 1, '4'),
(00000536, 000030, 'December 2016', 0, '5'),
(00000535, 000030, 'November 2016', 1, '6'),
(00000534, 000030, 'November 2016', 0, '8'),
(00000533, 000030, 'October 2016', 1, '10'),
(00000532, 000030, 'October 2016', 0, '13'),
(00000531, 000030, 'September 2016', 1, '16'),
(00000530, 000030, 'September 2016', 0, '20'),
(00000529, 000030, 'August 2016', 1, '24'),
(00000528, 000030, 'August 2016', 0, '28'),
(00000527, 000030, 'July 2016', 1, '33'),
(00000526, 000030, 'July 2016', 0, '38'),
(00000525, 000030, 'June 2016', 1, '45'),
(00000524, 000030, 'June 2016', 0, '55'),
(00000523, 000030, 'May 2016', 1, '60'),
(00000522, 000030, 'May 2016', 0, '70'),
(00000521, 000030, 'April 2016', 1, '80'),
(00000520, 000030, 'April 2016', 0, '90'),
(00000519, 000030, 'March 2016', 1, '92'),
(00000518, 000030, 'March 2016', 0, '95'),
(00000517, 000030, 'February 2016', 1, '96'),
(00000516, 000030, 'February 2016', 0, '97'),
(00000515, 000030, 'January 2016', 1, '98'),
(00000514, 000030, 'January 2016', 0, '99'),
(00000513, 000030, 'December 2015', 1, '100');

-- --------------------------------------------------------

--
-- Table structure for table `data_laporan_harian`
--

CREATE TABLE `data_laporan_harian` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `progres_harian` varchar(15) NOT NULL,
  `id_aktifitas` int(6) UNSIGNED ZEROFILL NOT NULL,
  `item` text,
  `id_laporan_harian` int(6) UNSIGNED ZEROFILL NOT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `link_foto` varchar(100) DEFAULT NULL,
  `status_pekerjaan` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_laporan_harian`
--

INSERT INTO `data_laporan_harian` (`id`, `progres_harian`, `id_aktifitas`, `item`, `id_laporan_harian`, `deskripsi`, `link_foto`, `status_pekerjaan`) VALUES
(000342, '', 000018, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', 000030, NULL, NULL, 0),
(000341, '', 000017, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', 000030, NULL, NULL, 0),
(000340, '', 000016, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', 000030, NULL, NULL, 0),
(000339, '', 000015, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', 000030, NULL, NULL, 0),
(000338, '', 000014, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', 000030, NULL, NULL, 0),
(000337, '', 000013, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', 000030, NULL, NULL, 0),
(000336, '', 000012, 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', 000030, NULL, NULL, 0),
(000335, '', 000011, 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', 000030, NULL, NULL, 0),
(000334, '', 000010, 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', 000030, NULL, NULL, 0),
(000333, '', 000009, 'Paint maker|1#Colour|1#Product name|1#Aplication Methode|1#Cleaning Standard|1#Blasting/Roughness|1#Relative Humidity (RH)|1#Steel Temperature|1#Grade|1#Wet Temp|1#Dry Temp|1#', 000030, NULL, NULL, 0),
(000332, '', 000008, 'Grade/Type|1#Nomor Sertifikat|1#Quantity|1#Ukuran Material|1#Lubang|1#Deformasi|1#Pengelupasan permukaan|1#', 000030, NULL, NULL, 0),
(000331, '', 000007, 'Bukti Pembelian Barang|1#', 000030, NULL, NULL, 0),
(000330, '', 000018, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', 000029, NULL, NULL, 0),
(000329, '', 000017, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', 000029, NULL, NULL, 0),
(000328, '', 000016, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', 000029, NULL, NULL, 0),
(000327, '', 000015, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', 000029, NULL, NULL, 0),
(000326, '', 000014, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', 000029, NULL, NULL, 0),
(000325, '', 000013, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', 000029, NULL, NULL, 0),
(000324, '', 000012, 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', 000029, NULL, NULL, 0),
(000323, '', 000011, 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', 000029, NULL, NULL, 0),
(000322, '', 000010, 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', 000029, NULL, NULL, 0),
(000321, '', 000009, 'Paint maker|1#Colour|1#Product name|1#Aplication Methode|1#Cleaning Standard|1#Blasting/Roughness|1#Relative Humidity (RH)|1#Steel Temperature|1#Grade|1#Wet Temp|1#Dry Temp|1#', 000029, NULL, NULL, 0),
(000320, '', 000008, 'Grade/Type|1#Nomor Sertifikat|1#Quantity|1#Ukuran Material|1#Lubang|1#Deformasi|1#Pengelupasan permukaan|1#', 000029, NULL, NULL, 0),
(000319, '', 000007, 'Bukti Pembelian Barang|1#', 000029, NULL, NULL, 0),
(000318, '', 000018, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', 000028, NULL, NULL, 0),
(000317, '', 000017, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', 000028, NULL, NULL, 0),
(000316, '', 000016, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', 000028, NULL, NULL, 0),
(000315, '', 000015, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', 000028, NULL, NULL, 0),
(000314, '', 000014, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', 000028, NULL, NULL, 0),
(000313, '', 000013, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', 000028, NULL, NULL, 0),
(000312, '', 000012, 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', 000028, NULL, NULL, 0),
(000311, '', 000011, 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', 000028, NULL, NULL, 0),
(000310, '', 000010, 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', 000028, NULL, NULL, 0),
(000308, '', 000008, 'Grade/Type|1#Nomor Sertifikat|1#Quantity|1#Ukuran Material|1#Lubang|1#Deformasi|1#Pengelupasan permukaan|1#', 000028, NULL, NULL, 0),
(000309, '', 000009, 'Paint maker|1#Colour|1#Product name|1#Aplication Methode|1#Cleaning Standard|1#Blasting/Roughness|1#Relative Humidity (RH)|1#Steel Temperature|1#Grade|1#Wet Temp|1#Dry Temp|1#', 000028, NULL, NULL, 0),
(000307, '', 000007, 'Bukti Pembelian Barang|1#', 000028, NULL, NULL, 0),
(000306, '', 000018, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', 000027, NULL, NULL, 0),
(000305, '', 000017, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', 000027, NULL, NULL, 0),
(000304, '', 000016, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', 000027, NULL, NULL, 0),
(000303, '', 000015, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', 000027, NULL, NULL, 0),
(000302, '', 000014, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', 000027, NULL, NULL, 0),
(000301, '', 000013, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', 000027, NULL, NULL, 0),
(000300, '', 000012, 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', 000027, NULL, NULL, 0),
(000299, '', 000011, 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', 000027, NULL, NULL, 0),
(000298, '', 000010, 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', 000027, NULL, NULL, 0),
(000297, '', 000009, 'Paint maker|0#Colour|0#Product name|0#Aplication Methode|0#Cleaning Standard|0#Blasting/Roughness|0#Relative Humidity (RH)|0#Steel Temperature|0#Grade|0#Wet Temp|0#Dry Temp|0#', 000027, NULL, NULL, 0),
(000295, '', 000007, 'Bukti Pembelian Barang|1#', 000027, NULL, NULL, 0),
(000296, '', 000008, 'Grade/Type|1#Nomor Sertifikat|1#Quantity|1#Ukuran Material|1#Lubang|1#Deformasi|1#Pengelupasan permukaan|1#', 000027, NULL, NULL, 0),
(000294, '', 000018, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', 000026, NULL, NULL, 0),
(000293, '', 000017, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', 000026, NULL, NULL, 0),
(000292, '', 000016, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', 000026, NULL, NULL, 0),
(000291, '', 000015, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', 000026, NULL, NULL, 0),
(000290, '', 000014, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', 000026, NULL, NULL, 0),
(000289, '', 000013, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', 000026, NULL, NULL, 0),
(000288, '', 000012, 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', 000026, NULL, NULL, 0),
(000287, '', 000011, 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', 000026, NULL, NULL, 0),
(000286, '', 000010, 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', 000026, NULL, NULL, 0),
(000285, '', 000009, 'Paint maker|1#Colour|1#Product name|1#Aplication Methode|1#Cleaning Standard|1#Blasting/Roughness|1#Relative Humidity (RH)|1#Steel Temperature|1#Grade|1#Wet Temp|1#Dry Temp|1#', 000026, NULL, NULL, 0),
(000284, '', 000008, 'Grade/Type|1#Nomor Sertifikat|1#Quantity|1#Ukuran Material|1#Lubang|1#Deformasi|1#Pengelupasan permukaan|1#', 000026, NULL, NULL, 0),
(000283, '', 000007, 'Bukti Pembelian Barang|1#', 000026, NULL, NULL, 0),
(000282, '', 000018, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', 000025, NULL, NULL, 0),
(000281, '', 000017, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', 000025, NULL, NULL, 0),
(000280, '', 000016, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', 000025, NULL, NULL, 0),
(000279, '', 000015, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', 000025, NULL, NULL, 0),
(000278, '', 000014, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', 000025, NULL, NULL, 0),
(000276, '', 000012, 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', 000025, NULL, NULL, 0),
(000277, '', 000013, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', 000025, NULL, NULL, 0),
(000275, '', 000011, 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', 000025, NULL, NULL, 0),
(000274, '', 000010, 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', 000025, NULL, NULL, 0),
(000273, '', 000009, 'Paint maker|1#Colour|1#Product name|1#Aplication Methode|1#Cleaning Standard|1#Blasting/Roughness|1#Relative Humidity (RH)|1#Steel Temperature|1#Grade|1#Wet Temp|1#Dry Temp|1#', 000025, NULL, NULL, 0),
(000272, '', 000008, 'Grade/Type|1#Nomor Sertifikat|1#Quantity|1#Ukuran Material|1#Lubang|1#Deformasi|1#Pengelupasan permukaan|1#', 000025, NULL, NULL, 0),
(000271, '', 000007, 'Bukti Pembelian Barang|1#', 000025, NULL, NULL, 0),
(000270, '', 000018, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', 000024, NULL, NULL, 0),
(000269, '', 000017, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', 000024, NULL, NULL, 0),
(000268, '', 000016, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', 000024, NULL, NULL, 0),
(000267, '', 000015, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', 000024, NULL, NULL, 0),
(000266, '', 000014, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', 000024, NULL, NULL, 0),
(000265, '', 000013, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', 000024, NULL, NULL, 0),
(000264, '', 000012, 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', 000024, NULL, NULL, 0),
(000263, '', 000011, 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', 000024, NULL, NULL, 0),
(000262, '', 000010, 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', 000024, NULL, NULL, 0),
(000261, '', 000009, 'Paint maker|1#Colour|1#Product name|1#Aplication Methode|1#Cleaning Standard|1#Blasting/Roughness|1#Relative Humidity (RH)|1#Steel Temperature|1#Grade|1#Wet Temp|1#Dry Temp|1#', 000024, NULL, NULL, 0),
(000259, '', 000007, 'Bukti Pembelian Barang|1#', 000024, NULL, NULL, 0),
(000260, '', 000008, 'Grade/Type|1#Nomor Sertifikat|1#Quantity|1#Ukuran Material|1#Lubang|1#Deformasi|1#Pengelupasan permukaan|1#', 000024, NULL, NULL, 0),
(000258, '', 000018, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', 000023, NULL, NULL, 0),
(000257, '', 000017, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', 000023, NULL, NULL, 0),
(000256, '', 000016, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', 000023, NULL, NULL, 0),
(000255, '', 000015, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', 000023, NULL, NULL, 0),
(000253, '', 000013, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', 000023, NULL, NULL, 0),
(000254, '', 000014, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', 000023, NULL, NULL, 0),
(000252, '', 000012, 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', 000023, NULL, NULL, 0),
(000251, '', 000011, 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', 000023, NULL, NULL, 0),
(000250, '', 000010, 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', 000023, NULL, NULL, 0),
(000249, '', 000009, 'Paint maker|1#Colour|1#Product name|1#Aplication Methode|1#Cleaning Standard|1#Blasting/Roughness|1#Relative Humidity (RH)|1#Steel Temperature|1#Grade|1#Wet Temp|1#Dry Temp|1#', 000023, NULL, NULL, 0),
(000247, '', 000007, 'Bukti Pembelian Barang|1#', 000023, NULL, NULL, 0),
(000248, '', 000008, 'Grade/Type|1#Nomor Sertifikat|1#Quantity|1#Ukuran Material|1#Lubang|1#Deformasi|1#Pengelupasan permukaan|1#', 000023, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `data_laporan_mingguan`
--

CREATE TABLE `data_laporan_mingguan` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_laporan_mingguan` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_aktivitas` int(6) UNSIGNED ZEROFILL NOT NULL,
  `item_pengawasan` text NOT NULL,
  `progres_aktual` varchar(10) NOT NULL,
  `deskripsi` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_laporan_mingguan`
--

INSERT INTO `data_laporan_mingguan` (`id`, `id_laporan_mingguan`, `id_aktivitas`, `item_pengawasan`, `progres_aktual`, `deskripsi`) VALUES
(000724, 000064, 000007, 'Bukti Pembelian Barang|1#', '100', ''),
(000723, 000064, 000008, 'Grade/Type|1#Nomor Sertifikat|1#Quantity|1#Ukuran Material|1#Lubang|1#Deformasi|1#Pengelupasan permukaan|1#', '100', ''),
(000722, 000064, 000009, 'Paint maker|1#Colour|1#Product name|1#Aplication Methode|1#Cleaning Standard|1#Blasting/Roughness|1#Relative Humidity (RH)|1#Steel Temperature|1#Grade|1#Wet Temp|1#Dry Temp|1#', '100', ''),
(000721, 000064, 000010, 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', '0', ''),
(000720, 000064, 000011, 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', '0', ''),
(000719, 000064, 000012, 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', '0', ''),
(000718, 000064, 000013, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '0', ''),
(000717, 000064, 000014, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '0', ''),
(000716, 000064, 000015, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', '0', ''),
(000715, 000064, 000016, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '0', ''),
(000714, 000064, 000017, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '0', ''),
(000713, 000064, 000018, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', '0', ''),
(000712, 000063, 000007, 'Bukti Pembelian Barang|1#', '100', ''),
(000711, 000063, 000008, 'Grade/Type|1#Nomor Sertifikat|1#Quantity|1#Ukuran Material|1#Lubang|1#Deformasi|1#Pengelupasan permukaan|1#', '100', ''),
(000710, 000063, 000009, 'Paint maker|1#Colour|1#Product name|1#Aplication Methode|1#Cleaning Standard|1#Blasting/Roughness|1#Relative Humidity (RH)|1#Steel Temperature|1#Grade|1#Wet Temp|1#Dry Temp|1#', '100', ''),
(000709, 000063, 000010, 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', '0', ''),
(000708, 000063, 000011, 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', '0', ''),
(000707, 000063, 000012, 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', '0', ''),
(000706, 000063, 000013, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '0', ''),
(000705, 000063, 000014, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '0', ''),
(000704, 000063, 000015, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', '0', ''),
(000703, 000063, 000016, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '0', ''),
(000702, 000063, 000017, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '0', ''),
(000701, 000063, 000018, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', '0', ''),
(000700, 000062, 000007, 'Bukti Pembelian Barang|1#', '100', 'bukti pembayaran sudah diterima'),
(000699, 000062, 000009, 'Paint maker|1#Colour|1#Product name|1#Aplication Methode|1#Cleaning Standard|1#Blasting/Roughness|1#Relative Humidity (RH)|1#Steel Temperature|1#Grade|1#Wet Temp|1#Dry Temp|1#', '100', 'pengawasan pengerjaan blasting '),
(000698, 000062, 000008, 'Grade/Type|1#Nomor Sertifikat|1#Quantity|1#Ukuran Material|1#Lubang|1#Deformasi|1#Pengelupasan permukaan|1#', '100', 'barang datang tepat waktu'),
(000697, 000062, 000010, 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', '0', ''),
(000696, 000062, 000011, 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', '0', ''),
(000695, 000062, 000012, 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', '0', ''),
(000694, 000062, 000013, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '0', ''),
(000693, 000062, 000014, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '0', ''),
(000692, 000062, 000015, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', '0', ''),
(000691, 000062, 000016, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '0', ''),
(000690, 000062, 000017, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '0', ''),
(000689, 000062, 000018, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', '0', ''),
(000688, 000061, 000007, 'Bukti Pembelian Barang|1#', '100', 'bukti pembayaran sudah diterima'),
(000687, 000061, 000008, 'Grade/Type|1#Nomor Sertifikat|1#Quantity|1#Ukuran Material|1#Lubang|1#Deformasi|1#Pengelupasan permukaan|1#', '100', 'barang datang tepat waktu'),
(000686, 000061, 000009, 'Paint maker|1#Colour|1#Product name|1#Aplication Methode|1#Cleaning Standard|1#Blasting/Roughness|1#Relative Humidity (RH)|1#Steel Temperature|1#Grade|1#Wet Temp|1#Dry Temp|1#', '100', 'pengawasan pengerjaan blasting '),
(000685, 000061, 000010, 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', '0', ''),
(000684, 000061, 000011, 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', '0', ''),
(000683, 000061, 000012, 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', '0', ''),
(000682, 000061, 000013, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '0', ''),
(000681, 000061, 000014, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '0', ''),
(000680, 000061, 000015, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', '0', ''),
(000679, 000061, 000016, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '0', ''),
(000678, 000061, 000017, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '0', ''),
(000677, 000061, 000018, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', '0', ''),
(000676, 000060, 000007, 'Bukti Pembelian Barang|1#', '100', 'bukti pembayaran sudah diterima'),
(000675, 000060, 000008, 'Grade/Type|1#Nomor Sertifikat|1#Quantity|1#Ukuran Material|1#Lubang|1#Deformasi|1#Pengelupasan permukaan|1#', '100', 'barang datang tepat waktu'),
(000674, 000060, 000009, 'Paint maker|1#Colour|1#Product name|1#Aplication Methode|1#Cleaning Standard|1#Blasting/Roughness|1#Relative Humidity (RH)|1#Steel Temperature|1#Grade|1#Wet Temp|1#Dry Temp|1#', '100', 'pengawasan pengerjaan blasting '),
(000673, 000060, 000010, 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', '0', ''),
(000672, 000060, 000011, 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', '0', ''),
(000671, 000060, 000013, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '0', ''),
(000670, 000060, 000012, 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', '0', ''),
(000669, 000060, 000014, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '0', ''),
(000668, 000060, 000015, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', '0', ''),
(000667, 000060, 000016, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '0', ''),
(000666, 000060, 000017, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '0', ''),
(000665, 000060, 000018, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', '0', ''),
(000664, 000059, 000008, 'Grade/Type|1#Nomor Sertifikat|1#Quantity|1#Ukuran Material|1#Lubang|1#Deformasi|1#Pengelupasan permukaan|1#', '100', 'barang datang tepat waktu'),
(000663, 000059, 000007, 'Bukti Pembelian Barang|1#', '100', 'bukti pembayaran sudah diterima'),
(000662, 000059, 000009, 'Paint maker|1#Colour|1#Product name|1#Aplication Methode|1#Cleaning Standard|1#Blasting/Roughness|1#Relative Humidity (RH)|1#Steel Temperature|1#Grade|1#Wet Temp|1#Dry Temp|1#', '100', 'pengawasan pengerjaan blasting '),
(000661, 000059, 000010, 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', '0', ''),
(000660, 000059, 000011, 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', '0', ''),
(000659, 000059, 000012, 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', '0', ''),
(000658, 000059, 000013, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '0', ''),
(000657, 000059, 000014, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '0', ''),
(000656, 000059, 000015, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', '0', ''),
(000655, 000059, 000016, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '0', ''),
(000654, 000059, 000017, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '0', ''),
(000653, 000059, 000018, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', '0', ''),
(000652, 000058, 000008, 'Grade/Type|1#Nomor Sertifikat|1#Quantity|1#Ukuran Material|1#Lubang|1#Deformasi|1#Pengelupasan permukaan|1#', '100', 'barang datang tepat waktu'),
(000651, 000058, 000007, 'Bukti Pembelian Barang|1#', '100', 'bukti pembayaran sudah diterima'),
(000650, 000058, 000009, 'Paint maker|1#Colour|1#Product name|1#Aplication Methode|1#Cleaning Standard|1#Blasting/Roughness|1#Relative Humidity (RH)|1#Steel Temperature|1#Grade|1#Wet Temp|1#Dry Temp|1#', '100', 'pengawasan pengerjaan blasting '),
(000649, 000058, 000010, 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', '0', ''),
(000648, 000058, 000011, 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', '0', ''),
(000647, 000058, 000012, 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', '0', ''),
(000646, 000058, 000014, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '0', ''),
(000645, 000058, 000013, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '0', ''),
(000644, 000058, 000015, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', '0', ''),
(000643, 000058, 000016, 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '0', ''),
(000641, 000058, 000018, 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', '0', ''),
(000642, 000058, 000017, 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `galangan`
--

CREATE TABLE `galangan` (
  `id_galangan` int(6) UNSIGNED ZEROFILL NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `alamat_perusahaan` varchar(50) NOT NULL,
  `pimpinan_proyek` varchar(50) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galangan`
--

INSERT INTO `galangan` (`id_galangan`, `nama_perusahaan`, `alamat_perusahaan`, `pimpinan_proyek`, `telp`, `email`) VALUES
(000001, 'PT. Dumas Tanjung Perak Shipyard', 'Jl Nilam Barat No.12, Jawa Timur 60117', 'Wimpy', '09909090', 'Galangan@galangan.com'),
(000002, 'PT. Adiluhung Saranasegara Indonesia', 'JL. Raya Ujung Piring, Kec. Bangkalan, 69118', 'Sugiyarto', '(031) 3097039', 'adiluhung@gmail.com'),
(000003, 'PT. Lamongan Marine Industry (LMI)', 'Jl Raya Daendles Km. 63, Desa Sidokelar, Kec. Paci', 'Fikri', '09909090', 'lmi@gmail.com'),
(000004, 'PT.Dok Bahari Nusantara', 'Jl. Ambon, Lemahwungkuk, Kota Cirebon, Jawa Barat ', 'Andi', '02189456', 'dokbahari@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `gambar_rancangan`
--

CREATE TABLE `gambar_rancangan` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_kapal` int(6) UNSIGNED ZEROFILL NOT NULL,
  `link` varchar(70) NOT NULL,
  `judul` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE `group` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_workgroup_kapal` int(6) UNSIGNED ZEROFILL NOT NULL,
  `group` varchar(40) NOT NULL,
  `bobot_workgroup` varchar(15) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `satuan` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group`
--

INSERT INTO `group` (`id`, `id_workgroup_kapal`, `group`, `bobot_workgroup`, `kuantitas`, `satuan`) VALUES
(000221, 000234, 'Blok 20', '5', 30, 'ton'),
(000220, 000234, 'Blok 19', '5', 30, 'ton'),
(000219, 000234, 'Blok 18', '5', 30, 'ton'),
(000218, 000234, 'Blok 17', '5', 30, 'ton'),
(000217, 000234, 'Blok 16', '5', 30, 'ton'),
(000216, 000234, 'Blok 15', '5', 30, 'ton'),
(000215, 000234, 'Blok 14', '5', 30, 'ton'),
(000214, 000234, 'Blok 13', '5', 30, 'ton'),
(000213, 000234, 'Blok 12', '5', 30, 'ton'),
(000212, 000234, 'Blok 11', '5', 30, 'ton'),
(000211, 000234, 'Blok 10', '5', 30, 'ton'),
(000210, 000234, 'Blok 9', '5', 30, 'ton'),
(000209, 000234, 'Blok 8', '5', 30, 'ton'),
(000208, 000234, 'Blok 7', '5', 30, 'ton'),
(000207, 000234, 'Blok 6', '5', 30, 'ton'),
(000206, 000234, 'Blok 5', '5', 30, 'ton'),
(000205, 000234, 'Blok 4', '5', 30, 'ton'),
(000204, 000234, 'Blok 3', '5', 30, 'ton'),
(000203, 000234, 'Blok 2', '5', 30, 'ton'),
(000202, 000234, 'Blok 1', '5', 30, 'ton');

-- --------------------------------------------------------

--
-- Table structure for table `laporan_alat`
--

CREATE TABLE `laporan_alat` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `nama_alat` varchar(6) NOT NULL,
  `id_laporan_harian` int(6) UNSIGNED ZEROFILL NOT NULL,
  `jumlah_alat` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laporan_alat`
--

INSERT INTO `laporan_alat` (`id`, `nama_alat`, `id_laporan_harian`, `jumlah_alat`) VALUES
(000019, '', 000011, 0),
(000020, '', 000011, 0),
(000021, '', 000011, 0),
(000022, '', 000012, 0),
(000023, '', 000012, 0),
(000024, '', 000013, 0),
(000025, '', 000013, 0),
(000026, '', 000014, 0),
(000027, '', 000014, 0),
(000028, '', 000015, 0),
(000029, '', 000015, 0),
(000030, '', 000016, 0),
(000031, '', 000016, 0),
(000032, '', 000016, 0);

-- --------------------------------------------------------

--
-- Table structure for table `laporan_harian`
--

CREATE TABLE `laporan_harian` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_group` int(6) UNSIGNED ZEROFILL NOT NULL,
  `tgl_laporan` date NOT NULL,
  `cuaca_pagi` varchar(30) NOT NULL,
  `cuaca_siang` varchar(30) NOT NULL,
  `cuaca_sore` varchar(30) NOT NULL,
  `catatan_galangan` varchar(100) NOT NULL,
  `catatan_pm` varchar(100) NOT NULL,
  `tanggapan_galangan` varchar(100) NOT NULL,
  `tanggapan_pm` varchar(100) NOT NULL,
  `verifikasi_owner` int(3) NOT NULL,
  `verifikasi_pm` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laporan_harian`
--

INSERT INTO `laporan_harian` (`id`, `id_group`, `tgl_laporan`, `cuaca_pagi`, `cuaca_siang`, `cuaca_sore`, `catatan_galangan`, `catatan_pm`, `tanggapan_galangan`, `tanggapan_pm`, `verifikasi_owner`, `verifikasi_pm`) VALUES
(000030, 000208, '2015-12-22', '', '', '', '', '', '', '', 0, 0),
(000029, 000207, '2015-12-22', '', '', '', '', '', '', '', 0, 0),
(000028, 000206, '2016-01-27', '', '', '', '', '', '', '', 0, 0),
(000027, 000221, '2016-01-27', '', '', '', '', '', '', '', 0, 0),
(000026, 000205, '2016-01-27', '', '', '', '', '', '', '', 0, 0),
(000025, 000204, '2016-01-27', '', '', '', '', '', '', '', 0, 0),
(000024, 000203, '2016-01-27', '', '', '', '', '', '', '', 0, 0),
(000023, 000202, '2016-01-27', '', '', '', '', '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `laporan_mingguan`
--

CREATE TABLE `laporan_mingguan` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_group` int(6) UNSIGNED ZEROFILL NOT NULL,
  `tgl_input` date NOT NULL,
  `periode` varchar(40) NOT NULL,
  `mid_end` int(3) NOT NULL COMMENT '0=mid, 1=end',
  `progres_group` varchar(5) DEFAULT NULL,
  `verif_owner` int(3) DEFAULT NULL,
  `os_verifikator` varchar(50) DEFAULT NULL,
  `class_verifikator` varchar(50) DEFAULT NULL,
  `qa_verifikator` varchar(50) DEFAULT NULL,
  `catatan_os` varchar(128) DEFAULT NULL,
  `catatan_owner` varchar(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laporan_mingguan`
--

INSERT INTO `laporan_mingguan` (`id`, `id_group`, `tgl_input`, `periode`, `mid_end`, `progres_group`, `verif_owner`, `os_verifikator`, `class_verifikator`, `qa_verifikator`, `catatan_os`, `catatan_owner`) VALUES
(000064, 000208, '2015-12-22', 'December 2015', 1, '30', 0, NULL, NULL, NULL, '', NULL),
(000063, 000207, '2015-12-22', 'December 2015', 1, '30', 0, NULL, NULL, NULL, '', NULL),
(000062, 000206, '2016-01-27', 'December 2015', 1, '30', 0, NULL, NULL, NULL, '', NULL),
(000061, 000205, '2016-01-27', 'December 2015', 1, '30', 0, NULL, NULL, NULL, '', NULL),
(000060, 000204, '2016-01-27', 'December 2015', 1, '30', 0, NULL, NULL, NULL, '', NULL),
(000059, 000203, '2016-01-27', 'December 2015', 1, '30', 0, NULL, NULL, NULL, '', NULL),
(000058, 000202, '2016-01-27', 'December 2015', 1, '30', 0, NULL, NULL, NULL, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `laporan_tenaga_kerja`
--

CREATE TABLE `laporan_tenaga_kerja` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_laporan_harian` int(6) UNSIGNED ZEROFILL NOT NULL,
  `tenaga_kerja` varchar(50) NOT NULL,
  `jumlah` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laporan_tenaga_kerja`
--

INSERT INTO `laporan_tenaga_kerja` (`id`, `id_laporan_harian`, `tenaga_kerja`, `jumlah`) VALUES
(000019, 000011, '', 0),
(000020, 000011, '', 0),
(000021, 000011, '', 0),
(000022, 000012, '', 0),
(000023, 000012, '', 0),
(000024, 000013, '', 0),
(000025, 000013, '', 0),
(000026, 000014, '', 0),
(000027, 000014, '', 0),
(000028, 000015, '', 0),
(000029, 000015, '', 0),
(000030, 000016, 'satu', 2),
(000031, 000016, 'dua', 2),
(000032, 000016, 'tiga', 2);

-- --------------------------------------------------------

--
-- Table structure for table `owner`
--

CREATE TABLE `owner` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `alamat_perusahaan` varchar(50) DEFAULT NULL,
  `penanggung_jawab` varchar(50) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `link_foto` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `owner`
--

INSERT INTO `owner` (`id`, `nama_perusahaan`, `alamat_perusahaan`, `penanggung_jawab`, `no_telp`, `link_foto`, `email`, `password`) VALUES
(000001, 'DIRJEN PERHUBUNGAN LAUT', 'JAKARTA', 'Sugiarto', '97987987979', NULL, 'owner@owner.com', 'owner');

-- --------------------------------------------------------

--
-- Table structure for table `owner_surveyor`
--

CREATE TABLE `owner_surveyor` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `alamat_perusahaan` varchar(50) DEFAULT NULL,
  `penanggung_jawab` varchar(50) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `link_foto` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `owner_surveyor`
--

INSERT INTO `owner_surveyor` (`id`, `nama_perusahaan`, `alamat_perusahaan`, `penanggung_jawab`, `no_telp`, `link_foto`, `email`, `password`) VALUES
(000001, 'PT OS Berkarya', 'Surabaya', 'Tono OS', '085728640629', NULL, 'os@os.com', 'os');

-- --------------------------------------------------------

--
-- Table structure for table `pembuatan_kapal`
--

CREATE TABLE `pembuatan_kapal` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `nama_proyek` varchar(50) NOT NULL,
  `pemilik` varchar(50) NOT NULL,
  `kontraktor` varchar(50) NOT NULL,
  `konsultan` varchar(50) NOT NULL,
  `jenis_kapal` varchar(50) NOT NULL,
  `lpp` int(11) NOT NULL,
  `loa` int(11) NOT NULL,
  `lebar` int(11) NOT NULL,
  `tinggi` int(11) NOT NULL,
  `sarat_air` int(11) NOT NULL,
  `kecepatan` int(11) NOT NULL,
  `lama_pengerjaan` int(11) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `status_pengerjaan` int(11) NOT NULL,
  `id_os` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_galangan` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_owner` int(6) UNSIGNED ZEROFILL NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembuatan_kapal`
--

INSERT INTO `pembuatan_kapal` (`id`, `nama_proyek`, `pemilik`, `kontraktor`, `konsultan`, `jenis_kapal`, `lpp`, `loa`, `lebar`, `tinggi`, `sarat_air`, `kecepatan`, `lama_pengerjaan`, `tanggal_mulai`, `status_pengerjaan`, `id_os`, `id_galangan`, `id_owner`) VALUES
(000030, 'Kapal Perintis Tipe 750 DWT Tipe A', 'Direktorat Perhubungan Laut', '', '', 'Perintis', 59, 52, 12, 5, 3, 12, 14, '0000-00-00', 0, 000001, 000001, 000001),
(000031, 'Kapal Perintis Tipe 750 DWT Tipe B', 'Direktorat Perhubungan Laut', '', '', 'Perintis', 49, 52, 12, 5, 3, 12, 14, '0000-00-00', 0, 000001, 000002, 000001),
(000032, 'Kapal Perintis Tipe 750 DWT Tipe C ', 'Direktorat Perhubungan Laut', '', '', 'Perintis', 49, 52, 12, 5, 3, 12, 14, '0000-00-00', 0, 000001, 000003, 000001);

-- --------------------------------------------------------

--
-- Table structure for table `progres`
--

CREATE TABLE `progres` (
  `id` int(8) UNSIGNED ZEROFILL NOT NULL,
  `id_kapal` int(6) UNSIGNED ZEROFILL NOT NULL,
  `periode` varchar(25) DEFAULT NULL,
  `mid_end` int(3) DEFAULT NULL,
  `progres` varchar(10) NOT NULL,
  `tgl_input` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `progres`
--

INSERT INTO `progres` (`id`, `id_kapal`, `periode`, `mid_end`, `progres`, `tgl_input`) VALUES
(00000596, 000032, 'February 2017', 1, '0', NULL),
(00000595, 000032, 'February 2017', 0, '0', NULL),
(00000594, 000032, 'January 2017', 1, '0', NULL),
(00000593, 000032, 'January 2017', 0, '0', NULL),
(00000592, 000032, 'December 2016', 1, '0', NULL),
(00000591, 000032, 'December 2016', 0, '0', NULL),
(00000590, 000032, 'November 2016', 1, '0', NULL),
(00000589, 000032, 'November 2016', 0, '0', NULL),
(00000588, 000032, 'October 2016', 1, '0', NULL),
(00000587, 000032, 'October 2016', 0, '0', NULL),
(00000586, 000032, 'September 2016', 1, '0', NULL),
(00000585, 000032, 'September 2016', 0, '0', NULL),
(00000584, 000032, 'August 2016', 1, '0', NULL),
(00000583, 000032, 'August 2016', 0, '0', NULL),
(00000582, 000032, 'July 2016', 1, '0', NULL),
(00000581, 000032, 'July 2016', 0, '0', NULL),
(00000580, 000032, 'June 2016', 1, '0', NULL),
(00000579, 000032, 'June 2016', 0, '0', NULL),
(00000578, 000032, 'May 2016', 1, '0', NULL),
(00000577, 000032, 'May 2016', 0, '0', NULL),
(00000576, 000032, 'April 2016', 1, '0', NULL),
(00000575, 000032, 'April 2016', 0, '0', NULL),
(00000574, 000032, 'March 2016', 1, '0', NULL),
(00000573, 000032, 'March 2016', 0, '0', NULL),
(00000572, 000032, 'February 2016', 1, '0', NULL),
(00000571, 000032, 'February 2016', 0, '0', NULL),
(00000570, 000032, 'January 2016', 1, '0', NULL),
(00000569, 000032, 'January 2016', 0, '0', NULL),
(00000568, 000031, 'February 2017', 1, '0', NULL),
(00000567, 000031, 'February 2017', 0, '0', NULL),
(00000566, 000031, 'January 2017', 1, '0', NULL),
(00000565, 000031, 'January 2017', 0, '0', NULL),
(00000564, 000031, 'December 2016', 1, '0', NULL),
(00000563, 000031, 'December 2016', 0, '0', NULL),
(00000562, 000031, 'November 2016', 1, '0', NULL),
(00000561, 000031, 'November 2016', 0, '0', NULL),
(00000560, 000031, 'October 2016', 1, '0', NULL),
(00000559, 000031, 'October 2016', 0, '0', NULL),
(00000558, 000031, 'September 2016', 1, '0', NULL),
(00000557, 000031, 'September 2016', 0, '0', NULL),
(00000556, 000031, 'August 2016', 1, '0', NULL),
(00000555, 000031, 'August 2016', 0, '0', NULL),
(00000554, 000031, 'July 2016', 1, '0', NULL),
(00000553, 000031, 'July 2016', 0, '0', NULL),
(00000552, 000031, 'June 2016', 1, '0', NULL),
(00000551, 000031, 'June 2016', 0, '0', NULL),
(00000550, 000031, 'May 2016', 1, '0', NULL),
(00000549, 000031, 'May 2016', 0, '0', NULL),
(00000548, 000031, 'April 2016', 1, '0', NULL),
(00000547, 000031, 'April 2016', 0, '0', NULL),
(00000546, 000031, 'March 2016', 1, '0', NULL),
(00000545, 000031, 'March 2016', 0, '0', NULL),
(00000544, 000031, 'February 2016', 1, '0', NULL),
(00000543, 000031, 'February 2016', 0, '0', NULL),
(00000542, 000031, 'January 2016', 1, '0', NULL),
(00000541, 000031, 'January 2016', 0, '0', NULL),
(00000540, 000030, 'February 2017', 0, '0', NULL),
(00000539, 000030, 'January 2017', 1, '0', NULL),
(00000538, 000030, 'January 2017', 0, '0', NULL),
(00000537, 000030, 'December 2016', 1, '0', NULL),
(00000536, 000030, 'December 2016', 0, '0', NULL),
(00000535, 000030, 'November 2016', 1, '0', NULL),
(00000534, 000030, 'November 2016', 0, '0', NULL),
(00000533, 000030, 'October 2016', 1, '0', NULL),
(00000532, 000030, 'October 2016', 0, '0', NULL),
(00000531, 000030, 'September 2016', 1, '0', NULL),
(00000530, 000030, 'September 2016', 0, '0', NULL),
(00000529, 000030, 'August 2016', 1, '0', NULL),
(00000528, 000030, 'August 2016', 0, '0', NULL),
(00000527, 000030, 'July 2016', 1, '0', NULL),
(00000526, 000030, 'July 2016', 0, '0', NULL),
(00000525, 000030, 'June 2016', 1, '0', NULL),
(00000524, 000030, 'June 2016', 0, '0', NULL),
(00000523, 000030, 'May 2016', 1, '0', NULL),
(00000522, 000030, 'May 2016', 0, '0', NULL),
(00000521, 000030, 'April 2016', 1, '0', NULL),
(00000520, 000030, 'April 2016', 0, '0', NULL),
(00000519, 000030, 'March 2016', 1, '0', NULL),
(00000518, 000030, 'March 2016', 0, '0', NULL),
(00000517, 000030, 'February 2016', 1, '0', NULL),
(00000516, 000030, 'February 2016', 0, '0', NULL),
(00000515, 000030, 'January 2016', 1, '0', NULL),
(00000514, 000030, 'January 2016', 0, '0', NULL),
(00000513, 000030, 'December 2015', 1, '1.05', '2015-12-22');

-- --------------------------------------------------------

--
-- Table structure for table `project_manager`
--

CREATE TABLE `project_manager` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `alamat_perusahaan` varchar(50) NOT NULL,
  `penanggung_jawab` varchar(50) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `link_foto` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tenaga_kerja`
--

CREATE TABLE `tenaga_kerja` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `keahlian` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tenaga_kerja`
--

INSERT INTO `tenaga_kerja` (`id`, `keahlian`) VALUES
(000001, 'keahlian1'),
(000002, 'keahlian2'),
(000003, 'keahlian3'),
(000004, 'keahlian4'),
(000005, 'keahlian5');

-- --------------------------------------------------------

--
-- Table structure for table `workgroup`
--

CREATE TABLE `workgroup` (
  `id_workgroup` int(6) UNSIGNED ZEROFILL NOT NULL,
  `bobot` int(11) NOT NULL,
  `nama_pekerjaan` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workgroup`
--

INSERT INTO `workgroup` (`id_workgroup`, `bobot`, `nama_pekerjaan`) VALUES
(000001, 10, 'Design/Approval Drawing'),
(000002, 10, 'Hull Construction'),
(000003, 15, 'Hull Outfitting'),
(000004, 15, 'Machinery Outfitting'),
(000005, 20, 'Pipping System'),
(000006, 10, 'Electrical Outfitting'),
(000007, 10, 'Painting & Coating'),
(000008, 10, 'Inventory');

-- --------------------------------------------------------

--
-- Table structure for table `workgroup_kapal`
--

CREATE TABLE `workgroup_kapal` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_kapal` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_workgroup` int(6) UNSIGNED ZEROFILL NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workgroup_kapal`
--

INSERT INTO `workgroup_kapal` (`id`, `id_kapal`, `id_workgroup`) VALUES
(000177, 000023, 000001),
(000178, 000023, 000002),
(000179, 000023, 000003),
(000180, 000023, 000004),
(000181, 000023, 000005),
(000182, 000023, 000006),
(000183, 000023, 000007),
(000184, 000023, 000008),
(000185, 000024, 000001),
(000186, 000024, 000002),
(000187, 000024, 000003),
(000188, 000024, 000004),
(000189, 000024, 000005),
(000190, 000024, 000006),
(000191, 000024, 000007),
(000192, 000024, 000008),
(000193, 000025, 000001),
(000194, 000025, 000002),
(000195, 000025, 000003),
(000196, 000025, 000004),
(000197, 000025, 000005),
(000198, 000025, 000006),
(000199, 000025, 000007),
(000200, 000025, 000008),
(000201, 000026, 000001),
(000202, 000026, 000002),
(000203, 000026, 000003),
(000204, 000026, 000004),
(000205, 000026, 000005),
(000206, 000026, 000006),
(000207, 000026, 000007),
(000208, 000026, 000008),
(000209, 000027, 000001),
(000210, 000027, 000002),
(000211, 000027, 000003),
(000212, 000027, 000004),
(000213, 000027, 000005),
(000214, 000027, 000006),
(000215, 000027, 000007),
(000216, 000027, 000008),
(000248, 000031, 000008),
(000247, 000031, 000007),
(000246, 000031, 000006),
(000245, 000031, 000005),
(000244, 000031, 000004),
(000243, 000031, 000003),
(000242, 000031, 000002),
(000241, 000031, 000001),
(000240, 000030, 000008),
(000239, 000030, 000007),
(000238, 000030, 000006),
(000237, 000030, 000005),
(000236, 000030, 000004),
(000235, 000030, 000003),
(000234, 000030, 000002),
(000233, 000030, 000001),
(000249, 000032, 000001),
(000250, 000032, 000002),
(000251, 000032, 000003),
(000252, 000032, 000004),
(000253, 000032, 000005),
(000254, 000032, 000006),
(000255, 000032, 000007),
(000256, 000032, 000008);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `aktifitas`
--
ALTER TABLE `aktifitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alat_kerja`
--
ALTER TABLE `alat_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `baseline`
--
ALTER TABLE `baseline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_laporan_harian`
--
ALTER TABLE `data_laporan_harian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_laporan_mingguan`
--
ALTER TABLE `data_laporan_mingguan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galangan`
--
ALTER TABLE `galangan`
  ADD PRIMARY KEY (`id_galangan`);

--
-- Indexes for table `gambar_rancangan`
--
ALTER TABLE `gambar_rancangan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan_alat`
--
ALTER TABLE `laporan_alat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan_harian`
--
ALTER TABLE `laporan_harian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan_mingguan`
--
ALTER TABLE `laporan_mingguan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan_tenaga_kerja`
--
ALTER TABLE `laporan_tenaga_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner`
--
ALTER TABLE `owner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner_surveyor`
--
ALTER TABLE `owner_surveyor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembuatan_kapal`
--
ALTER TABLE `pembuatan_kapal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `progres`
--
ALTER TABLE `progres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_manager`
--
ALTER TABLE `project_manager`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenaga_kerja`
--
ALTER TABLE `tenaga_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `workgroup`
--
ALTER TABLE `workgroup`
  ADD PRIMARY KEY (`id_workgroup`);

--
-- Indexes for table `workgroup_kapal`
--
ALTER TABLE `workgroup_kapal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(8) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `aktifitas`
--
ALTER TABLE `aktifitas`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `alat_kerja`
--
ALTER TABLE `alat_kerja`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `baseline`
--
ALTER TABLE `baseline`
  MODIFY `id` int(8) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=597;
--
-- AUTO_INCREMENT for table `data_laporan_harian`
--
ALTER TABLE `data_laporan_harian`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=343;
--
-- AUTO_INCREMENT for table `data_laporan_mingguan`
--
ALTER TABLE `data_laporan_mingguan`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=725;
--
-- AUTO_INCREMENT for table `galangan`
--
ALTER TABLE `galangan`
  MODIFY `id_galangan` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gambar_rancangan`
--
ALTER TABLE `gambar_rancangan`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `group`
--
ALTER TABLE `group`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;
--
-- AUTO_INCREMENT for table `laporan_alat`
--
ALTER TABLE `laporan_alat`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `laporan_harian`
--
ALTER TABLE `laporan_harian`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `laporan_mingguan`
--
ALTER TABLE `laporan_mingguan`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `laporan_tenaga_kerja`
--
ALTER TABLE `laporan_tenaga_kerja`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `owner`
--
ALTER TABLE `owner`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `owner_surveyor`
--
ALTER TABLE `owner_surveyor`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pembuatan_kapal`
--
ALTER TABLE `pembuatan_kapal`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `progres`
--
ALTER TABLE `progres`
  MODIFY `id` int(8) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=597;
--
-- AUTO_INCREMENT for table `project_manager`
--
ALTER TABLE `project_manager`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tenaga_kerja`
--
ALTER TABLE `tenaga_kerja`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `workgroup`
--
ALTER TABLE `workgroup`
  MODIFY `id_workgroup` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `workgroup_kapal`
--
ALTER TABLE `workgroup_kapal`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=257;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
