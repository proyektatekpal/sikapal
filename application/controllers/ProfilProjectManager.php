<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfilProjectManager extends CI_Controller {

	public function __construct()
    {
        parent::__construct(); 
        if ( !$this->session->userdata('isLogin') && $this->session->userdata('level') != 'projectmanager')
        { 
            redirect('access');
        }
        // $this->load->library('form_validation');
        $this->load->model('m_profil');
    }

	public function index()
	{
        
	}

    public function editProfil()
    {
        $email = $this->session->userdata('email');
        $Data['detailPM'] = $this->m_profil->getProjectManagerProfil($email);
        $this->load->view('header');
        $this->load->view('sidebar-pm');
        $this->load->view('pm/edit-profil', $Data);
        $this->load->view('footer');
    }

    public function simpanEdit()
    {
        
    }

    public function ubahPassword()
    {
        $this->load->view('header');
        $this->load->view('sidebar-pm');
        $this->load->view('pm/ubah-password');
        $this->load->view('footer');
    }

    public function simpanPassword()
    {
        
    }
}



