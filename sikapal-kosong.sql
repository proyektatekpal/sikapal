-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 24, 2015 at 08:01 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sikapal`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(8) UNSIGNED ZEROFILL NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `email`, `password`) VALUES
(00000001, 'administrator', 'admin@admin.com', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `aktifitas`
--

CREATE TABLE `aktifitas` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_workgroup` int(6) UNSIGNED ZEROFILL NOT NULL,
  `aktifitas` varchar(40) NOT NULL,
  `item` text,
  `bobot` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aktifitas`
--

INSERT INTO `aktifitas` (`id`, `id_workgroup`, `aktifitas`, `item`, `bobot`) VALUES
(000003, 000001, 'Design', NULL, '20'),
(000004, 000001, 'Owner Apporaval', NULL, '20'),
(000005, 000001, 'Class Apporaval', NULL, '30'),
(000006, 000001, 'Doc. Issued', NULL, '30'),
(000007, 000002, 'Purchase Order', 'Bukti Pembelian Barang|0#', '10'),
(000008, 000002, 'Arrival At yard', 'Grade/Type|0#Nomor Sertifikat|0#Quantity|0#Ukuran Material|0#Lubang|0#Deformasi|0#Pengelupasan permukaan|0#', '10'),
(000009, 000002, 'Fab. Blasting & Shop Primer', 'Paint maker|0#Colour|0#Product name|0#Aplication Methode|0#Cleaning Standard|0#Blasting/Roughness|0#Relative Humidity (RH)|0#Steel Temperature|0#Grade|0#Wet Temp|0#Dry Temp|0#', '10'),
(000010, 000002, 'Fab. Marking', 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', '10'),
(000011, 000002, 'Fab. Cutting', 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', '10'),
(000012, 000002, 'Fab. Bending', 'Ketepatan Posisi (Straight Line)|0#Ketepatan Posisi Joint|0#Gap/Celah|0#', '10'),
(000013, 000002, 'Ass. Fitt Up', 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '10'),
(000014, 000002, 'Ass. Welding', 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '10'),
(000015, 000002, 'Ass. Inspection', 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Kerataan gading antara gading besar|0#Kerataan pada joint plate|0#', '5'),
(000016, 000002, 'Erec. Fitt Up', 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '5'),
(000017, 000002, 'Erec. Welding', 'Crack|0#Kesalahan Pakai elektrode|0#Belum Dilas|0#Panjang kaki las|0#Bentuk hasil las|0#Cacat bekas stopper|0#Spatter|0#Porosity|0#', '5'),
(000018, 000002, 'Erec. Inspection', 'Kerataan Pelat antara gading - gading|0#Kerataan Pelat di tepi bebas|0#Plimsol Mark|0#Draft Mark|0#Main Dimension|0#', '5'),
(000019, 000003, 'Purchase Order', 'Bukti Pembelian Barang|0#', '10'),
(000020, 000003, 'Arrival At yard', 'Grade/Type|0#Nomor Sertifikat|0#Quantity|0#Ukuran Material|0#Visual Check|0#', '10'),
(000021, 000003, 'Fab.Marking', 'Tanda-tanda/kode penempatan komponen|0#Ukuran panjang dan lebar|0#Memeriksa peyimpangan garis potong dan garis pemasangan|0#', '10'),
(000022, 000003, 'Fab. Cutting', 'Arah sudut bevel|0#Kekasaran (Tepi Bebas)|0#Kekasaran (Alur Las)|0#Takik (Tepi Bebas)|0#Takik ( Alur Las)|0#Dimensi Hasil Pemotongan|0#', '10'),
(000023, 000003, 'Fab. Fitt Up', 'Memeriksa Kesalahan Pemasangan|0#Belum Terpasang|0#Kelurusan (alignment)|0#Celah/Gap|0#', '10'),
(000024, 000003, 'Fab. Welding', 'Crack|0#Kesalahan Pakai elektrode|0#Panjang kaki las|0#Bentuk Hasil Las|0#Cacat Bekas Stopper|0#Spatter|0#Porosity|0#', '10'),
(000025, 000003, 'Fab. Inspection', NULL, '10'),
(000026, 000003, 'On Board Fitt Up', NULL, '10'),
(000027, 000003, 'On Board Install', NULL, '10'),
(000029, 000003, 'On Board Testing ', NULL, '10'),
(000030, 000004, 'Purchase Order', NULL, '10'),
(000031, 000004, 'Arrival At yard', NULL, '10'),
(000032, 000004, 'Fab. Fitt Up', NULL, '10'),
(000033, 000004, 'Fab. Welding ', NULL, '10'),
(000034, 000004, 'Fab. Inspection', NULL, '10'),
(000035, 000004, 'On Board Fitt Up', NULL, '10'),
(000036, 000004, 'On Board Install', NULL, '10'),
(000037, 000004, 'On Board Inspection', NULL, '15'),
(000038, 000004, 'On Board Testing', NULL, '15'),
(000039, 000005, 'Purchase Order', NULL, '10'),
(000040, 000005, 'Arrival At yard', NULL, '10'),
(000041, 000005, 'Fab. Marking', NULL, '10'),
(000042, 000005, 'Fab. Cutting', NULL, '10'),
(000043, 000005, 'Fab.  Fitt Up', NULL, '10'),
(000044, 000005, 'Fab.  Welding', NULL, '10'),
(000045, 000005, 'Fab. Inspection', NULL, '10'),
(000046, 000005, 'On Board Fitt Up', NULL, '10'),
(000047, 000005, 'On Board Install', NULL, '10'),
(000048, 000005, 'On Board Inspection', NULL, '5'),
(000049, 000005, 'On Board Testing', NULL, '5'),
(000050, 000006, 'Purchase Order', NULL, '10'),
(000051, 000006, 'Arrival At yard', NULL, '10'),
(000052, 000006, 'On Board Fitt Up', NULL, '10'),
(000053, 000006, 'On Board Install', NULL, '10'),
(000054, 000006, 'On Board Connect', NULL, '20'),
(000055, 000006, 'On Board Inspection', NULL, '20'),
(000056, 000006, 'On Board Testing', NULL, '20'),
(000057, 000007, 'Purchase Order', NULL, '20'),
(000058, 000007, 'Arrival At yard', NULL, '20'),
(000059, 000007, 'Surface Prep.', NULL, '20'),
(000060, 000007, 'Coating/Painting', NULL, '20'),
(000061, 000007, 'Inspection', NULL, '20'),
(000062, 000008, 'Purchase Order', NULL, '25'),
(000063, 000008, 'Arrival At yard', NULL, '25'),
(000064, 000008, 'Loading On Board', NULL, '25'),
(000065, 000008, 'Inspection', NULL, '25');

-- --------------------------------------------------------

--
-- Table structure for table `alat_kerja`
--

CREATE TABLE `alat_kerja` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `alat` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `baseline`
--

CREATE TABLE `baseline` (
  `id` int(8) UNSIGNED ZEROFILL NOT NULL,
  `id_kapal` int(6) UNSIGNED ZEROFILL NOT NULL,
  `periode` varchar(25) DEFAULT NULL,
  `mid_end` int(3) DEFAULT NULL,
  `target` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_laporan_harian`
--

CREATE TABLE `data_laporan_harian` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `progres_harian` varchar(15) NOT NULL,
  `id_aktifitas` int(6) UNSIGNED ZEROFILL NOT NULL,
  `item` text,
  `id_laporan_harian` int(6) UNSIGNED ZEROFILL NOT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `link_foto` varchar(100) DEFAULT NULL,
  `status_pekerjaan` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_laporan_mingguan`
--

CREATE TABLE `data_laporan_mingguan` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_laporan_mingguan` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_aktivitas` int(6) UNSIGNED ZEROFILL NOT NULL,
  `item_pengawasan` varchar(100) NOT NULL,
  `progres_aktual` varchar(10) NOT NULL,
  `deskripsi` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `galangan`
--

CREATE TABLE `galangan` (
  `id_galangan` int(6) UNSIGNED ZEROFILL NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `alamat_perusahaan` varchar(50) NOT NULL,
  `pimpinan_proyek` varchar(50) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galangan`
--

INSERT INTO `galangan` (`id_galangan`, `nama_perusahaan`, `alamat_perusahaan`, `pimpinan_proyek`, `telp`, `email`) VALUES
(000001, 'PT. DUMAS TANJUNG PERAK SHIPYARD', 'SURABAYA', 'Wimpy', '09909090', 'Galangan@galangan.com'),
(000002, 'PT. Adiluhung Saranasegara Indonesia', 'JL. Raya Ujung Piring, Kec. Bangkalan, 69118', 'Sugiyarto', '(031) 3097039', 'adiluhung@gmail.com'),
(000003, 'PT. Lamongan Marine Industry (LMI)', 'Lamongan', 'Fikri', '09909090', 'lmi@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `gambar_rancangan`
--

CREATE TABLE `gambar_rancangan` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_kapal` int(6) UNSIGNED ZEROFILL NOT NULL,
  `link` varchar(70) NOT NULL,
  `judul` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE `group` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_workgroup_kapal` int(6) UNSIGNED ZEROFILL NOT NULL,
  `group` varchar(40) NOT NULL,
  `bobot_workgroup` varchar(15) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `satuan` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `laporan_alat`
--

CREATE TABLE `laporan_alat` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `nama_alat` varchar(6) NOT NULL,
  `id_laporan_harian` int(6) UNSIGNED ZEROFILL NOT NULL,
  `jumlah_alat` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `laporan_harian`
--

CREATE TABLE `laporan_harian` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_group` int(6) UNSIGNED ZEROFILL NOT NULL,
  `tgl_laporan` date NOT NULL,
  `cuaca_pagi` varchar(30) NOT NULL,
  `cuaca_siang` varchar(30) NOT NULL,
  `cuaca_sore` varchar(30) NOT NULL,
  `catatan_galangan` varchar(100) NOT NULL,
  `catatan_pm` varchar(100) NOT NULL,
  `tanggapan_galangan` varchar(100) NOT NULL,
  `tanggapan_pm` varchar(100) NOT NULL,
  `verifikasi_owner` int(3) NOT NULL,
  `verifikasi_pm` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `laporan_mingguan`
--

CREATE TABLE `laporan_mingguan` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_group` int(6) UNSIGNED ZEROFILL NOT NULL,
  `tgl_input` date NOT NULL,
  `periode` varchar(40) NOT NULL,
  `mid_end` int(3) NOT NULL COMMENT '0=mid, 1=end',
  `progres_group` varchar(5) DEFAULT NULL,
  `verif_owner` int(3) DEFAULT NULL,
  `catatan_os` varchar(128) DEFAULT NULL,
  `catatan_owner` varchar(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `laporan_tenaga_kerja`
--

CREATE TABLE `laporan_tenaga_kerja` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_laporan_harian` int(6) UNSIGNED ZEROFILL NOT NULL,
  `tenaga_kerja` varchar(50) NOT NULL,
  `jumlah` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `owner`
--

CREATE TABLE `owner` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `alamat_perusahaan` varchar(50) DEFAULT NULL,
  `penanggung_jawab` varchar(50) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `link_foto` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `owner`
--

INSERT INTO `owner` (`id`, `nama_perusahaan`, `alamat_perusahaan`, `penanggung_jawab`, `no_telp`, `link_foto`, `email`, `password`) VALUES
(000001, 'DIRJEN PERHUBUNGAN LAUT', 'JAKARTA', 'Sugiarto', '97987987979', NULL, 'owner@owner.com', 'owner');

-- --------------------------------------------------------

--
-- Table structure for table `owner_surveyor`
--

CREATE TABLE `owner_surveyor` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `alamat_perusahaan` varchar(50) DEFAULT NULL,
  `penanggung_jawab` varchar(50) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `link_foto` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `owner_surveyor`
--

INSERT INTO `owner_surveyor` (`id`, `nama_perusahaan`, `alamat_perusahaan`, `penanggung_jawab`, `no_telp`, `link_foto`, `email`, `password`) VALUES
(000001, 'PT OS Berkarya', 'Surabaya', 'Tono OS', '085728640629', NULL, 'os@os.com', 'os');

-- --------------------------------------------------------

--
-- Table structure for table `pembuatan_kapal`
--

CREATE TABLE `pembuatan_kapal` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `nama_proyek` varchar(50) NOT NULL,
  `pemilik` varchar(50) NOT NULL,
  `kontraktor` varchar(50) NOT NULL,
  `konsultan` varchar(50) NOT NULL,
  `jenis_kapal` varchar(50) NOT NULL,
  `lpp` int(11) NOT NULL,
  `loa` int(11) NOT NULL,
  `lebar` int(11) NOT NULL,
  `tinggi` int(11) NOT NULL,
  `sarat_air` int(11) NOT NULL,
  `kecepatan` int(11) NOT NULL,
  `lama_pengerjaan` int(11) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `status_pengerjaan` int(11) NOT NULL,
  `id_os` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_galangan` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_owner` int(6) UNSIGNED ZEROFILL NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembuatan_kapal`
--

INSERT INTO `pembuatan_kapal` (`id`, `nama_proyek`, `pemilik`, `kontraktor`, `konsultan`, `jenis_kapal`, `lpp`, `loa`, `lebar`, `tinggi`, `sarat_air`, `kecepatan`, `lama_pengerjaan`, `tanggal_mulai`, `status_pengerjaan`, `id_os`, `id_galangan`, `id_owner`) VALUES
(000021, 'Kapal Perintis Tipe 750 DWT Tipe A', 'Direktorat Perhubungan Laut', '', '', 'Perintis', 59, 52, 12, 5, 3, 12, 15, '0000-00-00', 0, 000001, 000002, 000001),
(000022, 'Tes Sore', 'Afandi', '', '', 'Perintis', 12, 12, 12, 12, 12, 12, 15, '0000-00-00', 0, 000001, 000002, 000001);

-- --------------------------------------------------------

--
-- Table structure for table `progres`
--

CREATE TABLE `progres` (
  `id` int(8) UNSIGNED ZEROFILL NOT NULL,
  `id_kapal` int(6) UNSIGNED ZEROFILL NOT NULL,
  `periode` varchar(25) DEFAULT NULL,
  `mid_end` int(3) DEFAULT NULL,
  `progres` varchar(10) NOT NULL,
  `tgl_input` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `project_manager`
--

CREATE TABLE `project_manager` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `alamat_perusahaan` varchar(50) NOT NULL,
  `penanggung_jawab` varchar(50) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `link_foto` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tenaga_kerja`
--

CREATE TABLE `tenaga_kerja` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `keahlian` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `workgroup`
--

CREATE TABLE `workgroup` (
  `id_workgroup` int(6) UNSIGNED ZEROFILL NOT NULL,
  `bobot` int(11) NOT NULL,
  `nama_pekerjaan` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workgroup`
--

INSERT INTO `workgroup` (`id_workgroup`, `bobot`, `nama_pekerjaan`) VALUES
(000001, 10, 'Design/Approval Drawing'),
(000002, 10, 'Hull Construction'),
(000003, 15, 'Hull Outfitting'),
(000004, 15, 'Machinery Outfitting'),
(000005, 20, 'Pipping System'),
(000006, 10, 'Electrical Outfitting'),
(000007, 10, 'Painting & Coating'),
(000008, 10, 'Inventory');

-- --------------------------------------------------------

--
-- Table structure for table `workgroup_kapal`
--

CREATE TABLE `workgroup_kapal` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_kapal` int(6) UNSIGNED ZEROFILL NOT NULL,
  `id_workgroup` int(6) UNSIGNED ZEROFILL NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `aktifitas`
--
ALTER TABLE `aktifitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alat_kerja`
--
ALTER TABLE `alat_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `baseline`
--
ALTER TABLE `baseline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_laporan_harian`
--
ALTER TABLE `data_laporan_harian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_laporan_mingguan`
--
ALTER TABLE `data_laporan_mingguan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galangan`
--
ALTER TABLE `galangan`
  ADD PRIMARY KEY (`id_galangan`);

--
-- Indexes for table `gambar_rancangan`
--
ALTER TABLE `gambar_rancangan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan_alat`
--
ALTER TABLE `laporan_alat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan_harian`
--
ALTER TABLE `laporan_harian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan_mingguan`
--
ALTER TABLE `laporan_mingguan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan_tenaga_kerja`
--
ALTER TABLE `laporan_tenaga_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner`
--
ALTER TABLE `owner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner_surveyor`
--
ALTER TABLE `owner_surveyor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembuatan_kapal`
--
ALTER TABLE `pembuatan_kapal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `progres`
--
ALTER TABLE `progres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_manager`
--
ALTER TABLE `project_manager`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenaga_kerja`
--
ALTER TABLE `tenaga_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `workgroup`
--
ALTER TABLE `workgroup`
  ADD PRIMARY KEY (`id_workgroup`);

--
-- Indexes for table `workgroup_kapal`
--
ALTER TABLE `workgroup_kapal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(8) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `aktifitas`
--
ALTER TABLE `aktifitas`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `alat_kerja`
--
ALTER TABLE `alat_kerja`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `baseline`
--
ALTER TABLE `baseline`
  MODIFY `id` int(8) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=315;
--
-- AUTO_INCREMENT for table `data_laporan_harian`
--
ALTER TABLE `data_laporan_harian`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `data_laporan_mingguan`
--
ALTER TABLE `data_laporan_mingguan`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;
--
-- AUTO_INCREMENT for table `galangan`
--
ALTER TABLE `galangan`
  MODIFY `id_galangan` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `gambar_rancangan`
--
ALTER TABLE `gambar_rancangan`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `group`
--
ALTER TABLE `group`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT for table `laporan_alat`
--
ALTER TABLE `laporan_alat`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `laporan_harian`
--
ALTER TABLE `laporan_harian`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `laporan_mingguan`
--
ALTER TABLE `laporan_mingguan`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `laporan_tenaga_kerja`
--
ALTER TABLE `laporan_tenaga_kerja`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `owner`
--
ALTER TABLE `owner`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `owner_surveyor`
--
ALTER TABLE `owner_surveyor`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pembuatan_kapal`
--
ALTER TABLE `pembuatan_kapal`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `progres`
--
ALTER TABLE `progres`
  MODIFY `id` int(8) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=315;
--
-- AUTO_INCREMENT for table `project_manager`
--
ALTER TABLE `project_manager`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tenaga_kerja`
--
ALTER TABLE `tenaga_kerja`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `workgroup`
--
ALTER TABLE `workgroup`
  MODIFY `id_workgroup` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `workgroup_kapal`
--
ALTER TABLE `workgroup_kapal`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
