<link rel="stylesheet" href="<?php echo base_url()?>assets/theme/plugins/lightbox2-master/css/lightbox.css">
<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>

<script src="<?php echo base_url()?>assets/theme/plugins/lightbox2-master/js/lightbox.js"></script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Detail Kapal</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
         

          <div class="row">
            <form action="" method="post" role="form">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Laporan Harian</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                </div><!-- /.box -->
              </div>

              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <table class="table table-bordered">
                        <tbody>
                          <tr>
                            <th>Hari/Tanggal</th>
                            <td>Selasa, 17 September 2015</td>
                          </tr>
                          <tr>
                            <th>Pekerjaan</th>
                            <td>Pembangunan Kapal Perintis Tipe 750 DWT (PAKET D)</td>
                          </tr>
                         <tr>
                            <th>Pemilik</th>
                            <td>Satker Peningkatan Keselamatan Lalu Lintas Angkutan Laut, Kementrian Perhubungan</td>
                          </tr>
                          <tr>
                            <th>Kontraktor Pelaksana</th>
                            <td>PT. Adiluhung Sarana Segara Indonesia</td>
                          </tr>
                          <tr>
                            <th>Konsultan Pengawas</th>
                            <td>PT. Mulia Artha Loka</td>
                          </tr>
                          <tr>
                            <th>Status</th>
                            <td><p style="color:red"><strong>Belum disetujui</strong></p></td>
                          </tr>
                           


                      </tbody>
                    </table>
                  </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Tenaga Kerja</h3>
                    </div>
                    <div class="box-body with-border">
                      <table class="table table-bordered">
                        <tbody>
                          <tr>
                            <th>Project Manager</th>
                            <td>1 orang</td>
                          </tr>
                          <tr>
                            <th>Design Engineer</th>
                            <td>2 orang</td>
                          </tr>
                         <tr>
                            <th>PP Control</th>
                            <td>2 orang</td>
                          </tr>
                          <tr>
                            <th>Site Produksi</th>
                            <td>1 orang</td>
                          </tr>
                          <tr>
                            <th>Labour</th>
                            <td>1 orang</td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Cuaca</h3>
                    </div>
                    <div class="box-body with-border">
                      <table class="table table-bordered">
                        <tbody>
                           <tr>
                            <th>Pagi</th>
                            <td>Cerah</td>
                          </tr>
                          <tr>
                            <th>Siang</th>
                            <td>Cerah</td>
                          </tr>
                         <tr>
                            <th>Sore</th>
                            <td>Cerah</td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-header with-border">
                     <h3 class="box-title">Alat yg Digunakan</h3>
                    </div>
                    <div class="box-body with-border">
                      <table class="table table-bordered">
                        <tbody>
                          <tbody>
                           <tr>
                            <th>Mesin Sandblast</th>
                            <td>1</td>
                          </tr>
                          <tr>
                            <th>Mesin Las</th>
                            <td>6</td>
                          </tr>
                         <tr>
                            <th>Crane</th>
                            <td>2</td>
                          </tr>
                          <tr>
                            <th>Mesin CNC</th>
                            <td>1</td>
                          </tr>
                          <tr>
                            <th>Mesin Potong</th>
                            <td>2</td>
                          </tr>
                      </tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Material yg Didatangkan</h3>
                    </div>
                    <div class="box-body with-border">
                      <table class="table table-bordered">
                        <tbody>
                          
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>

            <!-- <div class="col-md-12">
                <div class="box box-primary">          
                    <div class="box-body with-border">
                      <table class="table table-bordered">
                      <thead>
                              <tr>
                                <th>No. </th>
                                <th>Jenis Pekerjaan</th>
                                <th>Uraian Pekerjaan</th>
                                <th>Gambar</th>
                                <th>Tenaga Kerja</th>
                                
                              </tr>
                      </thead>
                      <tbody>
                           <tr>
                            <td>1</td>
                            <td>Produksi Block 116-01 </td>
                            <td><i>Marking dan cutting pelat konstruksi (Pemotongan dengan menggunakan mesin CNC dan manual). Pemasangan penegar pelat Double bottom</i></td>  
                            <td>
                              <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" data-lightbox="example-set" data-title="Contoh Gambar"><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt="" height="70px" width="70px" /></a>
                            </td>                 
                            <td>7 Orang</td>
                            
                          </tr>
                          <tr>
                            <td>2</td>
                            <td>Produksi Block 117-01</td>
                            <td><i>Jig fitting pelat main tank top (double bottom). Sandblasting dan Primer painting pelat</i></td>
                            <td>
                              <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" data-lightbox="example-set" data-title="Contoh Gambar"><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt="" height="70px" width="70px" /></a>
                            </td>
                            <td>5 Orang</td>
                          </tr>
                         <tr>
                            <td>3</td>
                            <td>Produksi Block 115-01</td>
                            <td><i>Marking dan cutting pelat konstruksi (Pemotongan dengan menggunakan mesin CNC dan manual). Pemasangan penegar pelat Double bottom</i></td>
                            <td>
                              <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" data-lightbox="example-set" data-title="Contoh Gambar"><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt="" height="70px" width="70px" /></a>
                            </td>
                            <td>5 Orang</td>
                          </tr>
                          <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><strong>20 Orang</strong></td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div> -->

        <div class="col-md-12">
          <!-- MAP & BOX PANE -->
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">MainGroup 1</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body form-horizontal">
                      <div class="col-md-4">
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-5 control-label">Group:</label>
                            <div class="col-sm-7">
                              <select name="workgroup" id="workgroup" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Group</option>
                                  <option>Design & Approval Drawing</option>
                                  <option>Work Preparation & General</option>
                                  <option>Hull construction</option>
                              </select>
                            </div>
                          </div>
                        </div>
                    </div>
          
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

        <div class="col-md-12">
          <!-- MAP & BOX PANE -->
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">MainGroup 2</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding" style="display: block;">
              <div class="row">
                <div class="col-md-9 col-sm-8">
                  
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

        <div class="col-md-12">
                <div class="box box-primary box-solid">

                    <div class="box-header with-border">
                      <h3 class="box-title">Catatan Owner Surveyor</h3>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                         <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Catatan</strong></span>
                            <input type="Email" class="form-control" placeholder="" value="" readonly="" />
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Kirim Tanggapan</strong></span>
                            <input type="Email" class="form-control" placeholder="" value=""/>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                         
                        </div>       
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
            </div>

            

              
            </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
 </div><!-- /.content-wrapper -->

