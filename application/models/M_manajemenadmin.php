<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class M_manajemenadmin extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
  
  
    public function insertData($tabel, $data)
    {
        $this->db->insert($tabel, $data);
        return $this->db->affected_rows();
    }

    public function initBaseline($loop)
    {
        $this->db->select_max('id');
        $query = $this->db->get('pembuatan_kapal');
        $id_kapal = $query->row();

        for($i=1; $i<=$loop*2; $i++){
            $data = array(
                'id_kapal' => $id_kapal->id,                
                'target' => 0
            );

            $this->db->insert('baseline', $data);  
        }   
    }

    public function initProgres($loop)
    {
        $this->db->select_max('id');
        $query = $this->db->get('pembuatan_kapal');
        $id_kapal = $query->row();

        for($i=1; $i<=$loop*2; $i++){
            $data = array(
                'id_kapal' => $id_kapal->id,                
                'progres' => 0
            );

            $this->db->insert('progres', $data);  
        }   
    }

    public function get_id()
    {
        $this->db->select_max('id');
        $query = $this->db->get('pembuatan_kapal');
        return $query->row();
    }

    public function initWorkgroupkapal()
    {
        $this->db->select_max('id');
        $query = $this->db->get('pembuatan_kapal');
        $id_kapal = $query->row();

        $data_workgroup = $this->db->get('workgroup');
        
       foreach ($data_workgroup->result() as $kapal) {
            $data = array(
                'id_kapal' => $id_kapal->id,                
                'id_workgroup' => $kapal->id_workgroup
            );

            $this->db->insert('workgroup_kapal', $data);             
        }
    }

    public function jumlahWorkgroup()
    {
        $this->db->select('bobot');
        //$this->db->from('workgroup');
        $query = $this->db->get('workgroup');
        $jumlahWorkgroup = $query->num_rows();
       

        $this->db->select_sum('bobot');
        //$this->db->from('workgroup');
        $query = $this->db->get('workgroup');
       // $jumlahWorkgroup = $query->num_rows();
        $jumlahBobot = $query->result();


        $data = array(
           'jumlahWorkgroup' => $jumlahWorkgroup ,
           'bobot' =>  $jumlahBobot[0]->bobot
            );

        return $data;
    }

    public function addWorgroup($data)
    {
        $this->db->insert('workgroup', $data);
        return $this->db->affected_rows();
    }

    public function delWorkgroup($id)
    {
        $this->db->delete('workgroup', array('id_workgroup' => $id)); 
    }

    public function updateworkgroup($data)
    {
        $this->db->where('id_workgroup', $data['id']);

         $toDB = array(
           'nama_pekerjaan' => $data['nama_pekerjaan'] ,
           'bobot' =>  $data['bobot'] 
            );

        $this->db->update('workgroup', $toDB);     
    }


    public function getWorkgroup()
    {
        $query = $this->db->get_where('workgroup');
        return $query->result();
    }


    public function addOwner($data)
    {
        $this->db->insert('owner', $data);
        return $this->db->affected_rows();
    }

    public function getOwner()
    {
        $query = $this->db->get_where('owner');
        return $query->result();
    }

     public function updateOwner($data)
    {
        $this->db->where('id', $data['id']);

        $toDB = array(
           'nama_perusahaan' => $data['nama_perusahaan'] ,
           'alamat_perusahaan' =>  $data['alamat_perusahaan'],
           'penanggung_jawab' =>  $data['penanggung_jawab'],
           'no_telp' =>  $data['no_telp'],
           'email' =>  $data['email'],
           'password' =>  $data['password']
            );

        $this->db->update('owner', $toDB);     
    }

     public function delowner($id)
    {
        $this->db->delete('owner', array('id' => $id)); 
    }


    public function insertAktifitas($data)
    {
        $this->db->insert('aktifitas', $data);
        return $this->db->affected_rows();
    }

    public function getActivity($id_workgroup)
    {
        $data =  $this->db->get_where('aktifitas', array('id_workgroup' => $id_workgroup));
        return $data->result();
    }

    public function delActivity($id)
    {
        $this->db->delete('aktifitas', array('id' => $id)); 
    }

    public function updateActivity($data)
    {
        $this->db->where('id', $data['id']);

        $toDB = array(
           'aktifitas' => $data['aktifitas'] ,
           'bobot' =>  $data['bobot'] 
            );

        $this->db->update('aktifitas', $toDB);     
    }

    public function addGalangan($data)
    {
        $this->db->insert('galangan', $data);
        return $this->db->affected_rows();
    }

    public function getGalangan()
    {
        $query = $this->db->get_where('galangan');
        return $query->result();
    }

    public function updateGalangan($data)
    {
        $this->db->where('id_galangan', $data['id']);

         $toDB = array(
           'nama_perusahaan' => $data['nama_perusahaan'] ,
           'alamat_perusahaan' =>  $data['alamat_perusahaan'],
           'pimpinan_proyek' =>  $data['pimpinan_proyek'],
           'telp' =>  $data['telp'],
           'email' =>  $data['email'] 
            );

        $this->db->update('galangan', $toDB);     
    }

    public function delGalangan($id)
    {
        $this->db->delete('galangan', array('id_galangan' => $id)); 
    }

    public function addOS($data)
    {
        $this->db->insert('owner_surveyor', $data);
        return $this->db->affected_rows();
    }

    public function getOS()
    {
        $query = $this->db->get_where('owner_surveyor');
        return $query->result();
    }

    public function updateOS($data)
    {
        $this->db->where('id', $data['id']);

         $toDB = array(
           'nama_perusahaan' => $data['nama_perusahaan'] ,
           'alamat_perusahaan' =>  $data['alamat_perusahaan'],
           'password' =>  $data['password'],
           'no_telp' =>  $data['no_telp'],
           'email' =>  $data['email'] 
            );

        $this->db->update('owner_surveyor', $toDB);     
    }

    public function delOS($id)
    {
        $this->db->delete('owner_surveyor', array('id' => $id)); 
    }


    public function getProyek()
    {
        // $data_proyek = $this->db->get_where('pembuatan_kapal');
        // $data_owner = $this->db->get_where('owner');
        // $data_os = $this->db->get_where('owner_surveyor');
        // $data_galangan = $this->db->get_where('galangan');

        $this->db->select('pembuatan_kapal.id as id_proyek,pembuatan_kapal.nama_proyek, owner.nama_perusahaan as nama_owner, owner_surveyor.nama_perusahaan as nama_os, galangan.nama_perusahaan as nama_galangan, status_pengerjaan');
        $this->db->from('pembuatan_kapal');
        $this->db->join('owner', 'pembuatan_kapal.id_owner = owner.id', 'left');
        $this->db->join('owner_surveyor', 'pembuatan_kapal.id_os = owner_surveyor.id', 'left');
        $this->db->join('galangan', 'pembuatan_kapal.id_galangan = galangan.id_galangan', 'left');

        $query = $this->db->get();
        return $query->result();
        
    }

    public function getItem()
    {
        $query = $this->db->get_where('aktifitas');
        return $query->result();
    }

    public function insertItem($data, $id)
    {
        $this->db->where('id',$id);
        $this->db->update('aktifitas', $data);     
    }

    public function getDataItem($id)
    {
        $query = $this->db->get_where('aktifitas', array('id' => $id));
        return $query->result();
    }

    public function deleteitem($id)
    {
        $toDB = array(
           'item' => ''
           // 'bobot' =>  $data['bobot'] 
            );
        $this->db->where('id',$id);
        $this->db->update('aktifitas', $toDB);
        // $this->db->get_where('aktifitas', array('id' => $id));
        // return $query->result();
    }  
}  

?>