<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
              <i class="fa fa-edit"></i> <strong>Laporan Mingguan</strong>
          </h1>
          <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <?php echo($menu); ?>
          <div class="row">
            <form action="" method="post" role="form">
              <div class="col-md-12">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title">Laporan Mingguan</h3>
                    </div><!-- /.box-header -->
                     <div class="box-body form-horizontal">

                        <div class="col-md-5">
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-4 control-label">Periode:</label>
                            <div class="col-sm-8">
                              <select name="periode" id="periode" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Periode</option>
                                  <option>Januari Awal</option>
                                  <option>Januari Akhir</option>
                                  <option>Februari Awal</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-4 control-label">Tanggal input</label>
                            <div class="col-sm-8">
                                                   
                            </div>
                          </div>
                        </div>

                        <div class="col-md-5 col-md-offset-2">
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-4 control-label">Workgroup:</label>
                            <div class="col-sm-8">
                              <select name="workgroup" id="workgroup" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">--Pilih Workgroup--</option>
                                  <?php 
                                  foreach ($workgroup as $wg) {
                                  ?>
                                    <option value="<?php echo $wg->id?>"><?php echo $wg->nama_pekerjaan?></option>
                                  <?php
                                  }
                                  ?>
                                  <!-- <option>Design & Approval Drawing</option>
                                  <option>Work Preparation & General</option>
                                  <option>Hull construction</option> -->
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-4 control-label">Group:</label>
                            <div class="col-sm-8">
                              <select name="pilih_group" id="pilih_group" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">--Pilih Group--</option>
                              </select>
                            </div>
                          </div>
                          <a id="ok_button2" style="margin-left:15px;" class="btn bg-navy pull-right"  title="ok">Ok</a>
                      </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->              

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Laporan</h3>
                    </div><!-- /.box-header -->
                        <!-- form start -->
                    <div class="box-body">
                        <table class="table table-bordered table-striped" id="tabelinput">
                          <thead>
                            <tr>
                              <th class="text-center">NO</th>
                              <th class="text-center">Aktivitas</th>
                              <th class="text-center">Status</th>
                              <th class="text-center">Project Actual %</th>
                              <th class="text-center">Deskripsi</th>
                              <th class="text-center">Foto</th>
                            </tr>
                          </thead>
                          <tbody id="data_laporan">

                          </tbody>
                        </table>
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-body with-border">
                      <table class="table table-bordered">
                        <tbody>
                          <tr>
                            <th>Catatan :</th>
                            <td><input type="text" class="form-control"/></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>

              </div>
            </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<script type="text/javascript">
  $(document).ready(function(){
      $('#workgroup').bind('change', function() {
        $.ajax({
            url: '<?php echo base_url()?>ManajemenKapalOS/getGroupByWorkgroup/' + $("#workgroup").val(), 
            dataType: 'json',
            success: function(data) {
                $('#pilih_group').html('');
                $('#pilih_group').append('<option style="display: none;">--Pilih Group--</option>');
                $.each(data, function(i,item){
                    if (item.id_group != 'empty' ) {
                        $('#pilih_group').append('<option value="'+item.id_group+'">'+item.name+'</option>');
                    } else {
                        $('#pilih_group').html('');
                        $('#pilih_group').append('<option value=""> -- -- </option>');
                    }
                });
            }
        });
      });

  });  
</script>