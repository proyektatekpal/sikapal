      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url()?>assets/theme/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>Alexander Pierce</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
           <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i><span>Profil</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>ProfilAdmin/editprofil"><i class="fa fa-circle-o"></i>Edit Profil</a></li>
                <li><a href="<?php echo base_url()?>ProfilAdmin/ubahpassword"><i class="fa fa-circle-o"></i>Ubah Password</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i><span>Workgroup</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>ManajemenKapalAdmin/AddWorkgroup"><i class="fa fa-share"></i> <span>Tambah Workgroup</span></a></li>
                <li><a href="<?php echo base_url()?>ManajemenKapalAdmin/ListWorkgroup"><i class="fa fa-share"></i> <span>Lihat Workgroup</span></a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i><span>Proses</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>ManajemenKapalAdmin/AddActivity"><i class="fa fa-share"></i> <span>Tambah Proses</span></a></li>
                <li><a href="<?php echo base_url()?>ManajemenKapalAdmin/ListActivity"><i class="fa fa-share"></i> <span>Lihat Proses</span></a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i><span>Item</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>ManajemenKapalAdmin/AddItem"><i class="fa fa-share"></i> <span>Tambah Item</span></a></li>
                <li><a href="<?php echo base_url()?>ManajemenKapalAdmin/ListItem"><i class="fa fa-share"></i> <span>Lihat Item</span></a></li>
              </ul>
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i><span>Owner</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>ManajemenKapalAdmin/AddOwner"><i class="fa fa-share"></i> <span>Tambah Owner</span></a></li>
                <li><a href="<?php echo base_url()?>ManajemenKapalAdmin/ListOwner"><i class="fa fa-share"></i> <span>Lihat Owner</span></a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i><span>Galangan</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>ManajemenKapalAdmin/AddGalangan"><i class="fa fa-share"></i> <span>Tambah Galangan</span></a></li>
                <li><a href="<?php echo base_url()?>ManajemenKapalAdmin/ListGalangan"><i class="fa fa-share"></i> <span>Lihat Galangan</span></a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i><span>Owner Surveyor</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>ManajemenKapalAdmin/AddOS"><i class="fa fa-share"></i> <span>Tambah OS</span></a></li>
                <li><a href="<?php echo base_url()?>ManajemenKapalAdmin/ListOS"><i class="fa fa-share"></i> <span>Lihat OS</span></a></li>
              </ul>
            </li>
           <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i><span>Kapal</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>ManajemenKapalAdmin/TambahKapal"><i class="fa fa-share"></i> <span>Tambah Kapal</span></a></li>
                <li><a href="<?php echo base_url()?>ManajemenKapalAdmin/DaftarKapal"><i class="fa fa-share"></i> <span>Lihat kapal</span></a></li>
              </ul>
            </li>
              
            <!-- <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i><span>WorkGroup</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>ManajemenKapalOS/AddWorkGroup"><i class="fa fa-circle-o"></i>Tambah</a></li>
                <li><a href="<?php echo base_url()?>ManajemenKapalOS/LihatWorkGroup"><i class="fa fa-circle-o"></i>Daftar WorkGroup</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i><span>Sub WorkGroup</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>ManajemenKapalOS/AddSubWorkGroup"><i class="fa fa-circle-o"></i>Tambah</a></li>
                <li><a href="<?php echo base_url()?>ManajemenKapalOS/LihatSubWorkGroup"><i class="fa fa-circle-o"></i>Daftar Sub WorkGroup</a></li>
              </ul>
            </li> -->

              <!---
              <ul class="treeview-menu">
                <li>
                  <a href="#"><i class="fa fa-circle-o"></i> Kapal 1 <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="<?php echo base_url()?>OwnerSurveyor/gambar"><i class="fa fa-circle-o"></i> Gambar </a></li>
                    <li><a href="<?php echo base_url()?>OwnerSurveyor/s_curve"><i class="fa fa-circle-o"></i> S-Curve </a></li>
                    <li><a href="<?php echo base_url()?>OwnerSurveyor/laporan"><i class="fa fa-circle-o"></i> Laporan </a></li>
                  </ul>
                </li>
                <li>
                  <a href="#"><i class="fa fa-circle-o"></i> Kapal 2 <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Gambar </a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> S-Curve </a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Laporan </a></li>
                  </ul>
                  -->
                </li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>