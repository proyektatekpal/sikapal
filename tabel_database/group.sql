-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 17 Jan 2016 pada 11.29
-- Versi Server: 5.6.14
-- Versi PHP: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `sikapal`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `group`
--

CREATE TABLE IF NOT EXISTS `group` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_workgroup_kapal` int(6) unsigned zerofill NOT NULL,
  `group` varchar(40) NOT NULL,
  `bobot_workgroup` varchar(15) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

--
-- Dumping data untuk tabel `group`
--

INSERT INTO `group` (`id`, `id_workgroup_kapal`, `group`, `bobot_workgroup`, `kuantitas`, `satuan`) VALUES
(000007, 000002, 'BLOK 100', '5', 20, 'TON'),
(000008, 000002, 'BLOK 101', '5', 20, 'TON'),
(000009, 000002, 'BLOK 102', '5', 20, 'TON'),
(000010, 000002, 'BLOK 103', '5', 20, 'TON'),
(000011, 000002, 'BLOK 104', '5', 20, 'TON'),
(000012, 000002, 'BLOK 105', '5', 20, 'TON'),
(000013, 000002, 'BLOK 106', '5', 20, 'TON'),
(000014, 000002, 'BLOK 107', '5', 20, 'TON'),
(000015, 000002, 'BLOK 108', '5', 20, 'TON'),
(000016, 000002, 'BLOK 109', '5', 20, 'TON'),
(000017, 000002, 'BLOK 110', '5', 20, 'TON'),
(000018, 000002, 'BLOK 111', '5', 20, 'TON'),
(000019, 000002, 'BLOK 112', '5', 20, 'TON'),
(000020, 000002, 'BLOK 113', '5', 20, 'TON'),
(000021, 000002, 'BLOK 114', '5', 20, 'TON'),
(000022, 000002, 'BLOK 115', '5', 20, 'TON'),
(000023, 000002, 'BLOK 116', '5', 20, 'TON'),
(000024, 000002, 'BLOK 117', '5', 20, 'TON'),
(000025, 000002, 'BLOK 118', '5', 20, 'TON'),
(000026, 000002, 'BLOK 119', '5', 20, 'TON'),
(000027, 000003, 'Pintu', '4', 1, 'SHIPSHET'),
(000028, 000003, 'Seal Trunk', '4', 1, 'SHIPSHET'),
(000029, 000003, 'Jendela Aluminium Persegi', '4', 1, 'SHIPSHET'),
(000030, 000003, 'Jendela Aluminium Bulat', '4', 1, 'SHIPSHET'),
(000031, 000003, 'Exhaust Fan 10" WC/KM', '4', 1, 'SHIPSHET'),
(000032, 000003, 'Double Bollard', '4', 12, 'BH'),
(000033, 000003, 'Lantai Deck ABK/PMP Dilapisi Deck  Kompo', '4', 1, 'SHIPSHET'),
(000034, 000003, 'Dinding Isolasi  ( Klass B-15 uncomb )', '4', 1, 'SHIPSHET'),
(000035, 000003, 'Langit-langit ', '4', 1, 'SHIPSHET'),
(000036, 000003, 'House Pipe & Belimouth', '4', 1, 'SHIPSHET'),
(000037, 000003, 'WC/KM dengan Pelengkap', '4', 1, 'SHIPSHET'),
(000038, 000003, 'Cerobong Asap', '4', 1, 'SHIPSHET'),
(000039, 000003, 'Life Boat ', '4', 2, 'SHIPSHET'),
(000040, 000003, 'Alat Lift / Release untuk Life Boat ( de', '4', 2, 'SHIPSHET'),
(000041, 000003, 'Perlengkapan Tidur Penumpang Ekonomi', '4', 1, 'SHIPSHET'),
(000042, 000003, 'Tangga Samping', '4', 1, 'SHIPSHET'),
(000043, 000003, 'Perlengkapan Tidur Kamar ABK', '4', 1, 'SHIPSHET'),
(000044, 000003, 'Tangga dengan Railing', '4', 1, 'SHIPSHET'),
(000045, 000003, 'Plat Nama dan Tanda-tanda', '4', 1, 'SHIPSHET'),
(000046, 000003, 'Mast Tiang Compass', '4', 1, 'SHIPSHET'),
(000047, 000003, 'Natural Ventilasi Ruang PNP', '4', 1, 'SET'),
(000048, 000003, 'Natural Ventilasi Ruang Cargo', '4', 1, 'SET'),
(000049, 000003, 'Mekanik Ventilasi Ruang PNP', '4', 1, 'SET'),
(000050, 000003, 'AC Split 1 PK', '4', 1, 'SET'),
(000051, 000003, 'Televisi uk. 29"', '', 1, 'SET'),
(000052, 000004, 'MAIN ENGINE', '10', 2, 'UNIT'),
(000053, 000004, 'Poros, Propeller & St. Lube', '10', 2, 'UNIT'),
(000054, 000004, 'Kemudi & Perlengkapannya', '10', 2, 'UNIT'),
(000055, 000004, 'Ventilator Alam Kamar Mesin', '10', 2, 'UNIT'),
(000056, 000004, 'Ventilator Mekanis Kamar Mesin', '10', 2, 'UNIT'),
(000057, 000004, 'Steering Gear dengan Perlengkapannya', '10', 2, 'UNIT'),
(000058, 000004, 'Engine telegraph', '10', 2, 'UNIT'),
(000059, 000004, 'Windlass', '10', 2, 'UNIT'),
(000060, 000004, 'Unit Peralatan Hydraulic', '10', 2, 'UNIT'),
(000061, 000004, 'Cargo Crane', '10', 2, 'UNIT'),
(000062, 000006, 'Kabel Fitting', '10', 1, 'UNIT'),
(000063, 000006, 'Panel Penerangan', '10', 1, 'UNIT'),
(000064, 000006, 'MSB', '10', 1, 'UNIT'),
(000065, 000006, 'ESB', '10', 1, 'UNIT'),
(000066, 000006, 'Panel Navigasi & Komunikasi', '10', 1, 'UNIT'),
(000067, 000006, 'Panel Stater', '10', 1, 'UNIT'),
(000068, 000006, 'Panel Darurat', '10', 1, 'UNIT'),
(000069, 000006, 'Flood Light Haluan', '10', 1, 'UNIT'),
(000070, 000006, 'Lampu Navigasi', '10', 1, 'UNIT'),
(000071, 000006, 'Genset Pelabuhan', '10', 1, 'UNIT');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
