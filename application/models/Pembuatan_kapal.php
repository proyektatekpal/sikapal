<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class Pembuatan_kapal extends CI_Model
{
	protected $table = "pembuatan_kapal";
	protected $all_field = "pembuatan_kapal.id, nama_proyek, pemilik, jenis_kapal, lpp, loa, lebar, tinggi, sarat_air, kecepatan, lama_pengerjaan, tanggal_mulai, status_pengerjaan, pembuatan_kapal.id_galangan";
    public function __construct()
    {
        parent::__construct();
    }
  
  
    public function get($where = NULL, $select = NULL) {  //where dalam array
    	if($select!=NULL){
    		$this->db->select($select);
    	}
        $this->db->select($this->all_field);
    	$this->db->from($this->table);
    	if($where!=NULL){
    		$this->db->where($where);
    	}
    	$query = $this->db->get();
    	return $query->result();
    }

    public function join_galangan(){
    	$this->db->select('galangan.nama_perusahaan as perusahaan_galangan,galangan.alamat_perusahaan as alamat_galangan,pimpinan_proyek,telp,galangan.email as email_galangan');
        $this->db->join('galangan','galangan.id_galangan = pembuatan_kapal.id_galangan','left');
    }

    public function join_os(){
        $this->db->select('owner_surveyor.nama_perusahaan as perusahaan_os,owner_surveyor.alamat_perusahaan as alamat_os,penanggung_jawab,no_telp,link_foto,owner_surveyor.email as email_os');
        $this->db->join('owner_surveyor','owner_surveyor.id = pembuatan_kapal.id_os','left');
    }

    public function join_progres(){
        
    }

    public function order_by(){
        $this->db->order_by('pembuatan_kapal.id_galangan ASC');
    }
    
}  

?>