<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Daftar Owner Surveyor</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Modal -->
          <div class="modal fade" id="DelConf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus Data</h4>
                </div>
                <div class="modal-body">
                  Data akan dihapus, Anda yakin?
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                  <a class="btn bg-navy" id="modalDelete" href="#">Ya</a>
                  <!-- <button type="button" class="btn btn-primary" href="<?php echo base_url()?>ManajemenKapalAdmin/DelWorkGroup/">Hapus</button> -->
                </div>
              </div>
            </div>
          </div>

          <div class="modal fade" id="UpdateData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Update Data</h4>
                </div>
                <div class="modal-body">
                <form enctype="multipart/form-data" id="form1" action="#" method="post" role="form">
                  <div class="box-body">
                    <div class="form-group">
                      <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama" value="" >
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Masukkan Alamat" value="" >
                    </div>
                    <div class="form-group">
                      <input type="number" class="form-control number" id="telp" name="telp" placeholder="Masukkan No Telp" value="" >
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control " id="email" name="email" placeholder="Masukkan Alamat Email" value="" >
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control " id="password" name="password" placeholder="Masukkan Passord" value="" >
                    </div>
                  </div><!-- /.box-body -->
                
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>

                  <!-- <a class="btn bg-navy" id="modalUpdate" type="submit" >Simpan</a> -->
                  <!-- <button type="button" class="btn btn-primary" href="<?php echo base_url()?>ManajemenKapalAdmin/DelWorkGroup/">Hapus</button> -->
                </div>
                </form>
              </div>
            </div>
          </div>


          
          <!-- Main row -->
          <div class="row">
            	<div class="col-xs-12">
                	<div class="box box-primary box-solid">
                      <div class="box-header">
                          <h3 class="box-title">Daftar Owner Surveyor</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                          <table id="list-kapal" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th class="col-xs-1">No. </th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Telp</th>
                                <th>Email</th>
                                <th>Password</th>
                                <th>Tindakan</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                              $num = 1;
                              foreach ($list_os as $kapal) {
                                  echo '<tr>';
                                  echo '<td>'.$num.'</td>';
                                  echo '<td>'.$kapal->nama_perusahaan.'</td>';
                                  echo '<td>'.$kapal->alamat_perusahaan.'</td>';
                                  echo '<td>'.$kapal->no_telp.'</td>';
                                  echo '<td>'.$kapal->email.'</td>';
                                  echo '<td>'.$kapal->password.'</td>';
                                  echo '<td><a  class="btn bg-navy update" data-url="'.base_url().'" data-password="'.$kapal->password.'" data-email="'.$kapal->email.'" data-telp="'.$kapal->no_telp.'" data-nama="'.$kapal->nama_perusahaan.'" data-alamat="'.$kapal->alamat_perusahaan.'" data-id="'.$kapal->id.'" data-toggle="modal" data-target="#UpdateData">Edit</a>
                                  <a  class="btn bg-navy hapus" data-url="'.base_url().'" data-id="'.$kapal->id.'" data-toggle="modal" data-target="#DelConf">Hapus</a></td>';
                                  echo '</tr>';
                                  $num++;
                              }
                              ?>
                            </tbody>
                            <tfoot>
                              <tr>
                                <th class="col-xs-1">No. </th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Telp</th>
                                <th>Email</th>
                                <th>Password</th>
                                <th>Tindakan</th>
                              </tr>
                              </tr>
                            </tfoot>
                          </table>
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>
            

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <script type="text/javascript">
	    $('.hapus').click(function(){
          var id = $(this).data('id');
          var base_url = $(this).data('url');

          $('#modalDelete').attr('href',base_url +'ManajemenKapalAdmin/DelOS/'+id);
      });


       $('.update').click(function(){
          var id = $(this).data('id');
          var nama = $(this).data('nama');
          var alamat = $(this).data('alamat');
          var telp = $(this).data('telp');
          var email = $(this).data('email');
          var password = $(this).data('password');
          var base_url = $(this).data('url');
          
          $('#nama').attr('value',nama);
          $('#alamat').attr('value',alamat);
          $('#telp').attr('value',telp);
          $('#email').attr('value',email);
          $('#password').attr('value',password);
          $('#form1').attr('action', base_url + 'ManajemenKapalAdmin/UpdateOS/'+id);
      });
</script>