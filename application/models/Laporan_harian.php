<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class Laporan_harian extends CI_Model
{

    protected $table = "laporan_harian";
    public function __construct()
    {
        parent::__construct();
    }
  
  
    public function getLaporanTerakhir($id_group)
    {
        $result = $this->db->query("SELECT a.* FROM laporan_harian a LEFT JOIN laporan_harian b on b.id_group = a.id_group and b.tgl_laporan > a.tgl_laporan WHERE
  									b.tgl_laporan is NULL and a.id_group = '$id_group'");
        return $result->row();
    }

    public function getjoin_group($id_kapal)
    {
        $this->db->select("$this->table.*, group.*, workgroup_kapal.*");
        $this->db->from($this->table);
        $this->db->join("group", "$this->table.id_group = group.id", "left");
        $this->db->join("workgroup_kapal", "group.id_workgroup_kapal = workgroup_kapal.id");
        $this->db->where(array("id_kapal"=>$id_kapal));
        $this->db->group_by('tgl_laporan');
        $res = $this->db->get();
        return $res->result();
    }


}  

?>