<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class Gambar_rancangan extends CI_Model
{
	protected $table = "gambar_rancangan";

    public function __construct()
    {
        parent::__construct();
    }
  
  
    function get_photo($perPage, $uri) {    
        $this->db->order_by('id','DESC');
        $query = $getData = $this->db->get('gambar_rancangan', $perPage, $uri);
        if($getData->num_rows() > 0)
            return $query;
        else
            return null;
    }
    
    
}  

?>