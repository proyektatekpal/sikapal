<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class M_manajemenos extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
  
  
    public function addWorgroup($data)
    {
        //echo($data);
        $this->db->insert('workgroup', $data);
    }

    public function getDetailOS($email)
    {
        $query = $this->db->query("SELECT o.id,o.nama_perusahaan, o.Email, o.no_telp
                                    FROM owner_surveyor o
                                    WHERE o.Email = '$email'");
        return $query->row();
    }

    

    public function getDetailKapal($id_pembuatan)
    {
        $query = $this->db->get_where('pembuatan_kapal', array('id' => $id_pembuatan));
        return $query->row();
    }

    public function getWorkgroup($id_pembuatan)
    {
        $query = $this->db->get_where('workgroup', array('id_kapal' => $id_pembuatan));
        return $query->result();
    }

    public function getTenagakerja()
    {
        $query = $this->db->get_where('tenaga_kerja');
        return $query->result();
    }

    public function getAlat()
    {
        $query = $this->db->get_where('alat_kerja');
        return $query->result();
    }

    public function insertLaporanHarian($Data)
    {
        $this->db->insert('laporan_harian', $Data);
    }

    public function getAktifitas($id_group, $id_laporan_max)
    {
        $this->db->select('lp.id as id_data,data_laporan_harian.deskripsi, data_laporan_harian.item, data_laporan_harian.link_foto,
            data_laporan_harian.status_pekerjaan, aktifitas.id as aktifitas_id, aktifitas.aktifitas, lp.catatan_pm, lp.tanggapan_pm');
        $this->db->from('laporan_harian as lp');
        $this->db->join('data_laporan_harian', 'data_laporan_harian.id_laporan_harian = lp.id', 'left');
        $this->db->join('aktifitas', 'data_laporan_harian.id_aktifitas = aktifitas.id', 'left');
        $this->db->where(array('lp.id_group' => $id_group, 'lp.id' => $id_laporan_max ));   
        //$this->db->where(array('lp.id' => 000019 ));       
        $query = $this->db->get();
        return $query->result();
    }

    public function getAktifitasDefault($Data)
    {
        $query = $this->db->get_where('aktifitas', array('id_workgroup' => $Data));
        return $query->result();
    }


    public function getMaxIdLapHarian()
    {
        $this->db->select_max('id');
        $query = $this->db->get('laporan_harian');
        return $query->row()->id;
    }

    public function insertDataLaporanHarian($Data)
    {
        $this->db->insert('data_laporan_harian', $Data);
    }
    
    public function insertLaporantenagakerja($Data)
    {
        $this->db->insert('laporan_tenaga_kerja', $Data);
    }

    public function insertLaporanalat($Data)
    {
        $this->db->insert('laporan_alat', $Data);
    }

    public function cekDataLaporanHarian($id_kapal)
    {
        $this->db->select_max('lp.id');
        $this->db->from('laporan_harian as lp');
        $this->db->join('group', 'lp.id_group = group.id', 'left');
        $this->db->join('workgroup_kapal', 'workgroup_kapal.id = group.id_workgroup_kapal', 'left');
        $this->db->where(array( 'lp.tgl_laporan' => date('Y-m-d'))); 
        $id = $this->db->get()->row()->id;

        return $id;
         // $query = $this->db->get();
         // return $query->result();
    }

    public function getIdMax($id_group)
    {
        $this->db->select_max('id');
        $query = $this->db->get_where('laporan_harian', array('id_group' => $id_group));
        return $query->row()->id;
    }

    public function getLaporanTenagaKerja($id_kapal)
    {
        //getID laporan dulu -___-
        $this->db->select_max('lp.id');
        $this->db->from('laporan_harian as lp');
        $this->db->join('group', 'lp.id_group = group.id', 'left');
        $this->db->join('workgroup_kapal', 'workgroup_kapal.id = group.id_workgroup_kapal', 'left');
        $this->db->where(array('workgroup_kapal.id_kapal' => $id_kapal)); 
        $id = $this->db->get()->row()->id;

        $this->db->select('*');
        $query = $this->db->get_where('laporan_tenaga_kerja', array('id_laporan_harian' => $id));
        return $query->result();
    }  
    public function getlaporanCuaca($id_kapal)
    {
         //getID laporan dulu -___-
        $this->db->select_max('lp.id');
        $this->db->from('laporan_harian as lp');
        $this->db->join('group', 'lp.id_group = group.id', 'left');
        $this->db->join('workgroup_kapal', 'workgroup_kapal.id = group.id_workgroup_kapal', 'left');
        $this->db->where(array('workgroup_kapal.id_kapal' => $id_kapal)); 
        $id = $this->db->get()->row()->id;

        $this->db->select('*');
        $query = $this->db->get_where('laporan_harian', array('id' => $id));
        return $query->row();
    }

    public function getlaporanAlat($id_kapal)
    {
        //getID laporan dulu -___-
        $this->db->select_max('lp.id');
        $this->db->from('laporan_harian as lp');
        $this->db->join('group', 'lp.id_group = group.id', 'left');
        $this->db->join('workgroup_kapal', 'workgroup_kapal.id = group.id_workgroup_kapal', 'left');
        $this->db->where(array('workgroup_kapal.id_kapal' => $id_kapal)); 
        $id = $this->db->get()->row()->id;

        $this->db->select('*');
        $query = $this->db->get_where('laporan_alat', array('id_laporan_harian' => $id));
        return $query->result();
    }

    public function getListTanggal($id_kapal)
    {
        // $this->db->select('lp.tgl_laporan, lp.verifikasi_owner');
        // $this->db->from('laporan_harian as lp');
        // $this->db->join('group', 'lp.id_group = group.id', 'left');
        // $this->db->join('workgroup_kapal', 'workgroup_kapal.id = group.id_workgroup_kapal', 'left');
        // $this->db->where(array('workgroup_kapal.id_kapal' => $id_kapal));
        // $this->db->group_by('tgl_laporan'); 
        // $query = $this->db->get();

        $this->db->select('group.group');
        $this->db->from('group');
        $this->db->join('workgroup_kapal', 'group.id_workgroup_kapal = workgroup_kapal.id', 'left');
        $this->db->where(array('workgroup_kapal.id_kapal' => $id_kapal));
        $query = $this->db->get();

        
        return $query->result();

    }
    public function getDetailLapTenagaKerja($id_kapal, $tgl_laporan)
    {
        //getID laporan dulu -___-
        $this->db->select_max('lp.id');
        $this->db->from('laporan_harian as lp');
        $this->db->join('group', 'lp.id_group = group.id', 'left');
        $this->db->join('workgroup_kapal', 'workgroup_kapal.id = group.id_workgroup_kapal', 'left');
        $this->db->where(array('workgroup_kapal.id_kapal' => $id_kapal, 'lp.tgl_laporan' => $tgl_laporan)); 
        $id_laporan = $this->db->get()->row()->id;

        //dapetin data laporan
        $this->db->select('*');
        $this->db->from('laporan_tenaga_kerja');
        $this->db->where(array('laporan_tenaga_kerja.id_laporan_harian' => $id_laporan));
        $query = $this->db->get();
        return $query->result(); 
    }

    public function getDetailLapCuaca($id_kapal, $tgl_laporan)
    {
        //getID laporan dulu -___-
        $this->db->select_max('lp.id');
        $this->db->from('laporan_harian as lp');
        $this->db->join('group', 'lp.id_group = group.id', 'left');
        $this->db->join('workgroup_kapal', 'workgroup_kapal.id = group.id_workgroup_kapal', 'left');
        $this->db->where(array('workgroup_kapal.id_kapal' => $id_kapal, 'lp.tgl_laporan' => $tgl_laporan)); 
        $id_laporan = $this->db->get()->row()->id;

        //dapetin data laporan
        $this->db->select('*');
        $this->db->from('laporan_harian');
        $this->db->where(array('laporan_harian.id' => $id_laporan));
        $query = $this->db->get();
        return $query->result(); 
    }

    public function getDetailLapAlat($id_kapal, $tgl_laporan)
    {
        //getID laporan dulu -___-
        $this->db->select_max('lp.id');
        $this->db->from('laporan_harian as lp');
        $this->db->join('group', 'lp.id_group = group.id', 'left');
        $this->db->join('workgroup_kapal', 'workgroup_kapal.id = group.id_workgroup_kapal', 'left');
        $this->db->where(array('workgroup_kapal.id_kapal' => $id_kapal, 'lp.tgl_laporan' => $tgl_laporan)); 
        $id_laporan = $this->db->get()->row()->id;

        //dapetin data laporan
        $this->db->select('*');
        $this->db->from('laporan_alat');
        $this->db->where(array('laporan_alat.id_laporan_harian' => $id_laporan));
        $query = $this->db->get();
        return $query->result(); 
    }

    public function getIdWorkgroup($id)
    {
        $this->db->select('id_workgroup');
        $query = $this->db->get_where('workgroup_kapal', array('id' => $id));
        return $query->row()->id_workgroup;
    }
}  
?>