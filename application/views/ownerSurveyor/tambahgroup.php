<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>

<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Tambah Group</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>


        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
        <div class="row">
            <form action="" method="post" role="form">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Tambah Group</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-horizontal">
                        <div class="col-md-4">

                          <div class="form-group">
                            <label for="workgroup" class="col-sm-5 control-label">Workgroup:</label>
                            <div class="col-sm-7">
                              <select name="workgroup" id="workgroup" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Workgroup</option>
                                  <option>Design & Approval Drawing</option>
                                  <option>Work Preparation & General</option>
                                  <option>Hull construction</option>
                              </select>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="jumlahActivity" class="col-sm-5 control-label">Jumlah Group:</label>
                            <div class="col-sm-7">
                              <input type="number" min="1" class="form-control input_number" placeholder="jumlah activity" id="jumlah_activity">
                            </div>
                          </div>
                        </div>
                       
                       <a id="ok_button" style="margin-left:15px;margin-right:15px;" class="btn bg-navy pull-left"  title="ok">Ok</a>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
              </div>


              <div class="col-md-6">
                <div class="box box-primary laporan_hidden" hidden>
                    <div class="box-header with-border">
                      <h3 class="box-title">Data Group</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <table class="table table-bordered table-striped" id="tabelinput">
                          <thead>
                            <tr>
                              <th class="text-center">NO</th>
                              <th class="text-center">Nama Group</th>
                              <th class="text-center">Bobot %</th>
                            </tr>
                          </thead>
                          <tbody id="data_activity">

                          </tbody>
                          <!-- <tfoot>
                            <tr>
                              <th class="text-center">NO</th>
                              <th class="text-center">JENIS PEKERJAAN<span style="color:red;"><b>*</b></span></th>
                              <th class="text-center">URAIAN PEKERJAAN</th>
                              <th class="text-center">TENAGA KERJA<span style="color:red;"><b>*</b></span></th>
                              <th class="text-center">UPLOAD GAMBAR</th>
                            </tr>
                          </tfoot> -->
                        </table>
                        
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <div class="pull-right">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div><!-- /.box -->
              </div>
            </form>
          </div><!-- /.row (main row) -->


        </section><!-- right col -->

            <!-- right col (We are only adding the ID to make the widgets sortable)-->
          </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<script type="text/javascript">
	    $(document).ready(function()
	    {
	      	$('#list-kapal').DataTable();

          $("#ok_button").click(function()
          {  
            $("#data_activity").html('');
            var jumlah_activity = $('#jumlah_activity').val();

            if(jumlah_activity =='' | jumlah_activity <= 0)
            {
                  alert('Silahkan isi jumlah peekerjaan dengan benar');
            }
            else
            {
            	for(var i=0; i<jumlah_activity; i++)
                  {
                      var kolom = '';
                      kolom += '<tr>';
                      kolom += '<td class="col-md-1"><input type="text" class="form-control" value="'+(i+1)+'"/></td>';
                      kolom += '<td class="col-md-4"><input type="text" class="form-control"/></td>';
                      kolom += '<td class="col-md-1"><input type="number" class="form-control input_number"/></td>';
                      kolom += '<tr>';
                      $("#data_activity").append(kolom);
            		}
            		$('.laporan_hidden').show();
            }
            
           });
        }); 
</script>