<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class Data_laporan_harian extends CI_Model
{
    protected $table = "data_laporan_harian";

    public function __construct()
    {
        parent::__construct();
    }
  
  
    public function getDataLaporanTerakhir($id_laporan_harian)
    {
        $this->db->select("$this->table.id,$this->table.item,progres_harian,id_aktifitas,id_laporan_harian,deskripsi,link_foto,status_pekerjaan");
        $this->db->select("id_workgroup,aktifitas,bobot");
        $this->db->from($this->table);
        $this->db->join("aktifitas", "$this->table.id_aktifitas = aktifitas.id", "left");
        $this->db->where(array('id_laporan_harian'=>$id_laporan_harian));
        $res = $this->db->get();
        return $res->result();
    }


}  

?>