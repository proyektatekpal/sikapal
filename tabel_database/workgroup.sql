-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 23, 2016 at 10:29 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sikapal`
--

-- --------------------------------------------------------

--
-- Table structure for table `workgroup`
--

CREATE TABLE `workgroup` (
  `id_workgroup` int(6) UNSIGNED ZEROFILL NOT NULL,
  `bobot` int(11) NOT NULL,
  `nama_pekerjaan` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workgroup`
--

INSERT INTO `workgroup` (`id_workgroup`, `bobot`, `nama_pekerjaan`) VALUES
(000001, 10, 'Design/Approval Drawing'),
(000002, 10, 'Hull Construction'),
(000003, 15, 'Hull Outfitting'),
(000004, 15, 'Machinery Outfitting'),
(000005, 20, 'Pipping System'),
(000006, 10, 'Electrical Outfitting'),
(000007, 10, 'Painting & Coating'),
(000008, 10, 'Inventory');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `workgroup`
--
ALTER TABLE `workgroup`
  ADD PRIMARY KEY (`id_workgroup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `workgroup`
--
ALTER TABLE `workgroup`
  MODIFY `id_workgroup` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
