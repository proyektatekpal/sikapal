<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Tambah Galangan</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          
          <div class="row">

          <div class="col-md-12">
                <?php
                  if ($cek_isi=='isi') {
                      if ($eksekusi) { ?>
                          <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <h4><i class="icon fa fa-check"></i> Sukses!</h4>
                              Data Berhasil Dimasukkan
                          </div>
                      <?php } else { ?>
                          <div class="alert alert-warning alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <h4><i class="icon fa fa-warning"></i> Gagal!</h4>
                              Data Gagal Dimasukkan
                          </div>
                      <?php }
                  }
                  ?>
            </div>

            <div class="col-md-12">
                <div class="box box-primary box-solid">

                    <div class="box-header with-border">
                      <h3 class="box-title">Data Galangan</h3>
                    </div><!-- /.box-header -->

                    <?php echo validation_errors(); ?>
                    <form enctype="multipart/form-data" id="form1" action="<?php echo base_url()."ManajemenKapalAdmin/SaveGalangan/";?>" method="post">
                    <div class="box-body">
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Nama perusahaan</strong></span>
                            <input type="text" class="form-control" placeholder="" value="" name="nama_perusahaan" />
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Alamat Perusahaan</strong></span>
                            <input type="text" class="form-control" placeholder="" value="" name="alamat_perusahaan"/>
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Pimpinan Proyek</strong></span>
                            <input type="text" class="form-control " placeholder="" value="" name="pimpinan_proyek"/>
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>NO Telepon</strong></span>
                            <input type="text" class="form-control " placeholder="" value="" name="telp"/>
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Email</strong></span>
                            <input type="Email" class="form-control" placeholder="" value="" name="email"/>
                        </div>
                         <!-- <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Password</strong></span>
                            <input type="password" class="form-control" placeholder="" value=""/>
                        </div>             -->
                        <!-- <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Foto</strong></span>
                            <input input type="file" id="foto" style="margin-left:15px" />
                        </div>     -->         
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
              </div>


            


            <div class="col-md-12">
              <div class="box box-primary">
                      <div class="box-footer">
                          <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                          <!-- <a style="margin-left:15px;margin-right:15px;" class="btn bg-navy pull-right" href="<?php echo base_url()?>dashboard" title="Kembali">Kembali</a> -->
                          <button style="margin-right:15px;" type="reset" class="btn btn-warning pull-right">Reset</button>
                      </div>
                  </div>
            </div>
          </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <script type="text/javascript">
	    $(document).ready(function(){
	      	$('#list-kapal').DataTable();
	    }); 
	</script>