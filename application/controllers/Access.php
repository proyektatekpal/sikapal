<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Access extends CI_Controller {

	public function __construct()
    {
        parent::__construct();  
        $this->load->model('m_login');
        $this->load->library('form_validation');
    }

	public function index()
	{
        $session = $this->session->userdata('isLogin');
    
        if($session == FALSE)
        {
            $dataLogin['userada'] = '';
            $this->load->view('access/login-form', $dataLogin);
        }else
        {
            redirect('dashboard');
        }
	}

	public function verifyLogin()
    {

        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        $this->form_validation->set_message('required', '%s wajib diisi');

        if($this->form_validation->run()==FALSE)
        {
            $this->load->view('access/login-form');
        }
        else
        {
        	
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $level = $this->input->post('select_level');       		

            $cek = $this->m_login->cekPengguna($level, $email, $password);
            
            if($cek <> 0)
            {
                $this->session->set_userdata('isLogin', TRUE);
                $this->session->set_userdata('email',$email);
                $this->session->set_userdata('level', $level);

                redirect('dashboard');
            }else
            {      
                $dataLogin['userada'] = 'tidak';
                $this->load->view('access/login-form', $dataLogin);
            }
        }
        
    }

	public function register()
	{
		$this->load->view('access/register-form');
	}

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('access');
    }
}
