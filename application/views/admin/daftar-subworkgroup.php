<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Daftar Sub Workgroup</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">

         <?php echo($menu); ?>
          <!-- Main row -->
          <div class="row">
            	<div class="col-xs-12">
                	<div class="box box-primary box-solid">
                      <div class="box-header">
                          <h3 class="box-title">Daftar Sub Workgroup</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                          <table id="list-kapal" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th class="col-xs-1">No. </th>
                                <th>Nama Sub Workgroup</th>
                                <th>Tindakan</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>1</td>
                                <td>Contoh 1</td>
                                <td>
                                  <button type="submit" class="btn btn-primary pull-left">Edit</button>
                                  <button style="margin-left:15px;" type="reset" class="btn btn-warning pull-left">Delete</button>
                                </td>  
                              </tr>
                              <tr>
                                <td>2</td>
                                <td>Contoh 2</td>
                                <td>
                                  <button type="submit" class="btn btn-primary pull-left">Edit</button>
                                  <button style="margin-left:15px;" type="reset" class="btn btn-warning pull-left">Delete</button>
                                </td> 
                              </tr>
                            </tbody>
                            <tfoot>
                              <tr>
                                <th>No. </th>
                                <th>Nama Sub Workgroup</th>
                                <th>Tindakan</th>
                              </tr>
                            </tfoot>
                          </table>
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>
            

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <script type="text/javascript">
	    $(document).ready(function(){
	      	$('#list-kapal').DataTable();
	    }); 
	</script>