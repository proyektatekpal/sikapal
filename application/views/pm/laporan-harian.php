<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Detail Kapal</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
         
                
          <div class="row">
            <form action="" method="post" role="form">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Laporan Harian</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                          <table id="list-kapal" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>Tanggal Laporan</th>
                                <th>Status</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Selasa, 17 September 2015<a target="_blank" class="pull-right" href="<?php echo base_url()?>ManajemenKapalOwner/DetailHarian" title="Detail"><i class="fa fa-chevron-circle-right"></i></a></td>
                                <td>Belum Disetujui</td>
                              </tr>
                              <tr>            
                              <td>Rabu, 18 September 2015 <a target="_blank" class="pull-right" href="<?php echo base_url()?>ManajemenKapalOS/DetailHarian" title="Detail"><i class="fa fa-chevron-circle-right"></i></a></td>
                                <td>Belum Disetujui</td>
                              </tr>
                            </tbody>
                            <tfoot>
                              <tr>
                                 <th>Tanggal Laporan</th>
                                 <th>Status</th>
                              </tr>
                            </tfoot>
                          </table>
                      </div><!-- /.box-body -->

                </div><!-- /.box -->
              </div>
            </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

 