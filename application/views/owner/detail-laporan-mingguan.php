<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
              <i class="fa fa-edit"></i> <strong>Laporan Mingguan</strong>
          </h1>
          <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <?php echo($menu); ?>
          <div class="row">
            <form action="<?php echo base_url()?>ManajemenKapalOwner/simpanLaporanMingguan" method="post" role="form">
              <div class="col-md-12">
                <input type="hidden" name="id_mingguan" value="<?php echo $data_mingguan->id?>">
                <input type="hidden" name="id_kapal" value="<?php echo $kapal->id?>">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title">Laporan Mingguan</h3>
                    </div><!-- /.box-header -->
                     <div class="box-body form-horizontal">

                        <div class="col-md-5">
                          <div class="form-group">
                            <label for="periode" class="col-sm-4 control-label">Periode:</label>
                            <div class="col-sm-8">
                              <?php 
                                $masa = ($data_mingguan->mid_end == 0) ? "mid" : "end";
                              ?>
                                <input type="text" class="form-control" name="periode" value="<?php echo $masa.' '.$data_mingguan->periode?>" readonly>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="tgl_input" class="col-sm-4 control-label">Tanggal input</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="tgl_input" value="<?php echo $data_mingguan->tgl_input?>" readonly>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="tgl_input" class="col-sm-4 control-label">OS Verifikator</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="os_" value="<?php echo $data_mingguan->os_verifikator?>" readonly>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="tgl_input" class="col-sm-4 control-label">Class Verifikator</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="class_" value="<?php echo $data_mingguan->class_verifikator?>" readonly>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="tgl_input" class="col-sm-4 control-label">Qa Verifikator</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="qa_" value="<?php echo $data_mingguan->qa_verifikator?>" readonly>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="tgl_input" class="col-sm-4 control-label">Verifikasi</label>
                            <div class="col-sm-8">
                                <div class="col-md-4">
                                  <label><input type="radio" name="verifikasi" value="1" class="checkuncheck" <?php if($data_mingguan->verif_owner=='1') echo 'checked'?>>Setujui</label>
                                </div>
                                <div class="col-md-4">
                                  <label><input type="radio" name="verifikasi" value="2" class="checkuncheck" <?php if($data_mingguan->verif_owner=='2') echo 'checked'?>>Tolak</label>
                                </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-5 col-md-offset-2">
                          <div class="form-group">
                            <label for="workgroup" class="col-sm-4 control-label">Workgroup:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="workgroup" value="<?php echo $data_mingguan->nama_pekerjaan?>" readonly>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="pilih_group" class="col-sm-4 control-label">Group:</label>
                            <div class="col-sm-8" id="div_group">
                                <input type="text" class="form-control" name="workgroup" value="<?php echo $data_mingguan->group?>" readonly>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="pilih_group" class="col-sm-4 control-label">Progres:</label>
                            <div class="col-sm-8" id="div_group">
                                <input type="text" class="form-control" name="progres_group" value="<?php echo $data_mingguan->progres_group.' %'?>" readonly>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="pilih_group" class="col-sm-4 control-label">Kuantitas:</label>
                            <div class="col-sm-8" id="div_group">
                                <input type="text" class="form-control" name="kuantitas" value="<?php echo $data_mingguan->kuantitas.' '.$data_mingguan->satuan?>" readonly>
                            </div>
                          </div>
                      </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->              

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Laporan</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-striped" id="tabelinput">
                          <thead>
                            <tr>
                              <th class="text-center">NO</th>
                              <th class="text-center col-md-3">Proses</th>
                              <th class="text-center col-md-2">Hasil/Rekomendasi</th>
                              <th class="text-center col-md-1">Status</th>
                              <th class="text-center col-md-1">Project Actual %</th>
                              <th class="text-center col-md-5">Deskripsi</th>
                            </tr>
                          </thead>
                          <tbody id="data_laporan">
                          <?php foreach ($detail_mingguan as $key =>$detail) {
                              $tmp_item = explode('#', $detail->item_pengawasan);
                              $banyak_item = count($tmp_item);
                                  for ($i=0; $i<($banyak_item-1); $i++) { 
                                    $item_pengawasan = explode('|', $tmp_item[$i]);
                                    $status_item = ($item_pengawasan[1]==0) ? "x" : "v";
                                    // print_r($item_pengawasan[1]);
                                      if($i==0){
                          ?>
                                          <tr>
                                              <td><?php echo ($key+1)?></td>
                                              <td><input type="text" name="aktifitas[]" value="<?php echo $detail->aktifitas?>" class="form-control" readonly></td>
                                              <td><input type="text" name="nama_item[]" value="<?php echo $item_pengawasan[0]?>" class="form-control text-center" readonly></td>
                                              <td><input type="text" name="status_item[]" value="<?php echo $status_item?>" class="form-control text-center" readonly></td>
                                              <td><input type="number" min="1" max="100" name="project_actual[]" value="<?php echo $detail->progres_aktual?>" class="form-control text-center input_number" readonly></td>
                                              <td><input type="text" name="deskripsi[]" value="<?php echo $detail->deskripsi?>" class="form-control" readonly></td>
                                          </tr>
                          <?php
                                      }
                                      else{
                          ?>
                                          <tr>
                                              <td></td>
                                              <td></td>
                                              <td><input type="text" name="nama_item[]" value="<?php echo $item_pengawasan[0]?>" class="form-control text-center" readonly></td>
                                              <td><input type="text" name="status_item[]" value="<?php echo $status_item?>" class="form-control text-center" readonly></td>
                                              <td></td>
                                              <td></td>
                                          </tr>
                          <?php
                                      }
                                  }
                          }?>
                          
                          </tbody>
                        </table>
                    </div>
                </div>

                <div class="box box-primary">

                    <div class="box-header with-border">
                      <h3 class="box-title">Catatan Owner Surveyor</h3>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                         <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Catatan</strong></span>
                            <input type="text" class="form-control" placeholder="" value="<?php echo $data_mingguan->catatan_os?>" readonly/>
                        </div>
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon" style="background-color:#C4C9D4;min-width:250px;text-align:left;"><strong>Kirim Tanggapan</strong></span>
                            <input type="text" class="form-control" placeholder="" name="tanggapan_owner" value="<?php echo (!empty($data_mingguan->catatan_owner) ? $data_mingguan->catatan_owner : '')?>"/>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                    </div><!-- /.box-body -->

                </div>
                

              </div>
            </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<script type="text/javascript">
  $(document).ready(function(){
      $('#pilih_workgroup').bind('change', function() {
        $.ajax({
            url: '<?php echo base_url()?>ManajemenKapalOS/getGroupByWorkgroup/' + $("#pilih_workgroup").val(), 
            dataType: 'json',
            success: function(data) {
                $('#pilih_group').html('');
                $('#div_bobot').html('');
                $('#pilih_group').append('<option style="display: none;">--Pilih Group--</option>');
                $.each(data, function(i,item){
                    var bobot_group = item.bobot
                    if (item.id_group != 'empty' ) {
                        $('#pilih_group').append('<option value="'+item.id_group+'">'+item.name+'</option>');
                        $('#div_bobot').append('<input type="hidden" name="bobot_group['+item.id_group+']" value="'+item.bobot+'">')
                    } else {
                        $('#pilih_group').html('');
                        $('#pilih_group').append('<option value=""> -- -- </option>');
                    }
                });
            }
        });
      });


      // $('#ok_btn').click(function(){
      //     $.ajax({
      //       url: '<?php echo base_url()?>ManajemenKapalOS/getLastLaporanHarian', 
      //       dataType: 'json',
      //       type: 'POST',
      //       data: {"id_group":$('#pilih_group').val(), "id_workgroup":$('#pilih_workgroup').val()},
      //       success: function(data) {
      //           $("#data_laporan").html('');
      //           $.each(data, function(i,item){
      //               var data_item_pengawasan = item.item_pengawasan.split('#');
      //               var jumlah_item = data_item_pengawasan.length - 1;

      //               for(var j=0; j<jumlah_item; j++){
      //                   var row = "";
      //                   var item_pengawasan = data_item_pengawasan[j].split('|');
      //                   var nama_item = item_pengawasan[0];
      //                   var status_item = '';
      //                   if(item_pengawasan[1] == 0){
      //                       status_item = 'belum';
      //                   }
      //                   else{
      //                       status_item = 'selesai';
      //                   }
      //                   if(j==0){
      //                       row += '<tr>';
      //                       row += '<td>'+(i+1)+'</td>';
      //                       row += '<td><input type="text" name="aktifitas[]" value="'+item.aktifitas+'" class="form-control" readonly></td>';
      //                       row += '<td><input type="text" name="nama_item[]" value="'+nama_item+'" class="form-control text-center" readonly></td>';
      //                       row += '<td><input type="text" name="status_item[]" value="'+status_item+'" class="form-control text-center" readonly></td>';
      //                       row += '<td><input type="number" min="1" max="100" name="project_actual['+item.id+']" class="form-control text-center input_number" onkeyup="this.value=this.value.replace(/[^0-9]/g,\'\')"></td>';
      //                       row += '<td><input type="text" name="deskripsi['+item.id+']" value="'+item.deskripsi+'" class="form-control" readonly></td>';
      //                       row += '<input type="hidden" name="id_aktifitas['+item.id+']" value="'+item.id+'">';
      //                       row += '<input type="hidden" name="bobot_aktifitas['+item.id+']" value="'+item.bobot_aktifitas+'">';
      //                       row += '<input type="hidden" name="item_pengawasan['+item.id+']" value="'+item.item_pengawasan+'">';
      //                       row += '</tr>';
      //                       $("#data_laporan").append(row);
      //                   } else{
      //                       row += '<tr>';
      //                       row += '<td></td>';
      //                       row += '<td></td>';
      //                       row += '<td><input type="text" name="item_pengawasan[]" value="'+nama_item+'" class="form-control text-center" readonly></td>';
      //                       row += '<td><input type="text" name="status_item[]" value="'+status_item+'" class="form-control text-center" readonly></td>';
      //                       row += '<td></td>';
      //                       row += '<td></td>';
      //                       row += '</tr>';
      //                       $("#data_laporan").append(row);
      //                   }
      //               }

      //           });
      //       }
      //     });
      // });

  });  
</script>