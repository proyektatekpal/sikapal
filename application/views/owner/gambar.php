<link rel="stylesheet" href="<?php echo base_url()?>assets/theme/plugins/lightbox2-master/css/lightbox.css">
<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>
<script src="<?php echo base_url()?>assets/theme/plugins/lightbox2-master/js/lightbox.js"></script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Detail Kapal</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <nav class="navbar navbar-default">
              <div class="container-fluid">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url()."manajemenKapalOwner/detailKapal/".$kapal->id?>"><strong><?php echo $kapal->nama_proyek;?></strong></a>
                  </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                      <li><a href="<?php echo base_url().'manajemenKapalOwner/Gambar/'.$kapal->id?>">Gambar</a></li>
                      <li><a href="<?php echo base_url().'manajemenKapalOwner/SCurve/'.$kapal->id?>">S-Curve</a></li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Laporan <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url().'manajemenKapalOwner/LaporanHarian/'.$kapal->id?>">Harian</a></li>
                            <li><a href="<?php echo base_url().'manajemenKapalOwner/LaporanMingguan/'.$kapal->id?>">2 Mingguan</a></li>
                          </ul>
                      </li>
                      <!-- <li><a href="#">Pesan</a></li> -->
                    </ul>
                  </div><!-- /.navbar-collapse -->

              </div><!-- /.container-fluid -->
          </nav>

         <div class="row">

                <div class="col-md-12">
                <div class="box box-primary box-solid">

                    <div class="box-header with-border">
                      <h3 class="box-title">Gambar Rancangan</h3>
                    </div><!-- /.box-header -->

                    <div class="box-body">
      <!-- href="'+baseurl +'upload/' +item.link_foto+'" data-lightbox="example-set" data-title="'+item.link_foto+'"><img class="example-image" src="'+baseurl +'upload/' +item.link_foto+'" alt="" height="70px" width="85px" /></center></td>'; -->
                        
                        <?php foreach($photo->result() as $row):?>
                        <div class="col-md-4">
                          <a class="example-image-link" href="<?= base_url().'gambar_rancangan/'.($row->link) ?>" data-lightbox="example-set" data-title="<?= ($row->judul)?>">
                        <?php     
                          
                              $image = array(
                                'src' => 'gambar_rancangan/'.($row->link),
                                // 'alt' => ($row->judul),
                                'class' => 'example-image-link',
                                'width' => '240',
                                'height' => '180',
                                'title' => ($row->judul),
                                // 'rel' => 'lightbox',
                                // 'href' => 'gambar_rancangan/'.($row->link),
                                // 'data-lightbox' => 'example-set',
                                // /'data-title' => 'gambar_rancangan/'.($row->link)
                              );
                              echo img($image); ?>
                          <br/>
                          <strong><?php echo($row->judul);?></strong>
                          <br/>
                          </a></div>
                          <?php endforeach; ?>
                    </div><!-- /.box-body -->
                   <!--  <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" data-lightbox="example-set" data-title="Contoh Gambar"><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt="" height="70px" width="70px" /></a> -->

                </div><!-- /.box -->
              </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <script type="text/javascript">
	    $(document).ready(function(){
	      	$('#list-kapal').DataTable();
	    }); 
	</script>