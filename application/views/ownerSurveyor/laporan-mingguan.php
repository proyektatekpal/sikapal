<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
              <i class="fa fa-edit"></i> <strong>Laporan Mingguan</strong>
          </h1>
          <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <?php echo($menu); ?>
          <div class="row">
            <form action="<?php echo base_url()?>ManajemenKapalOS/simpanLaporanMingguan" method="post" role="form">
              <div class="col-md-12">
              <input type="hidden" name="id_kapal" value="<?php echo $kapal->id?>">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title">Laporan Mingguan</h3>
                    </div><!-- /.box-header -->
                     <div class="box-body form-horizontal">

                        <div class="col-md-5">
                          <div class="form-group">
                            <label for="periode" class="col-sm-4 control-label">Periode:</label>
                            <div class="col-sm-8">
                              <select name="periode" id="periode" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">--Pilih Periode--</option>
                                  <?php 
                                  $masa = null;
                                  foreach ($periode as $periode) {
                                      if($periode->mid_end == 0){
                                          $masa = 'mid';
                                      }
                                      else{
                                          $masa = 'end';
                                      }
                                  ?>

                                      <option value="<?php echo $periode->mid_end.'-'.$periode->periode?>"><?php echo $masa.' - '.$periode->periode?></option>
                                  <?php
                                  }
                                  ?>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="tgl_input" class="col-sm-4 control-label">Tanggal input</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="tgl_input" value="<?php echo $today?>" readonly>                  
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="os_verifikator" class="col-sm-4 control-label">OS verifikator</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="os_verifikator" value=""> 
                            </div>                 
                          </div>
                          <div class="form-group">
                            <label for="class_verifikator" class="col-sm-4 control-label">Class verifikator</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="class_verifikator" value="">                  
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="qa_verifikator" class="col-sm-4 control-label">Qa verifikator</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="qa_verifikator" value="">                  
                            </div>
                          </div>
                        </div>

                        <div class="col-md-5 col-md-offset-2">
                          <div class="form-group">
                            <label for="workgroup" class="col-sm-4 control-label">Workgroup:</label>
                            <div class="col-sm-8">
                              <select name="pilih_workgroup" id="pilih_workgroup" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">--Pilih Workgroup--</option>
                                  <?php 
                                  foreach ($workgroup as $wg) {
                                  ?>
                                    <option value="<?php echo $wg->id?>"><?php echo $wg->nama_pekerjaan?></option>
                                  <?php
                                  }
                                  ?>
                              </select>
                              <?php 
                              foreach ($workgroup as $wg) {
                              ?>
                                <input type="hidden" name="bobot_workgroup[<?php echo $wg->id?>]" value="<?php echo $wg->bobot?>">
                              <?php
                              }
                              ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="pilih_group" class="col-sm-4 control-label">Group:</label>
                            <div class="col-sm-8" id="div_group">
                              <select name="pilih_group" id="pilih_group" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;" value="">--Pilih Group--</option>
                              </select>
                              <div class="bobot" id="div_bobot">
                              </div>
                            </div>
                          </div>
                          <a id="ok_btn" style="margin-left:15px;" class="btn bg-navy pull-right"  title="ok">Ok</a>
                      </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->              

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Laporan</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group" style="background-color:#eee; padding:10px;width:19%;float:right">
                            <div class="col-sm-12" id="div_group">
                              <strong>Keterangan status :</strong>
                            </div>
                            <div class="col-sm-11 col-sm-offset-1" id="div_group">
                              <strong>v = diterima </strong>
                            </div>
                            <div class="col-sm-11 col-sm-offset-1" id="div_group">
                              <strong>x = belum / ditolak </strong>
                            </div>
                        </div>
                        <table class="table table-bordered table-striped" id="tabelinput">
                          <thead>
                            <tr>
                              <th class="text-center">NO</th>
                              <th class="text-center col-md-3">Proses</th>
                              <th class="text-center col-md-2">Hasil/Rekomendasi</th>
                              <th class="text-center col-md-1">Status</th>
                              <th class="text-center col-md-1">Project Actual %</th>
                              <th class="text-center col-md-5">Deskripsi</th>
                            </tr>
                          </thead>
                          <tbody id="data_laporan">
                            <!-- <tr>
                              <td>1</td>
                              <td>act</td>
                              <td>setuju</td>
                              <td><input type="text" name="project_actual[]" class="form-control"></td>
                              <td>desc</td>
                              <td>ft</td>
                            </tr> -->
                          </tbody>
                        </table>
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-body with-border">
                      <table class="table table-bordered">
                        <tbody>
                          <tr>
                            <th>Catatan :</th>
                            <td><input type="text" name="catatan_os" class="form-control"/></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>

              </div>
            </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<script type="text/javascript">
  $(document).ready(function(){
      $('#pilih_workgroup').bind('change', function() {
        $.ajax({
            url: '<?php echo base_url()?>ManajemenKapalOS/getGroupByWorkgroup/' + $("#pilih_workgroup").val(), 
            dataType: 'json',
            success: function(data) {
                $('#pilih_group').html('');
                $('#div_bobot').html('');
                $('#pilih_group').append('<option style="display: none;">--Pilih Group--</option>');
                $.each(data, function(i,item){
                    var bobot_group = item.bobot
                    if (item.id_group != 'empty' ) {
                        $('#pilih_group').append('<option value="'+item.id_group+'">'+item.name+'</option>');
                        $('#div_bobot').append('<input type="hidden" name="bobot_group['+item.id_group+']" value="'+item.bobot+'">')
                    } else {
                        $('#pilih_group').html('');
                        $('#pilih_group').append('<option value=""> -- -- </option>');
                    }
                });
            }
        });
      });


      $('#ok_btn').click(function(){
          $.ajax({
            url: '<?php echo base_url()?>ManajemenKapalOS/getLastLaporanHarian', 
            dataType: 'json',
            type: 'POST',
            data: {"id_group":$('#pilih_group').val(), "id_workgroup":$('#pilih_workgroup').val()},
            success: function(data) {
                $("#data_laporan").html('');
                $.each(data, function(i,item){
                    var data_item_pengawasan = item.item_pengawasan.split('#');
                    var jumlah_item = data_item_pengawasan.length - 1;
                    var item_diterima = 0;
                    var item_ditolak = 0;
                    for(var j=0; j<jumlah_item; j++){
                        var row = "";
                        var item_pengawasan = data_item_pengawasan[j].split('|');
                        var nama_item = item_pengawasan[0];
                        var status_item = '';
                        
                        if(item_pengawasan[1] == 0){
                            status_item = 'x';
                            item_ditolak++;
                        }
                        else{
                            status_item = 'v';
                            item_diterima++;
                        }
                        if(j==0){
                            row += '<tr>';
                            row += '<td>'+(i+1)+'</td>';
                            row += '<td><input type="text" name="aktifitas[]" value="'+item.aktifitas+'" class="form-control" readonly></td>';
                            row += '<td><input type="text" name="nama_item[]" value="'+nama_item+'" class="form-control text-center" readonly></td>';
                            row += '<td><input type="text" name="status_item[]" value="'+status_item+'" class="form-control text-center" readonly></td>';
                            row += '<td><input type="number" min="0" max="100" id="in_'+item.id+'" name="project_actual['+item.id+']" value="" class="form-control text-center input_number" onkeyup="this.value=this.value.replace(/[^0-9]/g,\'\')" readonly></td>';
                            row += '<td><input type="text" name="deskripsi['+item.id+']" value="" class="form-control"</td>';
                            row += '<input type="hidden" name="id_aktifitas['+item.id+']" value="'+item.id+'">';
                            row += '<input type="hidden" name="bobot_aktifitas['+item.id+']" value="'+item.bobot_aktifitas+'">';
                            row += '<input type="hidden" name="item_pengawasan['+item.id+']" value="'+item.item_pengawasan+'">';
                            row += '</tr>';
                            $("#data_laporan").append(row);
                        } else{
                            row += '<tr>';
                            row += '<td></td>';
                            row += '<td></td>';
                            row += '<td><input type="text" name="item_pengawasan[]" value="'+nama_item+'" class="form-control text-center" readonly></td>';
                            row += '<td><input type="text" name="status_item[]" value="'+status_item+'" class="form-control text-center" readonly></td>';
                            row += '<td></td>';
                            row += '<td></td>';
                            row += '</tr>';
                            $("#data_laporan").append(row);
                        }
                    }

                    var kolom_input = "in_"+item.id;
                    var progres_aktual = Math.round(item_diterima/jumlah_item*100*100)/100;
                    // console.log($(kolom_input).val());
                    $("#"+kolom_input).val(progres_aktual);
                    // alert(input_progres);


                });
            }
          });
      });

  });  
</script>