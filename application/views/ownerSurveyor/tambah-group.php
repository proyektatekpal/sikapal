<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
              <i class="fa fa-edit"></i> <strong>Detail Kapal</strong>
          </h1>
          <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          
            <?php echo($menu); ?>
            <div class="row">
                
                <div class="col-md-12">
                  
                    <form action="<?php echo base_url()?>ManajemenKapalOS/simpangroup" method="post">
                    <?php
                    if ($cek_isi=='isi') {
                        if ($eksekusi) { ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-check"></i> Sukses!</h4>
                                Data Berhasil Disimpan
                            </div>
                        <?php } else { ?>
                            <div class="alert alert-warning alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-warning"></i> Gagal!</h4>
                                Data Gagal Disimpan
                            </div>
                        <?php }
                    }
                    ?>

                    <input type="hidden" name="id_kapal" value="<?php echo $kapal->id?>">
                    <div class="box box-primary box-solid">
                        <div class="box-header with-border">
                          <h3 class="box-title">Tambah Group</h3>
                        </div>
                        <div class="box-body form-horizontal">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="workgroup" class="col-sm-5 control-label">Workgroup:</label>
                                <div class="col-sm-7">
                                  <select name="workgroup" id="workgroup" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                      <option style="display: none;">Pilih Workgroup</option>
                                      <?php 
                                      foreach ($workgroup as $wg) {
                                        echo '<option value="'.$wg->id.'">'.$wg->nama_pekerjaan.'</option>';
                                      }
                                      ?>s
                                  </select>
                                </div>
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="jumlahActivity" class="col-sm-5 control-label">Jumlah Group:</label>
                                <div class="col-sm-7">
                                  <input type="number" min="1" class="form-control input_number" placeholder="jumlah group" id="jumlah_group" name="jumlah_group">
                                </div>
                              </div>
                            </div>
                           
                            <a id="ok_button" style="margin-left:15px;margin-right:15px;" class="btn bg-navy pull-left"  title="ok">Ok</a>
                        </div>
                    </div>

                    <div class="box box-primary laporan_hidden" hidden>
                        <div class="box-body">
                            <table class="table table-bordered table-striped" id="tabelinput">
                                <thead>
                                    <tr>
                                      <th class="text-center col-md-1">NO</th>
                                      <th class="text-center">Nama Group</th>
                                      <th class="text-center col-md-1">Bobot (%)</th>
                                      <th class="text-center col-md-1">Kuantitas</th>
                                      <th class="text-center col-md-2">Satuan</th>
                                    </tr>
                                </thead>
                                <tbody id="data_group">

                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </div>
                    </div>

                    </form>
                </div>


            </div>

          </div>

        </section>
      </div>

<script type="text/javascript">
  $(document).ready(function(){
        $("#ok_button").click(function(){  
            $("#data_activity").html('');
            var jumlah_group = $('#jumlah_group').val();

            if(jumlah_group =='' | jumlah_group <= 0)
            {
                  alert('Silahkan isi jumlah peekerjaan dengan benar');
            }
            else
            {
                for(var i=0; i<jumlah_group; i++)
                  {
                      var kolom = '';
                      kolom += '<tr>';
                      kolom += '<td class="text-center"><input type="text" class="form-control" value="'+(i+1)+'"/></td>';
                      kolom += '<td><input type="text" name="group'+(i+1)+'" class="form-control"/></td>';
                      kolom += '<td><input type="number" name="bobot'+(i+1)+'" class="form-control input_number"/></td>';
                      kolom += '<td><input type="number" name="kuantitas'+(i+1)+'" class="form-control input_number"/></td>';
                      kolom += '<td><input type="text" name="satuan'+(i+1)+'" class="form-control"/></td>';
                      kolom += '<tr>';
                      $("#data_group").append(kolom);
                    }
                    $('.laporan_hidden').show();
            }
            
        });
    }); 
</script>