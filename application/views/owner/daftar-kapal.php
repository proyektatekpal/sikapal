<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Daftar Kapal</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="modal fade bs-example-modal-lg" id="Rangkuman" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Rangkuman Projeck Kapal</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-xs-6">
                      <div class="box-header with-border">
                        <h3 class="box-title">Projek Aktual</h3>
                      </div>
                      <table id="data_rangkuman" class="table table-bordered table-striped data-table">
                                <thead>
                                  <tr>
                                    <th>Bulan</th>
                                    <th>Periode</th>
                                    <th>Progress</th>
                                  </tr>
                                </thead>
                                <tbody id="data_progres_aktual">
                                  
                                </tbody>
                      </table>
                    </div>
                    <div class="col-xs-6">
                      <div class="box-header with-border">
                        <h3 class="box-title">Projek Rencana</h3>
                      </div>
                      <table id="data_rangkuman" class="table table-bordered table-striped data-table">
                                <thead>
                                  <tr>
                                    <th>Bulan</th>
                                    <th>Periode</th>
                                    <th>Progress</th>
                                  </tr>
                                </thead>
                                <tbody id="data_progres_rencana">
                                  
                                </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
          </div>

          <!-- Main row -->
          <div class="row">
            	<div class="col-xs-12">
                	<div class="box box-primary box-solid">
                      <div class="box-header">
                          <h3 class="box-title">Daftar Proyek Kapal</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                          <table id="list-kapal" class="table table-bordered table-striped data-table">
                            <thead>
                              <tr>
                                <th>No. </th>
                                <th>Nama Kapal</th>
                                <th>Nama Galangan</th>
                                <!-- <th>Owner Surveyor</th> -->
                                <th>Rangkuman Progress</th>
                                <!-- <th>Proyek Rencana %</th>
                                <th>Proyek Aktual %</th> -->
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                              $num = 1;
                              foreach ($list_kapal as $kapal) {
                                  echo '<tr>';
                                  echo '<td>'.$num.'</td>';
                                  echo '<td>'.$kapal->nama_proyek.' <a target="_blank" class="pull-right" href="'.base_url().'ManajemenKapalOwner/DetailKapal/'.$kapal->id.'" title="Detail"><i class="fa fa-chevron-circle-right"></i></a></td>';
                                  echo '<td>'.$kapal->nama_perusahaan.'</td>';
                                  echo '<td><input type="button" value="Lihat" style="margin-left:15px;" class="btn bg-navy pull-left" data-toggle="modal" data-target="#Rangkuman" onClick="getRangkuman(\'' . $kapal->id . '\')" /></input></td>';
                                  // echo '<td><a href="#" onclick="myFunction(); return false;">click me too</a></td>';
                                  // echo '<td>'.$kapal->nama_os.'</td>';
                                  // if($kapal->status_pengerjaan == 0)
                                  //   echo '<td>Sedang Dikerjakan</td>';
                                  // else
                                  //   echo '<td>Sudah Selesai</td>';
                                  // echo '</tr>';
                                   $num++;
                              }
                              ?>

                              <!-- <tr>
                                <td>2.</td>
                                <td>Ranggawuni <a target="_blank" class="pull-right" href="<?php //echo base_url()?>ManajemenKapal/OwnerSurveyor/DetailKapal" title="Detail"><i class="fa fa-chevron-circle-right"></i></a></td>
                                <td>PT. Cisarut Suprautama</td>
                                <td>Albert Einstein</td>
                                <td>Alifa</td>
                                <td>Sedang Dikerjakan</td>
                              </tr>
                              <tr>
                                <td>3.</td>
                                <td>Jelapatih <a target="_blank" class="pull-right" href="#" title="Detail"><i class="fa fa-chevron-circle-right"></i></a></td>
                                <td>Merpati Marine Service</td>
                                <td>Thomas Alfa Edison</td>
                                <td>Ridho</td>
                                <td>Sedang Dikerjakan</td>
                              </tr> -->
                              
                            </tbody>
                            <tfoot>
                              <tr>
                                <th>No. </th>
                                <th>Nama Kapal</th>
                                <th>Nama Galangan</th>
                                <!-- <th>Owner Surveyor</th> -->
                                <th>Rangkuman Progress</th>
                                <!-- <th>Proyek Rencana %</th>
                                <th>Proyek Aktual %</th> -->
                              </tr>
                            </tfoot>
                          </table>
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>
            

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<script type="text/javascript">

      function getRangkuman(id_kapal) 
      {
        var kolom = '';
        var num = 0;
        document.getElementById('data_progres_aktual').innerHTML = '';
        document.getElementById('data_progres_rencana').innerHTML = '';

        $.ajax({
                        url: '<?php echo base_url()?>ManajemenKapalOwner/GetprojekAktual/', 
                        dataType: 'json',
                        type: 'POST',
                        // async: false,
                        data: {"id_kapal":id_kapal},
                        success: function(data) {
                          $.each(data, function(i,item){
                            
                              num ++;
                              kolom =  '';
                              kolom += '<tr>';
                              if(num % 2 != 0)
                              {
                                kolom += '<td>' + item.periode +'</td>';
                              }
                              else
                              {
                                kolom += '<td></td>';
                              }
                              if(item.mid_end == 0)
                              {
                                kolom += '<td>Awal</td>';
                              }
                              else
                              {
                                 kolom += '<td>Akhir</td>';
                              }
                              // kolom += '<td>'+ item.mid_end +' </td>';
                              kolom += '<td>'+ item.progres+'</td>';
                              // kolom += '<td><input type="button" value="Lihat" style="margin-left:15px;" class="btn bg-navy pull-left" data-toggle="modal" data-target="#Rangkuman" onClick="getRangkuman(\'' . $kapal->id . '\')" /></input></td>';
                              kolom += '</tr>';
                                                        
                             
                          
                            $("#data_progres_aktual").append(kolom);
                          });                
                        }
        });
        
        num = 0;
        $.ajax({
                        url: '<?php echo base_url()?>ManajemenKapalOwner/GetprojekRencana/', 
                        dataType: 'json',
                        type: 'POST',
                        // async: false,
                        data: {"id_kapal":id_kapal},
                        success: function(data) {
                          $.each(data, function(i,item){
                            
                              num ++;
                              kolom =  '';
                              kolom += '<tr>';
                              if(num % 2 != 0)
                              {
                                kolom += '<td>' + item.periode +'</td>';
                              }
                              else
                              {
                                kolom += '<td></td>';
                              }

                              if(item.mid_end == 0)
                              {
                                kolom += '<td>Awal</td>';
                              }
                              else
                              {
                                 kolom += '<td>Akhir</td>';
                              }

                              // kolom += '<td>'+ item.mid_end +' </td>';
                              kolom += '<td>'+ item.progres+'</td>';
                              // kolom += '<td><input type="button" value="Lihat" style="margin-left:15px;" class="btn bg-navy pull-left" data-toggle="modal" data-target="#Rangkuman" onClick="getRangkuman(\'' . $kapal->id . '\')" /></input></td>';
                              kolom += '</tr>';
                          
                            $("#data_progres_rencana").append(kolom);
                          });                
                        }
        });
      }

      $(document).ready(function()
      {
        
      }); 
  </script>