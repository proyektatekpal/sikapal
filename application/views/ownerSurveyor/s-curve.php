<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Detail Kapal</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <?php echo($menu); ?>

          <!-- <div class="row">
          <section class="col-lg-12 connectedSortable">
            	<div class="box box-primary box-solid">
	                <div class="box-header with-border">
	                  	<h3 class="box-title">S-Curve</h3>
	                  	<div class="box-tools pull-left">
	                    	<button class="btn btn-box-tool" data-widget="remove"></button>
	                  	</div>
	                </div>
	                <div class="box-body">
			            <div class="box-tools pull-left">
	                  		<div class="box-body">
	                    		<img src="<?php echo base_url()?>assets/_/images/sample/s-curve.png ?>" alt="test" style="width:304px;height:228px;">
	                    	</div>
	                  	</div>     
	                </div>
              	</div>
            </section> -->

            <div class="row">
              <div class="col-md-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title">S-Curve</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <canvas id="myChart" class="col-md-12"></canvas>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div>
          

            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <section class="col-lg-12">
            	<div class="box box-primary">
	                <!-- <div class="box-header with-border">
	                
		                </div> --><!-- /.box-header -->
		                <div class="box-footer">
		                	<button type="submit" class="btn btn-primary pull-right">Verifikasi</button>
		                	<a style="margin-left:15px;margin-right:15px;" class="btn bg-navy pull-right" href="<?php echo base_url()?>dashboard" title="Kembali">Kembali</a>
	                   	</div>
				    
              	</div><!-- /.box -->
            </section>
            </div><!-- /.row (main row) -
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<script type="text/javascript">
      $(document).ready(function(){
          $('#list-kapal').DataTable();
      }); 
      var defaultConfig = {
                animation: true,
                animationSteps: 60,
                animationEasing: "easeOutQuart",
                showScale: true,
                scaleOverride: false,
                scaleSteps: null,
                scaleStepWidth: null,
                scaleStartValue: null,
                scaleLineColor: "rgba(0,0,0,.1)",
                scaleLineWidth: 1,
                scaleShowLabels: true,
                scaleLabel: "<%=value%>",
                scaleIntegersOnly: true,
                scaleBeginAtZero: false,
                scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
                scaleFontSize: 12,
                scaleFontStyle: "normal",
                scaleFontColor: "#666",

                // Boolean - whether or not the chart should be responsive and resize when the browser does.
                responsive: false,

                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: true,

                // Boolean - Determines whether to draw tooltips on the canvas or not
                showTooltips: true,

                // Function - Determines whether to execute the customTooltips function instead of drawing the built in tooltips (See [Advanced - External Tooltips](#advanced-usage-custom-tooltips))
                customTooltips: false,

                // Array - Array of string names to attach tooltip events
                tooltipEvents: ["mousemove", "touchstart", "touchmove"],

                // String - Tooltip background colour
                tooltipFillColor: "rgba(0,0,0,0.8)",

                // String - Tooltip label font declaration for the scale label
                tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

                // Number - Tooltip label font size in pixels
                tooltipFontSize: 14,

                // String - Tooltip font weight style
                tooltipFontStyle: "normal",

                // String - Tooltip label font colour
                tooltipFontColor: "#fff",

                // String - Tooltip title font declaration for the scale label
                tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

                // Number - Tooltip title font size in pixels
                tooltipTitleFontSize: 14,

                // String - Tooltip title font weight style
                tooltipTitleFontStyle: "bold",

                // String - Tooltip title font colour
                tooltipTitleFontColor: "#fff",

                // Number - pixel width of padding around tooltip text
                tooltipYPadding: 6,

                // Number - pixel width of padding around tooltip text
                tooltipXPadding: 6,

                // Number - Size of the caret on the tooltip
                tooltipCaretSize: 8,
                tooltipCornerRadius: 6,
                tooltipXOffset: 10,
                tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",
                multiTooltipTemplate: "<%= value %>",
                onAnimationProgress: function(){},
                onAnimationComplete: function(){}
            }
            var data = {
                  labels: ["Mid Aug", "End Aug", "Mid Sept", "End Sept", "Mid Okt", "End Okt", "Mid Nov"],
                  datasets: [
                      {
                          label: "My First dataset",
                          fillColor: "rgba(220,220,220,0.2)",
                          strokeColor: "rgba(220,220,220,1)",
                          pointColor: "rgba(220,220,220,1)",
                          pointStrokeColor: "#fff",
                          pointHighlightFill: "#fff",
                          pointHighlightStroke: "rgba(220,220,220,1)",
                          data: [65, 59, 80, 81, 56, 55, 40]
                      },
                      {
                          label: "My Second dataset",
                          fillColor: "rgba(151,187,205,0.2)",
                          strokeColor: "rgba(151,187,205,1)",
                          pointColor: "rgba(151,187,205,1)",
                          pointStrokeColor: "#fff",
                          pointHighlightFill: "#fff",
                          pointHighlightStroke: "rgba(151,187,205,1)",
                          data: [28, 48, 40, 19, 86, 27, 90]
                      }
                  ]
              };
            var ctx = document.getElementById("myChart").getContext("2d");
            var myLineChart = new Chart(ctx).Line(data, defaultConfig);
  </script>


