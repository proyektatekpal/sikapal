<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
	        <h1>
	          	<i class="fa fa-edit"></i> <strong>Daftar Kapal</strong>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Dashboard</li>
	        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
            	<div class="col-xs-12">
                	<div class="box box-primary box-solid">
                      <div class="box-header">
                          <h3 class="box-title">Daftar Proyek Kapal</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                          <table id="list-kapal" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>No. </th>
                                <th>Nama Kapal</th>
                                <th>Nama Galangan</th>
                                <th>Project Manager</th>
                                <th>Owner Surveyor</th>
                                <th>Status Pengerjaan</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                              $num = 1;
                              foreach ($list_kapal as $kapal) {
                                  echo '<tr>';
                                  echo '<td>'.$num.'</td>';
                                  echo '<td>'.$kapal->nama_kapal.' <a target="_blank" class="pull-right" href="'.base_url().'ManajemenKapalPM/DetailKapal/'.$kapal->id_pembuatan.'" title="Detail"><i class="fa fa-chevron-circle-right"></i></a></td>';
                                  echo '<td>'.$kapal->nama_galangan.'</td>';
                                  echo '<td>'.$kapal->nama_pm.'</td>';
                                  echo '<td>'.$kapal->nama_os.'</td>';
                                  if($kapal->status_pengerjaan == 0)
                                    echo '<td>Sedang Dikerjakan</td>';
                                  else
                                    echo '<td>Sudah Selesai</td>';
                                  echo '</tr>';
                                  $num++;
                              }
                              ?>
                              <!-- <tr>
                                <td>1.</td>
                                <td>Ranggawuni <a target="_blank" class="pull-right" href="<?php //echo base_url()?>manajemenKapalPM/detailkapal" title="Detail"><i class="fa fa-chevron-circle-right"></i></a></td>
                                <td>PT. Cisarut Suprautama</td>
                                <td>Albert Einstein</td>
                                <td>Alifa</td>
                                <td>Belum selesai</td>
                              </tr>
                              <tr>
                                <td>2.</td>
                                <td>Jelapatih <a target="_blank" class="pull-right" href="#" title="Detail"><i class="fa fa-chevron-circle-right"></i></a></td>
                                <td>Merpati Marine Service</td>
                                <td>Thomas Alfa Edison</td>
                                <td>Ridho</td>
                                <td>Belum selesai</td>
                              </tr> -->
                            </tbody>
                            <tfoot>
                              <tr>
                                <th>No. </th>
                                <th>Nama Kapal</th>
                                <th>Nama Galangan</th>
                                <th>Project Manager</th>
                                <th>Owner Surveyor</th>
                                <th>Status Pengerjaan</th>
                              </tr>
                            </tfoot>
                          </table>
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>
            

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <script type="text/javascript">
	    $(document).ready(function(){
	      	$('#list-kapal').DataTable();
	    }); 
	</script>