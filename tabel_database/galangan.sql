-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 17 Jan 2016 pada 11.32
-- Versi Server: 5.6.14
-- Versi PHP: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `sikapal`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `galangan`
--

CREATE TABLE IF NOT EXISTS `galangan` (
  `id_galangan` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` varchar(50) NOT NULL,
  `alamat_perusahaan` varchar(50) NOT NULL,
  `pimpinan_proyek` varchar(50) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id_galangan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `galangan`
--

INSERT INTO `galangan` (`id_galangan`, `nama_perusahaan`, `alamat_perusahaan`, `pimpinan_proyek`, `telp`, `email`) VALUES
(000001, 'PT. DUMAS TANJUNG PERAK SHIPYARD', 'SURABAYA', 'Wimpy', '09909090', 'Galangan@galangan.com'),
(000002, 'PT. Adiluhung Saranasegara Indonesia', 'JL. Raya Ujung Piring, Kec. Bangkalan, 69118', 'Sugiyarto', '(031) 3097039', 'adiluhung@gmail.com'),
(000003, 'PT. Lamongan Marine Industry (LMI)', 'Lamongan', 'Fikri', '09909090', 'lmi@gmail.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
