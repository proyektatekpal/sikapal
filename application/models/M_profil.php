<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class M_profil extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
  
  
    public function getOwnerProfil($email)
    {
        $query = $this->db->get_where('owner', array('email' => $email));
        return $query->row();
    }

    public function getOwnerSurveyorProfil($email)
    {
        $query = $this->db->get_where('ownerSurveyor', array('email' => $email));
        return $query->row();
    }

    public function getProjectManagerProfil($email)
    {
        $query = $this->db->get_where('projectManager', array('email' => $email));
        return $query->row();
    }



}  

?>