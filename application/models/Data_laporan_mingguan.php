<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class Data_laporan_mingguan extends CI_Model
{
	protected $table = "data_laporan_mingguan";
    protected $all_field = 'data_laporan_mingguan.id,id_laporan_mingguan,id_aktivitas,item_pengawasan,progres_aktual,deskripsi';
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data)
    {
    	return $this->db->insert($this->table, $data);
    }

    public function get($where=NULL)
    {
        $this->db->select($this->all_field);
        if(!empty($where)){
            $this->db->where($where);
        }
        $this->db->from($this->table);
        $res = $this->db->get();
        if($res->num_rows()==1){
            return $res->row();
        }
        else{
            return $res->result();
        }
    }

    public function join_mingguan()
    {
        $this->db->select('id_group,tgl_input,periode,mid_end,progres_group,verif_owner,catatan_os,catatan_owner');
        $this->db->join('laporan_mingguan', 'laporan_mingguan.id = data_laporan_mingguan.id_laporan_mingguan', 'left');
    }

    public function join_aktifitas()
    {
        $this->db->select('id_workgroup,aktifitas,item,bobot');
        $this->db->join('aktifitas', 'aktifitas.id = data_laporan_mingguan.id_aktivitas', 'left');
    }

}  

?>