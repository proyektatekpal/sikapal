<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfilOwnerSurveyor extends CI_Controller {

	public function __construct()
    {
        parent::__construct(); 
        if ( !$this->session->userdata('isLogin') && $this->session->userdata('level') != 'ownersurveyor')
        { 
            redirect('access');
        }
        // $this->load->library('form_validation');
        $this->load->model('m_profil');
    }

	public function index()
	{
        
	}

    public function editProfil()
    {
        $email = $this->session->userdata('email');
        $DataOS['data'] = $this->m_profil->getOwnerSurveyorProfil($email);
        $this->load->view('header');
        $this->load->view('sidebar-os');
        $this->load->view('ownersurveyor/edit-profil', $DataOS);
        $this->load->view('footer');
    }

    public function simpanEdit()
    {
        
    }

    public function ubahPassword()
    {
        $this->load->view('header');
        $this->load->view('sidebar-os');
        $this->load->view('ownersurveyor/ubah-password');
        $this->load->view('footer');
    }

    public function simpanPassword()
    {
        
    }
}



