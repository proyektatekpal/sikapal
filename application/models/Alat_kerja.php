<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class Alat_kerja extends CI_Model
{
	protected $table = "alat_kerja";
	protected $all_field = "id, alat";
    public function __construct()
    {
        parent::__construct();
    }
  
    public function get()
    {
    	$this->db->select($this->all_field);
    	$this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }
}  

?>