<style typpe="text/css">
  .form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
  }

  .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
  }
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
              <i class="fa fa-edit"></i> <strong>Laporan Mingguan</strong>
          </h1>
          <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <?php echo($menu); ?>
          <div class="row">
            <form action="" method="post" role="form">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Laporan Mingguan</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                     <div class="box-body form-horizontal">
                        <div class="col-md-4">

                          
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-5 control-label">Periode:</label>
                            <div class="col-sm-7">
                              <select name="periode" id="periode" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Periode</option>
                                  <option>Januari Awal</option>
                                  <option>Januari Akhir</option>
                                  <option>Februari Awal</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-5 control-label">Tanggal input</label>
                            <div class="col-sm-7">
                              <!-- <select name="periode" id="periode" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Periode</option>
                                  <option>Januari Awal</option>
                                  <option>Januari Akhir</option>
                                  <option>Februari Awal</option>
                              </select>-->                            
                            </div>
                          </div>
                          <a id="ok_button" style="margin-left:15px;" class="btn bg-navy pull-right"  title="ok">Ok</a>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div>

              
              <div class="col-md-12 workgroup-group" hidden>
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Pilih Workgroup dan Group</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body form-horizontal">
                      <div class="col-md-4">
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-5 control-label">Workgroup:</label>
                            <div class="col-sm-7">
                              <select name="workgroup" id="workgroup" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Workgroup</option>
                                  <option>Design & Approval Drawing</option>
                                  <option>Work Preparation & General</option>
                                  <option>Hull construction</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="cuaca_siang" class="col-sm-5 control-label">Group:</label>
                            <div class="col-sm-7">
                              <select name="workgroup" id="workgroup" class="form-control selectpicker scrollable-menu" role="menu" data-live-search="true" data-size="3">
                                  <option style="display: none;">Pilih Group</option>
                                  <option>110</option>
                                  <option>111</option>
                                  <option>112</option>
                              </select>
                            </div>
                          </div>
                          <a id="ok_button2" style="margin-left:15px;" class="btn bg-navy pull-right"  title="ok">Ok</a>
                        </div>
                    </div>

                    <div class="box-body form-horizontal">
                      <div class="box box-primary laporan_hidden" hidden>
                        <div class="box-header with-border">
                            <h3 class="box-title">Data Laporan</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <table class="table table-bordered table-striped" id="tabelinput">
                              <thead>
                                <tr>
                                  <th class="text-center">NO</th>
                                  <th class="text-center">Aktivitas</th>
                                  <th class="text-center">Status</th>
                                  <th class="text-center">Project Actual %</th>
                                  <th class="text-center">Deskripsi</th>
                                  <th class="text-center">Foto</th>
                                </tr>
                              </thead>
                              <tbody id="data_laporan">

                              </tbody>
                            </table>

                            <div class="col-md-12">
                              <div class="box box-primary">
                                  <div class="box-header with-border">
                                    <table class="table table-bordered">
                                      <tbody>
                                        <tr>
                                          <th>Catatan :</th>
                                          <td><input type="text" class="form-control"/></td>
                                        </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                           </div>
                              
                        </div><!-- /.box-body -->

                        <div class="box-footer">

                            <div class="pull-right">

                              <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                      </div><!-- /.box -->
                    </div>
                    
                </div><!-- /.box -->
              </div>

              

              
            </form>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<script type="text/javascript">
      $(document).ready(function(){
          $('#list-kapal').DataTable();

          $("#ok_button").click(function()
          {  
             $('.workgroup-group').show();           
          });


          $("#ok_button2").click(function()
          {  
              $('.laporan_hidden').show();
          });


          // $("#workgroup").change(function()
          // {  
          //     $("#detail_pekerjaan").html('');
          //     var row = $('#jumlah_pekerjaan').val();
          //     var jns_alat = $('#jenis_alat').val();
          //     var jns_tenaga = $('#jenis_tenaga').val();
          //     if(row=='' | row<= 0 | jns_alat=='' | jns_alat<=0 | jns_tenaga=='' | jns_tenaga<=0)
          //     {
          //         alert('Silahkan isi jumlah peekerjaan dengan benar');
          //     }
          //     else
          //     {
          //         for(var i=0; i<row; i++)
          //         {
          //             var kolom = '';
          //             kolom += '<tr>';
          //             kolom += '<td class="col-md-1"><input type="text" class="form-control" value="'+(i+1)+'"/></td>';
          //             kolom += '<td class="col-md-3"><input type="text" class="form-control"/></td>';
          //             kolom += '<td class="col-md-5"><input type="text" class="form-control"/></td>';
          //             kolom += '<td class="col-md-2"><input type="number" class="form-control input_number"/></td>';
          //             kolom += '<td class="col-md-1"><input type="file" id="foto"></td>';
          //             kolom += '<tr>';
          //             $("#detail_pekerjaan").append(kolom);
          //         }

          //         for(var j=0; j<jns_alat; j++)
          //         {
          //              $.ajax({
          //                 url: '<?php echo base_url()?>manajemenKapalPM/getDataAlat', 
          //                 dataType: 'json',
          //                 type: 'POST',
          //                 success: function(data) {
          //                     // console.log(data);
          //                     var kolom = '';
          //                     kolom += '<div class="input-group">';
          //                     kolom += '<div class="input-group-btn">';
          //                     kolom += '<select type="button" class="btn btn-default dropdown-toggle">';
          //                     $.each(data, function(i,item){
          //                         console.log(item.id);
          //                         if (item.id != 'empty' ) {
          //                             kolom += '<option>'+item.nama+'</option>';
          //                         } else {
                                      
          //                         }
          //                     });

          //                     kolom += '</select>';
          //                     kolom += '</div>';
          //                     kolom += '<input class="form-control" type="number" value="" placeholder="Jumlah" name="">';
          //                     kolom += '</div>';
          //                     $("#list_alat").append(kolom);
          //                 }
          //             }); 
          //         } 
          //         $('.laporan_hidden').show();
          //     }
          // });
      });  
</script>





