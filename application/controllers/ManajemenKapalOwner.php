<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManajemenKapalOwner extends CI_Controller {

	public function __construct()
    {
        parent::__construct(); 
        if ( !$this->session->userdata('isLogin') && $this->session->userdata('level') != 'owner')
        { 
            redirect('access');
        }
        // $this->load->library('form_validation');
        $this->load->model(array('m_profil','m_manajemen'));

    }

	public function index()
	{
        redirect('ManajemenKapalOwner/daftar-kapal');
	}

    // public function Dashboard()
    // {
    //     $this->load->view('header');
    //     $this->load->view('sidebar-owner');
    //     $this->load->view('dashboard/home-owner');
    //     $this->load->view('footer');
    // }

    public function TambahKapal()
    {
        $Data['cek_isi'] = 'kosong';
        $Data['list_pm'] = $this->m_manajemen->getPM();
        $Data['list_os'] = $this->m_manajemen->getOS();

        $this->load->view('header');
        $this->load->view('sidebar-owner');
        $this->load->view('owner/tambah-kapal', $Data);
        $this->load->view('footer');
    }

    public function SimpanKapal()
    {
        $email = $this->session->userdata('email');
        $pm = $this->input->post('id_pm');
        $detail_pm = $this->m_manajemen->getDetailPM($pm);
        $detail_owner = $this->m_manajemen->getDetailOwner($email);
        $id_galangan = 
        $data_kapal = array(
                'id_galangan' => $detail_pm->id_galangan,
                'id_pm' => $pm,
                'id_owner' => $detail_owner->id_owner,
                'id_os' => $this->input->post('id_os'),
                'nama_kapal' => $this->input->post('nama_kapal'),
                'pjg_total' => $this->input->post('pjg_kapal'),
                'pjg_antaragaristegak' => $this->input->post('pjg_antara'),
                'lebar' => $this->input->post('lbr_kapal'),
                'tinggi' => $this->input->post('tinggi_kapal'),
                'sarat_air' => $this->input->post('sarat_air'),
                'jarak_jelajah' => $this->input->post('jarak_jelajah'),
                'tenaga_penggerak' => $this->input->post('tenaga_penggerak'),
                'tenaga_bantu' => $this->input->post('tenaga_bantu'),
                'kls_klasifikasi' => $this->input->post('kls_klasifikasi'),
                'abk' => $this->input->post('jml_abk'),
                'penumpang' => $this->input->post('jml_penumpang'),
                'tangki_bbm' => $this->input->post('tangki_bbm'),
                'tangki_ballast' => $this->input->post('tangki_ballast'),
                'lama_pengerjaan' => $this->input->post('lama_pengerjaan'),
                'tgl_mulai' => $this->input->post('tgl_mulai')
            );
        $Data['cek_isi'] = 'isi';
        $Data['eksekusi'] = $this->m_manajemen->insertData('pembuatankapal', $data_kapal);
        $Data['list_pm'] = $this->m_manajemen->getPM();
        $Data['list_os'] = $this->m_manajemen->getOS();

        $this->load->view('header');
        $this->load->view('sidebar-owner');
        $this->load->view('owner/tambah-kapal', $Data);
        $this->load->view('footer');
    }

    public function DaftarKapal()
    {
        $email = $this->session->userdata('email');
        $detail_owner = $this->m_manajemen->getDetailOwner($email);
        $id_owner = $detail_owner->id;
        $Data['list_kapal'] = $this->m_manajemen->getKapalOwner($id_owner);

        // //get Data Progres aktual
        // $Data['progres_aktual'] = $this->m_manajemen->getprojekAktual($id_owner);
        // //print_r($Data['progres_aktual']);

        $this->load->view('header');
        $this->load->view('sidebar-owner');
        $this->load->view('owner/daftar-kapal', $Data);
        $this->load->view('footer');
    }

    public function GetprojekAktual()
    {   
        $email = $this->session->userdata('email');
        $detail_owner = $this->m_manajemen->getDetailOwner($email);
        $id_owner = $detail_owner->id;

        $data = $this->m_manajemen->getprojekAktual($this->input->post('id_kapal'), $id_owner);

        $json = array();
        foreach ($data as $activity) {
            $json[] = array('periode'  => $activity->periode,
                            'mid_end' => $activity->mid_end,
                            'progres' => $activity->progres
            );
        }
        echo json_encode($json);

    }

    public function GetprojekRencana()
    {   
        $email = $this->session->userdata('email');
        $detail_owner = $this->m_manajemen->getDetailOwner($email);
        $id_owner = $detail_owner->id;

        $data = $this->m_manajemen->getprojekRencana($this->input->post('id_kapal'), $id_owner);

        $json = array();
        foreach ($data as $activity) {
            $json[] = array('periode'  => $activity->periode,
                            'mid_end' => $activity->mid_end,
                            'progres' => $activity->target
            );
        }
        echo json_encode($json);

    }

    public function DetailKapal($id_pembuatan)
    {
        $this->load->model(array('pembuatan_kapal'));
        // $Data['kapal'] = $this->m_manajemen->getDetailKapal($id_pembuatan);
        $this->pembuatan_kapal->join_galangan();
        $Data['kapal'] = $this->pembuatan_kapal->get(array('id' => $id_pembuatan))[0];
        // var_dump($Data['kapal']);exit();
        $this->load->view('header');
        $this->load->view('sidebar-owner');
        $this->load->view('owner/detail-kapal', $Data);
        $this->load->view('footer');
    }

    public function Gambar($id_pembuatan)
    {
        $this->load->library('pagination');
        $this->load->model('gambar_rancangan');
        //------------------------------------------------------------------------------------
        $getData = $this->db->get('gambar_rancangan');
        $a = $getData->num_rows();
        $config['base_url'] = site_url().'/ManajemenKapalOwner'; //set the base url for pagination
        $config['total_rows'] = $a; //total rows
        $config['per_page'] = '6'; //the number of per page for pagination
        $config['uri_segment'] = 3; //see from base_url. 3 for this case
        $config['full_tag_open'] = '<p class=pagination>';
        $config['full_tag_close'] = '</p>';
        $this->pagination->initialize($config); //initialize pagination
        //------------------------------------------------------------------------------------  
        
        $Data['photo'] = $this->gambar_rancangan->get_photo($config['per_page'],$this->uri->segment(3));

        $Data['kapal'] = $this->m_manajemen->getDetailKapal($id_pembuatan);

        //print_r($Data['kapal'] );
        $this->load->view('header');
        $this->load->view('sidebar-owner');
        $this->load->view('owner/gambar', $Data);
        $this->load->view('footer');
    }

    public function SCurve($id_pembuatan)
    {
        $this->load->model(array('baseline', 'pembuatan_kapal', 'progres'));
        $baseline = $this->baseline->get(array('id_kapal'=>$id_pembuatan));
        $progres = $this->progres->get(array('id_kapal'=>$id_pembuatan));
        $label = array();
        $bobot_baseline = array();
        $bobot_progres = array();
        foreach ($baseline as $b) {
            if($b->mid_end == 0){
                $label[] = 'mid '.$b->periode;
            }
            else{
                $label[] = 'end '.$b->periode;
            }
            $bobot_baseline[] = $b->target;
        }

        foreach ($progres as $p) {
            $bobot_progres[] = $p->progres;
        }


        $detail_kapal = $this->pembuatan_kapal->get(array('id' => $id_pembuatan));
        $Data['kapal'] = $detail_kapal[0];
        $Data['label'] = json_encode($label);
        $Data['bobot_baseline'] = json_encode($bobot_baseline);
        $Data['bobot_progres'] = json_encode($bobot_progres);

        $this->load->view('header');
        $this->load->view('sidebar-owner');
        $this->load->view('owner/s-curve', $Data);
        $this->load->view('footer');
    }

    public function LaporanHarian($id_pembuatan)
    {
        $this->load->model('M_manajemenOS');
        $Data['kapal'] = $this->M_manajemenOS->getDetailKapal($id_pembuatan);
        // $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 
        $this->load->model('laporan_harian');
        $Data['tanggal_laporan'] = $this->laporan_harian->getjoin_group($id_pembuatan);
       // print_r($Data['tanggal_laporan']);

        $this->load->view('header');
        $this->load->view('sidebar-owner');
        $this->load->view('owner/laporan-harian', $Data);
        $this->load->view('footer');
    }

    public function DetailHarian()
    {
        $this->load->model('M_manajemen');
        $this->load->model('M_manajemenOS');
        $id_kapal =  $this->uri->segment(3);
        $tgl_laporan =  $this->uri->segment(4);
        // $status = $this->uri->segment(5);

        $Data['tgl_laporan'] = $tgl_laporan;
        $Data['status'] =  $this->M_manajemen->getStatusLaporan($id_kapal, $tgl_laporan);

        //get data tenaga kerja
        $Data['tenaga_kerja'] = $this->M_manajemenOS->getDetailLapTenagaKerja($id_kapal, $tgl_laporan);
        //get data cuaca
        $Data['cuaca'] = $this->M_manajemenOS->getDetailLapCuaca($id_kapal, $tgl_laporan);
        //get data laporan_alat
        $Data['alat'] = $this->M_manajemenOS->getDetailLapAlat($id_kapal, $tgl_laporan);

         //get Data workgroup
        $this->load->model('workgroup_kapal');
        $Data['list_workgroup'] = $this->workgroup_kapal->join($id_kapal);
 

        $this->load->view('header');
        $this->load->view('sidebar-owner');
        $this->load->view('owner/detail-laporan-harian', $Data);
        $this->load->view('footer');
    }

    public function SaveTanggapan()
    {
        $this->load->model('M_manajemen');
        $this->M_manajemen->saveTanggapan($this->input->post('id_pembuatan'), $this->input->post('tanggapan_pm'));
    }

    public function ValidasiLaporan()
    {
        $this->load->model('M_manajemen');
        $this->M_manajemen->validasiLaporan($this->input->post('tgl_laporan'));
    }  

    public function LaporanMingguan($id_kapal)
    {
        $this->load->model(array('laporan_mingguan','m_manajemenOS'));
        $this->laporan_mingguan->join_workgroup();
        
        $Data['lap_mingguan'] = $this->laporan_mingguan->get(array('id_kapal'=>$id_kapal));
        //var_dump($Data['lap_mingguan']);exit();
        $Data['kapal'] = $this->m_manajemenOS->getDetailKapal($id_kapal);
        // $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 

        $this->load->view('header');
        $this->load->view('sidebar-owner');
        $this->load->view('owner/daftar-laporan-mingguan', $Data);
        $this->load->view('footer');
    }

    public function simpanlaporanmingguan()
    {
        $this->load->model(array('progres', 'workgroup_kapal','laporan_mingguan'));
        $id_kapal = $this->input->post('id_kapal');
        $id_workgroup_kapal = $this->input->post('pilih_workgroup');
        $id_group = $this->input->post('pilih_group');

        $id_aktifitas = $this->input->post('id_aktifitas');
        
        $data = array(
                'verif_owner' => $this->input->post('verifikasi'),
                'catatan_owner' => $this->input->post('tanggapan_owner')
            );
        $where = array(
                'id' => $this->input->post('id_mingguan')
            );

        $this->laporan_mingguan->update($data, $where);
        //var_dump($where);
        
        redirect('manajemenKapalOwner/LaporanMingguan/'.$id_kapal);
    }

    public function DetailMingguan($id_kapal, $id_laporan_mingguan)
    {
        $this->load->model(array('data_laporan_mingguan', 'm_manajemenOS', 'laporan_mingguan'));

        $this->data_laporan_mingguan->join_mingguan();
        $this->data_laporan_mingguan->join_aktifitas();
        $Data['detail_mingguan'] = $this->data_laporan_mingguan->get(array('id_laporan_mingguan' => $id_laporan_mingguan));
        $this->laporan_mingguan->join_workgroup();
        $Data['data_mingguan'] = $this->laporan_mingguan->get(array('laporan_mingguan.id'=>$id_laporan_mingguan))[0];
        //var_dump($Data['data_mingguan']);exit();
        $Data['kapal'] = $this->m_manajemenOS->getDetailKapal($id_kapal);
        $Data['menu'] = $this->load->view('ownerSurveyor/tabbar', $Data, true); 

        $this->load->view('header');
        $this->load->view('sidebar-owner');
        $this->load->view('owner/detail-laporan-mingguan', $Data);
        $this->load->view('footer');
    }

    public function GetDataChart()
    {
        $email = $this->session->userdata('email');
        $detail_owner = $this->m_manajemen->getDetailOwner($email);
        $id_owner = $detail_owner->id;

        $list_id = $this->m_manajemen->getidKapal($id_owner);

        $json = array();
        foreach ($list_id as $activity) {
            $query = $this->m_manajemen->getDataChart($activity->id);

            $json[] = array('nama_proyek'  => $activity->nama_proyek,
                            // 'progres' => $activity->mid_end,
                            'progres' => $query->progres
            );
        }
        echo json_encode($json);
    }

    public function GetDataProgres()
    {
        // $email = $this->session->userdata('email');
        // $detail_owner = $this->m_manajemen->getDetailOwner($email);
        // $id_owner = $detail_owner->id;

        $list_id = $this->m_manajemen->getidKapalbyIdGalangan($this->input->post('id_galangan'));

        $json = array();
        foreach ($list_id as $activity) {
            $query = $this->m_manajemen->getDataChart($activity->id);

            $json[] = array('nama_proyek'  => $activity->nama_proyek,
                            // 'progres' => $activity->mid_end,
                            'progres' => $query->progres
            );
        }
        echo json_encode($json);
    }   
}



